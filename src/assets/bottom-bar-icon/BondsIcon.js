import React from 'react';
import {SvgXml} from 'react-native-svg';

const BondsIcon = ({color, width, height, opacity}) => {
  const xml = `
  <svg width="25" height="24" viewBox="0 0 25 24" fill="none" fill-opacity="${opacity}" xmlns="http://www.w3.org/2000/svg">
<path d="M4.44895 15.1707L11.9697 22.6914H7.08542L4.35837 20.055L4.44895 15.1707Z" fill="#464B4E"/>
<path d="M0 11.3673L2.66637 14.0337L2.69715 18.8581L0 16.2805V11.3673Z" fill="#464B4E"/>
<path d="M0.0439758 9.73772L14.3362 24L17.0632 23.9701L24.0449 17.079V12.135L15.7151 20.4947L3.60999 8.35968L11.9099 0.0299L6.99572 0L0.133676 6.95086L0.0439758 9.73772Z" fill="#464B4E"/>
<path d="M6.00638 8.35969L14.3661 0.0299072H17.0931L18.202 1.22854L10.8906 8.38959L12.0295 9.52754L19.34 2.3366L21.7373 4.73387L14.5762 11.9846L15.6852 13.1235L22.9359 5.96241L24.0149 7.01155L24.0449 9.67792L15.6852 18.0675L6.00638 8.35969Z" fill="#464B4E"/>
</svg>
`;

  return <SvgXml xml={xml} width={width || '100%'} height={height || '100%'} />;
};

export default BondsIcon;
