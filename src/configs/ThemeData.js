const FONT_NORMAL = 'Montserrat';
const FONT_MEDIUM = 'Montserrat-Medium';
const FONT_BOLD = 'Montserrat-Bold';
const FONT_SEMI_BOLD = 'Montserrat-SemiBold';

const ROBOTO = 'Roboto';
const ROBOTO_BOLD = 'Roboto-Bold';
const ROBOTO_MEDIUM = 'Roboto-Medium';
const ROBOTO_LIGHT = 'Roboto-Light';

// const APP_MAIN_COLOR = '#086080';
const APP_MAIN_COLOR = '#5F6163';
const TEXT_COLOR = '#5F6163';
const BORDER_COLOR = '#E5E5E5';

export default {
  FONT_NORMAL,
  FONT_MEDIUM,
  FONT_BOLD,
  FONT_SEMI_BOLD,
  ROBOTO,
  ROBOTO_BOLD,
  ROBOTO_MEDIUM,
  ROBOTO_LIGHT,
  APP_MAIN_COLOR,
  BORDER_COLOR,
  TEXT_COLOR,
};
