import React, {useContext} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LoginScreen from '../screens/LoginScreen';
import AppDrawerNavigator from './AppDrawerNavigator';
import SignupScreen from '../screens/SignupScreen';
import ProfileUpdateScreen from '../screens/ProfileUpdateScreen';
import ForgotPasswordScreen from '../screens/ForgotPasswordScreen';
import LandingScreen from '../screens/LandingScreen';
import PreRegisterScreen from '../screens/PreRegisterScreen';
import {AppContext} from '../contexts/AppContextProvider';

const Stack = createStackNavigator();

const AppStackNavigator = () => {
  const {setLoginData, setProfileId, isLoggedIn} = useContext(AppContext);
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName={isLoggedIn ? 'Drawer' : 'Landing'}>
      <Stack.Screen name="Landing" component={LandingScreen} />
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="Signup" component={SignupScreen} />
      <Stack.Screen name="PreRegister" component={PreRegisterScreen} />
      <Stack.Screen name="ForgotPassword" component={ForgotPasswordScreen} />
      <Stack.Screen name="ProfileUpdate" component={ProfileUpdateScreen} />
      <Stack.Screen name="Drawer" component={AppDrawerNavigator} />
    </Stack.Navigator>
  );
};

export default AppStackNavigator;
