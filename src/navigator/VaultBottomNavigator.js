import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import LiquidVaultScreen from '../screens/VaultScreens/LiquidVaultScreen';
import DeFiVaultScreen from '../screens/VaultScreens/DeFiVaultScreen';
import FundsVaultScreen from '../screens/VaultScreens/FundsVaultScreen';
import AltsVaultScreen from '../screens/VaultScreens/AltsVaultScreen';
import ThemeData from '../configs/ThemeData';
import BotsScreen from '../screens/BotsScreen';

const Tab = createBottomTabNavigator();

const VaultBottomNavigator = () => (
  <Tab.Navigator
    initialRouteName="Liquid"
    tabBarOptions={{
      activeTintColor: ThemeData.APP_MAIN_COLOR,
      keyboardHidesTabBar: true,
      style: {margin: 0, padding: 0},
      labelStyle: {margin: 0},
      tabStyle: {margin: 0, padding: 0},
    }}
    tabBar={(props) => null}>
    <Tab.Screen name="Liquid" component={LiquidVaultScreen} />
    <Tab.Screen name="DeFi" component={DeFiVaultScreen} />
    <Tab.Screen name="Funds" component={FundsVaultScreen} />
    <Tab.Screen name="Bots" component={BotsScreen} />
    <Tab.Screen name="Alts" component={AltsVaultScreen} />
  </Tab.Navigator>
);

export default VaultBottomNavigator;
