import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import LiquidScreen from '../screens/MarketScreens/LiquidScreen';
import TokensScreen from '../screens/MarketScreens/TokensScreen';
import BondsScreen from '../screens/MarketScreens/BondsScreen';
import MoneyMarketScreen from '../screens/MarketScreens/MoneyMarketScreen';
import {createStackNavigator} from '@react-navigation/stack';
import GlobalPaymentTrackerScreen from '../screens/MarketScreens/GlobalPaymentTrackerScreen';
import ThemeData from '../configs/ThemeData';
import BondItemScreen from '../screens/MarketScreens/BondItemScreen';
import BondIssueScreen from '../screens/MarketScreens/BondIssueScreen';
import BotsScreen from '../screens/BotsScreen';
import StepOne from '../screens/MarketScreens/BuyBondScreens/StepOne';
import StepTwo from '../screens/MarketScreens/BuyBondScreens/StepTwo';
import StepThree from '../screens/MarketScreens/BuyBondScreens/StepThree';
import StepFour from '../screens/MarketScreens/BuyBondScreens/StepFour';

const MoneyMarketStack = createStackNavigator();

const MoneyMarketNavigator = () => (
  <MoneyMarketStack.Navigator
    initialRouteName="Home"
    screenOptions={{headerShown: false}}>
    <MoneyMarketStack.Screen name="Home" component={MoneyMarketScreen} />
    <MoneyMarketStack.Screen
      name="GlobalPayement"
      component={GlobalPaymentTrackerScreen}
    />
  </MoneyMarketStack.Navigator>
);

const BondsStack = createStackNavigator();

const BondsNavigatior = () => (
  <BondsStack.Navigator
    initialRouteName="Bonds"
    screenOptions={{headerShown: false}}>
    <BondsStack.Screen name="Bonds" component={BondsScreen} />
    <BondsStack.Screen name="StepOne" component={StepOne} />
    <BondsStack.Screen name="StepTwo" component={StepTwo} />
    <BondsStack.Screen name="StepThree" component={StepThree} />
    <BondsStack.Screen name="StepFour" component={StepFour} />
    <BondsStack.Screen name="BondItem" component={BondItemScreen} />
    <BondsStack.Screen name="BondIssue" component={BondIssueScreen} />
  </BondsStack.Navigator>
);

const Tab = createBottomTabNavigator();

const MarketBottomNavigator = () => (
  <Tab.Navigator
    initialRouteName="Liquid"
    tabBarOptions={{
      activeTintColor: ThemeData.APP_MAIN_COLOR,
      keyboardHidesTabBar: true,
      style: {margin: 0, padding: 0},
      labelStyle: {
        margin: 0,
      },
      tabStyle: {margin: 0, padding: 0},
    }}
    tabBar={(props) => null}>
    <Tab.Screen name="Liquid" component={LiquidScreen} />
    <Tab.Screen name="Forex" component={LiquidScreen} />
    <Tab.Screen name="Tokens" component={TokensScreen} />
    <Tab.Screen name="MoneyMarket" component={MoneyMarketScreen} />
    <Tab.Screen name="Bonds" component={BondsNavigatior} />
    <Tab.Screen name="Bots" component={BotsScreen} />
    {/* <Tab.Screen name="MoneyMarket" component={MoneyMarketNavigator} /> */}
  </Tab.Navigator>
);

export default MarketBottomNavigator;
