/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
} from '@react-navigation/drawer';
import {createStackNavigator} from '@react-navigation/stack';
import CustomDrawer from '../components/CustomDrawer';
import QRScannerScreen from '../screens/QRScannerScreen';
import SupportScreen from '../screens/SupportScreen';
import {createSharedElementStackNavigator} from 'react-navigation-shared-element';
import LearnScreen from '../screens/LearnScreen';
import InviteScreen from '../screens/InviteScreen';
import InviteDetailsScreen from '../screens/InviteDetailsScreen';
import UpdateHistoryScreen from '../screens/UpdateHistoryScreen';
import BlockCheckScreen from '../screens/BlockCheckScreen';
import AdminScreen from '../screens/AdminScreen';
import RegisterProspectScreen from '../screens/RegisterProspectScreen';
import ProfileSettingsScreen from '../screens/ProfileSettings/ProfileSettingsScreen';
import Configure2FAScreen from '../screens/ProfileSettings/Configure2FAScreen';
import InterestSettingScreen from '../screens/ProfileSettings/InterestSettingScreen';
import ChangePasswordScreen from '../screens/ProfileSettings/ChangePasswordScreen';
import ChangeUsernameSettings from '../screens/ProfileSettings/ChangeUsernameSettings';
import VaultBottomNavigator from './VaultBottomNavigator';
import MarketBottomNavigator from './MarketBottomNavigator';
import AssetScreen from '../screens/AssetScreen';
import QuoteScreen from '../screens/QuoteScreen';
import UpdateAppScreen from '../screens/ProfileSettings/UpdateAppScreen';
// import ExternalWithdrawScreen from '../screens/ExternalWithdrawScreens/WithdrawConfirm';
import NewWalletScreen from '../screens/NewWalletScreen';

import WithdrawAddressScreen from '../screens/ExternalWithdrawScreens/WithdrawAddressScreen';
import WithdrawQuoteScreen from '../screens/ExternalWithdrawScreens/WithdrawQuoteScreen';
import WithdrawConfirm from '../screens/ExternalWithdrawScreens/WithdrawConfirm';
import WithdrawVerifyEmail from '../screens/ExternalWithdrawScreens/WithdrawVerifyEmail';
import WithdrawSuccess from '../screens/ExternalWithdrawScreens/WithdrawSuccess';
import ViewAddressScreen from '../screens/ActionScreens/ViewAddressScreen';
import SubmitHashScreen from '../screens/ActionScreens/SubmitHashScreen';
import HashSuccessScreen from '../screens/ActionScreens/HashSuccessScreen';

// Add Forex
import AddForexStepOne from '../screens/AddForexScreens/AddForexStepOne';
import AddForexStepTwo from '../screens/AddForexScreens/AddForexStepTwo';
import AddForexStepThree from '../screens/AddForexScreens/AddForexStepThree';
import AddForexStepFour from '../screens/AddForexScreens/AddForexStepFour';
import AddForexStepFive from '../screens/AddForexScreens/AddForexStepFive';
import AddForexStepSix from '../screens/AddForexScreens/AddForexStepSix';
import AddForexStepSeven from '../screens/AddForexScreens/AddForexStepSeven';
import AddForexStepEight from '../screens/AddForexScreens/AddForexStepEight';

// Withdraw Forex
import WithdrawForexStepOne from '../screens/WithdrawForexScreens/WithdrawForexStepOne';
import WithdrawForexStepTwo from '../screens/WithdrawForexScreens/WithdrawForexStepTwo';
import WithdrawForexStepThree from '../screens/WithdrawForexScreens/WithdrawForexStepThree';
import WithdrawForexStepFour from '../screens/WithdrawForexScreens/WithdrawForexStepFour';
import WithdrawForexStepFive from '../screens/WithdrawForexScreens/WithdrawForexStepFive';
import WithdrawForexStepSix from '../screens/WithdrawForexScreens/WithdrawForexStepSix';
import WithdrawForexStepSeven from '../screens/WithdrawForexScreens/WithdrawForexStepSeven';
import WithdrawForexStepEight from '../screens/WithdrawForexScreens/WithdrawForexStepEight';

import BuyCryptoStepOne from '../screens/MarketScreens/BuyCryptoScreens/BuyCryptoStepOne';
import BuyCryptoStepTwo from '../screens/MarketScreens/BuyCryptoScreens/BuyCryptoStepTwo';
import SellCryptoStepOne from '../screens/MarketScreens/SellCryptoScreens/SellCryptoStepOne';
import SellCryptoStepTwo from '../screens/MarketScreens/SellCryptoScreens/SellCryptoStepTwo';
import ManageBankAccounts from '../screens/ProfileSettings/ManageBankAccounts';
import PaymentType from '../screens/ProfileSettings/AddAccountScreens/PaymenyType';
import BankType from '../screens/ProfileSettings/AddAccountScreens/BankType';
import AccountHolder from '../screens/ProfileSettings/AddAccountScreens/AccountHolder';
import AccountDetails from '../screens/ProfileSettings/AddAccountScreens/AccountDetails';
import SelectCountry from '../screens/ProfileSettings/AddAccountScreens/SelectCountry';

const Drawer = createDrawerNavigator();

const CustomDrawerContent = (props) => (
  <DrawerContentScrollView {...props} horizontal scrollEnabled={false} nav>
    <CustomDrawer />
  </DrawerContentScrollView>
);

const InviteTransitionStack = createSharedElementStackNavigator();

const InviteTransitionNavigator = () => (
  <InviteTransitionStack.Navigator
    initialRouteName="List"
    screenOptions={{
      headerShown: false,

      cardStyleInterpolator: ({current: {progress}}) => ({
        cardStyle: {opacity: progress},
      }),
    }}>
    <InviteTransitionStack.Screen name="List" component={InviteScreen} />
    <InviteTransitionStack.Screen
      name="Details"
      component={InviteDetailsScreen}
      sharedElements={(route, otherRoute, showing) => {
        const {item, title} = route.params;
        return ['item.title', `item.${item.key}.item`, 'item.appBar'];
      }}
    />
  </InviteTransitionStack.Navigator>
);

const SettingsTransitionStack = createSharedElementStackNavigator();

const SettingsTransitionNavigator = () => (
  <SettingsTransitionStack.Navigator
    initialRouteName="List"
    screenOptions={{
      headerShown: false,
    }}>
    <SettingsTransitionStack.Screen
      name="List"
      component={ProfileSettingsScreen}
    />
    <SettingsTransitionStack.Screen
      name="2FASettings"
      component={Configure2FAScreen}
    />
    <SettingsTransitionStack.Screen
      name="InterestSettings"
      component={InterestSettingScreen}
    />
    <SettingsTransitionStack.Screen
      name="ChangePassword"
      component={ChangePasswordScreen}
    />
    <SettingsTransitionStack.Screen
      name="ChangeUsername"
      component={ChangeUsernameSettings}
    />
    <SettingsTransitionStack.Screen
      name="UpdateApp"
      component={UpdateAppScreen}
    />
    <SettingsTransitionStack.Screen
      name="ManageAccount"
      component={ManageBankAccounts}
    />
    <SettingsTransitionStack.Screen
      name="AddAccount"
      component={AddAccountNavigator}
    />
  </SettingsTransitionStack.Navigator>
);

const ChildStack = createStackNavigator();

const ChildNavigator = () => (
  <ChildStack.Navigator
    initialRouteName="Markets"
    screenOptions={{headerShown: false}}>
    <ChildStack.Screen name="Markets" component={MarketBottomNavigator} />
    <ChildStack.Screen name="Vaults" component={NewWalletScreen} />
    <ChildStack.Screen
      name="QRScanner"
      component={QRScannerScreen}
      options={{}}
    />
    <ChildStack.Screen name="Support" component={SupportScreen} />
    <ChildStack.Screen name="Lean" component={LearnScreen} />
    <ChildStack.Screen name="Invite" component={InviteTransitionNavigator} />
    <ChildStack.Screen
      name="RegisterProspect"
      component={RegisterProspectScreen}
    />
    <ChildStack.Screen name="UpdateHistory" component={UpdateHistoryScreen} />
    <ChildStack.Screen name="BlockCheck" component={BlockCheckScreen} />
    <ChildStack.Screen name="AdminPage" component={AdminScreen} />
    <ChildStack.Screen
      name="Settings"
      component={SettingsTransitionNavigator}
    />
    <ChildStack.Screen name="Asset" component={AssetScreen} />
    <ChildStack.Screen name="Quote" component={QuoteScreen} />
    <ChildStack.Screen name="ExternalWithdraw" component={WithdrawNavigator} />
    <ChildStack.Screen name="ViewAddress" component={ViewAddressNavigator} />
    <ChildStack.Screen name="AddForexStepOne" component={AddForexNavigator} />
    <ChildStack.Screen
      name="WithdrawForexStepOne"
      component={WithdrawForexNavigator}
    />
    <ChildStack.Screen name="BuyCryptoStepOne" component={BuyCryptoNavigator} />
    <ChildStack.Screen
      name="SellCryptoStepOne"
      component={SellCryptoNavigator}
    />
    {/* <ChildStack.Screen
      name="ExternalWithdraw"
      component={ExternalWithdrawScreen}
    /> */}
  </ChildStack.Navigator>
);

const AddAccountNavigator = () => {
  return (
    <ChildStack.Navigator
      initialRouteName="accountPaymentType"
      screenOptions={{headerShown: false}}>
      <ChildStack.Screen name="accountPaymentType" component={PaymentType} />
      <ChildStack.Screen name="bankType" component={BankType} />
      <ChildStack.Screen name="selectCountry" component={SelectCountry} />
      <ChildStack.Screen name="accountHolder" component={AccountHolder} />
      <ChildStack.Screen name="accountDetails" component={AccountDetails} />
    </ChildStack.Navigator>
  );
};

const ViewAddressNavigator = () => {
  return (
    <ChildStack.Navigator
      initialRouteName="showAddress"
      screenOptions={{headerShown: false}}>
      <ChildStack.Screen name="showAddress" component={ViewAddressScreen} />
      <ChildStack.Screen name="submitHash" component={SubmitHashScreen} />
      <ChildStack.Screen name="hashSuccess" component={HashSuccessScreen} />
    </ChildStack.Navigator>
  );
};

const WithdrawNavigator = () => {
  return (
    <ChildStack.Navigator
      initialRouteName="Address"
      screenOptions={{headerShown: false}}>
      <ChildStack.Screen name="Address" component={WithdrawAddressScreen} />
      <ChildStack.Screen name="Quote" component={WithdrawQuoteScreen} />
      <ChildStack.Screen name="Confirmation" component={WithdrawConfirm} />
      <ChildStack.Screen name="verifyEmail" component={WithdrawVerifyEmail} />
      <ChildStack.Screen name="withdrawSuccess" component={WithdrawSuccess} />
    </ChildStack.Navigator>
  );
};

const AddForexNavigator = () => {
  return (
    <ChildStack.Navigator
      initialRouteName="AddForexStepOne"
      screenOptions={{headerShown: false}}>
      <ChildStack.Screen name="AddForexStepOne" component={AddForexStepOne} />
      <ChildStack.Screen name="AddForexStepTwo" component={AddForexStepTwo} />
      <ChildStack.Screen
        name="AddForexStepThree"
        component={AddForexStepThree}
      />
      <ChildStack.Screen name="AddForexStepFour" component={AddForexStepFour} />
      <ChildStack.Screen name="AddForexStepFive" component={AddForexStepFive} />
      <ChildStack.Screen name="AddForexStepSix" component={AddForexStepSix} />
      <ChildStack.Screen
        name="AddForexStepSeven"
        component={AddForexStepSeven}
      />
      <ChildStack.Screen
        name="AddForexStepEight"
        component={AddForexStepEight}
      />
    </ChildStack.Navigator>
  );
};

const WithdrawForexNavigator = () => {
  return (
    <ChildStack.Navigator
      initialRouteName="WithdrawForexStepOne"
      screenOptions={{headerShown: false}}>
      <ChildStack.Screen
        name="WithdrawForexStepOne"
        component={WithdrawForexStepOne}
      />
      <ChildStack.Screen
        name="WithdrawForexStepTwo"
        component={WithdrawForexStepTwo}
      />
      <ChildStack.Screen
        name="WithdrawForexStepThree"
        component={WithdrawForexStepThree}
      />
      <ChildStack.Screen
        name="WithdrawForexStepFour"
        component={WithdrawForexStepFour}
      />
      <ChildStack.Screen
        name="WithdrawForexStepFive"
        component={WithdrawForexStepFive}
      />
      <ChildStack.Screen
        name="WithdrawForexStepSix"
        component={WithdrawForexStepSix}
      />
      <ChildStack.Screen
        name="WithdrawForexStepSeven"
        component={WithdrawForexStepSeven}
      />
      <ChildStack.Screen
        name="WithdrawForexStepEight"
        component={WithdrawForexStepEight}
      />
    </ChildStack.Navigator>
  );
};

const BuyCryptoNavigator = () => {
  return (
    <ChildStack.Navigator
      initialRouteName="BuyCryptoStepOne"
      screenOptions={{headerShown: false}}>
      <ChildStack.Screen name="BuyCryptoStepOne" component={BuyCryptoStepOne} />
      <ChildStack.Screen name="BuyCryptoStepTwo" component={BuyCryptoStepTwo} />
    </ChildStack.Navigator>
  );
};

const SellCryptoNavigator = () => {
  return (
    <ChildStack.Navigator
      initialRouteName="SellCryptoStepOne"
      screenOptions={{headerShown: false}}>
      <ChildStack.Screen
        name="SellCryptoStepOne"
        component={SellCryptoStepOne}
      />
      <ChildStack.Screen
        name="SellCryptoStepTwo"
        component={SellCryptoStepTwo}
      />
    </ChildStack.Navigator>
  );
};

const AppDrawerNavigator = () => {
  return (
    <Drawer.Navigator
      initialRouteName="child"
      drawerContent={CustomDrawerContent}
      drawerStyle={{width: 'auto'}}
      backBehavior={'history'}>
      <Drawer.Screen name="child" component={ChildNavigator} />
    </Drawer.Navigator>
  );
};

export default AppDrawerNavigator;
