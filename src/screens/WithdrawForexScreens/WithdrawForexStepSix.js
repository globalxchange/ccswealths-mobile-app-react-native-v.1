import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import Clipboard from '@react-native-community/clipboard';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import AppMainLayout from '../../layouts/AppMainLayout';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import CryptoSwitchSearch from '../../components/CryptoSwitchSearch';
import FiatSwitchSearch from '../../components/FiatSwitchSearch';
import {APP_CODE} from '../../configs';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Info from '../../assets/info.svg';
import CheckBox from '@react-native-community/checkbox';

const WithdrawForexStepSix = ({route}) => {
  const {
    fromAmount,
    fromCurrency,
    confirmQuote,
    selectedAccount,
    path,
  } = route.params;
  const {navigate, goBack} = useNavigation();
  const {width, height} = Dimensions.get('window');
  const {setExternalAddress, activeWallet} = useContext(WithdrawalContext);
  const [addressInput, setAddressInput] = useState('');
  const [allPaymentMethods, setAllPaymentMethods] = useState([]);
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [selectedCoin, setSelectedCoin] = useState();
  const [loading, setLoading] = useState(false);
  const [check1, setCheck1] = useState(false);

  const confirmFunction1 = async () => {
    setLoading(true);
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();
    const profileId = await AsyncStorageHelper.getProfileId();

    axios
      .post(
        `https://comms.globalxchange.com/coin/vault/service/user/send/extenal/verify/mail`,
        {
          email: email,
          token: token,
          app_code: APP_CODE,
          coin: activeWallet.coinSymbol,
          amount: confirmQuote?.finalToAmount,
          coin_address: selectedAccount?.bank_account_id,
        },
      )
      .then(({data}) => {
        if (data.status) {
          navigate('WithdrawForexStepSeven', {
            fromAmount: fromAmount,
            fromCurrency: fromCurrency,
            selectedAccount: selectedAccount,
            path: path,
          });
        }
      });
  };

  const confirmFunction = () => {
    navigate('WithdrawForexStepSeven', {
      fromAmount: fromAmount,
      fromCurrency: fromCurrency,
      selectedAccount: selectedAccount,
      path: path,
    });
  };

  return (
    <AppMainLayout>
      {!isSearchOpen ? (
        <View style={styles.container}>
          <View>
            <View style={styles.headerImage}>
              <Image
                source={{uri: activeWallet.coinImage}}
                style={styles.headerIcon}
              />
              <Text style={styles.headerTitle}>{activeWallet.coinName}</Text>
            </View>
            <View style={styles.crumbStyle}>
              <Text style={{color: '#5F6163'}} onPress={(e) => goBack()}>
                Enter Amount
                {` -> `}
              </Text>
              <View style={styles.activeCrumb}>
                <Text style={{color: '#5F6163', fontWeight: '700'}}>
                  Withdrawal Confirmation
                </Text>
              </View>
            </View>
          </View>

          <ScrollView showsVerticalScrollIndicator={false}>
            <Text style={styles.titleText}>Step 3: Confirmation</Text>
            <View>
              <View style={styles.headerContainer}>
                <Text style={styles.headerText}>Debiting App</Text>
              </View>
              <View style={styles.coinDesign}>
                <Image
                  source={{uri: confirmQuote?.appData?.app_icon}}
                  style={{width: 24, height: 24}}
                />
                <Text style={[styles.quoteNumber, {paddingLeft: 10}]}>
                  {confirmQuote?.appData?.app_name}
                </Text>
              </View>
            </View>
            <View style={{marginTop: 40}}>
              <View style={styles.headerContainer}>
                <Text style={styles.headerText}>Debiting Vault</Text>
              </View>
              <View style={styles.coinDesign}>
                <Image
                  source={{uri: confirmQuote?.coinsData?.fromCoinData?.icon}}
                  style={{width: 24, height: 24}}
                />
                <Text style={[styles.quoteNumber, {paddingLeft: 10}]}>
                  Liquid {confirmQuote?.coinsData?.fromCoinData?.coin}
                </Text>
              </View>
            </View>
            <View style={{marginTop: 40}}>
              <View style={styles.headerContainer}>
                <Text style={styles.headerText}>Destination</Text>
              </View>
              <View
                // onPress={(e) => {
                //   navigate('WithdrawForexStepFour', {
                //     fromCurrency: fromCurrency,
                //     selectedAccount: item,
                //   });
                // }}
                style={styles.cardStyle}>
                <View style={styles.instituteWrapper}>
                  <Image
                    style={{width: 21, height: 21, borderRadius: 3}}
                    source={{
                      uri: selectedAccount?.instituteData?.profile_image,
                    }}
                  />
                  <Text style={styles.instituteName}>
                    {selectedAccount?.instituteData?.institute_name.length >
                    20 ? (
                      <Text>
                        {selectedAccount?.instituteData?.institute_name?.substring(
                          0,
                          20,
                        )}{' '}
                        ...
                      </Text>
                    ) : (
                      selectedAccount?.instituteData?.institute_name
                    )}
                  </Text>
                </View>
                <View>
                  <Text style={styles.accountNameLabel}>Account Name</Text>
                  <Text style={styles.accountNameStyle}>
                    {selectedAccount.account_name}
                  </Text>
                </View>
                <View style={styles.paymentMethodStyle}>
                  <Image
                    style={{width: 18, height: 18}}
                    source={{uri: selectedAccount?.paymentMethod_data?.icon}}
                  />
                  <Text style={styles.paymentMetodLable}>
                    {selectedAccount?.paymentMethod_data?.name}
                  </Text>
                </View>
              </View>
            </View>
            {/* debiting amount */}
            <View>
              <View style={[styles.headerContainer, {marginTop: 40}]}>
                <Text style={styles.headerText}>Debiting Amount</Text>
              </View>
              <View style={styles.quoteDesign}>
                <Text style={styles.quoteNumber}>
                  {confirmQuote?.finalFromAmount}
                </Text>
                <View style={styles.quoteTextWrapper}>
                  <Image
                    source={{uri: activeWallet.coinImage}}
                    style={{width: 13, height: 13}}
                  />
                  <Text style={styles.quoteText}>
                    {activeWallet.coinSymbol}
                  </Text>
                </View>
              </View>
            </View>
            {/* fee */}
            <View>
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Text style={styles.cardlabel}>Fees</Text>

                <Text style={styles.cardlabel}>
                  {confirmQuote?.reducedAmount?.toFixed(5)}{' '}
                  {confirmQuote?.fees_in_coin} (
                  {confirmQuote?.app_fee_percentage?.toFixed(4)}
                  %)
                </Text>
              </View>

              <View style={styles.feecardStyle1}>
                <View style={styles.inCardWrapper}>
                  <View
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Text style={styles.feeLabelStyle}>Affiliate</Text>
                    <Info width={13} height={13} />
                  </View>

                  <Text style={styles.feeLabelStyle}>
                    {confirmQuote?.brokerData?.fee?.toFixed(5)} (
                    {confirmQuote?.brokerData?.broker_fee_percentage?.toFixed(
                      4,
                    )}
                    %)
                  </Text>
                </View>
                <View style={styles.inCardWrapper}>
                  <View
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Text style={styles.feeLabelStyle}>App</Text>
                    <Info width={13} height={13} />
                  </View>

                  <Text style={styles.feeLabelStyle}>
                    {confirmQuote?.appData?.app_fee?.toFixed(4)} (
                    {confirmQuote?.appData?.app_fee_percentage?.toFixed(3)}%)
                  </Text>
                </View>
                <View style={styles.inCardWrapper}>
                  <View
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Text style={styles.feeLabelStyle}>Banker</Text>
                    <Info width={13} height={13} />
                  </View>

                  <Text style={styles.feeLabelStyle}>
                    {confirmQuote?.bankerData?.trade_fee_native?.toFixed(4)} (
                    {confirmQuote?.bankerData?.banker_fee_percentage?.toFixed(
                      3,
                    )}
                    %)
                  </Text>
                </View>
              </View>
            </View>
            {/* Receiving amount */}
            <View>
              <View style={[styles.headerContainer, {marginTop: 40}]}>
                <Text style={styles.headerText}>Receiving Amount</Text>
              </View>
              <View style={styles.quoteDesign}>
                <Text style={styles.quoteNumber}>
                  {confirmQuote?.finalToAmount}
                </Text>
                <View style={styles.quoteTextWrapper}>
                  <Image
                    source={{uri: fromCurrency.coin_metadata.coinImage}}
                    style={{width: 13, height: 13}}
                  />
                  <Text style={styles.quoteText}>
                    {fromCurrency.coin_metadata.coinSymbol}
                  </Text>
                </View>
              </View>
            </View>
            {/* Confirmation */}
            <Text style={styles.confirmHeader}>Confirmations</Text>
            <View style={styles.confirmation}>
              <CheckBox
                style={{transform: [{scaleX: 0.8}, {scaleY: 0.8}]}}
                value={check1}
                onValueChange={(newValue) => setCheck1(!check1)}
                boxType="square"
              />
              <Text style={styles.confirmationText}>
                I Consent To All The Aforementioned Parameters Regarding This
                Transfer Request.
              </Text>
            </View>
            {/* Buttons */}
            <View style={styles.halfButtonWrapper}>
              <TouchableOpacity
                style={styles.actionBtnWhite}
                onPress={(e) => goBack()}>
                <Text style={[styles.actionBtnText, {color: '#5F6163'}]}>
                  Back
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.actionBtn}
                onPress={(e) => (check1 ? confirmFunction() : null)}>
                {loading ? (
                  <ActivityIndicator size="small" color="#08152D" />
                ) : (
                  <Text style={styles.actionBtnText}>Next</Text>
                )}
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      ) : (
        <FiatSwitchSearch
          walletBalances={allPaymentMethods}
          setActiveCrypto={setSelectedCoin}
          onClose={() => setIsSearchOpen(false)}
        />
      )}
    </AppMainLayout>
  );
};

export default WithdrawForexStepSix;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 32,
    // paddingVertical: 25,
    backgroundColor: 'white',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 35,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 20,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    marginBottom: 45,
    fontWeight: 'bold',
  },
  titleText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 18,
    lineHeight: 22,
    color: ThemeData.APP_MAIN_COLOR,
    paddingBottom: 45,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_MEDIUM,
    color: '#5F6163',
    fontSize: 14,
    // fontWeight: '800',
    marginBottom: 20,
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  addressInput: {
    height: 63,
    borderColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    paddingHorizontal: 22,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 13,
    height: 16,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
    color: '#5F6163',
  },
  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    width: '48%',
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  midSectionStyle: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 15,
  },
  currencyWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: 74,
    borderColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    paddingHorizontal: 22,
  },
  currencyIcon: {
    width: 23,
    height: 23,
  },
  currencyName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: ThemeData.TEXT_COLOR,
    paddingLeft: 5,
  },
  coinDesign: {
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderRadius: 15,
    height: 74,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  quoteDesign: {
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderRadius: 15,
    height: 74,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  quoteNumber: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 20,
    color: ThemeData.APP_MAIN_COLOR,
  },
  quoteTextWrapper: {
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
    paddingHorizontal: 23,
    paddingVertical: 12,
    borderRadius: 25,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  quoteText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 11,
    paddingLeft: 5,
  },
  halfButtonWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  actionBtnWhite: {
    backgroundColor: 'white',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    width: '48%',
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
  },
  cardStyle: {
    borderWidth: 0.5,
    borderColor: '#e5e5e5',
    borderRadius: 15,
    // backgroundColor: 'white',
    height: 180,

    padding: 20,
    display: 'flex',
    justifyContent: 'space-between',
    // shadowOffset: {width: -2, height: 4},
    // shadowOpacity: 0.1,
    // shadowRadius: 3,
  },

  instituteWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  instituteName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 16,
    paddingLeft: 8,
  },
  accountNameLabel: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 9,
    paddingBottom: 8,
  },
  accountNameStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 14,
  },
  paymentMethodStyle: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  paymentMetodLable: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 14,
    paddingLeft: 4,
  },
  feecardStyle: {
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    height: 74,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 25,
  },
  feecardStyle1: {
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    // height: 74,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    // alignItems: 'center',
    paddingLeft: 25,
    paddingRight: 25,
  },
  cardlabel: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#5F6163',
    fontSize: 13,
    fontWeight: '600',
    paddingBottom: 15,
    paddingTop: 40,
    paddingLeft: 2,
  },
  inCardWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 30,
    borderBottomWidth: 0.5,
    borderColor: '#E5E5E5',
  },
  inCardWrapper1: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // paddingVertical: 30,
    borderColor: '#E5E5E5',
  },
  inCardCurrency: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 0.5,
    borderRadius: 15,
    borderColor: '#E5E5E5',
    paddingHorizontal: 20,
    height: 32,
  },
  coinImageStyle: {
    width: 13,
    height: 13,
  },
  coinSymbolStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 11,
    fontWeight: '800',
    color: '#5F6163',
    paddingLeft: 4,
    paddingRight: 11,
  },
  feeLabelStyle: {
    paddingRight: 7,
    fontFamily: ThemeData.FONT_NORMAL,
  },
  debitAmountStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 20,
    color: '#5F6163',
    fontWeight: '700',
  },
  confirmHeader: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    marginTop: 45,
    marginBottom: 20,
    fontSize: 20,
  },
  confirmation: {
    flexDirection: 'row',
    marginTop: 15,
  },
  confirmationText: {
    marginLeft: 12,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 11,
    color: '#5F6163',
    lineHeight: 25,
    marginTop: -4,
  },
});
