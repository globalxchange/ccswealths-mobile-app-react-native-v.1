import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import Clipboard from '@react-native-community/clipboard';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import AppMainLayout from '../../layouts/AppMainLayout';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import CryptoSwitchSearch from '../../components/CryptoSwitchSearch';
import FiatSwitchSearch from '../../components/FiatSwitchSearch';
import SkeltonItem from '../../components/SkeltonItem';

const WithdrawForexStepOne = ({}) => {
  const {navigate, goBack} = useNavigation();
  const {width, height} = Dimensions.get('window');
  const {setExternalAddress, activeWallet} = useContext(WithdrawalContext);
  const [addressInput, setAddressInput] = useState('');
  const [allCurrency, setAllCurrency] = useState([]);
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [selectedCoin, setSelectedCoin] = useState(null);
  const [loading, setLoading] = useState(true);

  const [activeCrypto, setActiveCrypto] = useState(null);

  useEffect(() => {
    setLoading(true);
    axios
      .get(
        `https://comms.globalxchange.com/coin/vault/service/payment/stats/get`,
        {
          params: {
            select_type: 'withdraw',
            from_currency: activeWallet.coinSymbol,
          },
        },
      )
      .then(({data}) => {
        setAllCurrency(data.pathData.from_currency);
        setLoading(false);
      });
  }, []);

  useEffect(() => {
    const foundData = allCurrency.find(
      (o) =>
        o.coin_metadata.coinSymbol === selectedCoin?.coin_metadata?.coinSymbol,
    );
    if (foundData) {
      navigate('WithdrawForexStepTwo', {
        fromCurrency: foundData,
      });
    }
  }, [selectedCoin, isSearchOpen]);

  return (
    <AppMainLayout>
      {!isSearchOpen ? (
        <View style={styles.container}>
          <View style={{paddingHorizontal: 25, paddingTop: 10, flex: 1}}>
            <View
              style={{
                height: '22%',
                // display: 'flex',
                // justifyContent: 'center',
              }}>
              <View style={styles.headerImage}>
                <Image
                  source={{uri: activeWallet.coinImage}}
                  style={styles.headerIcon}
                />
                <Text style={styles.headerTitle}>{activeWallet.coinName}</Text>
              </View>
              <View style={styles.crumbStyle}>
                <Text style={{color: '#5F6163'}} onPress={(e) => goBack()}>
                  Withdraw
                  {` -> `}
                </Text>
                <View style={styles.activeCrumb}>
                  <Text style={{color: '#5F6163', fontWeight: '700'}}>
                    Receiving Currency
                  </Text>
                </View>
              </View>
              <View style={styles.headerContainer}>
                <Text style={styles.headerText}>
                  Which Currency Do You Want To Recieve?
                </Text>
              </View>
            </View>
            <View
              style={[
                styles.midSectionStyle,
                {height: '65%', display: 'flex', justifyContent: 'center'},
              ]}>
              <TextInput
                onFocus={(e) => setIsSearchOpen(true)}
                style={styles.addressInput}
                placeholder="Search Assets..."
                value={addressInput}
                onChangeText={(text) => {
                  setAddressInput(text);
                  setExternalAddress(text);
                }}
              />
              <ScrollView
                contentContainerStyle={{
                  flexGrow: 1,
                  flexDirection: 'column',
                  justifyContent: 'space-between',
                }}>
                {!loading ? (
                  allCurrency?.length > 0 ? (
                    allCurrency.map((item, index) => {
                      return (
                        <TouchableOpacity
                          onPress={(e) =>
                            navigate('WithdrawForexStepTwo', {
                              fromCurrency: item,
                            })
                          }
                          style={[
                            styles.currencyWrapper,
                            {borderTopWidth: index === 0 ? 0 : 1},
                          ]}>
                          <Image
                            source={{uri: item.coin_metadata.coinImage}}
                            style={styles.currencyIcon}
                          />
                          <Text style={styles.currencyName}>
                            {item.coin_metadata.coinName}
                          </Text>
                        </TouchableOpacity>
                      );
                    })
                  ) : (
                    <View
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingTop: 40,
                      }}>
                      <Text
                        style={{
                          fontFamily: ThemeData.FONT_MEDIUM,
                          color: ThemeData.APP_MAIN_COLOR,
                        }}>
                        No Data
                      </Text>
                    </View>
                  )
                ) : (
                  Array(30)
                    .fill(' ')
                    .map((item) => {
                      return (
                        <View style={[styles.row, styles.currencyWrapper]}>
                          <View style={styles.coinContainer}>
                            <SkeltonItem
                              itemHeight={25}
                              itemWidth={25}
                              style={styles.coinImage}
                            />
                            <SkeltonItem itemHeight={20} itemWidth={80} />
                          </View>
                        </View>
                      );
                    })
                )}
              </ScrollView>
            </View>
            <View
              style={{
                height: '13%',
                display: 'flex',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                style={styles.actionBtn}
                onPress={(e) => goBack()}>
                <Text style={styles.actionBtnText}>Back</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      ) : (
        <FiatSwitchSearch
          walletBalances={allCurrency}
          setActiveCrypto={setSelectedCoin}
          onClose={() => setIsSearchOpen(false)}
        />
      )}
    </AppMainLayout>
  );
};

export default WithdrawForexStepOne;

const styles = StyleSheet.create({
  container: {
    flex: 1,

    // paddingHorizontal: 32,
    // paddingVertical: 25,
    // backgroundColor: 'red',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    // paddingTop: 35,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 25,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    fontWeight: 'bold',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 16,
    fontWeight: '800',
    // marginBottom: 40,
    lineHeight: 27,
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  addressInput: {
    height: 63,
    borderColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    paddingHorizontal: 22,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 13,
    height: 16,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
    color: '#5F6163',
  },
  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  midSectionStyle: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 15,
    marginBottom: 20,
  },
  currencyWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: 74,
    borderColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    paddingHorizontal: 22,
  },
  currencyIcon: {
    width: 23,
    height: 23,
  },
  currencyName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: ThemeData.TEXT_COLOR,
    paddingLeft: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 15,
  },
  coinContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    width: 25,
    height: 25,
    borderRadius: 13,
    marginRight: 5,
  },
  coinName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
});
