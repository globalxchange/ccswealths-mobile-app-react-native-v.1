import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import Clipboard from '@react-native-community/clipboard';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import AppMainLayout from '../../layouts/AppMainLayout';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import CryptoSwitchSearch from '../../components/CryptoSwitchSearch';
import FiatSwitchSearch from '../../components/FiatSwitchSearch';
import SkeltonItem from '../../components/SkeltonItem';

const WithdrawForexStepFour = ({route}) => {
  const {fromCurrency, selectedAccount} = route.params;
  const {navigate, goBack} = useNavigation();
  const {width, height} = Dimensions.get('window');
  const {setExternalAddress, activeWallet} = useContext(WithdrawalContext);
  const [addressInput, setAddressInput] = useState('');
  const [allBankers, setAllBankers] = useState([]);
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [selectedBanker, setSelectedBanker] = useState();
  const [loading, setLoading] = useState(true);

  // useEffect(() => {
  //   const foundData = allBankers.find(
  //     (o) => o.displayName === selectedBanker?.displayName,
  //   );
  //   if (foundData) {
  //     navigate('WithdrawForexStepFive', {
  //       fromCurrency: fromCurrency,
  //       country: country,
  //       paymentMethod: paymentMethod,
  //       banker: foundData,
  //     });
  //   }
  // }, [selectedBanker]);

  useEffect(() => {
    setLoading(true);

    axios
      .get(
        `https://comms.globalxchange.com/coin/vault/service/payment/stats/get`,
        {
          params: {
            select_type: 'withdraw',
            to_currency: fromCurrency.coin_metadata.coinSymbol,
            from_currency: activeWallet.coinSymbol,
            country: selectedAccount?.countryData?.name,
            paymentMethod: selectedAccount?.paymentMethod_data?.code,
          },
        },
      )
      .then(({data}) => {
        console.log(data, 'lwjbkjwekfwef');
        setAllBankers(data.pathData.banker);
        setLoading(false);
      });
  }, []);

  return (
    <AppMainLayout>
      {!isSearchOpen ? (
        <View style={styles.container}>
          <View style={{paddingHorizontal: 25, paddingTop: 20, flex: 1}}>
            <View
              style={{
                height: '22%',
              }}>
              <View style={styles.headerImage}>
                <Image
                  source={{uri: activeWallet.coinImage}}
                  style={styles.headerIcon}
                />
                <Text style={styles.headerTitle}>{activeWallet.coinName}</Text>
              </View>
              <View style={styles.crumbStyle}>
                <Text style={{color: '#5F6163'}} onPress={(e) => goBack()}>
                  Bank Account
                  {` -> `}
                </Text>
                <View style={styles.activeCrumb}>
                  <Text style={{color: '#5F6163', fontWeight: '700'}}>
                    Select OTCDesk
                  </Text>
                </View>
              </View>
              <View style={styles.headerContainer}>
                <Text style={styles.headerText}>
                  Select One Of The OTCDesks
                </Text>
              </View>
            </View>
            <View
              style={[
                styles.midSectionStyle,
                {height: '65%', display: 'flex', justifyContent: 'center'},
              ]}>
              <TextInput
                onFocus={(e) => setIsSearchOpen(true)}
                style={styles.addressInput}
                placeholder="Search OTCDesks..."
                value={addressInput}
                onChangeText={(text) => {
                  setAddressInput(text);
                  setExternalAddress(text);
                }}
              />
              <ScrollView
                contentContainerStyle={{
                  flexGrow: 1,
                  flexDirection: 'column',
                  justifyContent: 'space-between',
                }}>
                {!loading ? (
                  allBankers?.length > 0 ? (
                    allBankers.map((item, index) => {
                      return (
                        <TouchableOpacity
                          onPress={
                            (e) =>
                              navigate('WithdrawForexStepFive', {
                                fromCurrency: fromCurrency,
                                selectedAccount: selectedAccount,
                                banker: item,
                              })
                            // navigate('AddForexStepSix', {
                            //   fromCurrency: fromCurrency,
                            //   country: country,
                            //   paymentMethod: paymentMethod,
                            //   banker: item,
                            // })
                          }
                          style={[
                            styles.currencyWrapper,
                            {borderTopWidth: index === 0 ? 0 : 1},
                          ]}>
                          <Image
                            source={{uri: item?.icons?.image1}}
                            style={styles.currencyIcon}
                          />
                          <Text style={styles.currencyName}>
                            {item.displayName}
                          </Text>
                        </TouchableOpacity>
                      );
                    })
                  ) : (
                    <View
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingTop: 40,
                      }}>
                      <Text
                        style={{
                          fontFamily: ThemeData.FONT_MEDIUM,
                          color: ThemeData.APP_MAIN_COLOR,
                        }}>
                        No Data
                      </Text>
                    </View>
                  )
                ) : (
                  Array(3)
                    .fill(' ')
                    .map((item) => {
                      return (
                        <View style={[styles.row, styles.currencyWrapper]}>
                          <View style={styles.coinContainer}>
                            <SkeltonItem
                              itemHeight={25}
                              itemWidth={25}
                              style={styles.coinImage}
                            />
                            <SkeltonItem itemHeight={20} itemWidth={80} />
                          </View>
                        </View>
                      );
                    })
                )}
              </ScrollView>
            </View>
            <View
              style={{
                height: '13%',
                display: 'flex',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                style={styles.actionBtn}
                onPress={(e) => goBack()}>
                <Text style={styles.actionBtnText}>Back</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      ) : (
        <FiatSwitchSearch
          walletBalances={allBankers}
          setActiveCrypto={setSelectedBanker}
          onClose={() => setIsSearchOpen(false)}
        />
      )}
    </AppMainLayout>
  );
};

export default WithdrawForexStepFour;

const styles = StyleSheet.create({
  container: {
    flex: 1,

    // paddingHorizontal: 32,
    // paddingVertical: 25,
    // backgroundColor: 'red',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    // paddingTop: 35,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 25,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    fontWeight: 'bold',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 16,
    fontWeight: '800',
    lineHeight: 27,
    // marginBottom: 40,
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  addressInput: {
    height: 63,
    borderColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    paddingHorizontal: 22,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 13,
    height: 16,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
    color: '#5F6163',
  },
  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  midSectionStyle: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 15,
    marginBottom: 20,
  },
  currencyWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: 74,
    borderColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    paddingHorizontal: 22,
  },
  currencyIcon: {
    width: 23,
    height: 23,
  },
  currencyName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: ThemeData.TEXT_COLOR,
    paddingLeft: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 15,
  },
  coinContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    width: 25,
    height: 25,
    borderRadius: 13,
    marginRight: 5,
  },
  coinName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },

  //Accounts

  cardStyle: {
    borderTopWidth: 0.5,
    borderColor: '#e5e5e5',
    // backgroundColor: 'white',
    height: 180,

    padding: 24,
    display: 'flex',
    justifyContent: 'space-between',
    // shadowOffset: {width: -2, height: 4},
    // shadowOpacity: 0.1,
    // shadowRadius: 3,
  },

  cardStyle1: {
    // backgroundColor: 'white',
    height: 180,

    padding: 24,
    display: 'flex',
    justifyContent: 'space-between',
    // shadowOffset: {width: -2, height: 4},
    // shadowOpacity: 0.1,
    // shadowRadius: 3,
  },
  instituteWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  instituteName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 20,
    paddingLeft: 8,
  },
  accountNameLabel: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 9,
    paddingBottom: 8,
  },
  accountNameStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 14,
  },
  paymentMethodStyle: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  paymentMetodLable: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 17,
    paddingLeft: 4,
  },
});
