import React, {useContext, useEffect, useState, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  ActivityIndicator,
  Animated,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import Clipboard from '@react-native-community/clipboard';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import AppMainLayout from '../../layouts/AppMainLayout';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import CryptoSwitchSearch from '../../components/CryptoSwitchSearch';
import FiatSwitchSearch from '../../components/FiatSwitchSearch';
import SkeltonItem from '../../components/SkeltonItem';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {APP_CODE} from '../../configs';
import {useAppData} from '../../utils/CustomHook';

const WithdrawForexStepSeven = ({route}) => {
  const {
    fromAmount,
    fromCurrency,
    confirmQuote,
    selectedAccount,
    path,
  } = route.params;

  const {data: appData} = useAppData();
  const spread = useRef(new Animated.Value(1)).current;

  const {navigate, goBack} = useNavigation();
  const {width, height} = Dimensions.get('window');
  const {setExternalAddress, activeWallet} = useContext(WithdrawalContext);
  const [addressInput, setAddressInput] = useState('');
  const [accountList, setAccountList] = useState([]);
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [loading, setLoading] = useState(false);

  const [activeCrypto, setActiveCrypto] = useState(null);
  const [otpInput, setOtpInput] = useState('');
  // useEffect(() => {
  //   const foundData = allCountry.find(
  //     (o) => o.metadata.name === selectedCountry?.metadata?.name,
  //   );
  //   if (foundData) {
  //     navigate('WithdrawForexStepThree', {
  //       fromCurrency: fromCurrency,
  //       country: foundData,
  //     });
  //   }
  //   console.log(selectedCountry, 'jkflqeknrljnrflkern');
  // }, [selectedCountry]);

  useEffect(() => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(spread, {
          toValue: 1,
          duration: 0,
          useNativeDriver: true,
          types: 'linear',
        }),
        Animated.timing(spread, {
          toValue: 1.2,
          duration: 1000,
          useNativeDriver: true,
          types: 'linear',
        }),
        Animated.timing(spread, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
          types: 'linear',
        }),
      ]),
    ).start();
  }, []);

  const withdrawFunction = async () => {
    setLoading(true);
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();
    const profileId = await AsyncStorageHelper.getProfileId();

    axios
      .post(
        `https://comms.globalxchange.com/coin/vault/service/trade/execute`,
        {
          token: token,
          email: email,
          app_code: APP_CODE,
          profile_id: profileId,
          path_id: path,
          from_amount: fromAmount,
          stats: false,
          identifier: `Withdraw ${fromAmount} ${activeWallet.coinSymbol}`,
          userWithdrawData: selectedAccount?.bank_account_id,
          priceLock: false,
          priceLock_currency: 'false',
          ext_verify_code: otpInput,
        },
      )
      .then(({data}) => {
        if (data.status) {
          setLoading(false);
          navigate('WithdrawForexStepEight', {
            previousBalance: data?.userDebit.current_balance,
            updatedBalance: data?.userDebit.updated_balance,
            fromAmount: fromAmount,
            fromCurrency: fromCurrency,
          });
        }
      });
  };

  return (
    <AppMainLayout>
      <View style={styles.container}>
        <View style={{paddingHorizontal: 25, paddingTop: 20, flex: 1}}>
          <View
            style={{
              height: '22%',
            }}>
            <View style={styles.headerImage}>
              <Image
                source={{uri: activeWallet.coinImage}}
                style={styles.headerIcon}
              />
              <Text style={styles.headerTitle}>{activeWallet.coinName}</Text>
            </View>
            <View style={styles.crumbStyle}>
              <Text style={{color: '#5F6163'}} onPress={(e) => goBack()}>
                Withdrawal Confirmation
                {` -> `}
              </Text>
              <View style={styles.activeCrumb}>
                <Text style={{color: '#5F6163', fontWeight: '700'}}>
                  Email Verification
                </Text>
              </View>
            </View>
            <View style={styles.headerContainer}>
              <Text style={styles.headerText}>Step 4: Email Verification</Text>
            </View>
            <View>
              <Text style={styles.pinDesc}>
                Enter The Six Digit Withdrawal Code Which We Just Sent To Your
                Email
              </Text>
            </View>
          </View>
          <KeyboardAwareScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              flexGrow: 1,
              flexDirection: 'column',
              justifyContent: 'space-between',
              paddingTop: 50,
            }}>
            <OTPInputView
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
              style={styles.otpInput}
              pinCount={6}
              // autoFocusOnLoad
              code={otpInput}
              onCodeChanged={(code) => setOtpInput(code)}
            />
            <View style={styles.halfButtonWrapper}>
              <TouchableOpacity
                style={styles.actionBtnWhite}
                onPress={(e) => goBack()}>
                <Text style={[styles.actionBtnText, {color: '#5F6163'}]}>
                  Cancel
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.actionBtn}
                onPress={(e) => withdrawFunction()}>
                {loading ? (
                  <ActivityIndicator size="small" color="#08152D" />
                ) : (
                  <Text style={styles.actionBtnText}>Next</Text>
                )}
              </TouchableOpacity>
            </View>
          </KeyboardAwareScrollView>
        </View>
      </View>
      {loading ? (
        <View style={styles.overlay}>
          <Animated.Image
            style={{transform: [{scale: spread}], width: 100, height: 100}}
            resizeMode="contain"
            // source={require('../assets/ccs-header-icon.png')}
            source={{uri: appData?.app_icon}}
          />
        </View>
      ) : null}
    </AppMainLayout>
  );
};

export default WithdrawForexStepSeven;

const styles = StyleSheet.create({
  container: {
    flex: 1,

    // paddingHorizontal: 32,
    // paddingVertical: 25,
    // backgroundColor: 'red',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    // paddingTop: 35,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 25,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    fontWeight: 'bold',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 16,
    fontWeight: '800',
    lineHeight: 27,
    // marginBottom: 40,
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  addressInput: {
    height: 63,
    borderColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    paddingHorizontal: 22,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 13,
    height: 16,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
    color: '#5F6163',
  },
  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    width: '48%',
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  midSectionStyle: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 15,
    marginBottom: 20,
  },
  currencyWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: 74,
    borderColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    paddingHorizontal: 22,
  },
  currencyIcon: {
    width: 23,
    height: 23,
  },
  currencyName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: ThemeData.TEXT_COLOR,
    paddingLeft: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 15,
  },
  coinContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    width: 25,
    height: 25,
    borderRadius: 13,
    marginRight: 5,
  },
  coinName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },

  //Accounts

  cardStyle: {
    borderTopWidth: 0.5,
    borderColor: '#e5e5e5',
    // backgroundColor: 'white',
    height: 180,

    padding: 24,
    display: 'flex',
    justifyContent: 'space-between',
    // shadowOffset: {width: -2, height: 4},
    // shadowOpacity: 0.1,
    // shadowRadius: 3,
  },

  cardStyle1: {
    // backgroundColor: 'white',
    height: 180,

    padding: 24,
    display: 'flex',
    justifyContent: 'space-between',
    // shadowOffset: {width: -2, height: 4},
    // shadowOpacity: 0.1,
    // shadowRadius: 3,
  },
  instituteWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  instituteName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 20,
    paddingLeft: 8,
  },
  accountNameLabel: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 9,
    paddingBottom: 8,
  },
  accountNameStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 14,
  },
  paymentMethodStyle: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  paymentMetodLable: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 17,
    paddingLeft: 4,
  },
  pinDesc: {
    paddingTop: 10,
    fontSize: 12,
    lineHeight: 22,
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
  },
  otpInput: {
    height: 60,
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
  },
  underlineStyleBase: {
    borderWidth: 1,
    color: ThemeData.APP_MAIN_COLOR,
  },
  underlineStyleHighLighted: {
    borderColor: '#999C9A',
    color: ThemeData.APP_MAIN_COLOR,
  },

  halfButtonWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  actionBtnWhite: {
    backgroundColor: 'white',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    width: '48%',
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
  },
  overlay: {
    flex: 1,
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    opacity: 0.7,
    width: '100%',
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
