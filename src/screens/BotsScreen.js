import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import AppMainLayout from '../layouts/AppMainLayout';
import ActionBar from '../components/ActionBar';
import ThemeData from '../configs/ThemeData';

const BotsScreen = () => {
  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <View style={styles.container}>
        <Image
          source={require('../assets/investment-bot-logo.png')}
          resizeMode="contain"
          style={styles.botIcon}
        />
        <View style={styles.comingConatainer}>
          <Text style={styles.comingText}>Coming Soon</Text>
        </View>
      </View>
    </AppMainLayout>
  );
};

export default BotsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
  },
  botIcon: {
    width: '100%',
  },
  comingConatainer: {
    backgroundColor: '#4B5BB2',
    paddingHorizontal: 30,
    paddingVertical: 15,
  },
  comingText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: 'white',
  },
});
