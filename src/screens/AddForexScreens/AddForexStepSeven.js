import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import Clipboard from '@react-native-community/clipboard';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import AppMainLayout from '../../layouts/AppMainLayout';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import CryptoSwitchSearch from '../../components/CryptoSwitchSearch';
import FiatSwitchSearch from '../../components/FiatSwitchSearch';
import {APP_CODE} from '../../configs';
import ArrowDown from '../../assets/arrowDown.svg';

const AddForexStepSeven = ({route}) => {
  const {
    updatedBalance,
    fromAmount,
    fromCurrency,
    paymentMethod,
    path,
  } = route.params;
  const {navigate, goBack} = useNavigation();
  const {width, height} = Dimensions.get('window');
  const {setExternalAddress, activeWallet} = useContext(WithdrawalContext);
  const [addressInput, setAddressInput] = useState('');
  const [allPaymentMethods, setAllPaymentMethods] = useState([]);
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [selectedCoin, setSelectedCoin] = useState();
  const [steps, setSteps] = useState([]);
  const [selectedStep, setSelectedStep] = useState(null);

  // useEffect(() => {
  //   axios
  //     .get(
  //       `https://comms.globalxchange.com/coin/vault/service/payment/stats/get`,
  //       {
  //         params: {
  //           select_type: 'fund',
  //           to_currency: activeWallet.coinSymbol,
  //           from_currency: fromCurrency.coin_metadata.coinSymbol,
  //           country: country.metadata.name,
  //         },
  //       },
  //     )
  //     .then(({data}) => {
  //       setAllPaymentMethods(data.pathData.paymentMethod);
  //     });
  // }, []);

  useEffect(() => {
    axios
      .get(
        `https://comms.globalxchange.com/coin/vault/service/payment/paths/get?path_id=${path}`,
      )
      .then(({data}) => {
        const tempArr = Object.values(data.paths[0].total_steps);
        console.log(tempArr, 'kjqgdkwjegfkhgfjwe');
        setSteps([...tempArr]);
      });
  }, []);

  const handleSetStep = (item) => {
    if (selectedStep?.publicname === item?.publicname) {
      setSelectedStep(null);
    } else {
      setSelectedStep(item);
    }
  };

  return (
    <AppMainLayout>
      <View style={styles.container}>
        <View style={styles.headerImage}>
          <Image
            source={{uri: activeWallet.coinImage}}
            style={styles.headerIcon}
          />
          <Text style={styles.headerTitle}>{activeWallet.coinName}</Text>
        </View>
        <View style={styles.crumbStyle}>
          <Text style={{color: '#5F6163'}}>
            confirm Quote
            {` -> `}
          </Text>
          <View style={styles.activeCrumb}>
            <Text style={{color: '#5F6163', fontWeight: '700'}}>Success</Text>
          </View>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.headerContainer}>
            <Text style={styles.plainText}>
              Congratulations. You Have Successully Added {fromAmount}{' '}
              {activeWallet.coinSymbol} To Your Liquid {activeWallet.coinSymbol}
              Vault.
            </Text>
          </View>
          <View style={[styles.headerContainer, {paddingTop: 30}]}>
            <Text style={styles.headerText}>
              New {activeWallet.coinSymbol} Balance
            </Text>
          </View>
          <View style={styles.quoteDesign}>
            <Text style={styles.quoteNumber}>{updatedBalance.coinValue}</Text>
            <View style={styles.quoteTextWrapper}>
              <Image
                source={{uri: updatedBalance.coinImage}}
                style={{width: 13, height: 13}}
              />
              <Text style={styles.quoteText}>{updatedBalance.coinSymbol}</Text>
            </View>
          </View>

          <Text style={[styles.plainText, {paddingVertical: 30}]}>
            Please Complete The Following Steps Within 24 Hours To Actualize The
            Deposit.
          </Text>

          {steps.map((item, index) => {
            return (
              <View>
                <View
                  style={[
                    styles.accordianItemStyle,
                    {
                      borderTopWidth: index === 0 ? 0.5 : 0,
                      borderColor: '#e5e5e5',
                    },
                  ]}>
                  <Text
                    onPress={(e) => handleSetStep(item)}
                    style={styles.accordianText}>
                    {item?.publicname}
                  </Text>
                  <TouchableOpacity
                    style={{
                      // backgroundColor: 'red',
                      width: '20%',
                      height: 40,
                      display: 'flex',
                      flexDirection: 'row',
                      justifyContent: 'flex-end',
                      alignItems: 'center',
                    }}
                    onPress={(e) => handleSetStep(item)}>
                    <ArrowDown width={9} height={12} />
                  </TouchableOpacity>
                </View>
                {selectedStep?.publicname === item.publicname ? (
                  <Text style={styles.stepDesc}>{item.description}</Text>
                ) : null}
                {item.bankaccountid &&
                selectedStep?.publicname === item.publicname ? (
                  <TouchableOpacity
                    style={styles.paymentDetailsButton}
                    onPress={(e) => {
                      console.log(item, 'kqegdkfuwegfiwuf');
                      navigate('AddForexStepEight', {
                        bankAccountId: item.bankaccountid,
                      });
                    }}>
                    <Text style={styles.accountBtnText}>
                      See Payment Details
                    </Text>
                  </TouchableOpacity>
                ) : null}
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderColor: '#e5e5e5',
                  }}></View>
              </View>
            );
          })}
          <View style={styles.halfButtonWrapper}>
            <TouchableOpacity
              style={styles.actionBtn}
              onPress={(e) => {
                navigate('AddForexStepOne');
                goBack();
              }}>
              <Text style={styles.actionBtnText}>Go Back</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </AppMainLayout>
  );
};

export default AddForexStepSeven;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 32,
    // paddingVertical: 25,
    backgroundColor: 'white',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 35,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 20,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    marginBottom: 45,
    fontWeight: 'bold',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 16,
    fontWeight: '800',
    marginBottom: 25,
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  addressInput: {
    height: 63,
    borderColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    paddingHorizontal: 22,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 13,
    height: 16,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
    color: '#5F6163',
  },
  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    width: '100%',
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  midSectionStyle: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 15,
  },
  currencyWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: 74,
    borderColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    paddingHorizontal: 22,
  },
  currencyIcon: {
    width: 23,
    height: 23,
  },
  currencyName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: ThemeData.TEXT_COLOR,
    paddingLeft: 5,
  },
  quoteDesign: {
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderRadius: 15,
    height: 74,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  quoteNumber: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 20,
  },
  quoteTextWrapper: {
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
    paddingHorizontal: 23,
    paddingVertical: 12,
    borderRadius: 25,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  quoteText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 11,
    paddingLeft: 5,
  },
  halfButtonWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  actionBtnWhite: {
    backgroundColor: 'white',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    width: '48%',
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
  },
  plainText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#5F6163',
    fontSize: 15,
    lineHeight: 30,
  },
  accordianItemStyle: {
    height: 100,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // borderBottomWidth: 0.5,
    // borderColor: '#e5e5e5',
  },
  accordianText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: '#5F6163',
    lineHeight: 25,
    width: '75%',
  },
  arrowStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 30,
  },
  stepDesc: {
    paddingBottom: 30,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 13,
    color: '#5F6163',
    lineHeight: 25,
  },
  paymentDetailsButton: {
    height: 42,
    width: 160,
    backgroundColor: '#F2A900',
    borderRadius: 26,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 30,
  },
  accountBtnText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 11,
    color: 'white',
  },
});
