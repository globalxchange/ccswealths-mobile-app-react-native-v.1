import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import Clipboard from '@react-native-community/clipboard';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import AppMainLayout from '../../layouts/AppMainLayout';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import CryptoSwitchSearch from '../../components/CryptoSwitchSearch';
import FiatSwitchSearch from '../../components/FiatSwitchSearch';
import SkeltonItem from '../../components/SkeltonItem';

const AddForexStepTwo = ({route}) => {
  const {fromCurrency} = route.params;
  const {navigate, goBack} = useNavigation();
  const {width, height} = Dimensions.get('window');
  const {setExternalAddress, activeWallet} = useContext(WithdrawalContext);
  const [addressInput, setAddressInput] = useState('');
  const [allCountry, setAllCountry] = useState([]);
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [selectedCountry, setSelectedCountry] = useState();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const foundData = allCountry.find(
      (o) => o.metadata.name === selectedCountry?.metadata?.name,
    );
    if (foundData) {
      navigate('AddForexStepThree', {
        fromCurrency: fromCurrency,
        country: foundData,
      });
    }
    console.log(selectedCountry, 'lwjefjlkwbkefgwlejkfhk3wf');
  }, [selectedCountry]);

  useEffect(() => {
    setLoading(true);
    axios
      .get(
        `https://comms.globalxchange.com/coin/vault/service/payment/stats/get`,
        {
          params: {
            select_type: 'fund',
            to_currency: activeWallet.coinSymbol,
            from_currency: fromCurrency.coin_metadata.coinSymbol,
          },
        },
      )
      .then(({data}) => {
        setAllCountry(data.pathData.country);
        setLoading(false);
      });
  }, []);

  return (
    <AppMainLayout>
      {!isSearchOpen ? (
        <View style={styles.container}>
          <View>
            <View style={styles.headerImage}>
              <Image
                source={{uri: activeWallet.coinImage}}
                style={styles.headerIcon}
              />
              <Text style={styles.headerTitle}>{activeWallet.coinName}</Text>
            </View>
            <View style={styles.crumbStyle}>
              <Text style={{color: '#5F6163'}} onPress={(e) => goBack()}>
                Sending Currency
                {` -> `}
              </Text>
              <View style={styles.activeCrumb}>
                <Text style={{color: '#5F6163', fontWeight: '700'}}>
                  Country
                </Text>
              </View>
            </View>
            <View style={styles.headerContainer}>
              <Text style={styles.headerText}>
                Which Country Are The Funds Coming From?
              </Text>
            </View>
          </View>
          <View style={[styles.midSectionStyle, {height: height / 2}]}>
            <TextInput
              onFocus={(e) => setIsSearchOpen(true)}
              style={styles.addressInput}
              placeholder="Search Countries..."
              value={addressInput}
              onChangeText={(text) => {
                setAddressInput(text);
                setExternalAddress(text);
              }}
            />
            <ScrollView>
              {!loading ? (
                allCountry?.length > 0 ? (
                  allCountry.map((item, index) => {
                    return (
                      <TouchableOpacity
                        onPress={(e) =>
                          navigate('AddForexStepThree', {
                            fromCurrency: fromCurrency,
                            country: item,
                          })
                        }
                        style={[
                          styles.currencyWrapper,
                          {borderTopWidth: index === 0 ? 0 : 1},
                        ]}>
                        <Image
                          source={{uri: item.metadata.image}}
                          style={styles.currencyIcon}
                        />
                        <Text style={styles.currencyName}>
                          {item.metadata.name}
                        </Text>
                      </TouchableOpacity>
                    );
                  })
                ) : (
                  <View>
                    <Text>No Data</Text>
                  </View>
                )
              ) : (
                Array(3)
                  .fill(' ')
                  .map((item) => {
                    return (
                      <View style={[styles.row, styles.currencyWrapper]}>
                        <View style={styles.coinContainer}>
                          <SkeltonItem
                            itemHeight={25}
                            itemWidth={25}
                            style={styles.coinImage}
                          />
                          <SkeltonItem itemHeight={20} itemWidth={80} />
                        </View>
                      </View>
                    );
                  })
              )}
            </ScrollView>
          </View>
          <View>
            <TouchableOpacity
              style={styles.actionBtn}
              onPress={(e) => goBack()}>
              <Text style={styles.actionBtnText}>Back</Text>
            </TouchableOpacity>
          </View>
        </View>
      ) : (
        <FiatSwitchSearch
          walletBalances={allCountry}
          setActiveCrypto={setSelectedCountry}
          onClose={() => setIsSearchOpen(false)}
        />
      )}
    </AppMainLayout>
  );
};

export default AddForexStepTwo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 32,
    // paddingVertical: 25,
    backgroundColor: 'white',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 35,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 20,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    marginBottom: 45,
    fontWeight: 'bold',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 16,
    fontWeight: '800',
    marginBottom: 40,
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  addressInput: {
    height: 63,
    borderColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    paddingHorizontal: 22,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 13,
    height: 16,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
    color: '#5F6163',
  },
  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  midSectionStyle: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 15,
  },
  currencyWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: 74,
    borderColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    paddingHorizontal: 22,
  },
  currencyIcon: {
    width: 25,
    height: 25,
    borderRadius: 50,
  },
  currencyName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: ThemeData.TEXT_COLOR,
    paddingLeft: 10,
  },

  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 15,
  },
  coinContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    width: 25,
    height: 25,
    borderRadius: 13,
    marginRight: 5,
  },
  coinName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
});
