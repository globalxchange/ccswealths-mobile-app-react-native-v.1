import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import Clipboard from '@react-native-community/clipboard';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import AppMainLayout from '../../layouts/AppMainLayout';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import CryptoSwitchSearch from '../../components/CryptoSwitchSearch';
import FiatSwitchSearch from '../../components/FiatSwitchSearch';
import {APP_CODE} from '../../configs';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';

const AddForexStepSix = ({route}) => {
  const {
    fromAmount,
    fromCurrency,
    confirmQuote,
    paymentMethod,
    path,
  } = route.params;
  const {navigate, goBack} = useNavigation();
  const {width, height} = Dimensions.get('window');
  const {setExternalAddress, activeWallet} = useContext(WithdrawalContext);
  const [addressInput, setAddressInput] = useState('');
  const [allPaymentMethods, setAllPaymentMethods] = useState([]);
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [selectedCoin, setSelectedCoin] = useState();
  const [loading, setLoading] = useState(false);
  // useEffect(() => {
  //   axios
  //     .get(
  //       `https://comms.globalxchange.com/coin/vault/service/payment/stats/get`,
  //       {
  //         params: {
  //           select_type: 'fund',
  //           to_currency: activeWallet.coinSymbol,
  //           from_currency: fromCurrency.coin_metadata.coinSymbol,
  //           country: country.metadata.name,
  //         },
  //       },
  //     )
  //     .then(({data}) => {
  //       setAllPaymentMethods(data.pathData.paymentMethod);
  //     });
  // }, []);

  // const AddFiatFunction = () => {
  //   navigate('AddForexStepSeven', {
  //     // updatedBalance: data,
  //     fromAmount: fromAmount,
  //     fromCurrency: fromCurrency,
  //     paymentMethod: paymentMethod,
  //   });
  // };

  const AddFiatFunction = async () => {
    setLoading(true);
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();
    const profileId = await AsyncStorageHelper.getProfileId();

    axios
      .post(
        `https://comms.globalxchange.com/coin/vault/service/trade/execute`,
        {
          token: token,
          email: email,
          app_code: APP_CODE,
          profile_id: profileId,
          coin_purchased: activeWallet.coinSymbol,
          purchased_from: fromCurrency.coin_metadata.coinSymbol,
          from_amount: fromAmount,
          stats: false,
          identifier: `Add ${fromAmount} ${fromCurrency.coin_metadata.coinSymbol} Via ${paymentMethod._id}`,
          path_id: path,
        },
      )
      .then(({data}) => {
        if (data.status) {
          axios
            .post(
              `https://comms.globalxchange.com/coin/vault/service/coins/get`,
              {
                app_code: APP_CODE,
                email: email,
                displayCurrency: activeWallet?.coinSymbol,
                include_coins: [activeWallet?.coinSymbol],
              },
            )
            .then(({data}) => {
              console.log(data, 'lkdlwkenfkwjefbkwef');
              if (data.status) {
                navigate('AddForexStepSeven', {
                  updatedBalance: data.coins_data[0],
                  fromAmount: fromAmount,
                  fromCurrency: fromCurrency,
                  paymentMethod: paymentMethod,
                  path: path,
                });
                setLoading(false);
              }
            });
        }
      });
  };

  return (
    <AppMainLayout>
      {!isSearchOpen ? (
        <View style={styles.container}>
          <View>
            <View style={styles.headerImage}>
              <Image
                source={{uri: activeWallet.coinImage}}
                style={styles.headerIcon}
              />
              <Text style={styles.headerTitle}>{activeWallet.coinName}</Text>
            </View>
            <View style={styles.crumbStyle}>
              <Text style={{color: '#5F6163'}} onPress={(e) => goBack()}>
                Amount
                {` -> `}
              </Text>
              <View style={styles.activeCrumb}>
                <Text style={{color: '#5F6163', fontWeight: '700'}}>
                  Confirm Quote
                </Text>
              </View>
            </View>
            <View style={styles.headerContainer}>
              <Text style={styles.headerText}>
                Sending Via {paymentMethod._id}
              </Text>
            </View>
            <View style={styles.quoteDesign}>
              <Text style={styles.quoteNumber}>
                {confirmQuote?.finalFromAmount}
              </Text>
              <View style={styles.quoteTextWrapper}>
                <Image
                  source={{uri: fromCurrency.coin_metadata.coinImage}}
                  style={{width: 13, height: 13}}
                />
                <Text style={styles.quoteText}>
                  {fromCurrency.coin_metadata.coinSymbol}
                </Text>
              </View>
            </View>
            <View style={[styles.headerContainer, {marginTop: 40}]}>
              <Text style={styles.headerText}>
                Credited To {activeWallet.coinSymbol} Liquid Vault
              </Text>
            </View>
            <View style={styles.quoteDesign}>
              <Text style={styles.quoteNumber}>
                {confirmQuote?.finalToAmount}
              </Text>
              <View style={styles.quoteTextWrapper}>
                <Image
                  source={{uri: activeWallet.coinImage}}
                  style={{width: 13, height: 13}}
                />
                <Text style={styles.quoteText}>
                  {fromCurrency.coin_metadata.coinSymbol}
                </Text>
              </View>
            </View>
          </View>

          <View style={styles.halfButtonWrapper}>
            <TouchableOpacity
              style={styles.actionBtnWhite}
              onPress={(e) => goBack()}>
              <Text style={[styles.actionBtnText, {color: '#5F6163'}]}>
                Back
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.actionBtn}
              onPress={(e) => AddFiatFunction()}>
              {loading ? (
                <ActivityIndicator size="small" color="#08152D" />
              ) : (
                <Text style={styles.actionBtnText}>Next</Text>
              )}
            </TouchableOpacity>
          </View>
        </View>
      ) : (
        <FiatSwitchSearch
          walletBalances={allPaymentMethods}
          setActiveCrypto={setSelectedCoin}
          onClose={() => setIsSearchOpen(false)}
        />
      )}
    </AppMainLayout>
  );
};

export default AddForexStepSix;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 32,
    // paddingVertical: 25,
    backgroundColor: 'white',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 35,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 20,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    marginBottom: 45,
    fontWeight: 'bold',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 16,
    fontWeight: '800',
    marginBottom: 25,
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  addressInput: {
    height: 63,
    borderColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    paddingHorizontal: 22,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 13,
    height: 16,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
    color: '#5F6163',
  },
  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    width: '48%',
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  midSectionStyle: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 15,
  },
  currencyWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: 74,
    borderColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    paddingHorizontal: 22,
  },
  currencyIcon: {
    width: 23,
    height: 23,
  },
  currencyName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: ThemeData.TEXT_COLOR,
    paddingLeft: 5,
  },
  quoteDesign: {
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderRadius: 15,
    height: 74,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  quoteNumber: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 20,
  },
  quoteTextWrapper: {
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
    paddingHorizontal: 23,
    paddingVertical: 12,
    borderRadius: 25,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  quoteText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 11,
    paddingLeft: 5,
  },
  halfButtonWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  actionBtnWhite: {
    backgroundColor: 'white',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    width: '48%',
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
  },
});
