import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import Clipboard from '@react-native-community/clipboard';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import AppMainLayout from '../../layouts/AppMainLayout';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import CryptoSwitchSearch from '../../components/CryptoSwitchSearch';
import FiatSwitchSearch from '../../components/FiatSwitchSearch';
import {APP_CODE} from '../../configs';
import CopyIcon from '../../assets/copyIcon.svg';

const AddForexStepEight = ({route}) => {
  const {bankAccountId} = route.params;
  const {navigate, goBack} = useNavigation();
  const {width, height} = Dimensions.get('window');
  const {setExternalAddress, activeWallet} = useContext(WithdrawalContext);
  const [addressInput, setAddressInput] = useState('');
  const [allPaymentMethods, setAllPaymentMethods] = useState([]);
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [selectedCoin, setSelectedCoin] = useState();
  const [steps, setSteps] = useState([]);
  const [selectedStep, setSelectedStep] = useState(null);
  const [allStat, setAllStat] = useState([]);
  const [bankInfo, setBankInfo] = useState([]);
  // const bankAccountId = '55nzphffl8r1b9ng';

  const [showFullImage, setShowFullImage] = useState(null);

  useEffect(() => {
    console.log(bankAccountId, 'kjewbkwejgkfgweikd');
  }, [bankAccountId]);

  useEffect(() => {
    console.log(bankAccountId, 'jhefhagrfjewhf');
    axios
      .get(
        `https://comms.globalxchange.com/coin/user/bank/account/get?bank_account_id=${bankAccountId}`,
      )
      .then(({data}) => {
        console.log(data, 'jhefhagrfjewhf');
        if (data.status) {
          setAllStat(data?.bankAccounts_info[0]);
          const tempArr = Object.values(
            data.bankAccounts_info[0].bank_information,
          );
          setBankInfo([...tempArr]);
        }
      });
  }, [bankAccountId]);

  const handleSetStep = (item) => {
    if (selectedStep?.publicname === item?.publicname) {
      setSelectedStep(null);
    } else {
      setSelectedStep(item);
    }
  };

  return (
    <AppMainLayout>
      <View style={styles.container}>
        <View style={styles.headerImage}>
          <Image
            source={{uri: activeWallet.coinImage}}
            style={styles.headerIcon}
          />
          <Text style={styles.headerTitle}>{activeWallet.coinName}</Text>
        </View>
        <View style={styles.crumbStyle}>
          <Text style={{color: '#5F6163'}} onPress={(e) => goBack()}>
            Success
            {` -> `}
          </Text>
          <View style={styles.activeCrumb}>
            <Text style={{color: '#5F6163', fontWeight: '700'}}>
              Account Details
            </Text>
          </View>
        </View>
        {showFullImage === null ? (
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={[styles.headerContainer, {paddingTop: 30}]}>
              <Text style={styles.headerText}>Account Name</Text>
            </View>
            <View style={styles.quoteDesign}>
              <Text style={styles.quoteNumber}>
                {allStat?.account_name?.substring(0, 25)}&nbsp;
                {allStat?.account_name?.length > 25 ? '...' : null}
              </Text>

              <CopyIcon width={15} height={15} />
            </View>
            <View style={[styles.headerContainer, {paddingTop: 30}]}>
              <Text style={styles.headerText}>Payment Method</Text>
            </View>
            <View style={styles.quoteDesign}>
              <View style={styles.iconGroup}>
                <Image
                  source={{uri: allStat?.paymentMethod_data?.icon}}
                  style={{width: 22, height: 22, marginRight: 10}}
                />
                <Text style={styles.quoteNumber}>
                  {allStat?.paymentMethod_data?.name}
                </Text>
              </View>
              <CopyIcon width={15} height={15} />
            </View>
            {bankInfo?.map((item) => {
              return (
                <View>
                  <View style={[styles.headerContainer, {paddingTop: 30}]}>
                    <Text style={styles.headerText}>{item.name}</Text>
                    {item.valuetype !== 'text' ? (
                      <Text
                        onPress={(e) => setShowFullImage(item)}
                        style={[
                          styles.headerText,
                          {fontSize: 14, fontFamily: ThemeData.FONT_NORMAL},
                        ]}>
                        Expand
                      </Text>
                    ) : null}
                  </View>
                  {item.valuetype === 'text' ? (
                    <View style={styles.quoteDesign}>
                      <View style={styles.iconGroup}>
                        <Text style={styles.quoteNumber}>{item.value}</Text>
                      </View>
                      <CopyIcon width={15} height={15} />
                    </View>
                  ) : (
                    <View style={[styles.quoteDesign, {height: 200}]}>
                      <View style={styles.iconGroup}>
                        <Image
                          source={{uri: item.value}}
                          style={{width: '100%', height: 190}}
                        />
                      </View>
                    </View>
                  )}
                </View>
              );
            })}
            {/* Go back Button */}

            <View style={styles.halfButtonWrapper}>
              <TouchableOpacity
                style={styles.actionBtn}
                onPress={(e) => {
                  navigate('AddForexStepOne');
                  goBack();
                }}>
                <Text style={styles.actionBtnText}>Go Back</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        ) : (
          <View>
            <Image
              source={{uri: showFullImage?.value}}
              style={{width: '100%', height: '68%'}}
            />
            <View style={styles.halfButtonWrapper}>
              <TouchableOpacity
                style={styles.actionBtn}
                onPress={(e) => {
                  setShowFullImage(null);
                }}>
                <Text style={styles.actionBtnText}>Back</Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </View>
    </AppMainLayout>
  );
};

export default AddForexStepEight;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 32,
    // paddingVertical: 25,
    backgroundColor: 'white',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 35,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 20,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    marginBottom: 45,
    fontWeight: 'bold',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 16,
    fontWeight: '800',
    marginBottom: 25,
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  addressInput: {
    height: 63,
    borderColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    paddingHorizontal: 22,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 13,
    height: 16,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
    color: '#5F6163',
  },
  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    width: '100%',
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  midSectionStyle: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 15,
  },
  currencyWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: 74,
    borderColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    paddingHorizontal: 22,
  },
  currencyIcon: {
    width: 23,
    height: 23,
  },
  currencyName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: ThemeData.TEXT_COLOR,
    paddingLeft: 5,
  },
  quoteDesign: {
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderRadius: 15,
    height: 74,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  quoteNumber: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: '#5F6163',
  },
  quoteTextWrapper: {
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
    paddingHorizontal: 23,
    paddingVertical: 12,
    borderRadius: 25,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  quoteText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 11,
    paddingLeft: 5,
  },
  halfButtonWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  actionBtnWhite: {
    backgroundColor: 'white',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    width: '48%',
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
  },
  plainText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#5F6163',
    fontSize: 15,
    lineHeight: 30,
  },
  accordianItemStyle: {
    height: 100,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // borderBottomWidth: 0.5,
    // borderColor: '#e5e5e5',
  },
  accordianText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: '#5F6163',
    lineHeight: 25,
    width: '75%',
  },
  arrowStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 30,
  },
  stepDesc: {
    paddingBottom: 30,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 13,
    color: '#5F6163',
    lineHeight: 25,
  },
  paymentDetailsButton: {
    height: 42,
    width: 160,
    backgroundColor: '#F2A900',
    borderRadius: 26,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 30,
  },
  accountBtnText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 11,
    color: 'white',
  },
  iconGroup: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
});
