import React, {useContext, useState, useEffect} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import AppMainLayout from '../../layouts/AppMainLayout';
import CheckBox from '@react-native-community/checkbox';
import WithdrawalContext from '../../contexts/WithdrawalContext';

import RightArrow from '../../assets/rightArrow.svg';
import PriceLockInsurance from '../../assets/priceLockInsurance.svg';
import Info from '../../assets/info.svg';
import Time from '../../assets/time.svg';

import {useNavigation} from '@react-navigation/core';
import axios from 'axios';
import {AppContext} from '../../contexts/AppContextProvider';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {APP_CODE, GX_API_ENDPOINT} from '../../configs';

const WithdrawConfirm = () => {
  const {navigate, goBack} = useNavigation();
  const {
    activeWallet,
    withdrawalDetails,
    externalAddress,
    priceLock,
    setPriceLock,
    isShowQuote,
    setIsShowQuote,
  } = useContext(WithdrawalContext);
  const {walletCoinData} = useContext(AppContext);
  const [activeCoinBalance, setActiveCoinBalance] = useState('');
  const [loading, setLoading] = useState(false);

  const [check1, setCheck1] = useState(false);
  const [check2, setCheck2] = useState(false);

  useEffect(() => {
    if (walletCoinData && activeWallet) {
      const coinData = walletCoinData.find(
        (item) => item.coinSymbol === activeWallet.coinSymbol,
      );
      setActiveCoinBalance(coinData);
    }
  }, [activeWallet, walletCoinData]);

  const handleValidate = async () => {
    setLoading(true);
    const userEmail = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();
    axios
      .post(
        `https://comms.globalxchange.com/coin/vault/service/user/send/extenal/verify/mail`,
        {
          email: userEmail,
          token: token,
          app_code: APP_CODE,
          coin: activeWallet.coinSymbol,
          amount: withdrawalDetails.finalFromAmount,
          coin_address: externalAddress,
        },
      )
      .then((res) => {
        navigate('verifyEmail');
        setLoading(false);
      });
  };

  return (
    <AppMainLayout>
      <View style={styles.container}>
        <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
          <View style={styles.headerContainer}>
            <View style={styles.headerImage}>
              <Image
                source={{uri: activeWallet.coinImage}}
                style={styles.headerIcon}
              />
              <Text style={styles.headerTitle}>{activeWallet.coinName}</Text>
            </View>
          </View>
          <View style={styles.crumbStyle}>
            <Text style={{color: '#5F6163'}} onPress={(e) => navigate('Quote')}>
              Enter Amount{` -> `}
            </Text>
            <View style={styles.activeCrumb}>
              <Text style={{color: '#5F6163'}}>Confirmation</Text>
            </View>
          </View>
          <View style={styles.headerContainer}>
            <Text style={styles.headerText}>Step 3: Confirmation</Text>
          </View>
          <Text style={styles.descText}>
            Please confirm that all the following details are correct by
            clicking the terms and conditions.
          </Text>

          {/* Debiting App */}
          <View>
            <Text style={styles.cardlabel}>Debiting App</Text>
            <View style={styles.cardStyle}>
              <Image
                source={{uri: withdrawalDetails?.appData?.app_icon}}
                style={styles.cardImage}
              />
              <Text style={styles.cardText}>
                {withdrawalDetails?.appData?.app_name}
              </Text>
            </View>
          </View>
          {/* Debiting Vault */}
          <View>
            <Text style={styles.cardlabel}>Debiting Vault</Text>
            <View style={styles.cardStyle}>
              <Image
                source={{
                  uri: withdrawalDetails?.coinsData?.fromCoinData.icon,
                }}
                style={styles.cardImage}
              />
              <Text style={styles.cardText}>
                {withdrawalDetails?.coinsData?.fromCoinData.name}
              </Text>
            </View>
          </View>
          {/* Destination */}
          <View>
            <Text style={styles.cardlabel}>Destination</Text>
            <View style={styles.cardStyle}>
              <Image
                source={{uri: withdrawalDetails?.coinsData?.fromCoinData?.icon}}
                style={styles.cardImage}
              />
              <View>
                <Text style={styles.cardText}>
                  External {withdrawalDetails?.coinsData?.fromCoinData?.coin}{' '}
                  Address
                </Text>
                <Text style={styles.cardTextSub}>{externalAddress}</Text>
              </View>
            </View>
          </View>
          {/* Debiting Amount */}
          <View>
            <Text style={styles.cardlabel}>Debiting Amount</Text>
            <View style={styles.cardStyle1}>
              <View style={styles.inCardWrapper}>
                <Text style={styles.debitAmountStyle}>
                  {withdrawalDetails.finalFromAmount}
                </Text>
                <View style={styles.inCardCurrency}>
                  <Image
                    source={{
                      uri: activeWallet.coinImage,
                    }}
                    style={styles.coinImageStyle}
                  />
                  <Text style={styles.coinSymbolStyle}>
                    {activeWallet.coinSymbol}
                  </Text>
                </View>
              </View>
              <View style={styles.inCardWrapper1}>
                <Text style={styles.debitAmountStyle}>
                  {(
                    withdrawalDetails.finalFromAmount *
                    activeCoinBalance?.price?.USD
                  ).toFixed(2)}
                </Text>
                <View style={styles.inCardCurrency}>
                  {/* <Image
                    source={{uri: activeCoinBalance.coinImage}}
                    style={styles.coinImageStyle}
                  /> */}
                  <Text style={styles.coinSymbolStyle}>USD</Text>
                  <RightArrow width={6} height={6} />
                  {/* <Image source={smallArrRight} style={styles.arrowStyle} /> */}
                </View>
              </View>
            </View>
          </View>

          <View style={{paddingHorizontal: 10, paddingTop: 21}}>
            <Text style={styles.labelStyle}>
              Exchange Rate Booked At{' '}
              <Text style={styles.addressText}>
                {new Date(withdrawalDetails.currentTimestamp).toUTCString()}
              </Text>
            </Text>
          </View>

          <View>
            <Text style={styles.cardlabel}>PriceLock Insurance</Text>
            <View style={styles.cardStyle}>
              <PriceLockInsurance width={21} height={21} />
              {/* <Image source={priceLockInsurance} style={styles.cardImage} /> */}
              <Text style={styles.cardText}>
                Disabled
                {/* {priceLock ? 'Enabled' : 'Disabled'} */}
              </Text>
            </View>
          </View>

          {/* Fees */}

          <View>
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text style={styles.cardlabel}>Fees</Text>
              <Text style={styles.cardlabel}>
                {withdrawalDetails?.reducedAmount?.toFixed(2)}{' '}
                {withdrawalDetails?.fees_in_coin} (
                {withdrawalDetails?.reducedAmount?.toFixed(2)}%)
              </Text>
            </View>

            <View style={styles.cardStyle1}>
              <View style={styles.inCardWrapper}>
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text style={styles.feeLabelStyle}>Affiliate</Text>
                  <Info width={13} height={13} />
                  {/* <Image source={info} style={styles.coinImageStyle} /> */}
                </View>
                <Text style={styles.feeLabelStyle}>
                  {withdrawalDetails?.brokerData?.fee.toFixed(4)} (
                  {withdrawalDetails?.brokerData?.broker_fee_percentage.toFixed(
                    2,
                  )}
                  %)
                </Text>
              </View>
              <View style={styles.inCardWrapper}>
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text style={styles.feeLabelStyle}>App</Text>
                  <Info width={13} height={13} />
                </View>
                <Text style={styles.feeLabelStyle}>
                  {withdrawalDetails?.appData?.app_fee?.toFixed(4)} (
                  {withdrawalDetails?.appData?.app_fee_percentage?.toFixed(2)}%)
                </Text>
              </View>
              <View style={styles.inCardWrapper}>
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text style={styles.feeLabelStyle}>Banker</Text>
                  <Info width={13} height={13} />
                </View>
                <Text style={styles.feeLabelStyle}>
                  {withdrawalDetails.bankerData.trade_fee_native.toFixed(4)} (
                  {withdrawalDetails.bankerData.banker_fee_percentage.toFixed(
                    2,
                  )}
                  %)
                </Text>
              </View>
            </View>
          </View>

          {/* Receiving Amount */}
          <View>
            <Text style={styles.cardlabel}>Receiving Amount</Text>
            <View style={styles.cardStyle1}>
              <View style={styles.inCardWrapper}>
                <Text style={styles.debitAmountStyle}>
                  {withdrawalDetails.finalToAmount.toFixed(4)}
                </Text>
                <View style={styles.inCardCurrency}>
                  <Image
                    source={{
                      uri: activeWallet.coinImage,
                    }}
                    style={styles.coinImageStyle}
                  />
                  <Text style={styles.coinSymbolStyle}>
                    {activeWallet.coinSymbol}
                  </Text>
                </View>
              </View>
              <View style={styles.inCardWrapper1}>
                <Text style={styles.debitAmountStyle}>
                  {(
                    withdrawalDetails.finalToAmount *
                    activeCoinBalance?.price?.USD
                  ).toFixed(2)}
                </Text>
                <View style={styles.inCardCurrency}>
                  {/* <Image
                    source={{uri: activeWallet.coinImage}}
                    style={styles.coinImageStyle}
                  /> */}
                  <Text style={styles.coinSymbolStyle}>USD</Text>
                  <RightArrow width={6} height={6} />
                  {/* <Image source={smallArrRight} style={styles.arrowStyle} /> */}
                </View>
              </View>
            </View>
          </View>

          <View style={{paddingHorizontal: 10, paddingTop: 21}}>
            <Text
              style={{
                lineHeight: 25,
                color: '#5F6163',
                fontFamily: ThemeData.FONT_NORMAL,
                fontSize: 12,
              }}>
              Exchange Rate Booked At{' '}
              <Text style={styles.addressText}>
                {new Date(withdrawalDetails.currentTimestamp).toUTCString()}
              </Text>{' '}
              - Since You Have Not Turned On PriceLock Insurance Then The INR
              Valuation Of Your Bitcoin Will Be Different Upon Arrival.
            </Text>
          </View>

          {/* Estimated Time Of Arrival */}
          <View>
            <Text style={styles.cardlabel}>Estimated Time Of Arrival</Text>
            <View style={styles.cardStyle}>
              <Time width={16} height={22} />
              {/* <Image source={time} style={styles.cardImage} /> */}
              <View style={{paddingLeft: 6}}>
                <Text style={styles.cardText}>
                  {new Date(withdrawalDetails.next12hrsTimestamp)
                    .toUTCString()
                    .substring(17, 29)}{' '}
                  (12 Hours)
                </Text>
                <Text style={styles.cardTextSub}>
                  {new Date(withdrawalDetails.next12hrsTimestamp)
                    .toUTCString()
                    .substring(0, 16)}
                </Text>
              </View>
            </View>
          </View>

          {/* Latest Time Of Arrival */}
          <View>
            <Text style={styles.cardlabel}>Latest Time Of Arrival</Text>
            <View style={styles.cardStyle}>
              <Time width={16} height={22} />
              <View style={{paddingLeft: 6}}>
                <Text style={styles.cardText}>
                  {new Date(withdrawalDetails.next24hrsTimestamp)
                    .toUTCString()
                    .substring(17, 29)}{' '}
                  (24 Hours)
                </Text>
                <Text style={styles.cardTextSub}>
                  {new Date(withdrawalDetails.next24hrsTimestamp)
                    .toUTCString()
                    .substring(0, 16)}
                </Text>
              </View>
            </View>
          </View>

          {/* {accordionsList.map((item) => (
            <View key={item} style={styles.accordionItem}>
              <Text style={styles.accordionText}>{item}</Text>
              <Image
                style={styles.accordionArrow}
                source={require('../assets/arrow-carrot.png')}
              />
            </View>
          ))} */}
          <Text style={styles.confirmHeader}>Confirmations</Text>
          <View style={styles.confirmation}>
            <CheckBox
              style={{transform: [{scaleX: 0.8}, {scaleY: 0.8}]}}
              value={check1}
              onValueChange={(newValue) => setCheck1(!check1)}
              boxType="square"
            />
            <Text style={styles.confirmationText}>
              I Consent To All The Aforementioned Parameters Regarding This
              Transfer Request.
            </Text>
          </View>
          <View style={styles.confirmation}>
            <CheckBox
              style={{transform: [{scaleX: 0.8}, {scaleY: 0.8}]}}
              value={check2}
              onValueChange={(newValue) => setCheck2(!check2)}
              boxType="square"
            />
            <Text style={styles.confirmationText}>
              I Acknowledge That CCSWealth Is Not Able To Recover Funds Once
              They Have Sent To{' '}
              <Text style={styles.addressText}>{externalAddress}</Text>
            </Text>
          </View>
          <View style={{marginTop: 60, marginBottom: 40}}>
            <TouchableOpacity
              style={styles.action}
              onPress={(e) => {
                navigate('Quote');
              }}>
              <Text style={styles.actionText}>Back</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.action, styles.actionFilled]}
              onPress={!loading && check1 && check2 ? handleValidate : null}>
              <Text style={[styles.actionText, styles.actionTextFilled]}>
                {!loading ? (
                  'Confirm'
                ) : (
                  <ActivityIndicator size="small" color="white" />
                )}
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </AppMainLayout>
  );
};

export default WithdrawConfirm;

const styles = StyleSheet.create({
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 10,
  },
  headerIcon: {
    height: 40,
    width: 40,
  },
  cardlabel: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#5F6163',
    fontSize: 13,
    fontWeight: '600',
    paddingBottom: 15,
    paddingTop: 40,
    paddingLeft: 2,
  },
  cardStyle: {
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    height: 74,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 25,
  },
  cardStyle1: {
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    // height: 74,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    // alignItems: 'center',
    paddingLeft: 25,
    paddingRight: 25,
  },
  cardImage: {
    height: 24,
    width: 24,
  },
  cardText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: '#5F6163',
    fontWeight: '700',
    paddingLeft: 9,
  },
  cardTextSub: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 8,
    color: '#464B4E',
    fontWeight: '300',
    paddingTop: 2,
    paddingLeft: 8,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  container: {
    flex: 1,
    paddingHorizontal: 30,
    paddingBottom: 30,
  },
  headerContainer: {
    // alignItems: 'center',
    paddingTop: 20,
    // marginBottom: 20,
  },
  headerImg: {
    width: 200,
    height: 50,
    resizeMode: 'contain',
    marginBottom: 15,
  },
  // headerText: {
  //   textAlign: 'center',
  //   fontFamily: ThemeData.FONT_NORMAL,
  //   fontSize: 13,
  //   color: '#464B4E',
  //   paddingTop: 20,
  // },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 22,
    fontWeight: '800',
  },
  accordionItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    borderTopColor: ThemeData.BORDER_COLOR,
    paddingVertical: 30,
  },
  accordionText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 16,
  },
  accordionArrow: {
    resizeMode: 'contain',
    width: 18,
    height: 18,
    transform: [{rotate: '90deg'}],
  },
  confirmHeader: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    marginTop: 45,
    marginBottom: 28,
    fontSize: 20,
  },
  confirmation: {
    flexDirection: 'row',
    marginTop: 15,
  },
  confirmationText: {
    marginLeft: 12,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 11,
    color: '#5F6163',
    lineHeight: 25,
    marginTop: -4,
  },
  addressText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  actionsContainer: {
    flexDirection: 'row',
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
  },
  action: {
    height: 63,
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#E5E5E5',
  },
  actionFilled: {
    backgroundColor: '#5F6163',
    borderRadius: 15,
    height: 63,
    marginTop: 20,
  },
  actionText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
  },
  actionTextFilled: {
    color: 'white',
  },

  //In Card CSS
  inCardWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 30,
    borderBottomWidth: 0.5,
    borderColor: '#E5E5E5',
  },
  inCardWrapper1: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 30,
    borderColor: '#E5E5E5',
  },
  inCardCurrency: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 0.5,
    borderRadius: 15,
    borderColor: '#E5E5E5',
    paddingHorizontal: 20,
    height: 32,
  },

  debitAmountStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 20,
    color: '#5F6163',
    fontWeight: '700',
  },
  coinImageStyle: {
    width: 13,
    height: 13,
  },
  coinSymbolStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 11,
    fontWeight: '800',
    color: '#5F6163',
    paddingLeft: 4,
    paddingRight: 11,
  },

  arrowStyle: {
    width: 6,
    height: 6,
    paddingLeft: 11,
  },
  labelStyle: {
    fontSize: 12,
    lineHeight: 25,
    fontFamily: ThemeData.FONT_NORMAL,
  },
  feeLabelStyle: {
    paddingRight: 7,
    fontFamily: ThemeData.FONT_NORMAL,
  },

  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 24,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    marginBottom: 30,
    fontWeight: 'bold',
  },
});

const accordionsList = [
  'General',
  'Pricelock Insurance',
  'Fees',
  'Estimated Times',
];
