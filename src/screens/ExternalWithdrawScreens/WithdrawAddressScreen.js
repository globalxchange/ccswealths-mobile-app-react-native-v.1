import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import Clipboard from '@react-native-community/clipboard';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import AppMainLayout from '../../layouts/AppMainLayout';
import {useNavigation} from '@react-navigation/native';

const WithdrawAddressScreen = ({}) => {
  const {navigate} = useNavigation();
  const {setExternalAddress, activeWallet} = useContext(WithdrawalContext);
  const [addressInput, setAddressInput] = useState('');

  const onPasteClick = async () => {
    const copiedText = await Clipboard.getString();
    setAddressInput(copiedText.trim());
  };

  return (
    <AppMainLayout>
      <View style={styles.container}>
        <View style={styles.headerImage}>
          <Image
            source={{uri: activeWallet.coinImage}}
            style={styles.headerIcon}
          />
          <Text style={styles.headerTitle}>{activeWallet.coinName}</Text>
        </View>
        <View style={styles.crumbStyle}>
          <Text style={{color: '#5F6163'}}>Actions{` -> `}</Text>
          <View style={styles.activeCrumb}>
            <Text style={{color: '#5F6163'}}>Enter Address</Text>
          </View>
        </View>
        <View style={styles.container}>
          <View style={styles.headerContainer}>
            <Text style={styles.headerText}>Step 1: Enter Address</Text>
          </View>
          <Text style={styles.descText}>
            Enter The External {activeWallet.coinSymbol} Address That You Want
            The Coins Sent To
          </Text>
          <TextInput
            style={styles.addressInput}
            placeholder="Enter Address"
            value={addressInput}
            onChangeText={(text) => {
              setAddressInput(text);
              setExternalAddress(text);
            }}
          />
          <TouchableOpacity style={styles.pasteBtn} onPress={onPasteClick}>
            <Image
              source={require('../../assets/paste-icon-blue.png')}
              style={styles.pasteIcon}
            />
            <Text style={styles.pasteText}>Click To Paste Clipboard</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.actionBtn}
            onPress={(e) => navigate('Quote')}>
            <Text style={styles.actionBtnText}>Proceed To Amount</Text>
          </TouchableOpacity>
        </View>
      </View>
    </AppMainLayout>
  );
};

export default WithdrawAddressScreen;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 25,
    // paddingVertical: 25,
    backgroundColor: 'white',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 20,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 24,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    marginBottom: 30,
    fontWeight: 'bold',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 22,
    fontWeight: '800',
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  addressInput: {
    height: 63,
    marginVertical: 40,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 13,
    height: 16,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
    color: '#5F6163',
  },
  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
});
