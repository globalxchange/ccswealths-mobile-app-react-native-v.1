import React, {useContext, useState, useEffect} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import AppMainLayout from '../../layouts/AppMainLayout';
import CheckBox from '@react-native-community/checkbox';
import WithdrawalContext from '../../contexts/WithdrawalContext';

import RightArrow from '../../assets/rightArrow.svg';
import PriceLockInsurance from '../../assets/priceLockInsurance.svg';
import Info from '../../assets/info.svg';
import Time from '../../assets/time.svg';

import {useNavigation} from '@react-navigation/core';
import axios from 'axios';
import {AppContext} from '../../contexts/AppContextProvider';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {APP_CODE, GX_API_ENDPOINT} from '../../configs';

const WithdrawSuccess = ({route}) => {
  const {navigate, goBack} = useNavigation();
  const {previousBalance, updatedBalance} = route.params;
  const {
    activeWallet,
    withdrawalDetails,
    externalAddress,
    priceLock,
    setPriceLock,
    isShowQuote,
    setIsShowQuote,
  } = useContext(WithdrawalContext);
  const {walletCoinData} = useContext(AppContext);
  const [activeCoinBalance, setActiveCoinBalance] = useState('');

  useEffect(() => {
    if (walletCoinData && activeWallet) {
      const coinData = walletCoinData.find(
        (item) => item.coinSymbol === activeWallet.coinSymbol,
      );
      setActiveCoinBalance(coinData);
    }
  }, [activeWallet, walletCoinData]);

  return (
    <AppMainLayout>
      <View style={styles.container}>
        <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
          <View style={styles.headerContainer}>
            <View style={styles.headerImage}>
              <Image
                source={{uri: activeWallet.coinImage}}
                style={styles.headerIcon}
              />
              <Text style={styles.headerTitle}>{activeWallet.coinName}</Text>
            </View>
          </View>
          <View style={styles.crumbStyle}>
            <Text style={{color: '#5F6163'}}>
              Email Verification
              {` -> `}
            </Text>
            <View style={styles.activeCrumb}>
              <Text style={{color: '#5F6163'}}>Withdrawal Initiated</Text>
            </View>
          </View>
          <View style={styles.headerContainer}>
            <Text style={styles.headerText}>Congratulations</Text>
          </View>
          <Text style={styles.descText}>
            You Have Successully Initiated An External Withdrawal Of{' '}
            {withdrawalDetails.finalFromAmount} {activeWallet.coinSymbol}
          </Text>

          {/* Previous Balance */}
          <View>
            <Text style={styles.cardlabel}>
              Previous {activeWallet.coinSymbol} Balance
            </Text>
            <View style={styles.cardStyle}>
              <Text style={styles.cardText}>{previousBalance}</Text>
              <Image
                source={{uri: activeWallet.coinImage}}
                style={styles.cardImage}
              />
            </View>
          </View>

          {/* Updated Balance */}
          <View>
            <Text style={styles.cardlabel}>
              Updated {activeWallet.coinSymbol} Balance
            </Text>
            <View style={styles.cardStyle}>
              <Text style={styles.cardText}>{updatedBalance}</Text>
              <Image
                source={{uri: activeWallet.coinImage}}
                style={styles.cardImage}
              />
            </View>
          </View>

          <View style={{marginTop: 100, marginBottom: 40}}>
            <TouchableOpacity
              style={styles.action}
              onPress={(e) => {
                navigate('Address');
                goBack();
              }}>
              <Text style={styles.actionText}>Go To Vault</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </AppMainLayout>
  );
};

export default WithdrawSuccess;

const styles = StyleSheet.create({
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 10,
  },
  headerIcon: {
    height: 40,
    width: 40,
  },
  cardlabel: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#5F6163',
    fontSize: 13,
    fontWeight: '600',
    paddingBottom: 15,
    paddingTop: 40,
    paddingLeft: 2,
  },
  cardStyle: {
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    height: 74,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  cardStyle1: {
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    // height: 74,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    // alignItems: 'center',
    paddingLeft: 25,
    paddingRight: 25,
  },
  cardImage: {
    height: 24,
    width: 24,
  },
  cardText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: '#5F6163',
    fontWeight: '700',
    paddingLeft: 9,
  },
  cardTextSub: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 8,
    color: '#464B4E',
    fontWeight: '300',
    paddingTop: 2,
    paddingLeft: 8,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  container: {
    flex: 1,
    paddingHorizontal: 30,
    paddingBottom: 30,
  },
  headerContainer: {
    // alignItems: 'center',
    paddingTop: 20,
    // marginBottom: 20,
  },
  headerImg: {
    width: 200,
    height: 50,
    resizeMode: 'contain',
    marginBottom: 15,
  },
  // headerText: {
  //   textAlign: 'center',
  //   fontFamily: ThemeData.FONT_NORMAL,
  //   fontSize: 13,
  //   color: '#464B4E',
  //   paddingTop: 20,
  // },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 22,
    fontWeight: '800',
  },
  accordionItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    borderTopColor: ThemeData.BORDER_COLOR,
    paddingVertical: 30,
  },
  accordionText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 16,
  },
  accordionArrow: {
    resizeMode: 'contain',
    width: 18,
    height: 18,
    transform: [{rotate: '90deg'}],
  },
  confirmHeader: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    marginTop: 45,
    marginBottom: 28,
    fontSize: 20,
  },
  confirmation: {
    flexDirection: 'row',
    marginTop: 15,
  },
  confirmationText: {
    marginLeft: 12,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 11,
    color: '#5F6163',
    lineHeight: 25,
    marginTop: -4,
  },
  addressText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  actionsContainer: {
    flexDirection: 'row',
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
  },
  action: {
    height: 63,
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#E5E5E5',
  },
  actionFilled: {
    backgroundColor: '#5F6163',
    borderRadius: 15,
    height: 63,
    marginTop: 20,
  },
  actionText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
  },
  actionTextFilled: {
    color: 'white',
  },

  //In Card CSS
  inCardWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 30,
    borderBottomWidth: 0.5,
    borderColor: '#E5E5E5',
  },
  inCardWrapper1: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 30,
    borderColor: '#E5E5E5',
  },
  inCardCurrency: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 0.5,
    borderRadius: 15,
    borderColor: '#E5E5E5',
    paddingHorizontal: 20,
    height: 32,
  },

  debitAmountStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 20,
    color: '#5F6163',
    fontWeight: '700',
  },
  coinImageStyle: {
    width: 13,
    height: 13,
  },
  coinSymbolStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 11,
    fontWeight: '800',
    color: '#5F6163',
    paddingLeft: 4,
    paddingRight: 11,
  },

  arrowStyle: {
    width: 6,
    height: 6,
    paddingLeft: 11,
  },
  labelStyle: {
    fontSize: 12,
    lineHeight: 25,
    fontFamily: ThemeData.FONT_NORMAL,
  },
  feeLabelStyle: {
    paddingRight: 7,
    fontFamily: ThemeData.FONT_NORMAL,
  },

  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 24,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    marginBottom: 30,
    fontWeight: 'bold',
  },
});

const accordionsList = [
  'General',
  'Pricelock Insurance',
  'Fees',
  'Estimated Times',
];
