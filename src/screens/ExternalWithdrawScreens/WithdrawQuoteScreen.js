import {useNavigation} from '@react-navigation/core';
import axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Switch,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Image,
  ActivityIndicator,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import {formatterHelper, roundHelper, usdValueFormatter} from '../../utils';
import {APP_CODE} from '../../configs/index';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import AppMainLayout from '../../layouts/AppMainLayout';

const WithdrawQuoteScreen = ({onClose, addressInput}) => {
  const {navigate} = useNavigation();

  const {
    setWithdrawalDetails,
    priceLock,
    setPriceLock,
    activeWallet,
    setPathId,
  } = useContext(WithdrawalContext);
  const {walletCoinData} = useContext(AppContext);

  const [isCryptoFocused, setIsCryptoFocused] = useState(false);
  const [fiatInput, setFiatInput] = useState('');
  const [cryptoInput, setCryptoInput] = useState('');
  const [activeCoinBalance, setActiveCoinBalance] = useState('');
  const [isPriceLockEnabled, setIsPriceLockEnabled] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (walletCoinData && activeWallet) {
      const coinData = walletCoinData.find(
        (item) => item.coinSymbol === activeWallet.coinSymbol,
      );
      setActiveCoinBalance(coinData);
    }
  }, [activeWallet, walletCoinData]);

  const onFiatInputChange = (input) => {
    if (input) {
      const updatedFiatValue =
        parseFloat(input || 0) / (activeCoinBalance?.price?.USD || 1);
      setFiatInput(input);
      setCryptoInput(
        roundHelper(
          isNaN(parseFloat(updatedFiatValue)) ? '' : updatedFiatValue,
          activeCoinBalance?.coinSymbol,
        ),
      );
    } else {
      setCryptoInput('');
      setFiatInput('');
    }
  };

  const onCryptoInputChange = (input) => {
    console.log({input, activeCoinBalance: activeCoinBalance?.price?.USD});

    if (input) {
      const updatedFiatValue =
        parseFloat(input || 0) * (activeCoinBalance?.price?.USD || 0);

      console.log({updatedFiatValue});
      setCryptoInput(input);
      setFiatInput(
        roundHelper(
          isNaN(parseFloat(updatedFiatValue)) ? '' : updatedFiatValue,
          'USD',
        ),
      );
    } else {
      setCryptoInput('');
      setFiatInput('');
    }
  };

  const onProceedConfirm = async () => {
    setLoading(true);
    const token = await AsyncStorageHelper.getAppToken();
    const email = await AsyncStorageHelper.getLoginEmail();
    const profileId = await AsyncStorageHelper.getProfileId();

    axios
      .get(
        `https://comms.globalxchange.com/coin/vault/service/payment/paths/get?select_type=withdraw&to_currency=${activeWallet.coinSymbol}&from_currency=${activeWallet.coinSymbol}&paymentMethod=vault`,
      )
      .then(({data}) => {
        setPathId(data.paths[0].path_id);
        axios
          .post(
            `https://comms.globalxchange.com/coin/vault/service/trade/execute`,
            {
              token: token,
              email: email,
              app_code: APP_CODE,
              profile_id: profileId,
              path_id: data.paths[0].path_id,
              from_amount: cryptoInput,
              stats: true,
              // identifier: `Withdraw ${activeWallet.coinSymbol} To 3Cdqy7Vx3DiP5jADkS23fmTFNtq98YmsGQ}`,
              // userWithdrawData: '3Cdqy7Vx3DiP5jADkS23fmTFNtq98YmsGQ',
              identifier: `Withdraw ${activeWallet.coinSymbol} To ${addressInput}`,
              userWithdrawData: addressInput,
              priceLock: isPriceLockEnabled,
              priceLock_currency: 'false',
            },
          )
          .then(({data}) => {
            console.log(data);
            setWithdrawalDetails(data);
            // onClose();

            navigate('Confirmation');
            setLoading(false);
          });
      });
  };

  return (
    <AppMainLayout>
      <View style={styles.container}>
        <View style={styles.headerImage}>
          <Image
            source={{uri: activeWallet.coinImage}}
            style={styles.headerIcon}
          />
          <Text style={styles.headerTitle}>{activeWallet.coinName}</Text>
        </View>
        <View style={styles.crumbStyle}>
          <Text style={{color: '#5F6163'}} onPress={(e) => navigate('Address')}>
            Enter Address{` -> `}
          </Text>
          <View style={styles.activeCrumb}>
            <Text style={{color: '#5F6163'}}>Enter Amount</Text>
          </View>
        </View>
        <View style={styles.container}>
          <View style={styles.headerContainer}>
            <Text style={styles.headerText}>Step 2: Enter Amount</Text>
          </View>
          <Text style={styles.descText}>
            Enter The External {activeWallet.coinSymbol} Address That You Want
            The Coins Sent To
          </Text>
          <Text style={styles.balanceText}>
            {formatterHelper(
              activeCoinBalance?.coinValue || 0,
              activeCoinBalance?.coinSymbol,
            )}
            <Text style={styles.balanceTextCoin}>
              {' '}
              {activeCoinBalance.coinSymbol}
            </Text>
          </Text>
          <Text style={styles.usdBalance}>
            {usdValueFormatter.format(activeCoinBalance?.coinValueUSD)}
          </Text>
          <View style={styles.percentageContainer}>
            <TouchableOpacity style={[styles.percentageItem, {marginLeft: 0}]}>
              <Text style={styles.percentageText}>Custom</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.percentageItem}>
              <Text style={styles.percentageText}>100%</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.inputForm}>
            <View style={styles.inputContainer}>
              <TextInput
                style={[styles.input, isCryptoFocused && styles.focusedText]}
                placeholder={formatterHelper('0.00', activeWallet.coinSymbol)}
                onChangeText={(text) => onCryptoInputChange(text)}
                keyboardType="numeric"
                returnKeyType="done"
                value={cryptoInput}
                onFocus={() => setIsCryptoFocused(true)}
                placeholderTextColor="#878788"
              />
              <Text
                style={[
                  styles.cryptoName,
                  isCryptoFocused && styles.focusedText,
                ]}>
                {activeWallet.coinSymbol}
              </Text>
            </View>
            <View style={styles.divider} />
            <View style={styles.inputContainer}>
              <TextInput
                style={[styles.input, isCryptoFocused || styles.focusedText]}
                placeholder={formatterHelper('0.00', 'USD')}
                onChangeText={(text) => onFiatInputChange(text)}
                keyboardType="numeric"
                returnKeyType="done"
                value={fiatInput}
                onFocus={() => setIsCryptoFocused(false)}
                placeholderTextColor="#878788"
              />
              <Text
                style={[
                  styles.cryptoName,
                  isCryptoFocused || styles.focusedText,
                ]}>
                USD
              </Text>
            </View>
          </View>
          <View style={styles.priceLockContainer}>
            <Text style={styles.priceLock}>Turn On PriceLock Insurance</Text>
            <Switch
              style={{transform: [{scaleX: 0.7}, {scaleY: 0.7}]}}
              trackColor={{false: '#767577', true: 'darkgray'}}
              thumbColor="#5F6163"
              onValueChange={() => {
                setPriceLock(!priceLock);
                setIsPriceLockEnabled((previousState) => !previousState);
              }}
              value={isPriceLockEnabled}
            />
          </View>

          <TouchableOpacity
            style={styles.actionBtn}
            onPress={cryptoInput ? onProceedConfirm : null}>
            <Text style={styles.actionBtnText}>
              {!loading ? (
                'Proceed To Confirmation'
              ) : (
                <ActivityIndicator size="small" color="white" />
              )}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </AppMainLayout>
  );
};

export default WithdrawQuoteScreen;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 25,
    backgroundColor: 'white',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 25,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 24,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    marginBottom: 30,
    fontWeight: 'bold',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 22,
    fontWeight: '800',
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  balanceText: {
    fontSize: 35,
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    textAlign: 'center',
  },
  balanceTextCoin: {
    fontSize: 20,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  usdBalance: {
    fontFamily: ThemeData.FONT_BOLD,
    textAlign: 'center',
    color: '#5F6163',
    fontSize: 16,
  },
  percentageContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
  },
  percentageItem: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    width: 65,
    paddingVertical: 5,
    borderRadius: 5,
    marginLeft: 5,
  },
  percentageText: {
    fontFamily: ThemeData.FONT_MEDIUM,
    fontSize: 10,
    textAlign: 'center',
  },
  inputForm: {marginVertical: 50},
  inputContainer: {flexDirection: 'row', alignItems: 'center'},
  input: {
    flexGrow: 1,
    width: 0,
    color: '#5F6163',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingHorizontal: 10,
  },
  focusedInput: {
    color: '#1A6BB4',
  },
  inputButton: {
    borderWidth: 1,
    borderColor: '#EDEDED',
    justifyContent: 'center',
    paddingHorizontal: 8,
    paddingVertical: 2,
    marginLeft: 5,
  },
  inputButtonText: {
    color: '#5F6163',
    fontFamily: 'Montserrat',
    fontSize: 10,
  },
  cryptoName: {
    // marginLeft: 10,
    color: '#5F6163',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  focusedText: {
    color: '#5F6163',
  },
  divider: {
    backgroundColor: '#5F6163',
    height: 2,
    marginVertical: 5,
  },
  priceLockContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  priceLock: {
    color: '#5F6163',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  actionBtnDisabled: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    opacity: 0.3,
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 22,
    fontWeight: '800',
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
    marginBottom: 30,
  },
});
