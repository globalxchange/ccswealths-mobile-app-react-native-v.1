import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Animated,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import Clipboard from '@react-native-community/clipboard';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import AppMainLayout from '../../layouts/AppMainLayout';
import {useNavigation} from '@react-navigation/native';
import OTPInputView from '@twotalltotems/react-native-otp-input';
// import OTPInputView from '@twotalltotems/react-native-otp-input';
import Resend from '../../assets/resend.svg';
import AssetsFullLogo from '../../assets/AssetsFullLogo.svg';
import {APP_CODE} from '../../configs/index';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {WToast} from 'react-native-smart-tip';
import axios from 'axios';
import {useAppData} from '../../utils/CustomHook';

const WithdrawVerifyEmail = ({}) => {
  const {data: appData} = useAppData();
  const spread = useRef(new Animated.Value(1)).current;
  const {navigate} = useNavigation();
  const {
    setExternalAddress,
    activeWallet,
    externalAddress,
    withdrawalDetails,
    pathId,
  } = useContext(WithdrawalContext);

  const [otpInput, setOtpInput] = useState('');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (otpInput.length === 6) {
      initiateWithdrawal();
      // setTimeout(() => {
      //   navigate('withdrawSuccess');
      //   setLoading(false);
      // }, 4000);
    }
  }, [otpInput]);

  const initiateWithdrawal = async () => {
    const token = await AsyncStorageHelper.getAppToken();
    const email = await AsyncStorageHelper.getLoginEmail();
    const profileId = await AsyncStorageHelper.getProfileId();

    setLoading(true);

    axios
      .post(
        `https://comms.globalxchange.com/coin/vault/service/trade/execute`,
        {
          token: token,
          email: email,
          app_code: APP_CODE,
          profile_id: profileId,
          path_id: pathId,
          from_amount: withdrawalDetails.finalFromAmount,
          stats: false,
          identifier: `Withdraw ${activeWallet.coinSymbol} To ${externalAddress}`,
          userWithdrawData: externalAddress,
          priceLock: false,
          priceLock_currency: 'false',
          ext_verify_code: otpInput,
        },
      )
      .then(({data}) => {
        if (data.status) {
          console.log(data, 'tttttttttttttttttttttttttttttttttttttttt000');
          setLoading(false);
          navigate('withdrawSuccess', {
            previousBalance: data?.userDebit.current_balance,
            updatedBalance: data?.userDebit.updated_balance,
          });
          setLoading(false);
        } else {
          setLoading(false);
          WToast.show({data: data.message, position: WToast.position.TOP});
        }
      });
  };

  useEffect(() => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(spread, {
          toValue: 1,
          duration: 0,
          useNativeDriver: true,
          types: 'linear',
        }),
        Animated.timing(spread, {
          toValue: 1.2,
          duration: 1000,
          useNativeDriver: true,
          types: 'linear',
        }),
        Animated.timing(spread, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
          types: 'linear',
        }),
      ]),
    ).start();
  }, []);

  return (
    <AppMainLayout>
      {!loading ? (
        <View style={styles.container}>
          <View style={styles.headerImage}>
            <Image
              source={{uri: activeWallet.coinImage}}
              style={styles.headerIcon}
            />
            <Text style={styles.headerTitle}>{activeWallet.coinName}</Text>
          </View>
          <View style={styles.crumbStyle}>
            <Text
              style={{color: '#5F6163'}}
              onPress={(e) => navigate('Confirmation')}>
              Confirmation{` -> `}
            </Text>
            <View style={styles.activeCrumb}>
              <Text style={{color: '#5F6163'}}>Email Verification</Text>
            </View>
          </View>
          <View style={styles.container}>
            <View style={styles.headerContainer}>
              <Text style={styles.headerText}>Step 4: Email Verification</Text>
            </View>
            <Text style={styles.descText}>
              Enter The Six Digit Withdrawal Code Which We Just Sent To Your
              Email
            </Text>
            <View style={{marginVertical: 50}}>
              <OTPInputView
                codeInputFieldStyle={styles.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                style={styles.otpInput}
                pinCount={6}
                // autoFocusOnLoad
                code={otpInput}
                onCodeChanged={(code) => setOtpInput(code)}
              />
            </View>
            <TouchableOpacity style={styles.pasteBtn}>
              <Resend
                width={20}
                height={20}
                style={{opacity: '0.5', marginRight: 8}}
              />
              <Text style={styles.pasteText}>Resend Verification Code</Text>
            </TouchableOpacity>
            {/* <TouchableOpacity
            style={styles.actionBtn}
            onPress={(e) => navigate('Quote')}>
            <Text style={styles.actionBtnText}>Proceed To Amount</Text>
          </TouchableOpacity> */}
          </View>
        </View>
      ) : (
        <View
          style={{
            display: 'flex',
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Animated.Image
            style={{transform: [{scale: spread}], width: 200, height: 50}}
            resizeMode="contain"
            // source={require('../assets/ccs-header-icon.png')}
            source={{uri: appData?.data?.color_logo}}
          />

          <Text
            style={{
              fontFamily: ThemeData.FONT_NORMAL,
              paddingTop: 20,
              fontSize: 18,
              lineHeight: 35,
              textAlign: 'center',
              paddingHorizontal: 20,
            }}>
            Your {activeWallet.coinSymbol} Withdrawal Is Being Processed
          </Text>
        </View>
      )}
    </AppMainLayout>
  );
};

export default WithdrawVerifyEmail;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 25,
    // paddingVertical: 25,
    backgroundColor: 'white',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 20,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 24,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    marginBottom: 30,
    fontWeight: 'bold',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 21,
    fontWeight: '800',
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  addressInput: {
    height: 63,
    marginVertical: 40,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 13,
    height: 16,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 13,
    color: '#5F6163',
  },
  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  underlineStyleBase: {
    borderWidth: 1,
    // color: ThemeData.APP_MAIN_COLOR,
    color: '#5F6163',
  },
  underlineStyleHighLighted: {
    borderColor: '#999C9A',
    // color: ThemeData.APP_MAIN_COLOR,
    color: '#5F6163',
  },
  otpInput: {
    height: 60,
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
  },
});
