import {useNavigation} from '@react-navigation/native';
import React, {useContext, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionBar from '../components/ActionBar';
import {AppContext} from '../contexts/AppContextProvider';
import AppMainLayout from '../layouts/AppMainLayout';

const HomeScreen = () => {
  const navigation = useNavigation();

  const {setActiveRoute} = useContext(AppContext);

  useEffect(() => {
    navigation.addListener('blur', onScreenBlur);
    navigation.addListener('focus', onScreenFocus);

    return () => {
      navigation.removeListener('blur', onScreenBlur);
      navigation.removeListener('focus', onScreenFocus);
    };
  }, []);

  const onScreenBlur = (payload) => {};

  const onScreenFocus = (paylod) => {
    setActiveRoute('Markets');
  };

  return (
    <AppMainLayout>
      <ActionBar />
    </AppMainLayout>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({});
