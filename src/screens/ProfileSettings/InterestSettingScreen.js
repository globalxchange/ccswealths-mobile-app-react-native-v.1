import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import InterestSettings from '../../components/InterestSettings';
import ProfileSettingsItem from '../../components/ProfileSettingsItem';
import ProfileSettingsLayout from '../../layouts/ProfileSettingsLayout';

const InterestSettingScreen = () => {
  const [isLiquid, setIsLiquid] = useState();
  return (
    <ProfileSettingsLayout header="Earnings" subHeader="Manage Your Earnings">
      {isLiquid !== undefined ? (
        <InterestSettings onBack={() => setIsLiquid()} isLiquid={isLiquid} />
      ) : (
        <>
          <ProfileSettingsItem
            title="Liquid Interest"
            subText="Update First, Last, & Your Nick Name"
            onPress={() => setIsLiquid(true)}
          />
          <ProfileSettingsItem
            title="Bond Earnings"
            subText="Update First, Last, & Your Nick Name"
            onPress={() => setIsLiquid(false)}
          />
        </>
      )}
    </ProfileSettingsLayout>
  );
};

export default InterestSettingScreen;

const styles = StyleSheet.create({});
