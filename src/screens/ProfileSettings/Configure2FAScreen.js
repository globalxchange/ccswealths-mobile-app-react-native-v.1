import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import axios from 'axios';
import {GX_AUTH_URL} from '../../configs';
import LoadingAnimation from '../../components/LoadingAnimation';
import ProfileSettingsLayout from '../../layouts/ProfileSettingsLayout';
import MFASettings from '../../components/MFASettings';
import {useNavigation} from '@react-navigation/native';

const Configure2FAScreen = () => {
  const {goBack} = useNavigation();

  const INIT_BREADCRUMB = [
    {title: 'Settings', onPress: goBack},
    {title: 'Configure 2FA', onPress: null},
  ];

  const [is2FAActive, setIs2FAActive] = useState();
  const [breadCrumbs, setBreadCrumbs] = useState(INIT_BREADCRUMB);

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();

      axios
        .post(`${GX_AUTH_URL}/gx/user/mfa/status`, {email})
        .then(({data}) => {
          setIs2FAActive(data?.mfa_enabled || false);
        })
        .catch((error) => {
          console.log('Error on checking 2fa status', error);
        });
    })();

    return () => {};
  }, []);

  return (
    <ProfileSettingsLayout breadCrumbs={breadCrumbs}>
      {is2FAActive === undefined ? (
        <View style={{flex: 1, justifyContent: 'center'}}>
          <LoadingAnimation />
        </View>
      ) : (
        <MFASettings is2FAActive={is2FAActive} />
      )}
    </ProfileSettingsLayout>
  );
};

export default Configure2FAScreen;

const styles = StyleSheet.create({
  container: {},
});
