import {useFocusEffect, useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ProfileSettingsItem from '../../components/ProfileSettingsItem';
import PlatfromUpdateView from '../../components/UpdateApp/PlatfromUpdateView';
import ProfileSettingsLayout from '../../layouts/ProfileSettingsLayout';

const UpdateAppScreen = () => {
  const {goBack} = useNavigation();

  const [selectedPlatform, setSelectedPlatform] = useState('');

  const INIT_BREADCRUMB = [
    {title: 'Settings', onPress: goBack},
    {title: 'Update Application', onPress: () => setSelectedPlatform('')},
  ];

  const [breadCrumbs, setBreadCrumbs] = useState(INIT_BREADCRUMB);
  const [breadCrumbPos, setBreadCrumbPos] = useState(1);

  useEffect(() => {
    if (selectedPlatform) {
      setBreadCrumbs([...INIT_BREADCRUMB, {title: selectedPlatform}]);
      setBreadCrumbPos(2);
    } else {
      setBreadCrumbs(INIT_BREADCRUMB);
      setBreadCrumbPos(1);
    }
    return () => {};
  }, [selectedPlatform]);

  useFocusEffect(
    React.useCallback(() => {
      setBreadCrumbPos(1);
      setSelectedPlatform('');
    }, []),
  );

  return (
    <ProfileSettingsLayout
      breadCrumbs={breadCrumbs}
      breadCrumbPos={breadCrumbPos}>
      {selectedPlatform ? (
        <PlatfromUpdateView
          selectedPlatform={selectedPlatform}
          isAndroid={selectedPlatform === 'Android'}
        />
      ) : (
        <>
          <ProfileSettingsItem
            title={'Android'}
            subText={'For All Android Devices'}
            onPress={() => setSelectedPlatform('Android')}
          />
          <ProfileSettingsItem
            title={'iOS'}
            subText={'For All iPhones'}
            onPress={() => setSelectedPlatform('iOS')}
          />
        </>
      )}
    </ProfileSettingsLayout>
  );
};

export default UpdateAppScreen;

const styles = StyleSheet.create({});
