import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import React, {useState, useEffect} from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  Image,
  ScrollView,
  StyleSheet,
  TextInput,
  Dimensions,
} from 'react-native';
import ActionBar from '../../../components/ActionBar';
import FiatSwitchSearch from '../../../components/FiatSwitchSearch';
import ThemeData from '../../../configs/ThemeData';
import AppMainLayout from '../../../layouts/AppMainLayout';
import SkeltonItem from '../../../components/SkeltonItem';

const PaymentType = () => {
  const {navigate, goBack} = useNavigation();
  const {width, height} = Dimensions.get('window');
  const [loading, setLoading] = useState(false);
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [accountType, setAccountType] = useState('');
  const [allPaymentType, setAllPaymentType] = useState([]);
  const [selectedPaymentType, setSelectedPaymentType] = useState(null);

  useEffect(() => {
    const foundData = allPaymentType.find(
      (o) => o.name === selectedPaymentType?.name,
    );
    if (foundData) {
      if (foundData.name === 'UPI') {
        navigate('bankType', {
          paymentType: foundData,
        });
      } else if (foundData.name === 'Bank Transfer') {
        navigate('selectCountry', {
          paymentType: foundData,
        });
      }
    }
  }, [selectedPaymentType, isSearchOpen]);

  useEffect(() => {
    setLoading(true);
    axios
      .get(
        `https://comms.globalxchange.com/coin/vault/service/payment/methods/get`,
      )
      .then(({data}) => {
        setAllPaymentType(data.methods);
        setLoading(false);
      });
  }, []);

  return (
    <AppMainLayout>
      {!isSearchOpen ? (
        <View style={styles.container}>
          <ActionBar />
          <View style={{paddingHorizontal: 20, paddingTop: 10, flex: 1}}>
            <View style={{height: '15%'}}>
              <View style={styles.crumbStyle}>
                <View style={styles.activeCrumb}>
                  <Text style={{color: '#5F6163', fontWeight: '700'}}>
                    Payment Type
                  </Text>
                </View>
              </View>
              <View style={styles.headerContainer}>
                <Text style={styles.headerText}>
                  What Type Of Account Is It?
                </Text>
              </View>
            </View>
            <View style={[styles.midSectionStyle, {height: '70%'}]}>
              <TextInput
                onFocus={(e) => setIsSearchOpen(true)}
                style={styles.addressInput}
                placeholder="Search Types..."
                value={accountType}
                onChangeText={(text) => {
                  setAccountType(text);
                }}
              />
              <ScrollView
                contentContainerStyle={{
                  flexGrow: 1,
                  flexDirection: 'column',
                  justifyContent: 'space-between',
                }}>
                {!loading ? (
                  allPaymentType?.length > 0 ? (
                    allPaymentType.map((item, index) => {
                      return (
                        <TouchableOpacity
                          onPress={(e) => {
                            if (item.name === 'UPI') {
                              navigate('bankType', {
                                paymentType: item,
                              });
                            } else if (item.name === 'Bank Transfer') {
                              navigate('selectCountry', {
                                paymentType: item,
                              });
                            }
                          }}
                          style={[
                            styles.currencyWrapper,
                            {borderTopWidth: index === 0 ? 0 : 1},
                          ]}>
                          <Image
                            source={{uri: item.icon}}
                            style={styles.currencyIcon}
                          />
                          <Text style={styles.currencyName}>{item.name}</Text>
                        </TouchableOpacity>
                      );
                    })
                  ) : (
                    <View>
                      <Text>&nbsp;</Text>
                    </View>
                  )
                ) : (
                  Array(30)
                    .fill(' ')
                    .map((item) => {
                      return (
                        <View style={[styles.row, styles.currencyWrapper]}>
                          <View style={styles.coinContainer}>
                            <SkeltonItem
                              itemHeight={25}
                              itemWidth={25}
                              style={styles.coinImage}
                            />
                            <SkeltonItem itemHeight={20} itemWidth={80} />
                          </View>
                        </View>
                      );
                    })
                )}
              </ScrollView>
            </View>
            <View
              style={{
                height: '15%',
                display: 'flex',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                style={styles.actionBtn}
                onPress={(e) => goBack()}>
                <Text style={styles.actionBtnText}>Back</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      ) : (
        <FiatSwitchSearch
          walletBalances={[...allPaymentType]}
          setActiveCrypto={setSelectedPaymentType}
          onClose={() => setIsSearchOpen(false)}
        />
      )}
    </AppMainLayout>
  );
};

export default PaymentType;

const styles = StyleSheet.create({
  container: {
    flex: 1,

    // paddingHorizontal: 32,
    // paddingVertical: 25,
    // backgroundColor: 'red',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 35,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    // justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 25,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    fontWeight: 'bold',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 16,
    fontWeight: '800',
    // marginBottom: 40,
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  addressInput: {
    height: 63,
    borderColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    paddingHorizontal: 22,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 13,
    height: 16,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
    color: '#5F6163',
  },
  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  midSectionStyle: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 15,
  },
  currencyWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: 74,
    borderColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    paddingHorizontal: 22,
  },
  currencyIcon: {
    width: 23,
    height: 23,
  },
  currencyName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: ThemeData.TEXT_COLOR,
    paddingLeft: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 15,
  },
  coinContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    width: 25,
    height: 25,
    borderRadius: 13,
    marginRight: 5,
  },
  coinName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
});
