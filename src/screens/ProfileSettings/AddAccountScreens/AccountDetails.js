import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import React, {useState, useEffect, useRef} from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  Image,
  ScrollView,
  StyleSheet,
  TextInput,
  Dimensions,
  Animated,
} from 'react-native';
import ActionBar from '../../../components/ActionBar';
import FiatSwitchSearch from '../../../components/FiatSwitchSearch';
import ThemeData from '../../../configs/ThemeData';
import AppMainLayout from '../../../layouts/AppMainLayout';
import SkeltonItem from '../../../components/SkeltonItem';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import {useAppData} from '../../../utils/CustomHook';

const AccountDetails = ({route}) => {
  const spread = useRef(new Animated.Value(1)).current;
  const {paymentType, bankType, name, phone, address} = route.params;
  const {data: appData} = useAppData();
  const {navigate, goBack} = useNavigation();
  const {width, height} = Dimensions.get('window');
  const [loading, setLoading] = useState(false);
  const [isSearchOpen, setIsSearchOpen] = useState(false);

  const [allBankType, setAllBankType] = useState([]);
  const [selectedPaymentType, setSelectedPaymentType] = useState(null);

  const [upiID, setUpiID] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');

  const [accountNo, setAccountNo] = useState('');
  const [ifscCode, setIfscCode] = useState('');
  const [accountType, setAccountType] = useState('');

  const [depositCurrency, setDepositCurrency] = useState('');
  const [selectedAccType, setSelectedAccType] = useState('');

  useEffect(() => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(spread, {
          toValue: 1,
          duration: 0,
          useNativeDriver: true,
          types: 'linear',
        }),
        Animated.timing(spread, {
          toValue: 1.2,
          duration: 1000,
          useNativeDriver: true,
          types: 'linear',
        }),
        Animated.timing(spread, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
          types: 'linear',
        }),
      ]),
    ).start();
  }, []);

  const handleAddAccount = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    if (upiID && phoneNumber) {
      setLoading(true);

      axios
        .post(`https://comms.globalxchange.com/coin/user/bank/account/add`, {
          email: email,
          token: token,
          paymentMethod: 'GPAY',
          institute_id: bankType?._id,
          bank_name: bankType?.institute_name,
          country: 'India',
          account_name: name,
          phone_number: phone,
          beneficiary_address: address,
          currency: 'INR',
          bank_information: {
            value1: {
              name: 'UPI ID',
              valuetype: 'text',
              value: upiID,
            },
            value2: {
              name: 'Phone Number',
              valuetype: 'text',
              value: phoneNumber,
            },
          },
        })
        .then(({data}) => {
          console.log(data, 'kjwhkjfwekfjew');
          if (data.status) {
            setLoading(false);
            navigate('ManageAccount');
            // goBack();
          }
        });
    }
  };

  const handleAddAccountIndia = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    if (accountNo && ifscCode && accountType) {
      setLoading(true);
      axios
        .post(`https://comms.globalxchange.com/coin/user/bank/account/add`, {
          email: email,
          token: token,
          paymentMethod: 'BT',

          institute_id: bankType?._id,
          bank_name: bankType?.institute_name,
          country: 'India',
          account_name: name,
          phone_number: phone,
          beneficiary_address: address,
          currency: 'INR',
          bank_information: {
            value1: {
              name: 'Account Number',
              valuetype: 'text',
              value: accountNo,
            },
            value2: {
              name: 'Account Type',
              valuetype: 'text',
              value: accountType,
            },
            value3: {
              name: 'IFSC Code',
              valuetype: 'text',
              value: ifscCode,
            },
          },
        })
        .then(({data}) => {
          if (data.status) {
            setLoading(false);
            navigate('ManageAccount');
            // goBack();
          }
        });
    }
  };

  const conditionalAccountDetails = () => {
    if (paymentType.name === 'UPI') {
      return (
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              display: 'flex',
              height: height - 200,
              justifyContent: 'space-between',
            }}>
            <View>
              <View>
                <Text style={styles.headerText}>What Is Your UPI ID?</Text>
                <TextInput
                  style={styles.addressInput}
                  placeholder="Enter UPI ID.."
                  value={upiID}
                  onChangeText={(text) => {
                    setUpiID(text);
                  }}
                />
              </View>
              <View>
                <Text style={styles.headerText}>
                  What Is The UPI Phone Number?
                </Text>
                <TextInput
                  keyboardType="number-pad"
                  style={styles.addressInput}
                  placeholder="Enter Number..."
                  value={phoneNumber}
                  onChangeText={(text) => {
                    setPhoneNumber(text);
                  }}
                />
              </View>
            </View>
            <View style={styles.halfButtonWrapper}>
              <TouchableOpacity
                style={styles.actionBtnWhite}
                onPress={(e) => goBack()}>
                <Text style={[styles.actionBtnText, {color: '#5F6163'}]}>
                  Back
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.actionBtn}
                onPress={(e) => handleAddAccount()}>
                <Text style={styles.actionBtnText}>Submit</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      );
    } else if (paymentType.name === 'Bank Transfer') {
      return (
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              display: 'flex',
              height: height - 200,
              justifyContent: 'space-between',
            }}>
            <View>
              <View>
                <Text style={styles.headerText}>Account Number</Text>
                <TextInput
                  style={styles.addressInput}
                  placeholder="Enter Account No.."
                  value={accountNo}
                  onChangeText={(text) => {
                    setAccountNo(text);
                  }}
                />
              </View>
              <View>
                <Text style={styles.headerText}>IFSC Code</Text>
                <TextInput
                  style={styles.addressInput}
                  placeholder="Enter Account No.."
                  value={ifscCode}
                  onChangeText={(text) => {
                    setIfscCode(text);
                  }}
                />
              </View>
              <View>
                <Text style={styles.headerText}>Account Type</Text>
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                  }}>
                  <TouchableOpacity
                    style={
                      accountType === 'Current'
                        ? styles.roundButtonSelected
                        : styles.roundButton
                    }
                    onPress={(e) => setAccountType('Current')}>
                    <Text style={styles.accType}>Current</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={
                      accountType === 'Savings'
                        ? styles.roundButtonSelected
                        : styles.roundButton
                    }
                    onPress={(e) => setAccountType('Savings')}>
                    <Text style={styles.accType}>Savings</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View style={styles.halfButtonWrapper}>
              <TouchableOpacity
                style={styles.actionBtnWhite}
                onPress={(e) => goBack()}>
                <Text style={[styles.actionBtnText, {color: '#5F6163'}]}>
                  Back
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.actionBtn}
                onPress={(e) => handleAddAccountIndia()}>
                <Text style={styles.actionBtnText}>Submit</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      );
    }
  };

  return (
    <AppMainLayout>
      <ActionBar />
      <View style={{paddingHorizontal: 20, paddingTop: 20}}>
        <View style={styles.container}>
          <View>
            <View style={styles.crumbStyle}>
              <Text style={{color: '#5F6163'}} onPress={(e) => goBack()}>
                Account Holder
                {` -> `}
              </Text>
              <View style={styles.activeCrumb}>
                <Text style={{color: '#5F6163', fontWeight: '700'}}>
                  Account Details
                </Text>
              </View>
            </View>
          </View>

          {conditionalAccountDetails()}
        </View>
      </View>
      {loading ? (
        <View style={styles.overlay}>
          <Animated.Image
            style={{transform: [{scale: spread}], width: 100, height: 100}}
            resizeMode="contain"
            // source={require('../assets/ccs-header-icon.png')}
            source={{uri: appData?.app_icon}}
          />
        </View>
      ) : null}
    </AppMainLayout>
  );
};

export default AccountDetails;

const styles = StyleSheet.create({
  container: {
    height: '100%',
    // display: 'flex',
    // justifyContent: 'space-between',
    // flex: 1,

    // paddingHorizontal: 32,
    // paddingVertical: 25,
    backgroundColor: 'white',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 35,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    // justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 45,

    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    fontWeight: 'bold',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 16,
    fontWeight: '800',
    marginBottom: 20,
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  addressInput: {
    height: 63,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 15,
    paddingHorizontal: 22,
    fontFamily: ThemeData.FONT_MEDIUM,
    marginBottom: 45,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 13,
    height: 16,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
    color: '#5F6163',
  },
  // actionBtn: {
  //   backgroundColor: '#5F6163',
  //   borderRadius: 9,
  //   height: 54,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   marginBottom: 100,
  // },
  // actionBtnText: {
  //   textAlign: 'center',
  //   color: 'white',
  //   fontFamily: ThemeData.FONT_SEMI_BOLD,
  // },
  midSectionStyle: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 15,
  },
  currencyWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: 74,
    borderColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    paddingHorizontal: 22,
  },
  currencyIcon: {
    width: 23,
    height: 23,
  },
  currencyName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: ThemeData.TEXT_COLOR,
    paddingLeft: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 15,
  },
  coinContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    width: 25,
    height: 25,
    borderRadius: 13,
    marginRight: 5,
  },
  coinName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  halfButtonWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 80,
  },
  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',

    width: '48%',
  },
  actionBtnWhite: {
    backgroundColor: 'white',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',

    width: '48%',
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  overlay: {
    flex: 1,
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    opacity: 0.7,
    width: '100%',
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingIcon: {
    width: 150,
    height: 150,
    resizeMode: 'contain',
  },
  roundButton: {
    borderWidth: 0.5,
    borderColor: '#e5e5e5',
    borderRadius: 15,
    width: 150,
    height: 50,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  roundButtonSelected: {
    borderWidth: 0.5,
    borderColor: '#e5e5e5',
    borderRadius: 15,
    width: 150,
    height: 50,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
    color: 'white',
  },
  accType: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 13,
  },
});
