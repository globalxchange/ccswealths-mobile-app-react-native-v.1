import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import React, {useState, useEffect} from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  Image,
  ScrollView,
  StyleSheet,
  TextInput,
  Dimensions,
} from 'react-native';
import ActionBar from '../../../components/ActionBar';
import FiatSwitchSearch from '../../../components/FiatSwitchSearch';
import ThemeData from '../../../configs/ThemeData';
import AppMainLayout from '../../../layouts/AppMainLayout';
import SkeltonItem from '../../../components/SkeltonItem';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const AccountHolder = ({route}) => {
  const {paymentType, bankType, country} = route.params;
  const {navigate, goBack} = useNavigation();
  const {width, height} = Dimensions.get('window');
  const [loading, setLoading] = useState(false);
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [accountType, setAccountType] = useState('');
  const [allBankType, setAllBankType] = useState([]);
  const [selectedPaymentType, setSelectedPaymentType] = useState(null);

  const [accountName, setAccountName] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [address, setAddress] = useState('');
  const [depositCurrency, setDepositCurrency] = useState('');

  useEffect(() => {
    console.log(paymentType, 'ljwedlkjwebfkwjeb');
  }, []);

  return (
    <AppMainLayout>
      <ActionBar />
      <View style={[styles.container]}>
        <View style={styles.crumbStyle}>
          <Text style={{color: '#5F6163'}} onPress={(e) => goBack()}>
            Bank
            {` -> `}
          </Text>
          <View style={styles.activeCrumb}>
            <Text style={{color: '#5F6163', fontWeight: '700'}}>
              Account Holder
            </Text>
          </View>
        </View>
        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            flexGrow: 1,
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}>
          <View>
            <View>
              <Text style={styles.headerText}>Name On Bank Account</Text>
              <TextInput
                returnKeyType="done"
                style={styles.addressInput}
                placeholder="Enter Name..."
                value={accountName}
                onChangeText={(text) => {
                  setAccountName(text);
                }}
              />
            </View>
            <View>
              <Text style={styles.headerText}>Phone Number</Text>
              <TextInput
                keyboardType="number-pad"
                returnKeyType="done"
                style={styles.addressInput}
                placeholder="Enter Number..."
                value={phoneNumber}
                onChangeText={(text) => {
                  setPhoneNumber(text);
                }}
              />
            </View>
            <View>
              <Text style={styles.headerText}>Beneficiary Address</Text>
              <TextInput
                returnKeyType="done"
                style={styles.addressInput}
                placeholder="Enter Address..."
                value={address}
                onChangeText={(text) => {
                  setAddress(text);
                }}
              />
            </View>
            {paymentType.code !== 'GPAY' && paymentType.code !== 'BT' ? (
              <View>
                <Text style={styles.headerText}>Deposit Currency</Text>
                <TextInput
                  returnKeyType="done"
                  style={styles.addressInput}
                  placeholder="Enter Currency..."
                  value={depositCurrency}
                  onChangeText={(text) => {
                    setDepositCurrency(text);
                  }}
                />
              </View>
            ) : null}
          </View>
          <View style={styles.halfButtonWrapper}>
            <TouchableOpacity
              style={styles.actionBtnWhite}
              onPress={(e) => goBack()}>
              <Text style={[styles.actionBtnText, {color: '#5F6163'}]}>
                Back
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.actionBtn}
              onPress={(e) => {
                if (accountName && phoneNumber && address) {
                  navigate('accountDetails', {
                    paymentType: paymentType,
                    bankType: bankType,
                    name: accountName,
                    phone: phoneNumber,
                    address: address,
                  });
                }
              }}>
              <Text style={styles.actionBtnText}>Next</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </View>
    </AppMainLayout>
  );
};

export default AccountHolder;

const styles = StyleSheet.create({
  container: {
    // height: '100%',
    // display: 'flex',
    // justifyContent: 'space-between',
    flex: 1,
    paddingHorizontal: 20,
    // backgroundColor: 'red',
    // paddingHorizontal: 32,
    // paddingVertical: 25,
    backgroundColor: 'white',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 35,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    // justifyContent: 'center',
    paddingTop: 20,
    paddingBottom: 40,

    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    fontWeight: 'bold',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 16,
    fontWeight: '800',
    marginBottom: 20,
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  addressInput: {
    height: 63,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 15,
    paddingHorizontal: 22,
    fontFamily: ThemeData.FONT_MEDIUM,
    marginBottom: 45,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 13,
    height: 16,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
    color: '#5F6163',
  },
  // actionBtn: {
  //   backgroundColor: '#5F6163',
  //   borderRadius: 9,
  //   height: 54,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   marginBottom: 100,
  // },
  // actionBtnText: {
  //   textAlign: 'center',
  //   color: 'white',
  //   fontFamily: ThemeData.FONT_SEMI_BOLD,
  // },
  midSectionStyle: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 15,
  },
  currencyWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: 74,
    borderColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    paddingHorizontal: 22,
  },
  currencyIcon: {
    width: 23,
    height: 23,
  },
  currencyName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: ThemeData.TEXT_COLOR,
    paddingLeft: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 15,
  },
  coinContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    width: 25,
    height: 25,
    borderRadius: 13,
    marginRight: 5,
  },
  coinName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  halfButtonWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',

    width: '48%',
  },
  actionBtnWhite: {
    backgroundColor: 'white',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',

    width: '48%',
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
});
