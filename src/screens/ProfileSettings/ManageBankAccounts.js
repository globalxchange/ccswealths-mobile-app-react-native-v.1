import {useIsFocused, useNavigation} from '@react-navigation/native';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
} from 'react-native';
import ActionBar from '../../components/ActionBar';
import ThemeData from '../../configs/ThemeData';
import AppMainLayout from '../../layouts/AppMainLayout';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import SkeltonItem from '../../components/SkeltonItem';

const ManageBankAccounts = () => {
  const {navigate} = useNavigation();
  const [accountList, setAccountList] = useState([]);
  const isFocused = useIsFocused();
  const {width, height} = Dimensions.get('window');
  const [loading, setLoading] = useState(false);

  const getAccounts = async () => {
    setLoading(true);
    const email = await AsyncStorageHelper.getLoginEmail();
    axios
      .get(`https://comms.globalxchange.com/coin/user/bank/account/get`, {
        params: {
          email: email,
        },
      })
      .then(({data}) => {
        if (data.status) {
          setAccountList(data?.bankAccounts_info);
          setLoading(false);
        }
      });
  };

  useEffect(() => {
    getAccounts();
  }, [isFocused]);

  return (
    <AppMainLayout>
      <ActionBar />
      <View style={{paddingHorizontal: 20, paddingTop: 20, paddingBottom: 10}}>
        <View>
          <Text style={styles.headerText}>Your Bank Accounts</Text>
          <Text
            style={styles.subHeaderText}
            onPress={(e) => navigate('AddAccount')}>
            Add New Account
          </Text>
        </View>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          flexGrow: 1,
          flexDirection: 'column',
          justifyContent: 'space-between',
          paddingTop: 20,
        }}>
        {!loading ? (
          <View style={{paddingHorizontal: 20}}>
            {accountList.length > 0
              ? accountList.map((item) => {
                  return (
                    <View style={[styles.cardStyle]}>
                      <View style={styles.instituteWrapper}>
                        <Image
                          style={{width: 26, height: 26}}
                          source={{uri: item?.instituteData?.profile_image}}
                        />
                        <Text style={styles.instituteName}>
                          {item?.instituteData?.institute_name.length > 20 ? (
                            <Text>
                              {item?.instituteData?.institute_name?.substring(
                                0,
                                20,
                              )}{' '}
                              ...
                            </Text>
                          ) : (
                            item?.instituteData?.institute_name
                          )}
                        </Text>
                      </View>
                      <View>
                        <Text style={styles.accountNameLabel}>
                          Account Name
                        </Text>
                        <Text style={styles.accountNameStyle}>
                          {item.account_name}
                        </Text>
                      </View>
                      <View style={styles.paymentMethodStyle}>
                        <Image
                          style={{width: 23, height: 23}}
                          source={{uri: item?.paymentMethod_data?.icon}}
                        />
                        <Text style={styles.paymentMetodLable}>
                          {item?.paymentMethod_data?.name}
                        </Text>
                      </View>
                    </View>
                  );
                })
              : null}
          </View>
        ) : (
          Array(5)
            .fill(' ')
            .map((item) => {
              return (
                <View style={[styles.cardStyle, {marginHorizontal: 20}]}>
                  <View style={styles.instituteWrapper}>
                    <SkeltonItem
                      itemHeight={25}
                      itemWidth={25}
                      style={{borderRadius: 20, marginRight: 10}}
                    />

                    <SkeltonItem itemHeight={20} itemWidth={250} />
                  </View>
                  <View>
                    <SkeltonItem itemHeight={5} itemWidth={80} />
                    <SkeltonItem
                      itemHeight={15}
                      itemWidth={200}
                      style={{marginTop: 10}}
                    />
                  </View>
                  <View style={styles.paymentMethodStyle}>
                    <SkeltonItem
                      itemHeight={25}
                      itemWidth={25}
                      style={{borderRadius: 50, marginRight: 10}}
                    />
                    <SkeltonItem itemHeight={20} itemWidth={100} />
                  </View>
                </View>
              );
            })
        )}
      </ScrollView>
    </AppMainLayout>
  );
};

export default ManageBankAccounts;

const styles = StyleSheet.create({
  headerText: {
    fontSize: 25,
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    lineHeight: 30,
  },
  subHeaderText: {
    fontSize: 13,
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    lineHeight: 15,
    // textDecorationStyle: 'solid',
    textDecorationLine: 'underline',
    paddingTop: 15,
  },
  allAccountsView: {
    paddingTop: 20,
  },
  cardStyle: {
    borderWidth: 0.5,
    borderColor: '#e5e5e5',
    backgroundColor: 'white',
    borderRadius: 10,
    height: 200,
    marginBottom: 25,
    padding: 24,
    display: 'flex',
    justifyContent: 'space-between',
    shadowOffset: {width: -2, height: 4},
    shadowOpacity: 0.1,
    shadowRadius: 3,
  },
  instituteWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  instituteName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 20,
    paddingLeft: 8,
  },
  accountNameLabel: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 9,
    paddingBottom: 8,
  },
  accountNameStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 14,
  },
  paymentMethodStyle: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  paymentMetodLable: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 17,
    paddingLeft: 4,
  },
});
