import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import LoadingAnimation from '../../components/LoadingAnimation';
import {GX_AUTH_URL} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import ProfileSettingsLayout from '../../layouts/ProfileSettingsLayout';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import ChangePassword from '../../components/ChangePassword';

const ChangePasswordScreen = () => {
  const {goBack} = useNavigation();

  const INIT_BREADCRUMB = [
    {title: 'Settings', onPress: goBack},
    {title: 'Change Password', onPress: null},
  ];

  const [breadCrumbs, setBreadCrumbs] = useState(INIT_BREADCRUMB);
  const [isInitiated, setIsInitiated] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const forgotPassword = async () => {
    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();

    axios
      .post(`${GX_AUTH_URL}/gx/user/password/forgot/request`, {
        email,
      })
      .then(({data}) => {
        // console.log('OTP Request', data);
        if (data.status) {
          setIsInitiated(true);
        }
      })
      .finally(() => setIsLoading(false));
  };

  return (
    <ProfileSettingsLayout breadCrumbs={breadCrumbs}>
      {isLoading ? (
        <View style={{flex: 1, justifyContent: 'center'}}>
          <LoadingAnimation />
        </View>
      ) : isInitiated ? (
        <ChangePassword />
      ) : (
        <View style={styles.container}>
          <Text style={styles.header}>Change Password</Text>
          <Text style={styles.desc}>
            Once You Click Initiate Password Change. You Will Receive A Six
            Digit Code In Your Email Which You Have To Enter To Complete The
            Password Change.
          </Text>
          <TouchableOpacity
            style={styles.initiateButton}
            onPress={forgotPassword}>
            <Text style={styles.initiateButtonText}>Initiate</Text>
          </TouchableOpacity>
        </View>
      )}
    </ProfileSettingsLayout>
  );
};

export default ChangePasswordScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  header: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 26,
  },
  desc: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    lineHeight: 25,
    marginVertical: 30,
  },
  initiateButton: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    width: 160,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
  },
  initiateButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
});
