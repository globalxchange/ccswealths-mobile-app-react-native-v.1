import React from 'react';
import {StyleSheet} from 'react-native';
import ActionBar from '../components/ActionBar';
import RegisterProspect from '../components/RegisterProspect';
import AppMainLayout from '../layouts/AppMainLayout';

const RegisterProspectScreen = () => {
  return (
    <AppMainLayout disableBlockCheck>
      <ActionBar />
      <RegisterProspect />
    </AppMainLayout>
  );
};

export default RegisterProspectScreen;

const styles = StyleSheet.create({});
