import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import Clipboard from '@react-native-community/clipboard';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import AppMainLayout from '../../layouts/AppMainLayout';
import {useNavigation} from '@react-navigation/native';
import QRCode from 'react-native-qrcode-svg';
import axios from 'axios';
import {APP_CODE} from '../../configs';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';

const ViewAddressScreen = ({}) => {
  const {navigate, goBack} = useNavigation();
  const {setExternalAddress, activeWallet} = useContext(WithdrawalContext);
  const [coinAddress, setCoinAddress] = useState();
  const [loading, setLoading] = useState(false);
  const [coinData, setCoinData] = useState([]);
  const [isCopied, setIsCopied] = useState(false);

  const getSingleCoinData = async () => {
    console.log('resss', 'qwertyuioasdfghjklzxcvbnmsdfghjkrtyui');
    setLoading(true);
    const email = await AsyncStorageHelper.getLoginEmail();
    axios
      .post(`https://comms.globalxchange.com/coin/vault/service/coins/get`, {
        app_code: APP_CODE,
        email: email,
        include_coins: [activeWallet?.coinSymbol],
      })
      .then(({data}) => {
        console.log(data, email, 'kjqwdjwebfkjwkejfbkefk3rbf');
        setCoinData(data.coins_data[0]);
        setLoading(false);
      });
  };

  useEffect(() => {
    console.log('res', 'qwertyuioasdfghjklzxcvbnmsdfghjkrtyui');
    getSingleCoinData();
  }, [activeWallet]);

  useEffect(() => {
    console.log(coinData, 'oooooooooooooooooooooooooooooooooooo');

    if (coinData.native_deposit === true && coinData.coin_address !== '') {
      setCoinAddress(coinData.coin_address);
    } else if (
      coinData.native_deposit === true &&
      coinData.coin_address === ''
    ) {
      setCoinAddress(false);
    } else if (coinData.native_deposit === false) {
      setCoinAddress(null);
    }
  }, [coinData, loading]);

  const onCopyClick = () => {
    Clipboard.setString(coinAddress);
    setIsCopied(true);
    setTimeout(() => {
      setIsCopied(false);
    }, 5000);
  };

  const conditionalSubmitHash = () => {
    if (
      coinData.coinSymbol === 'ETH' ||
      coinData.coinSymbol === 'XRP' ||
      coinData.coinSymbol === 'TRX' ||
      coinData.eth_token
    ) {
      return (
        <TouchableOpacity
          style={styles.submitButton}
          onPress={(e) =>
            navigate('submitHash', {
              coinData: coinData,
              coinAddress: coinAddress,
            })
          }>
          <Text style={{color: '#464B4E', fontFamily: ThemeData.FONT_BOLD}}>
            Submit Hash
          </Text>
        </TouchableOpacity>
      );
    } else if (coinData.coinSymbol === 'SOL') {
      return (
        <TouchableOpacity
          style={styles.submitButton}
          onPress={(e) =>
            navigate('submitHash', {
              coinData: coinData,
              coinAddress: coinAddress,
            })
          }>
          <Text
            style={{
              color: '#464B4E',
              fontFamily: ThemeData.FONT_BOLD,
              fontSize: 12,
            }}>
            Submit Txn Signature
          </Text>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity style={[styles.submitButton, {opacity: 0.3}]}>
          <Text style={{color: '#464B4E', fontFamily: ThemeData.FONT_BOLD}}>
            Submit Hash
          </Text>
        </TouchableOpacity>
      );
    }
  };

  const conditionalMiddleSection = () => {
    if (coinAddress !== '' && coinAddress !== false && coinAddress !== null) {
      return (
        <View>
          <View style={styles.qrContainer}>
            <QRCode
              value={coinAddress}
              size={220}
              color={coinAddress ? ThemeData.APP_MAIN_COLOR : '#C5DAEC'}
            />
          </View>
          <View style={styles.addressWrapper}>
            <Text style={styles.addressStyle}>
              {coinAddress?.substring(0, 20)}...
            </Text>
          </View>
          <View style={styles.doubleButton}>
            <TouchableOpacity
              style={styles.copyButton}
              // onPress={() => Clipboard.setString({coinAddress})}
              onPress={onCopyClick}>
              <Text style={{color: '#464B4E', fontFamily: ThemeData.FONT_BOLD}}>
                {isCopied ? 'Address Copied' : 'Copy'}
              </Text>
            </TouchableOpacity>
            {conditionalSubmitHash()}
          </View>
        </View>
      );
    } else if (coinAddress === false) {
      return (
        <View style={{display: 'flex', justifyContent: 'space-between'}}>
          <Text style={styles.descText}>
            Looks Like Its Your First Time Add {activeWallet.coinSymbol}. Please
            Request Your Wallet Address By Clicking The Button Below
          </Text>
        </View>
      );
    } else if (coinAddress === null) {
      return (
        <View>
          <Text style={styles.descText}>
            Sorry We Don’t Currency Support Native Deposits For{' '}
            {coinData.coinName} But We Are Working On It!
          </Text>
        </View>
      );
    }
  };

  const handleGenerateAddress = async () => {
    const profileId = await AsyncStorageHelper.getProfileId();
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    console.log(coinData, profileId, 'jhgefkawgfkqwekfqkegf');
    if (coinData.coinSymbol === 'BTC') {
      setLoading(true);
      axios
        .post(
          `https://comms.globalxchange.com/coin/vault/service/coin/request/address`,
          {
            email: email,
            app_code: APP_CODE,
          },
        )
        .then(({data}) => {
          if (data.status) {
            console.log(data, 'BTC Addresssssssssssss');
            getSingleCoinData();
          }
        });
    } else if (coinData.coinSymbol === 'ETH' || coinData.eth_token) {
      setLoading(true);
      axios
        .post(
          `https://comms.globalxchange.com/coin/vault/service/address/create`,
          {
            app_code: APP_CODE,

            profile_id: profileId,
          },
        )
        .then(({data}) => {
          console.log(data, 'ETH Addresssssssssssss');
          if (data.status) {
            getSingleCoinData();
          }
        });
    } else if (
      coinData.coinSymbol === 'LTC' ||
      coinData.coinSymbol === 'DOGE'
    ) {
      setLoading(true);
      axios
        .post(
          `https://comms.globalxchange.com/coin/vault/service/bit/coins/generate/address`,
          {
            email: email,
            token: token,
            app_code: APP_CODE,
            coin: coinData.coinSymbol,
          },
        )
        .then(({data}) => {
          if (data.status) {
            console.log(data, 'LTC/DOGE Addresssssssssssss');
            getSingleCoinData();
          }
        });
    } else if (
      coinData.coinSymbol === 'TRX' ||
      coinData.coinSymbol === 'XRP' ||
      coinData.coinSymbol === 'SOL'
    ) {
      setLoading(true);
      axios
        .post(
          `https://comms.globalxchange.com/coin/vault/service/crypto/address/request`,
          {
            email: email,
            app_code: APP_CODE,
            coin: coinData.coinSymbol,
          },
        )
        .then(({data}) => {
          if (data.status) {
            console.log(data, 'TRX/XRP/SOL Addresssssssssssss');
            getSingleCoinData();
          }
        });
    }
  };

  return (
    <AppMainLayout>
      <View style={styles.container}>
        <View>
          <View style={styles.headerImage}>
            <Image
              source={{uri: activeWallet.coinImage}}
              style={styles.headerIcon}
            />
            <Text style={styles.headerTitle}>{activeWallet.coinName}</Text>
          </View>
          <View style={styles.crumbStyle}>
            <Text style={{color: '#5F6163'}} onPress={(e) => goBack()}>
              {activeWallet.coinName} Actions{` -> `}
            </Text>
            <View style={styles.activeCrumb}>
              <Text style={{color: '#5F6163', fontWeight: '700'}}>
                View Address
              </Text>
            </View>
          </View>
        </View>
        {!loading ? (
          conditionalMiddleSection()
        ) : (
          <ActivityIndicator size="small" color="darkgray" />
        )}
        <View>
          {coinAddress === false ? (
            <View style={styles.singleButton}>
              <TouchableOpacity
                onPress={(e) => handleGenerateAddress()}
                style={[styles.fullButtonWhite]}>
                <Text
                  style={{color: '#5F6163', fontFamily: ThemeData.FONT_BOLD}}>
                  Generate Address
                </Text>
              </TouchableOpacity>
            </View>
          ) : null}
          <View style={styles.singleButton}>
            <TouchableOpacity
              style={styles.fullButton}
              onPress={(e) => goBack()}>
              <Text style={{color: 'white', fontFamily: ThemeData.FONT_BOLD}}>
                Go Back
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </AppMainLayout>
  );
};

export default ViewAddressScreen;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 25,
    paddingVertical: 25,
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'space-between',
    flex: 1,
    flexDirection: 'column',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 20,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 15,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    // marginBottom: 20,
    fontWeight: 'bold',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 22,
    fontWeight: '800',
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },

  addressInput: {
    height: 63,
    marginVertical: 40,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 13,
    height: 16,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
    color: '#5F6163',
  },
  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  qrContainer: {
    // width: 220,
    // height: 220,
    marginVertical: 60,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  addressWrapper: {
    borderWidth: 0.5,
    borderRadius: 20,
    borderColor: '#EBEBEB',
    height: 72,
  },
  addressStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    paddingHorizontal: 23,
    paddingVertical: 30,
    color: '#464B4E',
  },
  doubleButton: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 50,
  },
  copyButton: {
    width: 148,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    height: 63,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  submitButton: {
    width: 178,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    height: 63,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  singleButton: {
    marginTop: 15,
    // marginBottom: 15,
  },
  fullButton: {
    borderWidth: 1,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    height: 63,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5F6163',
    marginBottom: 20,
  },
  fullButtonWhite: {
    borderWidth: 1,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    height: 63,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#5F6163',
    fontSize: 15,
    lineHeight: 25,
    textAlign: 'center',
  },
});
