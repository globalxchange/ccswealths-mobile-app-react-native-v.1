import React, {useContext, useState, useRef, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  ActivityIndicator,
  Animated,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import AppMainLayout from '../../layouts/AppMainLayout';
import Clipboard from '@react-native-community/clipboard';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {APP_CODE} from '../../configs';
import {WToast} from 'react-native-smart-tip';
import {useAppData} from '../../utils/CustomHook';

const SubmitHashScreen = ({route}) => {
  const spread = useRef(new Animated.Value(1)).current;
  const {coinData, coinAddress} = route.params;
  const {navigate, goBack} = useNavigation();
  const {data: appData} = useAppData();
  const {activeWallet} = useContext(WithdrawalContext);
  const [hashValue, setHashValue] = useState('');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(spread, {
          toValue: 1,
          duration: 0,
          useNativeDriver: true,
          types: 'linear',
        }),
        Animated.timing(spread, {
          toValue: 1.2,
          duration: 1000,
          useNativeDriver: true,
          types: 'linear',
        }),
        Animated.timing(spread, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
          types: 'linear',
        }),
      ]),
    ).start();
  }, []);

  const onPasteClick = async () => {
    const copiedText = await Clipboard.getString();
    setHashValue(copiedText.trim());
  };

  const getUpdatedBalance = (email, coin) => {
    axios
      .post(`https://comms.globalxchange.com/coin/vault/service/coins/get`, {
        app_code: APP_CODE,
        email: email,
        displayCurrency: 'INR',
        include_coins: [coin],
      })
      .then(({data}) => {
        if (data.status) {
          setLoading(false);
          navigate('hashSuccess', {
            successData: data,
          });
        }
      });
  };

  const handleSubmitHash = async () => {
    // const profileId = await AsyncStorageHelper.getProfileId();
    const email = await AsyncStorageHelper.getLoginEmail();
    // const token = await AsyncStorageHelper.getAppToken();

    if (coinData.coinSymbol === 'ETH' || coinData.eth_token) {
      setLoading(true);
      axios
        .post(
          `https://comms.globalxchange.com/coin/vault/service/deposit/eth/coins/request`,
          {
            app_code: APP_CODE,
            email: email,
            coin: coinData?.coinSymbol,
            txn_hash: hashValue,
          },
        )
        .then(({data}) => {
          if (data.status) {
            getUpdatedBalance(email, coinData.coinSymbol);
          } else {
            setLoading(false);
            WToast.show({data: 'Wrong Hash'});
          }
        });
    } else if (coinData.coinSymbol === 'XRP') {
      setLoading(true);
      axios
        .post(
          `https://comms.globalxchange.com/coin/vault/service/deposit/xrp/external/request`,
          {
            email: email,
            app_code: APP_CODE,
            hash: hashValue,
          },
        )
        .then(({data}) => {
          if (data.status) {
            getUpdatedBalance(email, coinData.coinSymbol);
          } else {
            setLoading(false);
            WToast.show({data: 'Wrong Hash'});
          }
        });
    } else if (coinData.coinSymbol === 'TRX') {
      setLoading(true);
      axios
        .post(
          `https://comms.globalxchange.com/coin/vault/service/deposit/trx/external/request`,
          {
            email: email,
            app_code: APP_CODE,
            hash: hashValue,
          },
        )
        .then(({data}) => {
          if (data.status) {
            getUpdatedBalance(email, coinData.coinSymbol);
          } else {
            setLoading(false);
            WToast.show({data: 'Wrong Hash'});
          }
        });
    } else if (coinData.coinSymbol === 'SOL') {
      setLoading(true);
      axios
        .post(
          `https://comms.globalxchange.com/coin/vault/service/deposit/eth/coins/request`,
          {
            email: email,
            app_code: APP_CODE,
            txn_signature: hashValue,
          },
        )
        .then(({data}) => {
          console.log(data, 'kwbekfwbefkjwbkfbkebfkrf');
          if (data.status) {
            getUpdatedBalance(email, coinData.coinSymbol);
          } else {
            setLoading(false);
            WToast.show({data: 'Wrong Hash'});
          }
        });
    }
  };

  // const handleSubmitHash = () => {
  //   navigate('hashSuccess', {
  //     coinData: coinData,
  //     // successData: data,
  //   });
  // };

  return (
    <AppMainLayout>
      <View style={styles.container}>
        <View>
          <View style={styles.headerImage}>
            <Image
              source={{uri: activeWallet.coinImage}}
              style={styles.headerIcon}
            />
            <Text style={styles.headerTitle}>{activeWallet.coinName}</Text>
          </View>
          <View style={styles.crumbStyle}>
            <Text style={{color: '#5F6163'}} onPress={(e) => goBack()}>
              View Address{` -> `}
            </Text>
            <View style={styles.activeCrumb}>
              <Text style={{color: '#5F6163', fontWeight: '700'}}>
                Submit Hash
              </Text>
            </View>
          </View>
          <View style={{paddingTop: 62}}>
            <Text style={styles.enterHash}>Enter Hash</Text>
            <Text style={styles.lablePlain}>
              Enter The Hash Of The {coinData.coinSymbol} Deposit Transaction To{' '}
              <Text style={styles.lableBold}>{coinAddress}</Text>
            </Text>

            <TextInput
              style={styles.addressInput}
              placeholder="Enter Hash..."
              value={hashValue}
              onChangeText={(text) => setHashValue(text)}
            />
            <TouchableOpacity style={styles.pasteBtn} onPress={onPasteClick}>
              <Image
                source={require('../../assets/paste-icon-blue.png')}
                style={styles.pasteIcon}
              />
              <Text style={styles.pasteText}>Click To Paste Clipboard</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.doubleButton}>
          <TouchableOpacity
            style={styles.copyButton}
            onPress={(e) => goBack()}
            // onPress={() => Clipboard.setString({coinAddress})}
            // onPress={onCopyClick}
          >
            <Text style={{color: 'white', fontFamily: ThemeData.FONT_BOLD}}>
              Back
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.submitButton}
            onPress={(e) => handleSubmitHash()}>
            <Text style={{color: '#464B4E', fontFamily: ThemeData.FONT_BOLD}}>
              {coinData.coinSymbol === 'SOL'
                ? 'Submit Txn Signature'
                : 'Submit Hash'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      {loading ? (
        <View style={styles.overlay}>
          <Animated.Image
            style={{transform: [{scale: spread}], width: 100, height: 100}}
            resizeMode="contain"
            // source={require('../assets/ccs-header-icon.png')}
            source={{uri: appData?.app_icon}}
          />
        </View>
      ) : null}
    </AppMainLayout>
  );
};

export default SubmitHashScreen;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 25,
    paddingVertical: 25,
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'space-between',
    flex: 1,
    flexDirection: 'column',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 20,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 15,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    // marginBottom: 20,
    fontWeight: 'bold',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 22,
    fontWeight: '800',
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  addressInput: {
    height: 63,
    marginVertical: 40,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    fontFamily: ThemeData.FONT_MEDIUM,
    borderRadius: 20,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 13,
    height: 16,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
    color: '#5F6163',
  },
  doubleButton: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 50,
  },
  copyButton: {
    width: 148,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    height: 63,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5F6163',
  },
  submitButton: {
    width: 178,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    height: 63,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  enterHash: {
    color: '#5F6163',
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 22,
    paddingBottom: 31,
  },
  lablePlain: {
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    lineHeight: 25,
  },
  lableBold: {
    color: '#5F6163',
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 12,
  },
  overlay: {
    flex: 1,
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    opacity: 0.7,
    width: '100%',
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingIcon: {
    width: 150,
    height: 150,
    resizeMode: 'contain',
  },
});
