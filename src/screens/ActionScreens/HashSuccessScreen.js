import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import AppMainLayout from '../../layouts/AppMainLayout';

const HashSuccessScreen = ({route}) => {
  const {coinData, successData} = route.params;
  const {navigate, goBack} = useNavigation();
  return (
    <AppMainLayout>
      <View style={styles.container}>
        <View>
          <View style={styles.headerImage}>
            <Image
              source={{uri: coinData.coinImage}}
              style={styles.headerIcon}
            />
            <Text style={styles.headerTitle}>{coinData.coinName}</Text>
          </View>
          <View style={styles.crumbStyle}>
            <Text style={{color: '#5F6163'}}>Submit Hash{` -> `}</Text>
            <View style={styles.activeCrumb}>
              <Text style={{color: '#5F6163'}}>Success</Text>
            </View>
          </View>
          <View style={{paddingTop: 62}}>
            <Text style={styles.enterHash}>Success</Text>
            <Text style={styles.lablePlain}>
              Congratulations. You Have Successully Submitted The Hash For Your
              Lasted {successData?.symbol} Deposit
            </Text>

            {/* Previous Balance */}
            <View>
              <Text style={styles.cardlabel}>
                Updated {successData?.symbol} Balance
              </Text>
              <View style={styles.cardStyle}>
                <Text style={styles.cardText}>{successData?.coinValue}</Text>
                <Image
                  source={{uri: successData?.coinImage}}
                  style={styles.cardImage}
                />
              </View>
            </View>

            {/* <View style={{marginTop: 100, marginBottom: 40}}>
              <TouchableOpacity
                style={styles.submitButton}
                onPress={(e) => handleSubmitHash()}>
                <Text
                  style={{color: '#464B4E', fontFamily: ThemeData.FONT_BOLD}}>
                  Submit Hash
                </Text>
              </TouchableOpacity>
            </View> */}
          </View>
        </View>
        <View style={styles.singleButton}>
          <TouchableOpacity
            onPress={(e) => goBack()}
            style={[styles.fullButton]}>
            <Text style={{color: 'white', fontFamily: ThemeData.FONT_BOLD}}>
              Go Back
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </AppMainLayout>
  );
};

export default HashSuccessScreen;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 25,
    paddingVertical: 25,
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'space-between',
    flex: 1,
    flexDirection: 'column',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 20,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 15,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    // marginBottom: 20,
    fontWeight: 'bold',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 22,
    fontWeight: '800',
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  cardlabel: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#5F6163',
    fontSize: 13,
    fontWeight: '600',
    paddingBottom: 15,
    paddingTop: 60,
    paddingLeft: 2,
  },
  cardStyle: {
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    height: 74,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  cardStyle1: {
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    // height: 74,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    // alignItems: 'center',
    paddingLeft: 25,
    paddingRight: 25,
  },
  cardImage: {
    height: 24,
    width: 24,
  },
  cardText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: '#5F6163',
    fontWeight: '700',
    paddingLeft: 9,
  },
  cardTextSub: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 8,
    color: '#464B4E',
    fontWeight: '300',
    paddingTop: 2,
    paddingLeft: 8,
  },
  enterHash: {
    color: '#5F6163',
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 22,
    paddingBottom: 31,
  },
  lablePlain: {
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    lineHeight: 25,
  },
  submitButton: {
    width: 178,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    height: 63,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  singleButton: {
    marginTop: 15,
    // marginBottom: 15,
  },
  fullButton: {
    borderWidth: 1,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    height: 63,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5F6163',
    marginBottom: 20,
  },
  fullButtonWhite: {
    borderWidth: 1,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    height: 63,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
});
