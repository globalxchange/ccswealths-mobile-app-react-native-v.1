import React, {useContext, useEffect, useState} from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  Dimensions,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import ThemeData from '../../../configs/ThemeData';
import {AppContext} from '../../../contexts/AppContextProvider';
import WithdrawalContext from '../../../contexts/WithdrawalContext';
import AppMainLayout from '../../../layouts/AppMainLayout';

import RightArrow from '../../../assets/rightArrow.svg';
import PriceLockInsurance from '../../../assets/priceLockInsurance.svg';
import Info from '../../../assets/info.svg';
import Time from '../../../assets/time.svg';
import {useNavigation, useRoute} from '@react-navigation/native';

const SellCryptoStepTwo = ({route}) => {
  const {
    activeListCategory,
    setSelectedCrypto,
    selectedCrypto,
    isLoggedIn,
    setActiveRoute,
    pathData,
    walletCoinData,
    getWalletCoinData,
    selectedCoin,
    setSelectedCoin,
  } = useContext(AppContext);
  const {width, height} = Dimensions.get('window');
  const [loading, setLoading] = useState(false);
  const {navigate, goBack} = useNavigation();
  const {resData} = route.params;
  return (
    <AppMainLayout>
      <View style={styles.container}>
        <View>
          <View style={styles.headerImage}>
            <Image
              source={{uri: selectedCoin?.coinImage}}
              style={styles.headerIcon}
            />
            <Text style={styles.headerTitle}>{selectedCoin?.coinName}</Text>
          </View>
          <View style={styles.crumbStyle}>
            <Text style={{color: '#5F6163'}} onPress={(e) => goBack()}>
              Buy {selectedCoin?.coinName}
              {` -> `}
            </Text>
            <View style={styles.activeCrumb}>
              <Text style={{color: '#5F6163', fontWeight: '700'}}>Success</Text>
            </View>
          </View>
        </View>
        <View style={{marginTop: 40}}>
          <Text
            style={{
              fontFamily: ThemeData.FONT_NORMAL,
              color: '#5F6163',
              lineHeight: 30,
            }}>
            Congratulations. You Have Successully Exchanged{' '}
            {resData?.user_debit?.amount} {resData?.user_debit?.coin} for{' '}
            {resData?.user_credit?.amount}
            {resData?.user_credit?.coin}
          </Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          {/* From */}
          <View>
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text style={styles.cardlabel}>
                New {resData?.userResult?.debit_obj?.coin} Balance
              </Text>
            </View>
            <View style={styles.cardStyle1}>
              <View style={styles.inCardWrapper1}>
                <Text style={styles.debitAmountStyle}>
                  {resData?.userResult?.debit_obj?.updated_balance?.toFixed(4)}
                </Text>

                <View style={styles.inCardCurrency}>
                  <Image
                    source={{
                      uri: selectedCoin.coinImage,
                    }}
                    style={styles.coinImageStyle}
                  />
                  <Text style={styles.coinSymbolStyle}>
                    {resData?.userResult?.debit_obj?.coin}
                  </Text>
                  <RightArrow width={6} height={6} />
                </View>
              </View>
            </View>
          </View>

          {/* To */}
          <View>
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text style={styles.cardlabel}>
                New {resData?.bankerResult?.credit_obj?.coin} Balance
              </Text>
            </View>
            <View style={styles.cardStyle1}>
              <View style={styles.inCardWrapper1}>
                <Text style={styles.debitAmountStyle}>
                  {resData?.bankerResult?.credit_obj?.updated_balance?.toFixed(
                    4,
                  )}
                </Text>
                <View style={styles.inCardCurrency}>
                  <Image
                    source={{
                      uri: selectedCoin.coinImage,
                    }}
                    style={styles.coinImageStyle}
                  />
                  <Text style={styles.coinSymbolStyle}>
                    {selectedCoin.coinSymbol}
                  </Text>
                  <RightArrow width={6} height={6} />
                </View>
              </View>
            </View>
          </View>

          <View style={styles.halfButtonWrapper}>
            <TouchableOpacity
              style={styles.actionBtn}
              onPress={(e) => {
                navigate('BuyCryptoStepOne');
                goBack();
              }}>
              {loading ? (
                <ActivityIndicator size="small" color="#08152D" />
              ) : (
                <Text style={styles.actionBtnText}>Trade Again</Text>
              )}
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </AppMainLayout>
  );
};

export default SellCryptoStepTwo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 32,
    // paddingVertical: 25,
    backgroundColor: 'white',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 35,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 20,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    // marginBottom: 45,
    fontWeight: 'bold',
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  cardlabel: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#5F6163',
    fontSize: 13,
    fontWeight: '600',
    paddingBottom: 15,
    paddingTop: 40,
    paddingLeft: 2,
  },
  cardStyle: {
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    height: 74,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 25,
  },
  cardStyle1: {
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    // height: 74,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    // alignItems: 'center',
    paddingLeft: 25,
    paddingRight: 25,
  },
  cardImage: {
    height: 24,
    width: 24,
  },
  cardText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: '#5F6163',
    fontWeight: '700',
    paddingLeft: 9,
  },
  cardTextSub: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 8,
    color: '#464B4E',
    fontWeight: '300',
    paddingTop: 2,
    paddingLeft: 8,
  },

  //In Card CSS
  inCardWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 30,
    borderBottomWidth: 0.5,
    borderColor: '#E5E5E5',
  },
  inCardWrapper1: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 30,
    borderColor: '#E5E5E5',
  },
  inCardCurrency: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 0.5,
    borderRadius: 15,
    borderColor: '#E5E5E5',
    paddingHorizontal: 20,
    height: 32,
  },
  coinImageStyle: {
    width: 13,
    height: 13,
  },
  coinSymbolStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 11,
    fontWeight: '800',
    color: '#5F6163',
    paddingLeft: 4,
    paddingRight: 11,
  },
  feeLabelStyle: {
    paddingRight: 7,
    fontFamily: ThemeData.FONT_NORMAL,
  },
  debitAmountStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 20,
    color: '#5F6163',
    fontWeight: '700',
  },

  //Button CSS

  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    width: '100%',
  },
  actionBtnWhite: {
    backgroundColor: 'white',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    width: '48%',
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  halfButtonWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
