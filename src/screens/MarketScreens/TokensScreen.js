import {useNavigation, useFocusEffect} from '@react-navigation/native';
import React, {useContext, useState, useRef} from 'react';
import {StyleSheet, View} from 'react-native';
import ActionBar from '../../components/ActionBar';
import MarketCover from '../../components/MarketCover';
import MarketListSwitcher from '../../components/MarketListSwitcher';
import {AppContext} from '../../contexts/AppContextProvider';
import AppMainLayout from '../../layouts/AppMainLayout';
import SearchLayout from '../../layouts/SearchLayout';
import OfferingsList from '../../components/OfferingsList';
import axios from 'axios';
import {GX_API_ENDPOINT} from '../../configs';
import {useQuery} from 'react-query';
import FilterView from '../../components/OfferingsList/FilterView';

const getTokenCategories = async () => {
  const {data} = await axios.get(
    `${GX_API_ENDPOINT}/coin/investment/types/get`,
  );

  const allItemObj = {
    title: 'All',
    icon: require('../../assets/tokens-icon.png'),
  };

  const parsedList = data?.investments?.map((item) => ({
    ...item,
    icon: {uri: item.icon},
    title: item.name,
  }));

  return [allItemObj, ...(parsedList || [])];
};

const TokensScreen = () => {
  const {} = useNavigation();

  const {
    setActiveListCategory,
    setHomeSearchInput,
    homeSearchInput,
    activeListCategory,
  } = useContext(AppContext);

  const {data: tokenCategories} = useQuery('tokenCategory', getTokenCategories);

  const [isShowSearch, setIsShowSearch] = useState(false);
  const [searchPlaceHolderText, setSearchPlaceHolderText] = useState('');
  const [searchList, setSearchList] = useState();
  const [subCategory, setSubCategory] = useState('');
  const [contractType, setContractType] = useState('');

  const searchCallback = useRef(() => {});

  useFocusEffect(
    React.useCallback(() => {
      if (tokenCategories) {
        setActiveListCategory(tokenCategories[0]);
      }
    }, [tokenCategories]),
  );

  const setSearchCallback = (callback) => {
    searchCallback.current = callback;
  };

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <View style={styles.container}>
        {/* <MarketCover showHeader={isShowSearch} /> */}
        <View style={[styles.transitionContainer]}>
          <MarketListSwitcher
            isShowSearch={isShowSearch}
            openSearch={() => setIsShowSearch(true)}
            openCountry={() => {}}
            list={tokenCategories}
          />
        </View>
        {isShowSearch && (
          <View style={styles.searchContainer}>
            <SearchLayout
              onBack={() => setIsShowSearch(false)}
              value={homeSearchInput}
              setValue={setHomeSearchInput}
              onSubmit={(_, item) =>
                searchCallback.current ? searchCallback.current(item) : null
              }
              placeholder={`Search ${activeListCategory?.title}`}
              list={searchList || []}
              showUserList
              setSelectedFilter={setActiveListCategory}
              selectedFilter={activeListCategory}
              dontFilter
              filters={tokenCategories}
              keyboardOffset={100}
            />
          </View>
        )}
        <FilterView
          tokenCategories={tokenCategories}
          setSubCategory={setSubCategory}
          subCategory={subCategory}
          contractType={contractType}
          setContractType={setContractType}
        />
        <OfferingsList
          setSearchPlaceHolderText={setSearchPlaceHolderText}
          setSearchList={setSearchList}
          setSearchCallback={setSearchCallback}
          subCategory={subCategory}
          contractType={contractType}
        />
      </View>
    </AppMainLayout>
  );
};

export default TokensScreen;

const styles = StyleSheet.create({
  container: {flex: 1},
  transitionContainer: {
    backgroundColor: 'white',
    paddingTop: 10,
  },
  fragmentContainer: {flex: 1},
  searchContainer: {
    ...StyleSheet.absoluteFill,
    backgroundColor: 'white',
    zIndex: 10,
  },
});
