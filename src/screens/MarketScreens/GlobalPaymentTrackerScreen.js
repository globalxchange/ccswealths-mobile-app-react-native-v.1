import axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  SectionList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ActionBar from '../../components/ActionBar';
import SkeltonItem from '../../components/SkeltonItem';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import AppMainLayout from '../../layouts/AppMainLayout';
import {formatterHelper, usdValueFormatter} from '../../utils';
import Moment from 'moment-timezone';
import {AppContext} from '../../contexts/AppContextProvider';
import {getAssetData} from '../../utils';
import ExpandableValue from '../../components/ExpandableValue';

const GlobalPaymentTrackerScreen = () => {
  const [totalData, setTotalData] = useState();
  const [intrestLogs, setIntrestLogs] = useState();

  useEffect(() => {
    axios
      .get(`${GX_API_ENDPOINT}/coin/iced/interest/logs/get/all`, {
        params: {type: 'deposit'},
      })
      .then(({data}) => {
        if (data.status) {
          setTotalData([
            {value: data.interest_payments || 0, label: 'Payment Till Day'},
            {
              value: usdValueFormatter.format(data.interest_paid || 0),
              label: 'Payouts Till Date',
            },
            {
              value: data.contracts || 0,
              label: 'Bonds',
            },
            {
              value: data.assets || 0,
              label: 'Assets',
            },
          ]);

          let tempList = data.interestLogs || [];

          const addedList = [];

          const sortedList = [];

          tempList.forEach((tmpItem) => {
            if (!addedList.includes(tmpItem)) {
              const date = Moment(tmpItem.timestamp);
              const subList = [];
              addedList.push(tmpItem);
              subList.push(tmpItem);

              tempList.forEach((item) => {
                if (!addedList.includes(item)) {
                  const itemDate = Moment(item.timestamp);
                  if (date.isSame(itemDate, 'day')) {
                    addedList.push(item);
                    subList.push(item);
                  }
                }
              });

              let dateSting = '';

              if (date.isSame(Moment(), 'day')) {
                dateSting = 'Today';
              } else if (date.isSame(Moment().add(-1, 'days'), 'day')) {
                dateSting = 'Yesterday';
              } else {
                dateSting = date.format('dddd, MMMM Do YYYY');
              }

              sortedList.push({
                title: dateSting,
                data: subList,
              });
            }
          });

          setIntrestLogs(sortedList);
        }
      })
      .catch((error) => {});
    return () => {};
  }, []);

  return (
    <AppMainLayout>
      <ActionBar />
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.headerText}>Global Payment Tracker</Text>
          <Text style={styles.headerSubText}>
            See Every Interest Payment In Live Time
          </Text>
        </View>
        <View style={styles.totalDataContainer}>
          {totalData ? (
            <FlatList
              data={totalData}
              horizontal
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => item.label + index}
              renderItem={({item}) => (
                <View style={styles.totalItem}>
                  <Text style={styles.totalDataValue}>{item.value}</Text>
                  <Text style={styles.totalDataLabel}>{item.label}</Text>
                </View>
              )}
            />
          ) : (
            <TotalSkeleton />
          )}
        </View>
        <View style={styles.filterContainer}>
          {FILTERES.map((item) => (
            <TouchableOpacity style={styles.filterItem} key={item.title}>
              <Text style={styles.filterName}>{item.title}</Text>
            </TouchableOpacity>
          ))}
        </View>
        {intrestLogs ? (
          <SectionList
            showsVerticalScrollIndicator={false}
            style={styles.transactionList}
            sections={intrestLogs}
            keyExtractor={(item, index) => item + index}
            renderItem={({item}) => <TxnItem item={item} />}
            renderSectionHeader={({section: {title}}) => (
              <View style={styles.transactionGroup}>
                <Text style={styles.date}>{title}</Text>
              </View>
            )}
          />
        ) : (
          <ListSkeleton />
        )}
      </View>
    </AppMainLayout>
  );
};

const TxnItem = ({item}) => {
  const {walletCoinData} = useContext(AppContext);

  const appName = item?.countryData?.name
    ? item?.countryData?.name.length > 6
      ? item?.countryData?.name
          .split(' ')
          .map((n) => (n[0].toUpperCase() === n[0] ? n[0] : null))
          .join('')
      : item?.countryData?.name
    : item.appData?.app_name;

  return (
    <View style={styles.txnItem}>
      <Image
        source={{uri: getAssetData(item.coin, walletCoinData)?.coinImage || ''}}
        style={styles.txnIcon}
        resizeMode="contain"
      />
      <ExpandableValue
        value={item.amount || item?.total_interest}
        coin={item.coin}
        style={styles.txnAmount}
        usdValue={item.usd_value || item?.earned_usd_value}
      />
      <View style={styles.fromApp}>
        <Text style={styles.fromAppName}>{appName}</Text>
        {item?.countryData?.image ? (
          <View style={styles.fromAppImage}>
            <Image
              resizeMode="contain"
              style={styles.fromAppImage}
              source={{uri: item?.countryData?.image}}
            />
          </View>
        ) : (
          <Image
            resizeMode="contain"
            style={styles.fromAppImage}
            source={{uri: item.appData?.app_icon}}
          />
        )}
      </View>
    </View>
  );
};

const ListSkeleton = () => (
  <>
    <View style={styles.transactionGroup}>
      <SkeltonItem itemHeight={13} itemWidth={70} style={styles.date} />
    </View>
    {Array(5)
      .fill(0)
      .map((_, index) => (
        <View key={index} style={styles.txnItem}>
          <SkeltonItem itemHeight={20} itemWidth={20} style={styles.txnIcon} />
          <SkeltonItem
            itemHeight={10}
            itemWidth={100}
            style={{marginLeft: 10}}
          />
          <View style={[styles.fromApp, {marginLeft: 'auto'}]}>
            <SkeltonItem
              itemHeight={10}
              itemWidth={70}
              style={styles.fromAppName}
            />
            <SkeltonItem
              itemHeight={20}
              itemWidth={20}
              style={styles.txnIcon}
            />
          </View>
        </View>
      ))}
  </>
);

const TotalSkeleton = () => (
  <View style={{flexDirection: 'row'}}>
    <View style={styles.totalItem}>
      <SkeltonItem
        itemHeight={50}
        itemWidth={100}
        style={styles.totalDataValue}
      />
      <SkeltonItem
        itemHeight={10}
        itemWidth={100}
        style={styles.totalDataLabel}
      />
    </View>
    <View style={styles.totalItem}>
      <SkeltonItem
        itemHeight={50}
        itemWidth={120}
        style={styles.totalDataValue}
      />
      <SkeltonItem
        itemHeight={10}
        itemWidth={100}
        style={styles.totalDataLabel}
      />
    </View>
    <View style={styles.totalItem}>
      <SkeltonItem
        itemHeight={50}
        itemWidth={120}
        style={styles.totalDataValue}
      />
      <SkeltonItem
        itemHeight={10}
        itemWidth={100}
        style={styles.totalDataLabel}
      />
    </View>
  </View>
);

export default GlobalPaymentTrackerScreen;

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: 'white', padding: 20},
  headerContainer: {},
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 22,
    color: ThemeData.APP_MAIN_COLOR,
  },
  headerSubText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    color: ThemeData.APP_MAIN_COLOR,
  },
  totalDataContainer: {
    backgroundColor: 'rgba(231, 231, 231, 0.5);',
    marginHorizontal: -20,
    marginTop: 30,
  },
  totalItem: {
    height: 120,
    justifyContent: 'center',
    paddingLeft: 20,
    paddingRight: 40,
  },
  totalDataValue: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 28,
    marginBottom: 5,
  },
  totalDataLabel: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
  filterContainer: {
    flexDirection: 'row',
    marginRight: -10,
    marginTop: 25,
  },
  filterItem: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingVertical: 8,
    marginRight: 10,
    flex: 1,
  },
  filterName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 10,
    textAlign: 'center',
  },
  transactionList: {},
  transactionGroup: {
    paddingBottom: 10,
    paddingTop: 30,
    backgroundColor: 'white',
  },
  date: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 14,
  },
  txnItem: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    flexDirection: 'row',
    paddingVertical: 28,
    paddingHorizontal: 20,
    marginBottom: 20,
    alignItems: 'center',
  },
  txnIcon: {
    width: 20,
    height: 20,
    borderRadius: 10,
  },
  txnAmount: {
    flex: 1,
    paddingHorizontal: 10,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  fromApp: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  fromAppImage: {
    width: 20,
    height: 20,
    borderRadius: 10,
  },
  fromAppName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    marginRight: 10,
    fontSize: 12,
    textAlign: 'right',
  },
});

const FILTERES = [
  {title: 'All Users'},
  {title: 'All Assets Types'},
  {title: 'From Bonds'},
];
