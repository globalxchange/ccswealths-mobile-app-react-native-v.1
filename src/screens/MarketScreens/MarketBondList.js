import {useNavigation} from '@react-navigation/native';
import React, {useContext} from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import FilterTabs from '../../components/MarketList/FilterTabs';

import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';

const MarketBondList = ({allBonds, appData}) => {
  const {setSelectedBond} = useContext(AppContext);
  const {navigate} = useNavigation();
  return (
    <>
      <FilterTabs />
      <ScrollView showsVerticalScrollIndicator={false}>
        {allBonds?.map((item) => {
          return (
            <View style={{borderBottomWidth: 2, borderColor: '#EBEBEB'}}>
              <View style={styles.indSectionContainer}>
                <View style={styles.indHeaderSection}>
                  <View
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={{uri: item.coin_metdata.coinImage}}
                      style={styles.headerIcon}
                    />
                    <Text style={[styles.headerTitle, {paddingLeft: 10}]}>
                      {item.coin_metdata.coinName}
                    </Text>
                  </View>
                  <View style={styles.pullRight}>
                    <Text style={styles.headerTitle}>
                      {item?.app_price_dc?.toFixed(4)}
                    </Text>
                  </View>
                </View>
                <View style={styles.indSection}>
                  <View>
                    <Text style={styles.subtitle}>Daily Rate:</Text>
                  </View>
                  <View style={styles.pullRight}>
                    <Text style={styles.subtitle}>
                      {item.interest_rate.toFixed(5)}%
                    </Text>
                  </View>
                </View>

                <View style={styles.indSection}>
                  <View>
                    <Text style={styles.subtitle}>Monthly Rate:</Text>
                  </View>
                  <View style={styles.pullRight}>
                    <Text style={styles.subtitle}>
                      {item.monthly_interest_rate.toFixed(5)}%
                    </Text>
                  </View>
                </View>
                <View style={styles.indSectionLast}>
                  <View>
                    <Text style={styles.subtitle}>Annual Rate:</Text>
                  </View>
                  <View style={styles.pullRight}>
                    <Text style={styles.subtitle}>
                      {item.yearly_interest_rate.toFixed(5)}%
                    </Text>
                  </View>
                </View>

                {/* <View style={styles.indSectionLast}>
                  <View>
                    <Text style={styles.title}>Term Earnings</Text>
                    <Text style={styles.subtitle}>
                      {item.coinsData[0].termEarnings.toFixed(5)}
                    </Text>
                  </View>
                  <View style={styles.pullRight}>
                    <Text style={styles.title}>Term Rate</Text>
                    <Text style={styles.subtitle}>
                      {item.term_interest_rate.toFixed(5)}%
                    </Text>
                  </View>
                </View> */}
              </View>
            </View>
          );
        })}
      </ScrollView>
    </>
  );
};

export default MarketBondList;

const styles = StyleSheet.create({
  container: {flex: 1},
  action: {
    height: 38,
    // flex: 1,
    width: 121,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    // borderWidth: 1,
    // borderColor: '#E5E5E5',
    backgroundColor: '#04AF76',
    marginBottom: 58,
    marginTop: 10,
  },
  actionFilled: {
    // backgroundColor: '#5F6163',
    color: 'white',
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 16,
  },
  indSectionContainer: {
    // backgroundColor: 'pink',
    paddingHorizontal: 30,
    paddingTop: 50,
  },
  indHeaderSection: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 30,
  },
  indSection: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 7,
    // borderBottomWidth: 0.5,
    // borderColor: '#EBEBEB',
  },
  indSectionLast: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 7,
    paddingBottom: 43,
  },
  headerTitle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 20,
    color: '#464B4E',
  },
  title: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 12,
    color: '#464B4E',
  },
  subtitle: {
    fontSize: 14,
    fontFamily: ThemeData.FONT_NORMAL,
    paddingTop: 11,
    color: '#464B4E',
  },
  pullRight: {
    display: 'flex',
    alignItems: 'flex-end',
  },
  transitionContainer: {
    backgroundColor: 'white',
    paddingTop: 10,
  },
  fragmentContainer: {flex: 1},
  searchContainer: {
    ...StyleSheet.absoluteFill,
    backgroundColor: 'white',
    zIndex: 10,
  },
  headerIcon: {
    width: 32,
    height: 32,
  },
});
