import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  Modal,
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  StyleSheet,
  Dimensions,
  Image,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {AppContext} from '../../contexts/AppContextProvider';

import Animated, {
  useCode,
  set,
  Clock,
  interpolate,
} from 'react-native-reanimated';
import ThemeData from '../../configs/ThemeData';
import Buy from '../../assets/buy.svg';
import Sell from '../../assets/sell.svg';
import {useNavigation, useRoute} from '@react-navigation/native';

const BuySellBottomSheet = ({
  isBottomSheetOpen,
  setIsBottomSheetOpen,
  selectedItem,
  setIsBuyBottomSheetOpen,
  setIsSellBottomSheetOpen,
}) => {
  const {
    checkOutData,
    setWalletAddress,
    walletBalances,
    setSheetHide,
    isSheetHide,
    setSelectedCoin,
  } = useContext(AppContext);
  const {navigate} = useNavigation();
  const {height, width} = Dimensions.get('window');
  const {bottom} = useSafeAreaInsets();
  const chatKeyboardAnimation = useRef(new Animated.Value(0));
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const [isOpenFeeAudit, setIsOpenFeeAudit] = useState(false);

  useEffect(() => {
    console.log(selectedItem, 'ljwkebfckwqbfg3fb3jkew');
  }, []);

  return (
    <Modal
      animationType="slide"
      visible={!isSheetHide && isBottomSheetOpen}
      transparent
      hardwareAccelerated
      statusBarTranslucent
      onDismiss={() => setIsBottomSheetOpen(false)}
      //   onRequestClose={onBackKeyPress}
      style={{backgroundColor: 'white'}}>
      <TouchableOpacity
        activeOpacity={1}
        style={styles.overlay}
        onPress={() => setIsBottomSheetOpen(false)}
        onPressOut={() => {}}>
        <TouchableWithoutFeedback style={{flex: 1}}>
          <Animated.View
            style={[
              styles.container,
              //   {maxHeight: isExpanded ? height - 60 : height * 0.6},
              {maxHeight: height / 2.2},
              {
                paddingBottom: bottom,
                transform: [
                  {
                    translateY: interpolate(chatKeyboardAnimation.current, {
                      inputRange: [0, 1],
                      outputRange: [0, -keyboardHeight],
                    }),
                  },
                ],
              },
            ]}>
            <View style={[styles.header]}>
              <Image
                style={styles.headerLogo}
                source={{uri: selectedItem?.coinImage}}
                resizeMode="contain"
              />
              <Text style={styles.headerText}>{selectedItem?.coinName}</Text>
            </View>

            <View style={styles.fragmentContainer}>
              <TouchableOpacity
                onPress={(e) => {
                  setIsBottomSheetOpen(false);
                  // setIsBuyBottomSheetOpen(true);
                  setSelectedCoin(selectedItem);
                  navigate('BuyCryptoStepOne', {
                    selectedItem: selectedItem,
                  });
                }}
                style={[
                  styles.actionWrapper,
                  {borderBottomWidth: 0.5, borderColor: '#e5e5e5'},
                ]}>
                <Buy width={30} height={30} />
                <View style={styles.textWrapper}>
                  <Text style={styles.textTitle}>Buy</Text>
                  <Text style={styles.textSubtitle}>
                    Buy {selectedItem?.coinName} With Another Currency
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.actionWrapper}
                onPress={(e) => {
                  setIsBottomSheetOpen(false);
                  // setIsBuyBottomSheetOpen(true);
                  setSelectedCoin(selectedItem);
                  navigate('SellCryptoStepOne', {
                    selectedItem: selectedItem,
                  });
                }}>
                <Sell width={30} height={30} />
                <View style={styles.textWrapper}>
                  <Text style={styles.textTitle}>Sell</Text>
                  <Text style={styles.textSubtitle}>
                    Sell {selectedItem?.coinName} With Another Currency
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </Animated.View>
        </TouchableWithoutFeedback>
      </TouchableOpacity>
    </Modal>
  );
};

export default BuySellBottomSheet;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
  fragmentContainer: {
    // flex: 1,
    paddingHorizontal: 40,
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor: 'white',
  },
  header: {
    backgroundColor: 'white',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 30,
    paddingBottom: 30,
    borderTopLeftRadius: 30,
    borderTopEndRadius: 30,
    borderBottomWidth: 0.2,
    borderColor: '#e5e5e5',
  },
  headerLogo: {
    height: 35,
    width: 35,
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#464B4E',
    fontSize: 35,
    paddingLeft: 10,
  },
  actionWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 30,
  },
  textWrapper: {
    paddingLeft: 20,
  },
  textTitle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 18,
    color: '#5F6163',
  },
  textSubtitle: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    color: '#5F6163',
    paddingTop: 2,
  },
});
