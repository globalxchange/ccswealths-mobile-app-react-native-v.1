import React, {useContext, useEffect, useState, useRef} from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  Dimensions,
  TextInput,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  Animated,
} from 'react-native';
import ThemeData from '../../../configs/ThemeData';
import {AppContext} from '../../../contexts/AppContextProvider';
import WithdrawalContext from '../../../contexts/WithdrawalContext';
import AppMainLayout from '../../../layouts/AppMainLayout';

import RightArrow from '../../../assets/rightArrow.svg';
import PriceLockInsurance from '../../../assets/priceLockInsurance.svg';
import Info from '../../../assets/info.svg';
import Time from '../../../assets/time.svg';
import {useNavigation, useRoute} from '@react-navigation/native';
import CryptoSwitchSearch from '../../../components/CryptoSwitchSearch';
import axios from 'axios';
import {APP_CODE} from '../../../configs';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import {useAppData} from '../../../utils/CustomHook';

const BuyCryptoStepOne = () => {
  const {
    activeListCategory,
    setSelectedCrypto,
    selectedCrypto,
    isLoggedIn,
    setActiveRoute,
    pathData,
    walletCoinData,
    getWalletCoinData,
    selectedCoin,
    setSelectedCoin,
    cryptoTableData,
    walletBalances,
  } = useContext(AppContext);
  const spread = useRef(new Animated.Value(1)).current;
  const {width, height} = Dimensions.get('window');
  const [loading, setLoading] = useState(false);
  const {navigate, goBack} = useNavigation();
  const [openSearch, setOpenSearch] = useState(false);
  const [activeCrypto, setActiveCrypto] = useState(null);
  const [coinList, setCoinList] = useState([]);
  const [fromBalance, setFromBalance] = useState(0);
  const [toBalance, setToBalance] = useState(0);
  const [fromValue, setFromValue] = useState(0);
  const [toValue, setToValue] = useState(0);
  const [pathId, setPathId] = useState('');
  const [toData, setToData] = useState(null);

  const [tradeLoading, setTradeLoading] = useState(false);

  const {data: appData} = useAppData();

  useEffect(() => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(spread, {
          toValue: 1,
          duration: 0,
          useNativeDriver: true,
          types: 'linear',
        }),
        Animated.timing(spread, {
          toValue: 1.2,
          duration: 1000,
          useNativeDriver: true,
          types: 'linear',
        }),
        Animated.timing(spread, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
          types: 'linear',
        }),
      ]),
    ).start();
  }, []);

  useEffect(() => {
    if (activeCrypto) {
      getUpdatedFromBalance(activeCrypto?.coinSymbol);
    }
  }, [activeCrypto]);
  useEffect(() => {
    getUpdatedToBalance(selectedCoin.coinSymbol);
  }, []);

  const getUpdatedToBalance = async (coin) => {
    const email = await AsyncStorageHelper.getLoginEmail();
    axios
      .post(`https://comms.globalxchange.com/coin/vault/service/coins/get`, {
        app_code: APP_CODE,
        email: email,
        displayCurrency: 'INR',
        include_coins: [coin],
      })
      .then(({data}) => {
        setToBalance(data.coins_data[0].coinValue);
      });
  };

  const getUpdatedFromBalance = async (coin) => {
    const email = await AsyncStorageHelper.getLoginEmail();
    axios
      .post(`https://comms.globalxchange.com/coin/vault/service/coins/get`, {
        app_code: APP_CODE,
        email: email,
        displayCurrency: 'INR',
        include_coins: [coin],
      })
      .then(({data}) => {
        setFromBalance(data.coins_data[0].coinValue);
        getPathId();
      });
  };
  const getPathId = () => {
    axios
      .get(
        `https://comms.globalxchange.com/coin/vault/service/payment/stats/get`,
        {
          params: {
            select_type: 'instant',
            to_currency: selectedCoin.coinSymbol,
            from_currency: activeCrypto?.coinSymbol,
            paymentMethod: 'vault',
            banker: 'Global X Change',
            app_code: APP_CODE,
          },
        },
      )
      .then(({data}) => {
        setPathId(data.pathData.paymentPaths[0].path_ids[0]);
      });
  };

  const getToValue = async () => {
    setLoading(true);
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();
    const profileId = await AsyncStorageHelper.getProfileId();

    axios
      .post(
        `https://comms.globalxchange.com/coin/vault/service/trade/execute`,
        {
          token: token,
          email: email,
          app_code: APP_CODE,
          profile_id: profileId,
          from_amount: fromValue,
          stats: true,
          identifier: `Exchange ${activeCrypto?.coinSymbol} For ${selectedCoin?.coinSymbol}`,
          path_id: pathId,
        },
      )
      .then(({data}) => {
        setToData(data);
        setLoading(false);
      });
  };

  useEffect(() => {
    if (fromValue && activeCrypto) {
      getToValue();
    }
  }, [fromValue, activeCrypto]);

  useEffect(() => {
    axios
      .get(
        `https://comms.globalxchange.com/coin/vault/service/payment/stats/get`,
        {
          params: {
            select_type: 'instant',
            to_currency: selectedCoin?.coinSymbol,
            paymentMethod: 'vault',
            banker: 'Global X Change',
            app_code: 'indianotc',
          },
        },
      )
      .then(({data}) => {
        if (data.status) {
          const tempArr = [];

          data.pathData.from_currency.map((item) => {
            tempArr.push(item.coin_metadata);
          });
          setCoinList(tempArr);
        }
      });
  }, []);

  const calcValue = (perc) => {
    var value = (Number(fromBalance) * Number(perc)) / 100;

    setFromValue(value.toString());
  };

  const conditionalToData = () => {
    if (loading) {
      return (
        <Text style={[styles.debitAmountStyle, {opacity: 0.5}]}>
          Loading ...
        </Text>
      );
    } else {
      if (toData) {
        return (
          <Text style={styles.debitAmountStyle}>{toData?.finalToAmount}</Text>
        );
      } else {
        return (
          <Text style={[styles.debitAmountStyle, {opacity: 0.5}]}>0.0000</Text>
        );
      }
    }
  };

  const handlePlaceTrade = async () => {
    setTradeLoading(true);
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();
    const profileId = await AsyncStorageHelper.getProfileId();

    axios
      .post(
        `https://comms.globalxchange.com/coin/vault/service/trade/execute`,
        {
          token: token,
          email: email,
          app_code: APP_CODE,
          profile_id: profileId,
          from_amount: fromValue,
          stats: false,
          identifier: `Exchange ${activeCrypto?.coinSymbol} For ${selectedCoin?.coinSymbol}`,
          path_id: pathId,
        },
      )
      .then(({data}) => {
        if (data.status) {
          setTradeLoading(false);
          navigate('BuyCryptoStepTwo', {
            resData: data,
          });
        }
      });
  };

  return (
    <AppMainLayout>
      {!openSearch ? (
        <View style={styles.container}>
          <View style={{paddingBottom: 30}}>
            <View style={styles.headerImage}>
              <Image
                source={{uri: selectedCoin?.coinImage}}
                style={styles.headerIcon}
              />
              <Text style={styles.headerTitle}>{selectedCoin?.coinName}</Text>
            </View>
            <View style={styles.crumbStyle}>
              <Text style={{color: '#5F6163'}} onPress={(e) => goBack()}>
                Actions
                {` -> `}
              </Text>
              <View style={styles.activeCrumb}>
                <Text style={{color: '#5F6163', fontWeight: '700'}}>
                  Buy {selectedCoin?.coinName}
                </Text>
              </View>
            </View>
          </View>
          <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="handled">
            {/* From */}
            <View>
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Text style={styles.cardlabel}>From</Text>
                <Text style={styles.cardlabel}>
                  Balance: {fromBalance?.toFixed(4)}
                </Text>
              </View>
              <View style={[styles.cardStyle1, {height: 70}]}>
                <View style={styles.inCardWrapper1}>
                  <TextInput
                    style={[styles.debitAmountStyle, {width: '50%'}]}
                    placeholder="0.0000"
                    keyboardType="decimal-pad"
                    value={fromValue}
                    onChangeText={(value) => setFromValue(value)}
                  />
                  {activeCrypto === null ? (
                    <TouchableOpacity
                      style={styles.inCardCurrency}
                      onPress={(e) => setOpenSearch(true)}>
                      <Text style={styles.coinSymbolStyle}>Select Asset</Text>
                      <RightArrow width={6} height={6} />
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      style={styles.inCardCurrency}
                      onPress={(e) => setOpenSearch(true)}>
                      <Image
                        source={{
                          uri: activeCrypto.coinImage,
                        }}
                        style={styles.coinImageStyle}
                      />
                      <Text style={styles.coinSymbolStyle}>
                        {activeCrypto.coinSymbol}
                      </Text>
                      <RightArrow width={6} height={6} />
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </View>
            {/* Percentage */}
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 20,
                marginRight: 30,
              }}>
              <TouchableOpacity
                style={styles.inCardCurrency}
                onPress={(e) => calcValue(25)}>
                <Text style={styles.coinSymbolStyle}>25%</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.inCardCurrency}
                onPress={(e) => calcValue(50)}>
                <Text style={styles.coinSymbolStyle}>50%</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.inCardCurrency}
                onPress={(e) => calcValue(100)}>
                <Text style={styles.coinSymbolStyle}>100%</Text>
              </TouchableOpacity>
            </View>
            {/* To */}
            <View>
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Text style={styles.cardlabel}>To</Text>
                <Text style={styles.cardlabel}>
                  Balance: {toBalance.toFixed(4)}
                  {/* {getUpdatedBalance(selectedCoin?.coinSymbol)} */}
                </Text>
              </View>
              <View style={[styles.cardStyle1, {height: 70}]}>
                <View style={styles.inCardWrapper1}>
                  {conditionalToData()}
                  <View style={styles.inCardCurrency}>
                    <Image
                      source={{
                        uri: selectedCoin.coinImage,
                      }}
                      style={styles.coinImageStyle}
                    />
                    <Text style={styles.coinSymbolStyle}>
                      {selectedCoin.coinSymbol}
                    </Text>
                    <RightArrow width={6} height={6} />
                  </View>
                </View>
              </View>
            </View>
            {/* Fees*/}
            <View>
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Text style={styles.cardlabel}>
                  Fees in {toData ? toData?.fees_in_coin : 'TBD'}
                </Text>
                {toData ? (
                  <Text style={styles.cardlabel}>
                    {toData?.gx_fee?.toFixed(5)} (
                    {toData?.percentage_fee_by_volume?.fees_with_broker?.toFixed(
                      4,
                    )}
                    %)
                  </Text>
                ) : (
                  <Text style={styles.cardlabel}>0.00 (0.00%)</Text>
                )}
              </View>

              <View style={styles.cardStyle1}>
                <View style={styles.inCardWrapper}>
                  <View
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Text style={styles.feeLabelStyle}>Affiliate</Text>
                    <Info width={13} height={13} />
                    {/* <Image source={info} style={styles.coinImageStyle} /> */}
                  </View>
                  {toData ? (
                    <Text style={styles.feeLabelStyle}>
                      {toData?.brokerData?.fee?.toFixed(5)} (
                      {toData?.brokerData?.broker_fee_percentage?.toFixed(4)}%)
                    </Text>
                  ) : (
                    <Text style={styles.feeLabelStyle}>0.00 (0.00%)</Text>
                  )}
                </View>
                <View style={styles.inCardWrapper}>
                  <View
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Text style={styles.feeLabelStyle}>App</Text>
                    <Info width={13} height={13} />
                  </View>
                  {toData ? (
                    <Text style={styles.feeLabelStyle}>
                      {toData?.appData?.app_fee?.toFixed(4)} (
                      {toData?.appData?.app_fee_percentage?.toFixed(3)}%)
                    </Text>
                  ) : (
                    <Text style={styles.feeLabelStyle}>0.00 (0.00%)</Text>
                  )}
                </View>
                <View style={styles.inCardWrapper}>
                  <View
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Text style={styles.feeLabelStyle}>Banker</Text>
                    <Info width={13} height={13} />
                  </View>
                  {toData ? (
                    <Text style={styles.feeLabelStyle}>
                      {toData?.bankerData?.trade_fee_native?.toFixed(4)} (
                      {toData?.bankerData?.banker_fee_percentage?.toFixed(3)}%)
                    </Text>
                  ) : (
                    <Text style={styles.feeLabelStyle}>0.00 (0.00%)</Text>
                  )}
                </View>
              </View>
            </View>
            <View style={styles.halfButtonWrapper}>
              <TouchableOpacity
                style={styles.actionBtnWhite}
                onPress={(e) => goBack()}>
                <Text style={[styles.actionBtnText, {color: '#5F6163'}]}>
                  Back
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.actionBtn}
                onPress={(e) => handlePlaceTrade()}>
                <Text style={styles.actionBtnText}>Place Trade</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      ) : (
        <CryptoSwitchSearch
          onClose={(e) => setOpenSearch(false)}
          setActiveCrypto={setActiveCrypto}
          walletBalances={coinList}
          fromBuyCrypto={true}
        />
      )}
      {tradeLoading ? (
        <View style={styles.overlay}>
          <Animated.Image
            style={{
              transform: [{scale: spread}],
              width: 100,
              height: 100,
            }}
            resizeMode="contain"
            // source={require('../assets/ccs-header-icon.png')}
            source={{uri: appData?.app_icon}}
          />
        </View>
      ) : null}
    </AppMainLayout>
  );
};

export default BuyCryptoStepOne;

const styles = StyleSheet.create({
  overlay: {
    flex: 1,
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    opacity: 0.8,
    width: '100%',
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    paddingHorizontal: 32,
    // paddingVertical: 25,
    backgroundColor: 'white',
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 35,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 20,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    // marginBottom: 45,
    fontWeight: 'bold',
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  cardlabel: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#5F6163',
    fontSize: 13,
    fontWeight: '600',
    paddingBottom: 15,
    paddingTop: 40,
    paddingLeft: 2,
  },
  cardStyle: {
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    height: 74,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 25,
  },
  cardStyle1: {
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    // height: 74,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    // alignItems: 'center',
    paddingLeft: 25,
    paddingRight: 25,
  },
  cardImage: {
    height: 24,
    width: 24,
  },
  cardText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: '#5F6163',
    fontWeight: '700',
    paddingLeft: 9,
  },
  cardTextSub: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 8,
    color: '#464B4E',
    fontWeight: '300',
    paddingTop: 2,
    paddingLeft: 8,
  },

  //In Card CSS
  inCardWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 30,
    borderBottomWidth: 0.5,
    borderColor: '#E5E5E5',
  },
  inCardWrapper1: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // paddingVertical: 30,
    borderColor: '#E5E5E5',
  },
  inCardCurrency: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 0.5,
    borderRadius: 15,
    borderColor: '#E5E5E5',
    paddingHorizontal: 20,
    height: 32,
  },
  coinImageStyle: {
    width: 13,
    height: 13,
  },
  coinSymbolStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 11,
    fontWeight: '800',
    color: '#5F6163',
    paddingLeft: 4,
    paddingRight: 11,
  },
  feeLabelStyle: {
    paddingRight: 7,
    fontFamily: ThemeData.FONT_NORMAL,
  },
  debitAmountStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 20,
    color: '#5F6163',
    fontWeight: '700',
  },

  //Button CSS

  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    width: '48%',
  },
  actionBtnWhite: {
    backgroundColor: 'white',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    width: '48%',
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  halfButtonWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 40,
  },
});
