/* eslint-disable react-native/no-inline-styles */
import React, {useState, useContext, useEffect} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import ActionBar from '../../components/ActionBar';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import AppMainLayout from '../../layouts/AppMainLayout';
import {useNavigation, useRoute} from '@react-navigation/core';
import {formatterHelper} from '../../utils';
import axios from 'axios';
import {GX_API_ENDPOINT} from '../../configs';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {WToast} from 'react-native-smart-tip';
import LoadingAnimation from '../../components/LoadingAnimation';

const BondIssueScreen = () => {
  const {goBack, navigate} = useNavigation();

  const {
    params: {selectedBond, selectedVault, earningsData},
  } = useRoute();

  const {walletCoinData, getWalletCoinData} = useContext(AppContext);

  const [percentageInput, setPercentageInput] = useState('');
  const [contractCount, setContractCount] = useState('');
  const [activeCoinType, setActiveCoinType] = useState(FUNDING_TABS[0]);
  const [activeList, setActiveList] = useState();
  const [activeFundingCurrency, setActiveFundingCurrency] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getWalletCoinData();
  }, []);

  useEffect(() => {
    if (walletCoinData) {
      if (activeCoinType?.title === 'Crypto') {
        const list = walletCoinData?.filter(
          (x) => x.asset_type !== 'Fiat' && x.coinValue > 0,
        );
        setActiveList(list);
      } else if (activeCoinType?.title === 'Fiat') {
        const list = walletCoinData?.filter(
          (x) => x.asset_type === 'Fiat' && x.coinValue > 0,
        );
        setActiveList(list);
      } else {
        setActiveList([]);
      }
    }
  }, [activeCoinType, walletCoinData]);

  useEffect(() => {
    if (activeFundingCurrency) {
      if (percentageInput) {
        updateValue(percentageInput);
      }
    }
  }, [activeFundingCurrency]);

  // console.log('earningsData', earningsData);
  // console.log('selectedBond', selectedBond);
  // console.log('selectedVault', selectedVault);

  const updatePercentage = (value) => {
    const coinBalance = activeFundingCurrency?.coinValue || 0;

    setPercentageInput(
      parseFloat(
        (
          (parseFloat(value || '0') *
            (earningsData?.contractValueUSd || 0) *
            100) /
          coinBalance
        ).toFixed(2),
      ).toString(),
    );
  };

  const updateValue = (percentage) => {
    const coinBalance = activeFundingCurrency?.coinValueUSD || 0;

    // console.log('coinBalance', coinBalance);
    // console.log('percentage', percentage);
    // console.log(
    //   'earningsData?.contractValueUSd',
    //   earningsData?.contractValueUSd,
    // );

    setContractCount(
      parseFloat(
        (
          (coinBalance * (parseFloat(percentage || '0') / 100)) /
          (earningsData?.contractValueUSd || 0)
        ).toFixed(2),
      ).toString(),
    );
  };

  const issueBond = async () => {
    if (!activeFundingCurrency) {
      return WToast.show({
        data: 'Please Select A Paying Currency',
        position: WToast.position.TOP,
      });
    }

    if (!contractCount || contractCount <= 0) {
      return WToast.show({
        data: 'Please Input Contract Count',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const postData = {
      email,
      token,
      coin: selectedBond?.coin,
      days: earningsData?.days - 1,
      num_of_bonds: contractCount,
      payCoin: activeFundingCurrency?.coinSymbol,
      simulate: true,
    };

    axios
      .post(`${GX_API_ENDPOINT}/coin/iced/contract/create`, postData)
      .then(({data}) => {
        // console.log('Bond Issue', data);

        setIsLoading(false);

        if (data.status) {
          WToast.show({
            data: 'Successfully Created Ice Contract',
            position: WToast.position.TOP,
          });

          navigate('Bonds');
        } else {
          WToast.show({
            data: data.message || 'Error occured on issueing bond',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        console.log('Error on creating bond', error);
        setIsLoading(false);
      });
  };

  return (
    <AppMainLayout>
      <ActionBar />

      <View style={styles.container}>
        {isLoading && (
          <View
            style={{
              ...StyleSheet.absoluteFill,
              justifyContent: 'center',
              backgroundColor: 'white',
              zIndex: 10,
            }}>
            <LoadingAnimation />
          </View>
        )}
        <View style={styles.headerContainer}>
          <Text style={styles.headerText}>Issue Your Bond</Text>
          <TouchableOpacity onPress={issueBond} style={styles.confirmButton}>
            <Text style={styles.confirmText}>Confirm</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={goBack}
          style={styles.selectedOptionsContainer}>
          <View style={styles.selectedCurrency}>
            <Image style={styles.selectedCurrencyIcon} resizeMode="contain" />
            <Text style={styles.selectedCurrencyName}>Bitcoin</Text>
          </View>
          <View style={styles.selectedPeriod}>
            <Text style={styles.selectedDays}>
              {earningsData?.days - 1} Days
            </Text>
          </View>
        </TouchableOpacity>
        <View style={[styles.inputForm]}>
          <Text style={styles.inputTitle}>Exact Amount</Text>
          <View
            style={[
              styles.inputGroup,
              {opacity: activeFundingCurrency ? 1 : 0.4},
            ]}>
            <View style={styles.inputItem}>
              <Text style={styles.inputCoin}>
                {activeFundingCurrency?.coinSymbol || 'Crypto'}
              </Text>
            </View>
            <View style={styles.inputItem}>
              <TextInput
                editable={!!activeFundingCurrency}
                placeholderTextColor={'#9A9A9A'}
                value={percentageInput}
                onChangeText={(text) => {
                  setPercentageInput(text);
                  updateValue(text);
                }}
                style={styles.input}
                placeholder="0%"
              />
            </View>
            <View style={[styles.inputItem, {borderRightWidth: 0}]}>
              <TextInput
                editable={!!activeFundingCurrency}
                placeholderTextColor={'#9A9A9A'}
                value={contractCount}
                onChangeText={(text) => {
                  setContractCount(text);
                  updatePercentage(text);
                }}
                style={styles.input}
                placeholder="0 Bonds"
              />
            </View>
          </View>
        </View>
        <View style={styles.coinListContainer}>
          <View style={styles.coinTypeSwitch}>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={FUNDING_TABS}
              keyExtractor={(item) => item.title}
              renderItem={({item}) => (
                <TouchableOpacity
                  disabled={item.disabled}
                  onPress={() => setActiveCoinType(item)}>
                  <View
                    style={[
                      styles.coinTypeItem,
                      {opacity: item.title === activeCoinType?.title ? 1 : 0.4},
                      item.disabled && {opacity: 0.2},
                    ]}>
                    <Text style={styles.coinTypeTitle}>{item.title}</Text>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
          <FlatList
            data={activeList}
            keyExtractor={(item) => item.coinSymbol}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) => (
              <TouchableOpacity
                style={[styles.listItem]}
                onPress={() => setActiveFundingCurrency(item)}>
                <Image
                  source={{url: item.coinImage}}
                  resizeMode="contain"
                  style={[styles.itemIcon]}
                />
                <Text style={styles.itemName}>{item.coinName}</Text>
                <Text style={styles.itemBalance}>
                  {formatterHelper(item.coinValue, item.coinSymbol)}
                </Text>
              </TouchableOpacity>
            )}
            ListEmptyComponent={
              <Text style={styles.emptyText}>
                Looks Like You Have Any {activeCoinType?.title} Assets
              </Text>
            }
          />
        </View>
      </View>
    </AppMainLayout>
  );
};

export default BondIssueScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
  },
  headerContainer: {
    marginTop: 30,
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    flex: 1,
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 24,
  },
  confirmButton: {
    borderColor: ThemeData.APP_MAIN_COLOR,
    borderWidth: 1,
    paddingVertical: 10,
    paddingHorizontal: 25,
  },
  confirmText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 13,
  },
  selectedOptionsContainer: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginTop: 30,
  },
  selectedCurrencyIcon: {
    width: 30,
    height: 30,
    borderRadius: 15,
  },
  selectedCurrency: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  selectedCurrencyName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 15,
  },
  selectedPeriod: {
    flex: 1,
    borderLeftColor: ThemeData.BORDER_COLOR,
    borderLeftWidth: 1,
    paddingVertical: 20,
  },
  selectedDays: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 15,
  },
  inputForm: {
    marginTop: 30,
  },
  inputTitle: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 16,
  },
  inputGroup: {
    marginTop: 10,
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  inputItem: {
    flex: 1,
    borderRightColor: ThemeData.BORDER_COLOR,
    borderRightWidth: 1,
  },
  inputCoin: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  input: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    height: 45,
  },
  coinListContainer: {
    flex: 1,
  },
  coinTypeSwitch: {
    marginTop: 30,
    marginBottom: 10,
  },
  coinTypeItem: {
    marginHorizontal: 5,
    paddingHorizontal: 10,
  },
  coinTypeTitle: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 15,
  },
  listItem: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingVertical: 15,
    paddingHorizontal: 20,
    marginTop: 15,
  },
  itemIcon: {
    width: 22,
    height: 22,
    borderRadius: 11,
  },
  itemName: {
    flex: 1,
    paddingHorizontal: 10,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  itemBalance: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  emptyText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 16,
    marginTop: 50,
    paddingHorizontal: 20,
  },
});

const FUNDING_TABS = [
  {title: 'Crypto'},
  {title: 'Fiat'},
  {title: 'Funds', disabled: true},
  {title: 'Loan', disabled: true},
  {title: 'Real Estate', disabled: true},
  {title: 'Private Equity', disabled: true},
  {title: 'Digital Properties', disabled: true},
  {title: 'Influence', disabled: true},
];
