import {useNavigation, useRoute} from '@react-navigation/native';
import React, {useContext, useEffect, useRef, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionBar from '../../components/ActionBar';
import BondsList from '../../components/BondsList';
import MarketCover from '../../components/MarketCover';
import MarketListSwitcher from '../../components/MarketListSwitcher';
import {AppContext} from '../../contexts/AppContextProvider';
import AppMainLayout from '../../layouts/AppMainLayout';
import SearchLayout from '../../layouts/SearchLayout';

const MoneyMarketScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const {
    setActiveListCategory,
    setHomeSearchInput,
    homeSearchInput,
    activeListCategory,
  } = useContext(AppContext);

  const [isShowSearch, setIsShowSearch] = useState(false);
  const [searchPlaceHolderText, setSearchPlaceHolderText] = useState('');
  const [searchList, setSearchList] = useState();

  const searchCallback = useRef(() => {});

  useEffect(() => {
    navigation.addListener('focus', onScreenFocus);
    navigation.addListener('blur', onScreeBlur);
    return () => {
      navigation.removeListener('focus', onScreenFocus);
      navigation.removeListener('blur', onScreeBlur);
    };
  }, []);

  const onScreenFocus = (paylod) => {
    if (route.name === 'MoneyMarket') {
      setActiveListCategory(categories[0]);
    } else {
      setActiveListCategory(categories[1]);
    }
  };

  const onScreeBlur = () => {
    setHomeSearchInput('');
    setIsShowSearch(false);
  };

  const setSearchCallback = (callback) => {
    searchCallback.current = callback;
  };

  return (
    <AppMainLayout>
      <ActionBar />
      {/* <Text>{route.name}</Text> */}
      <View style={styles.container}>
        {/* <MarketCover showHeader={isShowSearch} /> */}
        <View style={[styles.transitionContainer]}>
          <MarketListSwitcher
            isShowSearch={isShowSearch}
            openSearch={() => setIsShowSearch(true)}
            openCountry={() => {}}
            list={categories}
          />
        </View>
        {isShowSearch && (
          <View style={styles.searchContainer}>
            <SearchLayout
              onBack={() => setIsShowSearch(false)}
              value={homeSearchInput}
              setValue={setHomeSearchInput}
              onSubmit={(_, item) =>
                searchCallback.current ? searchCallback.current(item) : null
              }
              placeholder={`Search ${activeListCategory?.title}`}
              list={searchList || []}
              showUserList
              setSelectedFilter={setActiveListCategory}
              selectedFilter={activeListCategory}
              dontFilter
              filters={categories}
              keyboardOffset={100}
            />
          </View>
        )}
        <BondsList
          setSearchPlaceHolderText={setSearchPlaceHolderText}
          setSearchList={setSearchList}
          setSearchCallback={setSearchCallback}
        />
      </View>
    </AppMainLayout>
  );
};

export default MoneyMarketScreen;

const styles = StyleSheet.create({
  container: {flex: 1},
  transitionContainer: {
    backgroundColor: 'white',
    paddingTop: 10,
  },
  fragmentContainer: {flex: 1},
  searchContainer: {
    ...StyleSheet.absoluteFill,
    backgroundColor: 'white',
    zIndex: 10,
  },
});

const categories = [
  {
    title: 'MoneyMarkets',
    icon: require('../../assets/liquid-earnings-icon.png'),
  },
  {title: 'Bonds', icon: require('../../assets/bonds-icon.png')},
];
