/* eslint-disable react-native/no-inline-styles */
import React, {useContext} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableWithoutFeedback,
  ScrollView,
  Dimensions,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';

const {width} = Dimensions.get('window');

const CurrencySwitcher = ({
  openAddModal,
  isShowSearch,
  openSearch,
  openCountry,
  isCountryOpen,
  selectedCurrency,
  setSelectedCurrency,
  list = [],
}) => {
  const {activeListCategory, setActiveListCategory} = useContext(AppContext);

  const itemClick = (item) => {
    if (item.title === 'Add') {
      openAddModal();
    } else {
      setSelectedCurrency(item);
    }
  };

  return (
    <View style={[styles.container, isShowSearch && {marginTop: -30}]}>
      {!isShowSearch ? (
        <TouchableWithoutFeedback
          style={styles.itemContainer}
          onPress={openSearch}>
          <View style={styles.item}>
            <View style={[styles.iconContainer]}>
              <Image
                style={styles.icon}
                source={require('../../assets/search-new.png')}
                resizeMode="contain"
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
      ) : (
        <TouchableWithoutFeedback
          style={styles.itemContainer}
          onPress={openCountry}>
          <View style={styles.item}>
            <View
              style={[
                styles.iconContainer,
                isCountryOpen && styles.itemActive,
              ]}>
              <Image
                style={styles.icon}
                source={require('../../assets/country-icon.png')}
                resizeMode="contain"
              />
            </View>
            <Text
              style={[
                styles.title,
                {
                  display: isCountryOpen ? 'flex' : 'none',
                },
              ]}>
              Country
            </Text>
            {isCountryOpen && <View style={styles.bottomArrow} />}
          </View>
        </TouchableWithoutFeedback>
      )}
      <ScrollView
        style={[styles.scrollContainer]}
        horizontal
        showsHorizontalScrollIndicator={false}>
        {list.map((item) => (
          <TouchableWithoutFeedback
            key={item.coin}
            style={styles.itemContainer}
            onPress={() => itemClick(item)}>
            <View style={styles.item}>
              <View style={[styles.iconContainer]}>
                <Image
                  style={[
                    styles.icon,
                    !isCountryOpen &&
                      selectedCurrency?.coin === item.coin &&
                      styles.itemActive,
                  ]}
                  source={{uri: item.icon}}
                  resizeMode="contain"
                />
              </View>
              <Text
                numberOfLines={1}
                adjustsFontSizeToFit
                style={[
                  styles.title,
                  {
                    display:
                      !isCountryOpen && selectedCurrency?.coin === item.coin
                        ? 'flex'
                        : 'none',
                  },
                ]}>
                {item.coin}
              </Text>
            </View>
          </TouchableWithoutFeedback>
        ))}
      </ScrollView>
    </View>
  );
};

export default CurrencySwitcher;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginHorizontal: 10,
  },
  scrollContainer: {
    height: 90,
  },
  itemContainer: {paddingHorizontal: 5},
  item: {
    paddingVertical: 15,
    width: (width - 40) / 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContainer: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 2,
    backgroundColor: 'white',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  itemActive: {
    opacity: 1,
  },
  icon: {height: 22, width: 22, opacity: 0.4},
  title: {
    textAlign: 'center',
    color: '#788995',
    marginTop: 5,
    fontSize: 10,
    fontFamily: 'Montserrat-SemiBold',
  },
  bottomArrow: {
    backgroundColor: '#f7f7f7',
    width: 16,
    height: 16,
    position: 'absolute',
    bottom: -8,
    transform: [{rotate: '45deg'}],
  },
});
