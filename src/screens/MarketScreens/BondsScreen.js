import {useNavigation, useRoute} from '@react-navigation/native';
import axios from 'axios';
import React, {useContext, useEffect, useRef, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionBar from '../../components/ActionBar';
import BondsList from '../../components/BondsList';
import MarketCover from '../../components/MarketCover';
import MarketListSwitcher from '../../components/MarketListSwitcher';
import {AppContext} from '../../contexts/AppContextProvider';
import AppMainLayout from '../../layouts/AppMainLayout';
import SearchLayout from '../../layouts/SearchLayout';
import BondList from './BondList';
import CurrencySwitcher from './CurrencySwitcher';
import {useAppData} from '../../utils/CustomHook';
import SkeletonItemLoading from '../../components/MarketList/SkeletonItemLoading';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';

const BondsScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const {
    setActiveListCategory,
    setHomeSearchInput,
    homeSearchInput,
    activeListCategory,
  } = useContext(AppContext);

  const {data: appData} = useAppData();

  const [isShowSearch, setIsShowSearch] = useState(false);
  const [searchPlaceHolderText, setSearchPlaceHolderText] = useState('');
  const [searchList, setSearchList] = useState();

  const [currenciesData, setCurrenciesData] = useState([]);
  const [selectedCurrency, setSelectedCurrency] = useState(null);
  const [allBonds, setAllBonds] = useState([]);

  const searchCallback = useRef(() => {});

  useEffect(() => {
    navigation.addListener('focus', onScreenFocus);
    navigation.addListener('blur', onScreeBlur);
    return () => {
      navigation.removeListener('focus', onScreenFocus);
      navigation.removeListener('blur', onScreeBlur);
    };
  }, []);

  const onScreenFocus = (paylod) => {
    setActiveListCategory(categories[1]);
  };

  useEffect(() => {
    axios
      .get(
        `https://comms.globalxchange.com/coin/iced/banker/custom/bond/get?status=active&email=${appData.created_by}`,
      )
      .then(({data}) => {
        const tempArr = Object.values(data.currenciesData);
        setCurrenciesData([...tempArr]);
        setSelectedCurrency(tempArr[0]);
        getallBonds();
      });
  }, []);

  const getallBonds = async () => {
    const userEmail = await AsyncStorageHelper.getLoginEmail();

    const res = await axios.get(
      `https://comms.globalxchange.com/coin/iced/banker/custom/bond/list/data/get?user_email=${userEmail}&email=${appData.created_by}&coin=${selectedCurrency.coin}`,
    );

    setAllBonds([...res.data.bondsListData]);
  };

  useEffect(() => {
    setAllBonds([]);
    getallBonds();
  }, [selectedCurrency]);

  const onScreeBlur = () => {
    setHomeSearchInput('');
    setIsShowSearch(false);
  };

  const setSearchCallback = (callback) => {
    searchCallback.current = callback;
  };

  return (
    <AppMainLayout>
      <ActionBar />
      <View style={styles.container}>
        {/* <MarketCover showHeader={isShowSearch} /> */}
        <View style={[styles.transitionContainer]}>
          <CurrencySwitcher
            isShowSearch={isShowSearch}
            openSearch={() => setIsShowSearch(true)}
            openCountry={() => {}}
            list={currenciesData}
            selectedCurrency={selectedCurrency}
            setSelectedCurrency={setSelectedCurrency}
          />
        </View>
        {isShowSearch && (
          <View style={styles.searchContainer}>
            <SearchLayout
              onBack={() => setIsShowSearch(false)}
              value={homeSearchInput}
              setValue={setHomeSearchInput}
              onSubmit={(_, item) => {
                setSelectedCurrency(item);
                setIsShowSearch(false);
              }}
              placeholder={`Search ${activeListCategory?.title}`}
              list={currenciesData || []}
              showUserList
              setSelectedFilter={setActiveListCategory}
              selectedFilter={activeListCategory}
              dontFilter
              filters={categories}
              keyboardOffset={100}
              setSelectedCurrency={setSelectedCurrency}
            />
          </View>
        )}
        {allBonds.length > 0 ? (
          <BondList allBonds={allBonds} appData={appData} />
        ) : (
          <SkeletonItemLoading />
        )}
        {/* <BondsList
          setSearchPlaceHolderText={setSearchPlaceHolderText}
          setSearchList={setSearchList}
          setSearchCallback={setSearchCallback}
        /> */}
      </View>
    </AppMainLayout>
  );
};

export default BondsScreen;

const styles = StyleSheet.create({
  container: {flex: 1},
  transitionContainer: {
    backgroundColor: 'white',
    paddingTop: 10,
  },
  fragmentContainer: {flex: 1},
  searchContainer: {
    ...StyleSheet.absoluteFill,
    backgroundColor: 'white',
    zIndex: 10,
  },
});

const categories = [
  {
    title: 'MoneyMarkets',
    icon: require('../../assets/liquid-earnings-icon.png'),
  },
  {title: 'Bonds', icon: require('../../assets/bonds-icon.png')},
];
