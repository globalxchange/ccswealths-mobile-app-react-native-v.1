import {useNavigation} from '@react-navigation/native';
import React, {useContext, useEffect, useState, useRef} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Animated,
  Linking,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import CheckBox from '@react-native-community/checkbox';
import ThemeData from '../../../configs/ThemeData';
import AppMainLayout from '../../../layouts/AppMainLayout';
import axios from 'axios';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import {AppContext} from '../../../contexts/AppContextProvider';

const StepThree = ({route}) => {
  const spread = useRef(new Animated.Value(1)).current;
  const {selectedBond} = useContext(AppContext);
  const {bankerdata, bondCount, appData, payCoin} = route.params;
  const {navigate, goBack} = useNavigation();

  const [check1, setCheck1] = useState(false);
  const [check2, setCheck2] = useState(false);

  const [iceObject, setIceObject] = useState(null);
  const [appObject, setAppObject] = useState(null);

  const [loading, setLoading] = useState(false);

  const getProfileIds = async () => {
    const userEmail = await AsyncStorageHelper.getLoginEmail();
    axios
      .get(
        `https://comms.globalxchange.com/gxb/apps/registered/user?email=${userEmail}`,
      )
      .then(({data}) => {
        if (data.status) {
          setIceObject(data.userApps.find((o) => o.app_code === 'ice'));
          setAppObject(
            data.userApps.find((o) => o.app_code === appData.app_code),
          );
          // setAppObject(data.userApps.find((o) => o.app_code === 'EuropeanOTC'));
        }
      });
  };

  useEffect(() => {
    console.log(appData.app_icon, 'ljwebljbfkjrbfjkbere3rv');
  }, [appData]);

  useEffect(() => {
    if (appObject !== null && iceObject != null) {
      buyBondFunction();
    }
  }, [appObject, iceObject, appData]);

  const buyBondFunction = async () => {
    setLoading(true);
    const userEmail = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    axios
      .post(`https://comms.globalxchange.com/coin/iced/contract/create`, {
        email: userEmail,
        token: token,
        origin_app_code: appObject.app_code,
        profile_id: iceObject.profile_id,
        coin: selectedBond.coinsData[0].coin,
        days: selectedBond.days,
        num_of_bonds: bondCount,
        simulate: false,
        payCoin: payCoin.coinSymbol,
        stats: false,
        bond_type: 'custom',
        bond_template_id: selectedBond.bond_template_id,
        debitfromApp: appObject.app_code,
        debitfromProfileId: appObject.profile_id,
      })
      .then(({data}) => {
        console.log(data);
        if (data.status) {
          navigate('StepFour', {
            bankerdata: bankerdata,
            bondCount: bondCount,
            appData: appData,
            payCoin: payCoin,
          });
          setLoading(false);
        } else {
          setLoading(false);
          // navigate('StepFour', {
          //   bankerdata: bankerdata,
          //   appData: appData,
          //   bondCount: bondCount,
          //   payCoin: payCoin,
          // });

          WToast.show({data: data.message, position: WToast.position.TOP});
        }
      });
  };

  const handleBuy = () => {
    getProfileIds();
    // buyBondFunction();
    // navigate('StepFour');
  };

  useEffect(() => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(spread, {
          toValue: 1,
          duration: 0,
          useNativeDriver: true,
          types: 'linear',
        }),
        Animated.timing(spread, {
          toValue: 1.2,
          duration: 1000,
          useNativeDriver: true,
          types: 'linear',
        }),
        Animated.timing(spread, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
          types: 'linear',
        }),
      ]),
    ).start();
  }, []);

  return (
    <AppMainLayout>
      <View>
        <View
          style={[
            styles.topBackground,
            {
              backgroundColor: `#${appData.color_codes[0].primarycolourcode}`,
            },
          ]}></View>
        <View style={styles.logoContainer}>
          <View style={styles.logo}>
            <Image
              source={{uri: bankerdata.profilePicURL}}
              style={{width: 62, height: 62}}
            />
          </View>
        </View>
        <View style={styles.nameContainer}>
          <Text style={styles.bondName}>{bankerdata.displayName} Bond</Text>
          <View style={styles.crumbStyle}>
            <Text style={{color: '#5F6163'}} onPress={(e) => goBack()}>
              Payment Assets
              {` -> `}
            </Text>
            <View style={styles.activeCrumb}>
              <Text style={{color: '#5F6163', fontWeight: '700'}}>Terms</Text>
            </View>
          </View>
        </View>
        <ScrollView bounces={false}>
          <View style={{paddingHorizontal: 31, paddingTop: 30}}>
            <Text style={styles.bondName}>Step 3: Terms Of Bond</Text>
            <View>
              <View style={styles.confirmation}>
                <CheckBox
                  style={{transform: [{scaleX: 0.8}, {scaleY: 0.8}]}}
                  value={check1}
                  onValueChange={(newValue) => setCheck1(!check1)}
                  boxType="square"
                />
                <Text style={styles.confirmationText}>
                  I Understand That This Bond Purchase Cannot Be Reversed And I
                  Am Selecting A Bond Which Coincides With My Investment
                  Requirements.
                </Text>
              </View>
              <View style={[styles.confirmation, {marginTop: 20}]}>
                <CheckBox
                  style={{transform: [{scaleX: 0.8}, {scaleY: 0.8}]}}
                  value={check2}
                  onValueChange={(newValue) => setCheck2(!check2)}
                  boxType="square"
                />
                <Text style={[styles.confirmationText, {marginTop: 0}]}>
                  I Have Read & Agreed To The{' '}
                  <Text
                    style={{fontFamily: ThemeData.FONT_BOLD}}
                    onPress={(e) =>
                      Linking.openURL('https://bond.markets/termsandconditions')
                    }>
                    Terms And Conditions
                  </Text>
                  {/* <Text style={styles.addressText}>{externalAddress}</Text> */}
                </Text>
              </View>
            </View>
            {check1 && check2 ? (
              <TouchableOpacity
                style={[
                  styles.action,
                  {
                    marginTop: 79,
                    backgroundColor: 'rgba(4, 175, 118, 1)',
                  },
                ]}
                onPress={handleBuy}>
                <Text style={styles.actionText}>Buy {bondCount} Bonds</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                style={[
                  styles.action,
                  {
                    marginTop: 79,
                    backgroundColor: 'rgba(4, 175, 118, 0.3)',
                  },
                ]}
                onPress={(e) => {
                  navigate('StepFour');
                }}>
                <Text style={styles.actionText}>Buy {bondCount} Bonds</Text>
              </TouchableOpacity>
            )}
          </View>
        </ScrollView>
      </View>
      {loading ? (
        <View style={styles.overlay}>
          <Animated.Image
            style={{transform: [{scale: spread}], width: 100, height: 100}}
            resizeMode="contain"
            // source={require('../assets/ccs-header-icon.png')}
            source={{uri: appData?.app_icon}}
          />
        </View>
      ) : null}
    </AppMainLayout>
  );
};

export default StepThree;

const styles = StyleSheet.create({
  overlay: {
    flex: 1,
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    opacity: 0.8,
    width: '100%',
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  topBackground: {
    backgroundColor: '#4CAF50',
    height: 114,
  },
  logoContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -45,
  },
  logo: {
    width: 97,
    height: 97,
    borderRadius: 10,
    backgroundColor: 'white',
    // border: 0.5px solid #E5E5E5;
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  nameContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bondName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 22,
    color: '#5F6163',
    paddingTop: 16,
  },
  crumbs: {
    paddingTop: 10,
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#5F6163',
    fontSize: 13,
  },
  inputContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  divider: {
    backgroundColor: '#E5E5E5',
    height: 1,
    marginVertical: 18,
  },
  action: {
    height: 63,
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#E5E5E5',
  },

  actionText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_BOLD,
    color: 'white',
    fontSize: 17,
  },
  confirmation: {
    flexDirection: 'row',
    marginTop: 40,
  },
  confirmationText: {
    marginLeft: 10,
    paddingRight: 20,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 11,
    color: '#5F6163',
    lineHeight: 25,
    marginTop: -4,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 20,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    // marginBottom: 45,
    fontWeight: 'bold',
  },
});
