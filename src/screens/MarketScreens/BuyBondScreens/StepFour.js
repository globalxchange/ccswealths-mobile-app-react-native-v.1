import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {APP_CODE} from '../../../configs';
import ThemeData from '../../../configs/ThemeData';
import {AppContext} from '../../../contexts/AppContextProvider';
import AppMainLayout from '../../../layouts/AppMainLayout';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';

const StepFour = ({route}) => {
  const {bankerdata, appData, bondCount, payCoin} = route.params;
  const {navigate, goBack} = useNavigation();
  const {selectedBond} = useContext(AppContext);

  const [updatedBalance, setUpdatedBalance] = useState(null);

  const getUpdatedBalance = async () => {
    const userEmail = await AsyncStorageHelper.getLoginEmail();
    axios
      .post(`https://comms.globalxchange.com/coin/vault/service/coins/get`, {
        app_code: APP_CODE,
        email: userEmail,
        include_coins: [`${payCoin.coinSymbol}`],
      })
      .then(({data}) => {
        console.log(data, 'jqdjkbwekdbwekdw');
        setUpdatedBalance(data.coins_data[0].coinValue);
      });
  };

  useEffect(() => {
    getUpdatedBalance();
  }, []);

  return (
    <AppMainLayout>
      <View
        style={[
          styles.topBackground,
          {backgroundColor: `#${appData.color_codes[0].primarycolourcode}`},
        ]}></View>
      <View style={styles.logoContainer}>
        <View style={styles.logo}>
          <Image
            source={{uri: bankerdata.profilePicURL}}
            style={{width: 62, height: 62}}
          />
        </View>
      </View>
      <View style={styles.nameContainer}>
        <Text style={styles.bondName}>{bankerdata.displayName} Bond</Text>
        <Text style={styles.crumbs}>Terms {`->`} Success</Text>
      </View>
      <ScrollView bounces={false}>
        <View style={{paddingHorizontal: 31, paddingTop: 30}}>
          <Text style={styles.bondName}>Congratulations</Text>
          <Text
            style={[
              styles.crumbs,
              {lineHeight: 25, fontSize: 12, paddingTop: 15},
            ]}>
            You Have Successfully Purchased {bondCount}{' '}
            {selectedBond.coinsData[0].coin} Bonds
          </Text>

          {/* Updated Balance */}
          <View>
            <Text style={styles.cardlabel}>
              Updated {payCoin.coinSymbol} Balance
            </Text>
            <View style={styles.cardStyle}>
              <Text style={styles.cardText}>
                {updatedBalance ? updatedBalance : <Text>Loading ...</Text>}
              </Text>
              <Image
                source={{uri: payCoin.coinImage}}
                style={styles.cardImage}
              />
            </View>
          </View>

          <View style={{marginTop: 100, marginBottom: 40}}>
            <TouchableOpacity
              style={[
                styles.action,
                {marginBottom: 20, marginTop: -20, backgroundColor: '#5F6163'},
              ]}
              onPress={(e) => {
                // goBack();
                navigate('Vaults', {
                  type: 'bonds',
                });
              }}>
              <Text style={[styles.actionText, {color: 'white'}]}>
                See Bond Hash
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.action}
              onPress={(e) => {
                // goBack();
                navigate('Bonds');
              }}>
              <Text style={styles.actionText}>Back To Market</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </AppMainLayout>
  );
};

export default StepFour;

const styles = StyleSheet.create({
  topBackground: {
    backgroundColor: '#4CAF50',
    height: 114,
  },
  logoContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -45,
  },
  logo: {
    width: 97,
    height: 97,
    borderRadius: 10,
    backgroundColor: 'white',
    // border: 0.5px solid #E5E5E5;
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  nameContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bondName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 22,
    color: '#5F6163',
    paddingTop: 16,
  },
  crumbs: {
    paddingTop: 10,
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#5F6163',
    fontSize: 13,
  },
  inputContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  divider: {
    backgroundColor: '#E5E5E5',
    height: 1,
    marginVertical: 18,
  },
  action: {
    height: 63,
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#E5E5E5',
  },
  actionFilled: {
    backgroundColor: '#5F6163',
    borderRadius: 15,
    height: 63,
    marginTop: 20,
  },
  actionText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 17,
  },
  cardlabel: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#5F6163',
    fontSize: 13,
    fontWeight: '600',
    paddingBottom: 15,
    paddingTop: 40,
    paddingLeft: 2,
  },
  cardStyle: {
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    height: 74,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  cardStyle1: {
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    borderRadius: 15,
    // height: 74,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    // alignItems: 'center',
    paddingLeft: 25,
    paddingRight: 25,
  },
  cardImage: {
    height: 24,
    width: 24,
  },
  cardText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
    color: '#5F6163',
    fontWeight: '700',
    paddingLeft: 9,
  },
});
