import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import ThemeData from '../../../configs/ThemeData';
import AppMainLayout from '../../../layouts/AppMainLayout';
import SkeltonItem from '../../../components/SkeltonItem';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import {APP_CODE} from '../../../configs';

const StepTwo = ({route}) => {
  const {bankerdata, bondCount, appData} = route.params;
  const {navigate, goBack} = useNavigation();
  const [query, setQuery] = useState('');
  const [allVaults, setAllVaults] = useState([]);
  const [filteredVaults, setFilteredVaults] = useState([]);

  useEffect(() => {
    const tempArr = allVaults.filter(
      (item) =>
        item?.coinSymbol?.toLowerCase()?.includes(query.toLowerCase()) ||
        item?.coinName?.toLowerCase()?.includes(query.toLowerCase()),
    );

    setFilteredVaults([...tempArr]);
  }, [query]);

  const getAllVaults = async () => {
    const userEmail = await AsyncStorageHelper.getLoginEmail();
    axios
      .post(`https://comms.globalxchange.com/gxb/apps/register/user`, {
        email: userEmail,
        app_code: 'ice',
      })
      .then((res) => {
        console.log(res.data);
      });
    axios
      .post(`https://comms.globalxchange.com/coin/vault/service/coins/get`, {
        app_code: APP_CODE,
        email: userEmail,
        with_balances: 'true',
        orderby_dsc: true,
      })
      .then(({data}) => {
        setAllVaults(data.coins_data);
        setFilteredVaults(data.coins_data);
      });
  };

  useEffect(() => {
    getAllVaults();
  }, []);

  return (
    <AppMainLayout>
      <View
        style={[
          styles.topBackground,
          {backgroundColor: `#${appData.color_codes[0].primarycolourcode}`},
        ]}></View>
      <View style={styles.logoContainer}>
        <View style={styles.logo}>
          <Image
            source={{uri: bankerdata.profilePicURL}}
            style={{width: 62, height: 62}}
          />
        </View>
      </View>
      <View style={styles.nameContainer}>
        <Text style={styles.bondName}>{bankerdata.displayName} Bond</Text>
        <View style={styles.crumbStyle}>
          <Text style={{color: '#5F6163'}} onPress={(e) => goBack()}>
            Amount
            {` -> `}
          </Text>
          <View style={styles.activeCrumb}>
            <Text style={{color: '#5F6163', fontWeight: '700'}}>
              Payment Assets
            </Text>
          </View>
        </View>
      </View>
      <View style={{paddingHorizontal: 30, marginTop: 30}}>
        <Text style={styles.bondName}>Step 2: Amount</Text>
        <View style={styles.inputWrapper}>
          <TextInput
            placeholder="Select The Vault That You Want To Pay With"
            value={query}
            onChangeText={(text) => setQuery(text)}
          />
        </View>
      </View>
      <View
        style={{
          paddingHorizontal: 30,
          paddingTop: 31,
        }}>
        <ScrollView bounces={false}>
          <View style={styles.vaultsWrapper}>
            {filteredVaults.length > 0 ? (
              filteredVaults.map((item) => {
                return (
                  <TouchableOpacity
                    onPress={(e) =>
                      navigate('StepThree', {
                        bankerdata: bankerdata,
                        bondCount: bondCount,
                        appData: appData,
                        payCoin: item,
                      })
                    }>
                    <View style={styles.vault}>
                      <Image
                        source={{uri: item.coinImage}}
                        style={{width: 45, height: 45}}
                      />
                      <Text style={styles.vaultName}>{item.coinSymbol}</Text>
                      <Text style={styles.vaultValue}>
                        {item.coinValue.toFixed(6)}
                      </Text>
                    </View>
                  </TouchableOpacity>
                );
              })
            ) : (
              <View style={styles.vaultsWrapper}>
                {Array(6)
                  .fill(0)
                  .map((_, index) => (
                    <View style={[styles.vault, {height: 110}]}>
                      <SkeltonItem
                        itemHeight={45}
                        itemWidth={45}
                        style={styles.cryptoIcon}
                      />
                      <SkeltonItem
                        itemHeight={5}
                        itemWidth={50}
                        style={{marginTop: 10}}
                      />
                      <SkeltonItem
                        itemHeight={5}
                        itemWidth={70}
                        style={{marginTop: 5}}
                      />
                    </View>
                    // <View key={index} style={styles.item}>

                    //   <View style={styles.row}>
                    //     <SkeltonItem
                    //       itemHeight={25}
                    //       itemWidth={25}
                    //       style={styles.cryptoIcon}
                    //     />
                    //     <View style={styles.nameContainer}>
                    //       <SkeltonItem
                    //         itemHeight={10}
                    //         itemWidth={80}
                    //         style={styles.cryptoName}
                    //       />
                    //     </View>
                    //     <SkeltonItem
                    //       itemHeight={10}
                    //       itemWidth={60}
                    //       style={styles.cryptoPrice}
                    //     />
                    //   </View>
                    //   <View style={styles.row}>
                    //     <SkeltonItem
                    //       itemHeight={10}
                    //       itemWidth={60}
                    //       style={styles.cryptoPrice}
                    //     />
                    //     <SkeltonItem
                    //       itemHeight={10}
                    //       itemWidth={60}
                    //       style={styles.cryptoPrice}
                    //     />
                    //   </View>
                    //   <View style={[styles.row, {marginBottom: 0}]}>
                    //     <SkeltonItem
                    //       itemHeight={10}
                    //       itemWidth={60}
                    //       style={styles.cryptoPrice}
                    //     />
                    //     <SkeltonItem
                    //       itemHeight={10}
                    //       itemWidth={60}
                    //       style={styles.cryptoPrice}
                    //     />
                    //   </View>
                    // </View>
                  ))}
              </View>
            )}
            <View style={[styles.vault, {borderWidth: 0}]}></View>
          </View>
        </ScrollView>
      </View>
    </AppMainLayout>
  );
};

export default StepTwo;

const styles = StyleSheet.create({
  topBackground: {
    backgroundColor: '#4CAF50',
    height: 114,
  },
  logoContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
    marginTop: -45,
  },
  logo: {
    width: 97,
    height: 97,
    borderRadius: 10,
    backgroundColor: 'white',
    // border: 0.5px solid #E5E5E5;
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },

  vaultsWrapper: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  vault: {
    paddingVertical: 14,
    width: 103,
    borderRadius: 10,
    backgroundColor: 'white',
    // border: 0.5px solid #E5E5E5;
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  nameContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bondName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 22,
    color: '#5F6163',
    paddingTop: 16,
  },
  crumbs: {
    paddingTop: 10,
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#5F6163',
    fontSize: 13,
  },
  inputContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  divider: {
    backgroundColor: '#E5E5E5',
    height: 1,
    marginVertical: 18,
  },
  action: {
    height: 63,
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#E5E5E5',
  },
  actionFilled: {
    backgroundColor: '#5F6163',
    borderRadius: 15,
    height: 63,
    marginTop: 20,
  },
  actionText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_BOLD,
    color: 'white',
    fontSize: 17,
  },
  vaultName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 12,
    paddingTop: 15,
    color: '#5F6163',
  },
  vaultValue: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 9,
    paddingTop: 5,
    color: '#5F6163',
  },
  inputWrapper: {
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    height: 56,
    display: 'flex',
    justifyContent: 'center',
    padding: 21,
    borderRadius: 10,
    marginTop: 20,
  },
  cryptoIcon: {
    width: 45,
    height: 45,
    borderRadius: 50,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 20,
    // paddingBottom: 10,
    color: '#5F6163',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    fontWeight: 'bold',
  },
});
