import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import ThemeData from '../../../configs/ThemeData';
import AppMainLayout from '../../../layouts/AppMainLayout';

const StepOne = ({route}) => {
  const {bankerdata, coinsData, appData} = route.params;
  const {navigate} = useNavigation();
  const [bondCount, setBondCount] = useState(0);
  const [amount, setAmount] = useState(0);

  useEffect(() => {
    console.log();
    if (bondCount > 0) {
      const tempAmount = Number(bondCount) * Number(coinsData.bondCost);
      setAmount(tempAmount);
    }
  }, [bondCount]);

  return (
    <AppMainLayout>
      <View
        style={[
          styles.topBackground,
          {backgroundColor: `#${appData.color_codes[0].primarycolourcode}`},
        ]}></View>
      <View style={styles.logoContainer}>
        <View style={styles.logo}>
          <Image
            source={{uri: bankerdata.profilePicURL}}
            style={{width: 62, height: 62}}
          />
        </View>
      </View>
      <View style={styles.nameContainer}>
        <Text style={styles.bondName}>{bankerdata.displayName} Bond</Text>
        <Text style={styles.crumbs}>Amount</Text>
      </View>
      <ScrollView bounces={false}>
        <View style={{paddingHorizontal: 31, paddingTop: 30}}>
          <Text style={styles.bondName}>Step 1: Amount</Text>
          <Text
            style={[
              styles.crumbs,
              {lineHeight: 25, fontSize: 12, paddingTop: 15},
            ]}>
            How Many Bonds Do You Want To Buy?
          </Text>

          <View style={{paddingVertical: 50}}>
            <View style={styles.inputContainer}>
              <TextInput
                style={[styles.amountStyle, {width: '80%'}]}
                keyboardType="numeric"
                // returnKeyType="done"
                placeholder="0"
                autoFocus={true}
                value={bondCount}
                onChangeText={(text) => setBondCount(text)}
              />
              <Text style={styles.amountStyle}>Bonds</Text>
            </View>
            <View style={styles.divider} />
            <View style={styles.inputContainer}>
              <Text style={[styles.amountStyle, {fontSize: 16}]}>{amount}</Text>
              <Text style={[styles.amountStyle, {fontSize: 16}]}>
                {coinsData.coin}
              </Text>
            </View>
          </View>
          <TouchableOpacity
            style={[
              styles.action,
              {backgroundColor: `#${appData.color_codes[0].primarycolourcode}`},
            ]}
            onPress={(e) => {
              navigate('StepTwo', {
                bankerdata: bankerdata,
                bondCount: bondCount,
                appData: appData,
              });
            }}>
            <Text style={styles.actionText}>Next</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </AppMainLayout>
  );
};

export default StepOne;

const styles = StyleSheet.create({
  topBackground: {
    height: 114,
  },
  logoContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -45,
  },
  logo: {
    width: 97,
    height: 97,
    borderRadius: 10,
    backgroundColor: 'white',
    // border: 0.5px solid #E5E5E5;
    borderWidth: 0.5,
    borderColor: '#E5E5E5',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  nameContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bondName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 22,
    color: '#5F6163',
    paddingTop: 16,
  },
  crumbs: {
    paddingTop: 10,
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#5F6163',
    fontSize: 13,
  },
  inputContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  divider: {
    backgroundColor: '#E5E5E5',
    height: 1,
    marginVertical: 18,
  },
  action: {
    height: 63,
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    // borderWidth: 1,
    // borderColor: '#E5E5E5',
  },
  actionFilled: {
    backgroundColor: '#5F6163',
    borderRadius: 15,
    height: 63,
    marginTop: 20,
  },
  actionText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_BOLD,
    color: 'white',
    fontSize: 17,
  },
  amountStyle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 20,
    color: '#5F6163',
  },
});
