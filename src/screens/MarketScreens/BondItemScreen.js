import {useRoute} from '@react-navigation/native';
import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionBar from '../../components/ActionBar';
import BondsSlider from '../../components/BondsSlider';
import VaultSelector from '../../components/VaultSelector';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import AppMainLayout from '../../layouts/AppMainLayout';
import {formatterHelper, getAssetData} from '../../utils';

const BondItemScreen = () => {
  const {walletCoinData} = useContext(AppContext);

  const [selectedVault, setSelectedVault] = useState();

  const {params} = useRoute();

  useEffect(() => {
    if (walletCoinData) {
      const coin = getAssetData(
        params?.selectedBond?.coin || '',
        walletCoinData,
      );

      // console.log('params', params.selectedBond);
      setSelectedVault(coin);
    }
  }, [params, walletCoinData]);

  return (
    <AppMainLayout>
      <ActionBar />
      <View style={styles.container}>
        <Text style={styles.header}>Configure Time</Text>
        <Text style={styles.subHeader}>
          You Are Able To Set Any Length Of Time Of For Your Bond. The Length Of
          TIme You Select Dictactes The Daily Interest Rate & Resale Value Of
          The Bond.
        </Text>
        <VaultSelector
          onItemSelect={setSelectedVault}
          selectedItem={selectedVault}
          customView={
            <View style={styles.buttonContainer}>
              <Text style={styles.buttonText}>
                {selectedVault?.coinName} Bond -{' '}
                {formatterHelper(
                  selectedVault?.coinValue || 0,
                  selectedVault?.coinSymbol,
                )}{' '}
                {selectedVault?.coinSymbol}
              </Text>
            </View>
          }
        />
        <BondsSlider selectedVault={selectedVault} />
      </View>
    </AppMainLayout>
  );
};

export default BondItemScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 30,
  },
  header: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 25,
    marginBottom: 10,
  },
  subHeader: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
    lineHeight: 18,
  },
  buttonContainer: {
    borderColor: ThemeData.APP_MAIN_COLOR,
    borderWidth: 1,
    borderRadius: 4,
    height: 35,
    marginRight: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    marginTop: 20,
  },
  buttonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
    textAlign: 'center',
  },
});
