import {useNavigation, useRoute} from '@react-navigation/native';
import React, {useContext, useEffect, useRef, useState} from 'react';
import {BackHandler, StyleSheet, View, Text} from 'react-native';
import {Transition, Transitioning} from 'react-native-reanimated';
import ActionBar from '../../components/ActionBar';
import MarketCover from '../../components/MarketCover';
import MarketList from '../../components/MarketList';
import MarketListSwitcher from '../../components/MarketListSwitcher';
import {listCategories} from '../../configs';
import {AppContext} from '../../contexts/AppContextProvider';
import AppMainLayout from '../../layouts/AppMainLayout';
import SearchLayout from '../../layouts/SearchLayout';

const LiquidScreen = () => {
  const {
    isCountrySelectorActive,
    setActiveRoute,
    setActiveListCategory,
    setHomeSearchInput,
    homeSearchInput,
    activeListCategory,
  } = useContext(AppContext);

  const [searchPlaceHolderText, setSearchPlaceHolderText] = useState('');
  const [isShowSearch, setIsShowSearch] = useState(false);
  const [searchList, setSearchList] = useState();

  // const [loading, setLoading] = useState(true);

  const navigation = useNavigation();

  const route = useRoute();
  const searchCallback = useRef(() => {});
  const transitionViewRef = useRef();

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);

    navigation.addListener('focus', onScreenFocus);
    navigation.addListener('blur', onScreeBlur);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);

      navigation.removeListener('focus', onScreenFocus);
      navigation.removeListener('blur', onScreeBlur);
    };
  }, []);

  const transition = (
    <Transition.Together>
      <Transition.In
        type="slide-top"
        durationMs={200}
        interpolation="easeInOut"
      />
    </Transition.Together>
  );

  const handleBack = async () => {
    if (route.name === 'Liquid') {
      BackHandler.exitApp();
    }
    return true;
  };

  const onScreenFocus = (paylod) => {
    // setLoading(true);
    setActiveRoute('Markets');
    if (route.name === 'Forex') {
      setActiveListCategory(listCategories[1]);
      // setLoading(false);
    } else {
      setActiveListCategory(listCategories[0]);
      // setLoading(false);
    }
  };

  const onScreeBlur = () => {
    setHomeSearchInput('');
    setIsShowSearch(false);
  };

  const setSearchCallback = (callback) => {
    searchCallback.current = callback;
  };

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <View style={[styles.containerNew]}>
        {/* <MarketCover showHeader={isShowSearch} /> */}
        <Transitioning.View
          style={[styles.transitionContainer]}
          ref={transitionViewRef}
          transition={transition}>
          <MarketListSwitcher
            isShowSearch={isShowSearch}
            openSearch={() => setIsShowSearch(true)}
            openCountry={() => {}}
            isCountryOpen={isCountrySelectorActive}
            list={listCategories}
          />
        </Transitioning.View>
        {isShowSearch && (
          <View style={styles.searchContainer}>
            <SearchLayout
              onBack={() => setIsShowSearch(false)}
              value={homeSearchInput}
              setValue={setHomeSearchInput}
              onSubmit={(_, item) =>
                searchCallback.current ? searchCallback.current(item) : null
              }
              placeholder={
                isCountrySelectorActive
                  ? searchPlaceHolderText
                  : `${searchPlaceHolderText}`
              }
              list={searchList || []}
              showUserList
              setSelectedFilter={setActiveListCategory}
              selectedFilter={activeListCategory}
              dontFilter
              filters={listCategories}
              keyboardOffset={100}
            />
          </View>
        )}

        <MarketList
          transitionViewRef={transitionViewRef.current}
          setSearchPlaceHolderText={setSearchPlaceHolderText}
          setSearchList={setSearchList}
          setSearchCallback={setSearchCallback}
        />
      </View>
    </AppMainLayout>
  );
};

export default LiquidScreen;

const styles = StyleSheet.create({
  containerNew: {flex: 1},
  transitionContainer: {
    backgroundColor: 'white',
    paddingTop: 10,
  },
  fragmentContainer: {flex: 1},
  searchContainer: {
    ...StyleSheet.absoluteFill,
    backgroundColor: 'white',
    zIndex: 10,
  },
});
