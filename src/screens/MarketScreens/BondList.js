import {useNavigation} from '@react-navigation/native';
import React, {useContext} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import FilterTabs from '../../components/MarketList/FilterTabs';

import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';

const BondList = ({allBonds, appData}) => {
  const {setSelectedBond} = useContext(AppContext);
  const {navigate} = useNavigation();
  return (
    <>
      <FilterTabs />
      <ScrollView showsVerticalScrollIndicator={false}>
        {allBonds?.map((item) => {
          return (
            <View style={{borderBottomWidth: 1, borderColor: '#EBEBEB'}}>
              <View style={styles.indSectionContainer}>
                <View style={styles.indHeaderSection}>
                  <View>
                    <Text style={styles.headerTitle}>
                      {item.months.toFixed(0)} Months
                    </Text>
                    <Text style={styles.subtitle}>{item.days} Days</Text>
                  </View>
                  <View style={styles.pullRight}>
                    <Text style={styles.headerTitle}>
                      {item.coinsData[0].bondCost}
                    </Text>
                    <Text style={styles.subtitle}>
                      ${item.coinsData[0].bondCost_usd.toFixed(2)}
                    </Text>
                  </View>
                </View>
                <View style={styles.indSection}>
                  <View>
                    <Text style={styles.title}>Daily Earnings</Text>
                    <Text style={styles.subtitle}>
                      {item.coinsData[0].perDayEarnings.toFixed(5)}
                    </Text>
                  </View>
                  <View style={styles.pullRight}>
                    <Text style={styles.title}>Daily Rate</Text>
                    <Text style={styles.subtitle}>
                      {item.daily_interest_rate.toFixed(5)}%
                    </Text>
                  </View>
                </View>
                <View style={styles.indSection}>
                  <View>
                    <Text style={styles.title}>Monthly Earnings</Text>
                    <Text style={styles.subtitle}>
                      {item.coinsData[0].monthlyEarnings.toFixed(5)}
                    </Text>
                  </View>
                  <View style={styles.pullRight}>
                    <Text style={styles.title}>Monthly Rate</Text>
                    <Text style={styles.subtitle}>
                      {item.monthly_interest_rate.toFixed(5)}%
                    </Text>
                  </View>
                </View>
                <View style={styles.indSection}>
                  <View>
                    <Text style={styles.title}>Annual Earnings</Text>
                    <Text style={styles.subtitle}>
                      {item.coinsData[0].annualEarnings.toFixed(5)}
                    </Text>
                  </View>
                  <View style={styles.pullRight}>
                    <Text style={styles.title}>Annual Rate</Text>
                    <Text style={styles.subtitle}>
                      {item.annual_interest_rate.toFixed(5)}%
                    </Text>
                  </View>
                </View>
                <View style={styles.indSectionLast}>
                  <View>
                    <Text style={styles.title}>Term Earnings</Text>
                    <Text style={styles.subtitle}>
                      {item.coinsData[0].termEarnings.toFixed(5)}
                    </Text>
                  </View>
                  <View style={styles.pullRight}>
                    <Text style={styles.title}>Term Rate</Text>
                    <Text style={styles.subtitle}>
                      {item.term_interest_rate.toFixed(5)}%
                    </Text>
                  </View>
                </View>

                <TouchableOpacity
                  style={[
                    styles.action,
                    {
                      backgroundColor: `#${appData.color_codes[0].primarycolourcode}`,
                    },
                  ]}
                  onPress={(e) => {
                    setSelectedBond(item);
                    navigate('StepOne', {
                      bankerdata: item.bankerData,
                      coinsData: item.coinsData[0],
                      appData: appData,
                    });
                  }}>
                  <Text style={styles.actionFilled}>Buy</Text>
                </TouchableOpacity>
              </View>
            </View>
          );
        })}
      </ScrollView>
    </>
  );
};

export default BondList;

const styles = StyleSheet.create({
  container: {flex: 1},
  action: {
    height: 38,
    // flex: 1,
    width: 121,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    // borderWidth: 1,
    // borderColor: '#E5E5E5',
    backgroundColor: '#04AF76',
    marginBottom: 58,
    marginTop: 10,
  },
  actionFilled: {
    // backgroundColor: '#5F6163',
    color: 'white',
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 16,
  },
  indSectionContainer: {
    // backgroundColor: 'pink',
    paddingHorizontal: 30,
    paddingTop: 50,
  },
  indHeaderSection: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 10,
  },
  indSection: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 30,
    borderBottomWidth: 0.5,
    borderColor: '#EBEBEB',
  },
  indSectionLast: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 30,
  },
  headerTitle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 20,
    color: '#464B4E',
  },
  title: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 12,
    color: '#464B4E',
  },
  subtitle: {
    fontSize: 14,
    fontFamily: ThemeData.FONT_NORMAL,
    paddingTop: 11,
  },
  pullRight: {
    display: 'flex',
    alignItems: 'flex-end',
  },
  transitionContainer: {
    backgroundColor: 'white',
    paddingTop: 10,
  },
  fragmentContainer: {flex: 1},
  searchContainer: {
    ...StyleSheet.absoluteFill,
    backgroundColor: 'white',
    zIndex: 10,
  },
});
