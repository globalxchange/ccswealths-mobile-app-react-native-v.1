import Clipboard from '@react-native-community/clipboard';
import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import ActionBar from '../../components/ActionBar';
import ThemeData from '../../configs/ThemeData';
import AppMainLayout from '../../layouts/AppMainLayout';
import * as WebBrowser from 'expo-web-browser';

const MoneyMarketScreen = () => {
  const {navigate} = useNavigation();

  const [transactionHash, setTransactionHash] = useState('');

  const OPTIONS = [
    {title: 'Global Payments', onPress: () => navigate('GlobalPayement')},
    {title: 'My Bond Hashes'},
    {title: 'My Payments'},
  ];

  const onPasteClick = async () => {
    const pastedString = await Clipboard.getString();

    setTransactionHash((pastedString || '').trim());
  };

  const onSearchClick = () => {
    const hash = transactionHash.trim();

    if (!hash) {
      return WToast.show({
        data: 'Please Input A Asset Hash',
        position: WToast.position.TOP,
      });
    }

    const url = `https://assets.io/bonds/${hash}`;

    WebBrowser.openBrowserAsync(url);
  };

  return (
    <AppMainLayout>
      <ActionBar />
      <View style={styles.container}>
        <Image
          source={require('../../assets/money-markets-full-icon.png')}
          resizeMode="contain"
          style={styles.headerLogo}
        />
        <View style={styles.searchContainer}>
          <TextInput
            style={styles.searchInput}
            value={transactionHash}
            onChangeText={(text) => setTransactionHash(text)}
            placeholder="Search Any Asset Hash..."
            placeholderTextColor="#788995"
          />
          <TouchableOpacity onPress={onPasteClick} style={styles.searchAction}>
            <Image
              style={styles.searchActionIcon}
              resizeMode="contain"
              source={require('../../assets/paste-icon-grey.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={onSearchClick} style={styles.searchAction}>
            <Image
              style={styles.searchActionIcon}
              resizeMode="contain"
              source={require('../../assets/search-icon-grey.png')}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.optionListContainer}>
          <FlatList
            contentContainerStyle={{paddingRight: 20}}
            horizontal
            showsHorizontalScrollIndicator={false}
            data={OPTIONS}
            keyExtractor={(item) => item.title}
            renderItem={({item}) => (
              <TouchableOpacity
                onPress={item.onPress}
                style={styles.optionItem}>
                <Text style={styles.optionText}>{item.title}</Text>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
    </AppMainLayout>
  );
};

export default MoneyMarketScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    justifyContent: 'center',
  },
  headerLogo: {
    width: 280,
    height: 100,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 30,
  },
  searchContainer: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  searchInput: {
    flex: 1,
    paddingHorizontal: 25,
    height: 50,
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
  searchAction: {
    height: 50,
    width: 30,
    paddingVertical: 15,
    marginRight: 10,
  },
  searchActionIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  optionListContainer: {
    marginHorizontal: -20,
    marginTop: 30,
  },
  optionItem: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingVertical: 15,
    paddingHorizontal: 20,
    marginLeft: 20,
  },
  optionText: {
    color: '#788995',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 14,
  },
});
