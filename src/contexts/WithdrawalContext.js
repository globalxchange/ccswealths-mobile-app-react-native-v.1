import React, {Component} from 'react';
import {createContext} from 'react';
import ConnectModal from '../components/Connect/ConnectModal';

const WithdrawalContext = createContext();

export class WithdrawalContextProvider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      step: 0,
      activeWallet: '',
      vaultStep: 0,
      selectedGxVault: null,
      isCryptoFocused: true,
      cryptoInput: '',
      fiatInput: '',
      conversionRate: 1,
      isGXVaultSelectorExpanded: false,
      gxVaults: null,
      pathId: '',
      isConnectOpen: false,
      withdrawalDetails: null,
      externalAddress: '',
      priceLock: false,
      isShowQuote: false,
      pathId: '',
    };
  }

  setPathId = (pathId) => {
    this.setState({pathId});
  };

  setIsShowQuote = (isShowQuote) => {
    this.setState({isShowQuote});
  };

  setPriceLock = (priceLock) => {
    this.setState({priceLock});
  };
  setExternalAddress = (externalAddress) => {
    this.setState({externalAddress});
  };

  setWithdrawalDetails = (withdrawalDetails) => {
    this.setState({withdrawalDetails});
  };

  setStep = (step) => {
    this.setState({step});
  };
  setActiveWallet = (activeWallet) => {
    this.setState({activeWallet});
  };

  setVaultStep = (vaultStep) => {
    this.setState({vaultStep});
  };

  setActiveGxVault = (selectedGxVault) => {
    this.setState({selectedGxVault});
  };

  setIsCryptoFocused = (isCryptoFocused) => {
    this.setState({isCryptoFocused});
  };

  setCryptoInput = (cryptoInput) => {
    this.setState({cryptoInput: cryptoInput.toString()});
  };

  setFiatInput = (fiatInput) => {
    this.setState({fiatInput: fiatInput.toString()});
  };

  setIsGXVaultSelectorExpanded = (isGXVaultSelectorExpanded) => {
    this.setState({isGXVaultSelectorExpanded});
  };

  setGxVaults = (gxVaults) => {
    this.setState({gxVaults});
  };

  setPathId = (pathId) => {
    this.setState({pathId});
  };

  setIsConnectOpen = (isConnectOpen) => {
    this.setState({isConnectOpen});
  };

  clearDepositState = () => {
    this.setState({
      step: 0,
      vaultStep: 0,
      selectedGxVault: null,
      isCryptoFocused: true,
      cryptoInput: '',
      fiatInput: '',
      conversionRate: 1,
      isGXVaultSelectorExpanded: false,
      pathId: '',
    });
  };

  render() {
    return (
      <WithdrawalContext.Provider
        value={{
          ...this.state,
          setPathId: this.setPathId,
          setIsShowQuote: this.setIsShowQuote,
          setPriceLock: this.setPriceLock,
          setExternalAddress: this.setExternalAddress,
          setWithdrawalDetails: this.setWithdrawalDetails,
          setStep: this.setStep,
          setActiveWallet: this.setActiveWallet,
          setVaultStep: this.setVaultStep,
          setActiveGxVault: this.setActiveGxVault,
          setIsCryptoFocused: this.setIsCryptoFocused,
          setCryptoInput: this.setCryptoInput,
          setFiatInput: this.setFiatInput,
          setIsGXVaultSelectorExpanded: this.setIsGXVaultSelectorExpanded,
          clearDepositState: this.clearDepositState,
          setGxVaults: this.setGxVaults,
          setPathId: this.setPathId,
          setIsConnectOpen: this.setIsConnectOpen,
        }}>
        {this.props.children}
        <ConnectModal
          isOpen={this.state.isConnectOpen}
          setIsOpen={this.setIsConnectOpen}
        />
      </WithdrawalContext.Provider>
    );
  }
}

export default WithdrawalContext;
