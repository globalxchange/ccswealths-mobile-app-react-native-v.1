/* eslint-disable react-native/no-inline-styles */
import React, {useContext} from 'react';
import {StyleSheet, View} from 'react-native';
import CustomNumPad from '../components/CustomNumPad';
import {SafeAreaView, useSafeAreaInsets} from 'react-native-safe-area-context';
import BlockCheck from '../components/BlockCheck';
import {AppContext} from '../contexts/AppContextProvider';
import BlockcheckRequest from '../components/BlockcheckRequest';
import AppStatusBar from '../components/AppStatusBar';

const AppMainLayout = ({children, disableBlockCheck, isBottomNav}) => {
  const {bottom} = useSafeAreaInsets();

  const {
    showBCHelper,
    isBlockSheetOpen,
    setIsBlockSheetOpen,
    isBlockCheckSend,
  } = useContext(AppContext);

  return (
    <SafeAreaView style={[styles.container]}>
      <AppStatusBar backgroundColor="white" barStyle={'dark-content'} />
      <View style={{flex: 1}}>{children}</View>
      {!disableBlockCheck && showBCHelper && <BlockCheck />}
      <CustomNumPad />
      <BlockcheckRequest
        isOpen={isBlockSheetOpen}
        setIsOpen={setIsBlockSheetOpen}
        isSend={isBlockCheckSend}
      />
    </SafeAreaView>
  );
};

export default AppMainLayout;

const styles = StyleSheet.create({
  container: {backgroundColor: '#ffffff', flex: 1},
});
