import React, {useContext} from 'react';
import {StyleSheet, View, BackHandler} from 'react-native';
import VaultSelector from './VaultSelector';
import WithdrawForm from './WithdrawForm';
import FundingConfirmation from './FundingConfirmation';
import WithdrawLoading from './WithdrawLoading';
import WithdrawSummary from './WithdrawSummary';
import {useFocusEffect} from '@react-navigation/native';
import WithdrawalContext from '../../../contexts/WithdrawalContext';
import {AppContext} from '../../../contexts/AppContextProvider';

const VaultWithdraw = ({isKeyBoardOpen}) => {
  const {vaultStep, setVaultStep, activeWallet} = useContext(WithdrawalContext);

  const {walletBalances} = useContext(AppContext);

  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        if (vaultStep > 0) {
          setVaultStep(vaultStep - 1);
          return true;
        }
        return false;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () => {
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
      };
    }, [vaultStep]),
  );

  const renderActiveComponent = () => {
    switch (vaultStep) {
      case 0:
        return <VaultSelector />;
      case 1:
        return (
          <WithdrawForm
            activeWallet={activeWallet}
            walletBalances={walletBalances}
            isKeyBoardOpen={isKeyBoardOpen}
          />
        );
      case 2:
        return <FundingConfirmation />;
      case 3:
        return <WithdrawLoading />;
      case 4:
        return <WithdrawSummary />;
      default:
        return <VaultSelector />;
    }
  };

  return <View style={styles.container}>{renderActiveComponent()}</View>;
};

export default VaultWithdraw;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
