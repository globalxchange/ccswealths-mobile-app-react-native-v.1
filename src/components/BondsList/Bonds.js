import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  RefreshControl,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import {getAssetData, formatterHelper} from '../../utils';
import SkeltonItem from '../SkeltonItem';

const Bonds = ({setSearchList, setSearchCallback}) => {
  const {navigate} = useNavigation();

  const {cryptoTableData} = useContext(AppContext);

  const [unfilteredList, setUnfilteredList] = useState();

  useEffect(() => {
    getData();
  }, [cryptoTableData]);

  useEffect(() => {
    setSearchCallback((item) => onItemSelected(item));
    setSearchList(unfilteredList);
    return () => {
      setSearchList();
      setSearchCallback(() => {});
    };
  }, [unfilteredList]);

  const getData = () => {
    setUnfilteredList();
    axios
      .get(`${GX_API_ENDPOINT}/coin/iced/admin/get/data`, {
        params: {group_by: 'coin'},
      })
      .then(({data}) => {
        // console.log('Data', data);

        const rawData = data?.config_data || [];

        const parsed = [];

        rawData.forEach((item) => {
          if (item._id) {
            const [first, second, third] = item.data;

            const coinData = getAssetData(item._id, cryptoTableData);

            parsed.push({
              ...(first || {}),
              ...(second || {}),
              ...(third || {}),
              coinImage: coinData?.coinImage,
              coinName: coinData?.coinName,
              coinSymbol: coinData?.coinSymbol,
              // email: formatterHelper(first?.amount || 0, coinData.coinSymbol),
            });
          }
        });

        // console.log('Parsed', parsed);
        parsed.sort((a, b) => a.coin.localeCompare(b.coin));

        setUnfilteredList(parsed || []);
      })
      .catch((error) => {
        console.log('Error getting crypto', error);
      });
  };

  const onItemSelected = (item) => {
    navigate('BondItem', {selectedBond: item});
  };

  return (
    <View style={styles.container}>
      {unfilteredList ? (
        <FlatList
          style={styles.scrollView}
          data={unfilteredList}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item.coin}
          refreshControl={
            <RefreshControl
              refreshing={unfilteredList ? false : true}
              onRefresh={() => {
                getData();
              }}
            />
          }
          renderItem={({item}) => (
            <TouchableOpacity
              style={styles.itemContainer}
              onPress={() => onItemSelected(item)}>
              <View style={styles.row}>
                <View style={styles.coinContainer}>
                  <Image
                    source={{
                      uri: getAssetData(item.coin, cryptoTableData)?.coinImage,
                    }}
                    resizeMode="contain"
                    style={styles.coinImage}
                  />
                  <Text style={styles.coinName}>
                    {getAssetData(item.coin, cryptoTableData)?.coinName}
                  </Text>
                </View>
                <Text style={styles.coinValue}>
                  {formatterHelper(item.amount, item.coin)}
                </Text>
              </View>
              <View style={styles.row}>
                <View style={styles.metric}>
                  <Text style={styles.metricValue}>
                    {item?.base_rate_lower || 0}%
                  </Text>
                  <Text style={styles.metricLabel}>Base Rate</Text>
                </View>
                <View style={[styles.metric, {flex: 1}]}>
                  <Text style={styles.metricValue}>
                    {item?.base_velocity || 0}%
                  </Text>
                  <Text style={styles.metricLabel}>Velocity</Text>
                </View>
                <View style={styles.metric}>
                  <Text style={[styles.metricValue]}>
                    {item?.acceleration || 0}%
                  </Text>
                  <Text style={styles.metricLabel}>Acceleration</Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
          ListEmptyComponent={
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>No Bonds Found</Text>
            </View>
          }
        />
      ) : (
        <SkeletonItemLoading />
      )}
    </View>
  );
};

const SkeletonItemLoading = () => (
  <View style={styles.scrollView}>
    {Array(3)
      .fill(0)
      .map((_, index) => (
        <View key={index} style={styles.itemContainer}>
          <View style={styles.row}>
            <View style={styles.coinContainer}>
              <SkeltonItem
                itemHeight={25}
                itemWidth={25}
                style={styles.coinImage}
              />
              <SkeltonItem itemHeight={20} itemWidth={80} />
            </View>
            <SkeltonItem itemHeight={20} itemWidth={80} />
          </View>
          <View style={styles.row}>
            <View style={styles.metric}>
              <SkeltonItem itemHeight={30} itemWidth={40} />
              <SkeltonItem
                itemHeight={10}
                itemWidth={40}
                style={{marginTop: 5}}
              />
            </View>
            <View style={[styles.metric, {flex: 1, alignItems: 'center'}]}>
              <SkeltonItem itemHeight={30} itemWidth={40} />
              <SkeltonItem
                itemHeight={10}
                itemWidth={40}
                style={{marginTop: 5}}
              />
            </View>
            <View style={styles.metric}>
              <SkeltonItem itemHeight={30} itemWidth={40} />
              <SkeltonItem
                itemHeight={10}
                itemWidth={40}
                style={{marginTop: 5}}
              />
            </View>
          </View>
        </View>
      ))}
  </View>
);

export default Bonds;

const styles = StyleSheet.create({
  container: {backgroundColor: '#f7f7f7', flex: 1},
  scrollView: {flex: 1, paddingHorizontal: 10, paddingTop: 10},
  itemContainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    backgroundColor: 'white',
    paddingHorizontal: 25,
    paddingVertical: 20,
    marginBottom: 15,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 15,
  },
  coinContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    width: 25,
    height: 25,
    borderRadius: 13,
    marginRight: 5,
  },
  coinName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  coinValue: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 20,
  },
  metric: {},
  metricValue: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontSize: 18,
  },
  metricLabel: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontSize: 10,
  },
  emptyContainer: {
    marginTop: 20,
  },
  emptyText: {
    fontFamily: 'Montserrat',
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontSize: 16,
    paddingHorizontal: 30,
  },
});
