import axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  RefreshControl,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {APP_CODE, GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import {formatterHelper, percentageFormatter} from '../../utils';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import SkeltonItem from '../SkeltonItem';

const Liquid = ({setSearchList, setSearchCallback}) => {
  const {homeSearchInput} = useContext(AppContext);

  const [unfilteredList, setUnfilteredList] = useState();
  const [filteredList, setFilteredList] = useState([]);
  const [intrestBalances, setIntrestBalances] = useState('');
  const [itrestData, setItrestData] = useState();

  useEffect(() => {
    getData();
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();

      if (email) {
        axios
          .get(
            `${GX_API_ENDPOINT}/coin/vault/service/user/app/interest/balances/get`,
            {params: {app_code: APP_CODE, email}},
          )
          .then(({data}) => {
            console.log('data', data);

            const result = data?.result[0] ? data?.result[0].balances : [];

            const balances = result[0] ? result[0].liquid_balances : [];

            // console.log('balances', balances);

            setIntrestBalances(balances);
          })
          .catch((error) => {
            console.log('Error', error);
          });
      }
    })();
  }, []);

  useEffect(() => {
    if (itrestData && intrestBalances) {
      const parsedList = itrestData.map((item) => {
        const balanceData = intrestBalances?.find(
          (x) => x.coinSymbol === item.coin,
        );

        return {
          ...item,
          ...balanceData,
          email: `${parseFloat(item.interest_rate).toPrecision(3)}%`,
        };
      });

      // console.log('parsedList', parsedList);

      setUnfilteredList(parsedList);
    }
  }, [intrestBalances, itrestData]);

  useEffect(() => {
    setSearchCallback((item) => {});
    setSearchList(unfilteredList);
    return () => {
      setSearchList();
      setSearchCallback(() => {});
    };
  }, [unfilteredList]);

  const getData = () => {
    setUnfilteredList();
    axios
      .get(`${GX_API_ENDPOINT}/coin/iced/get/liquid/interest`, {
        params: {app_code: APP_CODE},
      })
      .then(({data}) => {
        const rawData = data?.interest_rates || [];

        // console.log('Parsed', rawData);

        setItrestData(rawData);
      })
      .catch((error) => {
        console.log('Error getting crypto', error);
      });
  };

  return (
    <View style={styles.container}>
      {unfilteredList ? (
        <FlatList
          style={styles.scrollView}
          data={unfilteredList}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item.coinName}
          refreshControl={
            <RefreshControl
              refreshing={unfilteredList ? false : true}
              onRefresh={() => {
                getData();
              }}
            />
          }
          renderItem={({item}) => (
            <View style={styles.itemContainer}>
              <View style={styles.row}>
                <View style={styles.coinContainer}>
                  <Image
                    source={{
                      uri: item?.coin_metdata?.coinImage,
                    }}
                    resizeMode="contain"
                    style={styles.coinImage}
                  />
                  <Text style={styles.coinName}>
                    {item?.coin_metdata?.coinName}
                  </Text>
                </View>
                <Text style={styles.coinValue}>
                  {percentageFormatter.format(item.interest_rate * 365)}%
                </Text>
              </View>
              <View style={styles.row}>
                <View style={styles.metric}>
                  <Text style={styles.metricValue}>
                    {formatterHelper(item?.coinValue || 0, item.coin)}
                  </Text>
                  <Text style={styles.metricLabel}>Balance</Text>
                </View>
                <View style={[styles.metric, {flex: 1}]}>
                  <Text style={styles.metricValue}>
                    {item?.base_velocity || 0}%
                  </Text>
                  <Text style={styles.metricLabel}>Velocity</Text>
                </View>
                <View style={styles.metric}>
                  <Text style={[styles.metricValue]}>
                    {item?.acceleration || 0}%
                  </Text>
                  <Text style={styles.metricLabel}>Acceleration</Text>
                </View>
              </View>
            </View>
          )}
          ListEmptyComponent={
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>No Bonds Found</Text>
            </View>
          }
        />
      ) : (
        <SkeletonItemLoading />
      )}
    </View>
  );
};

const SkeletonItemLoading = () => (
  <View style={styles.scrollView}>
    {Array(3)
      .fill(0)
      .map((_, index) => (
        <View key={index} style={styles.itemContainer}>
          <View style={styles.row}>
            <View style={styles.coinContainer}>
              <SkeltonItem
                itemHeight={25}
                itemWidth={25}
                style={styles.coinImage}
              />
              <SkeltonItem itemHeight={20} itemWidth={80} />
            </View>
            <SkeltonItem itemHeight={20} itemWidth={80} />
          </View>
          <View style={styles.row}>
            <View style={styles.metric}>
              <SkeltonItem itemHeight={30} itemWidth={40} />
              <SkeltonItem
                itemHeight={10}
                itemWidth={40}
                style={{marginTop: 5}}
              />
            </View>
            <View style={[styles.metric, {flex: 1, alignItems: 'center'}]}>
              <SkeltonItem itemHeight={30} itemWidth={40} />
              <SkeltonItem
                itemHeight={10}
                itemWidth={40}
                style={{marginTop: 5}}
              />
            </View>
            <View style={styles.metric}>
              <SkeltonItem itemHeight={30} itemWidth={40} />
              <SkeltonItem
                itemHeight={10}
                itemWidth={40}
                style={{marginTop: 5}}
              />
            </View>
          </View>
        </View>
      ))}
  </View>
);

export default Liquid;

const styles = StyleSheet.create({
  container: {backgroundColor: '#f7f7f7', flex: 1},
  scrollView: {flex: 1, paddingHorizontal: 10, paddingTop: 10},
  itemContainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    backgroundColor: 'white',
    paddingHorizontal: 25,
    paddingVertical: 20,
    marginBottom: 15,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 15,
  },
  coinContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    width: 25,
    height: 25,
    borderRadius: 13,
    marginRight: 5,
  },
  coinName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  coinValue: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 20,
  },
  metric: {},
  metricValue: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontSize: 18,
  },
  metricLabel: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontSize: 10,
  },
  emptyContainer: {
    marginTop: 20,
  },
  emptyText: {
    fontFamily: 'Montserrat',
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontSize: 16,
    paddingHorizontal: 30,
  },
});
