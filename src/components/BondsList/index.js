import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {AppContext} from '../../contexts/AppContextProvider';
import Bonds from './Bonds';
import Liquid from './Liquid';

const BondsList = ({
  setSearchPlaceHolderText,
  setSearchList,
  setSearchCallback,
}) => {
  const {activeListCategory} = useContext(AppContext);

  useEffect(() => {
    switch (activeListCategory.title) {
      case 'Bonds':
        setSearchPlaceHolderText('Search Bonds');
        setActiveFragment(
          <Bonds
            setSearchList={setSearchList}
            setSearchCallback={setSearchCallback}
          />,
        );
        break;
      case 'MoneyMarkets':
        setSearchPlaceHolderText('Search Bonds');
        setActiveFragment(
          <Liquid
            setSearchList={setSearchList}
            setSearchCallback={setSearchCallback}
          />,
        );
        break;
    }
  }, [activeListCategory]);

  const [activeFragment, setActiveFragment] = useState();
  return <View style={styles.container}>{activeFragment}</View>;
};

export default BondsList;

const styles = StyleSheet.create({
  container: {flex: 1},
});
