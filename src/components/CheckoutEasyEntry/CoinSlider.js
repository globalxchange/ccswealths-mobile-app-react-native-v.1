/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const CoinSlider = ({onItemPress, activePercentage}) => {
  return (
    <View style={styles.container}>
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={VALUES}
        keyExtractor={(item) => `${item}`}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() => onItemPress(item)}
            style={[
              styles.itemContainer,
              item === activePercentage && {
                borderColor: ThemeData.APP_MAIN_COLOR,
              },
              index === VALUES.length - 1 && {marginRight: 35},
            ]}>
            <Text style={styles.itemText}>{item.toFixed(1)}%</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default CoinSlider;

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    marginRight: -35,
  },
  itemContainer: {
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
    paddingHorizontal: 30,
    paddingVertical: 12,
    marginRight: 10,
  },
  itemText: {
    fontFamily: ThemeData.FONT_MEDIUM,
    color: '#788995',
    fontSize: 12,
  },
});

const VALUES = [10, 25, 50, 75, 100];
