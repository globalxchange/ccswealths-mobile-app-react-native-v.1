/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import PopupLayout from '../../layouts/PopupLayout';
import {getAssetData, formatterHelper, cryptoList} from '../../utils';
import CoinSlider from './CoinSlider';

const CheckoutEasyEntry = ({
  sellCurrency,
  buyCurrency,
  headerImage,
  setSellCurrencyInput,
  disabled,
}) => {
  const {walletCoinData} = useContext(AppContext);

  const [isOpen, setIsOpen] = useState(false);
  const [sellBalance, setSellBalance] = useState('');
  const [buyBalance, setBuyBalance] = useState('');
  const [walletInput, setWalletInput] = useState(0);
  const [activePercentage, setActivePercentage] = useState();

  useEffect(() => {
    if (walletCoinData && sellCurrency) {
      const coinData = getAssetData(sellCurrency?.coinSymbol, walletCoinData);

      if (coinData) {
        setSellBalance(coinData?.coinValue || 0);
      }
    }
  }, [walletCoinData, sellCurrency]);

  useEffect(() => {
    if (walletCoinData && buyCurrency) {
      const coinData = getAssetData(buyCurrency?.coinSymbol, walletCoinData);

      if (coinData) {
        setBuyBalance(coinData?.coinValue || 0);
      }
    }
  }, [walletCoinData, buyCurrency]);

  const onWalletActionClick = (percentage) => {
    setActivePercentage(percentage);
    const inputValue = ((percentage || 0) * (sellBalance || 0)) / 100;
    setWalletInput(inputValue);
  };

  const onEnterValueClick = () => {
    if (setSellCurrencyInput) {
      const formattedValue = cryptoList.includes(sellCurrency?.coinSymbol)
        ? walletInput.toFixed(5)
        : walletInput.toFixed(2);

      setSellCurrencyInput(formattedValue);
    }
    setIsOpen(false);
  };

  return (
    <>
      <TouchableOpacity
        disabled={disabled}
        onPress={() => setIsOpen(true)}
        style={[styles.easyEntryBtn]}>
        <Text style={[styles.easyEntryText, disabled && {opacity: 0.3}]}>
          Easy Entry
        </Text>
      </TouchableOpacity>
      <PopupLayout
        autoHeight
        noScrollView
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
        headerImage={headerImage}>
        <View style={styles.container}>
          <View style={styles.balanceContainer}>
            <Text style={styles.title}>
              Current {sellCurrency.coinSymbol} Balance
            </Text>
            <View style={styles.balanceView}>
              <Image
                source={{uri: sellCurrency.coinImage}}
                resizeMode="contain"
                style={styles.coinImage}
              />
              <Text style={styles.balanceCoin}>{sellCurrency.coinSymbol}</Text>
              <Text style={styles.balanceValue}>
                {formatterHelper(sellBalance, sellCurrency.coinSymbol)}
              </Text>
            </View>
          </View>
          <View style={styles.balanceContainer}>
            <Text style={styles.title}>
              Current {buyCurrency.coinSymbol} Balance
            </Text>
            <View style={styles.balanceView}>
              <Image
                source={{uri: buyCurrency.coinImage}}
                resizeMode="contain"
                style={styles.coinImage}
              />
              <Text style={styles.balanceCoin}>{buyCurrency.coinSymbol}</Text>
              <Text style={styles.balanceValue}>
                {formatterHelper(buyBalance, buyCurrency.coinSymbol)}
              </Text>
            </View>
          </View>
          <View style={styles.sliderContainer}>
            <Text style={styles.title}>
              How Much Of Your {sellCurrency.coinSymbol} Do You Want To Sell?
            </Text>
            <CoinSlider
              onItemPress={onWalletActionClick}
              activePercentage={activePercentage}
            />
          </View>
          <View style={styles.balanceContainer}>
            <Text style={styles.title}>Result</Text>
            <View style={styles.balanceView}>
              <Image
                source={{uri: sellCurrency.coinImage}}
                resizeMode="contain"
                style={styles.coinImage}
              />
              <Text style={styles.balanceCoin}>{sellCurrency.coinSymbol}</Text>
              <Text style={styles.balanceValue}>
                {formatterHelper(walletInput || 0, sellCurrency.coinSymbol)}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.actionContainer}>
          <TouchableOpacity
            onPress={() => setIsOpen(false)}
            style={[
              styles.outlinedButton,
              {flex: 1, justifyContent: 'center', alignItems: 'center'},
            ]}>
            <Text style={styles.outlinedText}>Close</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={onEnterValueClick}
            style={[
              styles.filledButton,
              {flex: 1, justifyContent: 'center', alignItems: 'center'},
            ]}>
            <Text style={styles.filledText}>Enter Values</Text>
          </TouchableOpacity>
        </View>
      </PopupLayout>
    </>
  );
};

export default CheckoutEasyEntry;

const styles = StyleSheet.create({
  easyEntryBtn: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    flex: 1,
    marginRight: 15,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 6,
  },
  easyEntryText: {
    fontSize: 13,
    fontFamily: 'Montserrat-SemiBold',
    color: '#9A9A9A',
  },
  container: {},
  balanceContainer: {
    marginBottom: 30,
  },
  title: {
    color: '#788995',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 11,
  },
  balanceView: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 15,
    marginTop: 10,
  },
  coinImage: {
    width: 22,
    height: 22,
    marginRight: 10,
  },
  balanceCoin: {
    flex: 1,
    color: '#788995',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  balanceValue: {
    color: '#788995',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  coinActionContainer: {
    flexDirection: 'row',
  },
  outlinedButton: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  outlinedText: {
    color: '#788995',
    fontFamily: ThemeData.FONT_MEDIUM,
    fontSize: 13,
  },
  filledButton: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  filledText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 13,
    color: 'white',
  },
  sliderContainer: {
    marginBottom: 30,
  },
  actionContainer: {
    flexDirection: 'row',
    marginHorizontal: -30,
    marginBottom: -30,
    marginTop: 10,
  },
});
