import React, {useContext, useRef, useState, useEffect} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {AppContext} from '../contexts/AppContextProvider';
import Animated, {
  useCode,
  set,
  Clock,
  interpolate,
} from 'react-native-reanimated';
import ReanimatedTimingHelper from '../utils/ReanimatedTimingHelper';
import {DepositContext} from '../contexts/DepositContext';
import {formatterHelper, usdValueFormatter} from '../utils';
import WithdrawalContext from '../contexts/WithdrawalContext';
import ThemeData from '../configs/ThemeData';

const actions = ['Transactions', 'Deposit', 'Withdraw'];

const WalletView = ({
  changeFragment,
  activeFragment,
  activeWallet,
  isKeyBoardOpen,
}) => {
  const {
    isCustomNumPadOpen,
    walletShowInUsd,
    setWalletShowInUsd,
    walletBalances,
  } = useContext(AppContext);

  const {isGXVaultSelectorExpanded} = useContext(DepositContext);
  const withdrawContext = useContext(WithdrawalContext);

  const [walletBalance, setWalletBalance] = useState('...');

  const buttonAnimation = useRef(new Animated.Value(1));

  const expandAnimation = useRef(new Animated.Value(1));

  useEffect(() => {
    if (walletBalances) {
      if (walletShowInUsd) {
        setWalletBalance(
          usdValueFormatter.format(
            walletBalances[`${activeWallet.coinSymbol.toLowerCase()}_balance`] *
              activeWallet.price.USD,
          ),
        );
      } else {
        setWalletBalance(
          `${formatterHelper(
            walletBalances[`${activeWallet.coinSymbol.toLowerCase()}_balance`],
            activeWallet.coinSymbol,
          )}`,
        );
      }
    }
  }, [walletBalances, activeWallet, walletShowInUsd]);

  useEffect(() => {
    // console.log('activeWallet', activeWallet);
    if (activeWallet.coinSymbol === 'USD') {
      setWalletShowInUsd(true);
    } else {
      setWalletShowInUsd(false);
    }
    return () => {};
  }, [activeWallet]);

  useCode(
    () =>
      isKeyBoardOpen || isCustomNumPadOpen
        ? [
            set(
              buttonAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ]
        : [
            set(
              buttonAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ],
    [isKeyBoardOpen, isCustomNumPadOpen],
  );

  useCode(
    () =>
      isGXVaultSelectorExpanded || withdrawContext.isGXVaultSelectorExpanded
        ? [
            set(
              expandAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ]
        : [
            set(
              expandAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ],
    [isGXVaultSelectorExpanded, withdrawContext.isGXVaultSelectorExpanded],
  );

  return (
    <Animated.View
      style={[
        styles.container,
        {
          maxHeight: interpolate(expandAnimation.current, {
            inputRange: [0, 1],
            outputRange: [0, 3000],
          }),
          paddingTop: interpolate(expandAnimation.current, {
            inputRange: [0, 1],
            outputRange: [0, 30],
          }),
        },
        (isGXVaultSelectorExpanded ||
          withdrawContext.isGXVaultSelectorExpanded) && {
          backgroundColor: ThemeData.APP_MAIN_COLOR,
        },
      ]}>
      {activeWallet ? (
        <View
          style={
            (isGXVaultSelectorExpanded ||
              withdrawContext.isGXVaultSelectorExpanded) && {opacity: 0}
          }>
          <View style={styles.nameContainer}>
            <Text style={styles.walletName}>
              {activeWallet.coinName} Wallet
            </Text>
            <View style={styles.switchContainer}>
              {activeWallet.coinSymbol !== 'USD' && (
                <TouchableOpacity
                  style={walletShowInUsd ? styles.switch : styles.switchActive}
                  onPress={() => setWalletShowInUsd(false)}>
                  <Text
                    style={
                      walletShowInUsd
                        ? styles.switchText
                        : styles.switchTextActive
                    }>
                    {activeWallet.coinSymbol}
                  </Text>
                </TouchableOpacity>
              )}
              <TouchableOpacity
                style={walletShowInUsd ? styles.switchActive : styles.switch}
                onPress={() => setWalletShowInUsd(true)}>
                <Text
                  style={
                    walletShowInUsd
                      ? styles.switchTextActive
                      : styles.switchText
                  }>
                  USD
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <Text style={styles.wallerBalance}>{walletBalance}</Text>
          <Animated.View
            style={[
              styles.actionsContainer,
              {
                height: interpolate(buttonAnimation.current, {
                  inputRange: [0, 1],
                  outputRange: [0, 45],
                }),
                marginTop: interpolate(buttonAnimation.current, {
                  inputRange: [0, 1],
                  outputRange: [0, 20],
                }),
              },
            ]}>
            {actions.map((item, index) => (
              <TouchableOpacity
                key={item}
                style={[
                  styles.actionsButton,
                  activeFragment === item && styles.actionsButtonActive,
                  index === actions.length - 1 && {marginRight: 0},
                ]}
                onPress={() => changeFragment(item)}>
                <Text
                  style={[
                    styles.buttonText,
                    activeFragment === item && styles.buttonTextActive,
                  ]}>
                  {item}
                </Text>
              </TouchableOpacity>
            ))}
          </Animated.View>
        </View>
      ) : null}
    </Animated.View>
  );
};

export default WalletView;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingBottom: 40,
    overflow: 'hidden',
    justifyContent: 'center',
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
  },
  nameContainer: {justifyContent: 'space-between', flexDirection: 'row'},
  walletName: {color: ThemeData.APP_MAIN_COLOR, fontFamily: 'Montserrat-Bold'},
  switchContainer: {flexDirection: 'row'},
  switch: {
    backgroundColor: 'white',
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
    borderColor: ThemeData.APP_MAIN_COLOR,
    borderWidth: 1,
  },
  switchActive: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
  },
  switchText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 10,
    fontFamily: 'Montserrat',
  },
  switchTextActive: {
    color: 'white',
    fontSize: 10,
    fontFamily: 'Montserrat-Bold',
  },
  wallerBalance: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat',
    fontSize: 35,
  },
  actionsContainer: {
    flexDirection: 'row',
    overflow: 'hidden',
  },
  actionsButton: {
    flexGrow: 1,
    width: 0,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: ThemeData.APP_MAIN_COLOR,
    justifyContent: 'center',
    marginRight: 15,
    borderRadius: 6,
    zIndex: 1,
  },
  actionsButtonActive: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
  },
  buttonText: {
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    fontSize: 12,
    color: ThemeData.APP_MAIN_COLOR,
  },
  buttonTextActive: {
    color: 'white',
  },
  listContainer: {backgroundColor: ThemeData.APP_MAIN_COLOR},
  txnItem: {},
});
