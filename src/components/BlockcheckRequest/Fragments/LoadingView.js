import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import LoadingAnimation from '../../LoadingAnimation';

const LoadingView = () => {
  return (
    <View style={[styles.loadingContainer]}>
      <LoadingAnimation />
    </View>
  );
};

export default LoadingView;

const styles = StyleSheet.create({
  loadingContainer: {
    marginVertical: 70,
  },
});
