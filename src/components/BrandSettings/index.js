import {useNavigation, useRoute} from '@react-navigation/native';
import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import SettingsSelector from './SettingsSelector';

const BrandSettings = () => {
  const {goBack} = useNavigation();
  const {params} = useRoute();

  const [selectedCategory, setSelectedCategory] = useState();

  const {selectedBrand} = params;

  return (
    <View style={styles.container}>
      <View style={styles.breadCrumbsContainer}>
        <Text onPress={goBack} style={styles.prevBreadCrumb}>
          Settings
        </Text>
        <Text style={styles.breadCrumbArrow}>{' -> '}</Text>
        <Text
          onPress={() => setSelectedCategory()}
          style={[
            selectedCategory ? styles.prevBreadCrumb : styles.currentBreadCrumb,
          ]}>
          {selectedBrand.mtToolName}
        </Text>
        {selectedCategory ? (
          <>
            <Text style={styles.breadCrumbArrow}>{' -> '}</Text>
            <Text
              style={
                styles.currentBreadCrumb
              }>{`${selectedCategory} ${selectedBrand.mtToolName}`}</Text>
          </>
        ) : null}
      </View>
      <View style={styles.fragmentContainer}>
        <SettingsSelector
          selectedBrand={selectedBrand}
          setSelectedCategory={setSelectedCategory}
        />
      </View>
    </View>
  );
};

export default BrandSettings;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  breadCrumbsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 30,
  },
  prevBreadCrumb: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    color: '#464B4E',
  },
  breadCrumbArrow: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
  },
  currentBreadCrumb: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    textDecorationLine: 'underline',
    fontSize: 12,
    color: '#464B4E',
  },
  fragmentContainer: {
    marginTop: 30,
    flex: 1,
  },
});
