import React, {useContext, useEffect, useState} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {AppContext} from '../../contexts/AppContextProvider';
import AssetsIoController from '../ControllerLists/AssetsIoController';
import OTCBotsBottomSheet from '../ControllerLists/OTCBotsBottomSheet';
import MTItem from './MTItem';

const SettingsSelector = ({setSelectedCategory, selectedBrand}) => {
  const {cryptoTableData} = useContext(AppContext);

  const [isAssetIoControllerOpen, setIsAssetIoControllerOpen] = useState(false);
  const [isLiquid, setIsLiquid] = useState(false);
  const [isEditor, setIsEditor] = useState(false);

  const [isOTCBotsOpen, setIsOTCBotsOpen] = useState(false);
  const [otcData, setOtcData] = useState('');

  useEffect(() => {
    if (isAssetIoControllerOpen) {
      setIsOTCBotsOpen(false);
    }
  }, [isAssetIoControllerOpen]);

  useEffect(() => {
    if (isOTCBotsOpen) {
      setIsAssetIoControllerOpen(false);
    }
  }, [isOTCBotsOpen]);

  let fiatAssets = 0;
  let cryptoAssets = 0;

  if (cryptoTableData) {
    // console.log('cryptoTableData', cryptoTableData);

    fiatAssets = cryptoTableData.filter((item) => item.asset_type === 'Fiat')
      .length;

    cryptoAssets = cryptoTableData.length - fiatAssets;
  }

  const instaProductsList = [
    {
      icon: require('../../assets/fiat-crypto-icon.png'),
      title: 'Fiat To Crypto',
      numbers: `${fiatAssets * cryptoAssets} Pairs`,
      fromAssets: 'Fiat',
      toAsset: 'Crypto',
      subTitle:
        'Examine Your Current Exchange Fee Structure For Fiat To Crypto',
    },
    {
      icon: require('../../assets/crypto-fiat-icon.png'),
      title: 'Crypto To Fiat',
      numbers: `${fiatAssets * cryptoAssets} Pairs`,
      fromAssets: 'Crypto',
      toAsset: 'Fiat',
      subTitle:
        'Examine Your Current Exchange Fee Structure For Crypto To Fiat',
    },
    {
      icon: require('../../assets/fiat-fiat-icon.png'),
      title: 'Fiat To Fiat',
      numbers: `${fiatAssets * fiatAssets} Pairs`,
      fromAssets: 'Fiat',
      toAsset: 'Fiat',
      subTitle: 'Examine Your Current Exchange Fee Structure For Fiat To Fiat',
    },
    {
      icon: require('../../assets/crypto-crypto-icon.png'),
      title: 'Crypto To Crypto',
      numbers: `${cryptoAssets * cryptoAssets} Pairs`,
      fromAssets: 'Crypto',
      toAsset: 'Crypto',
      subTitle:
        'Examine Your Current Exchange Fee Structure For Crypto To Crypto',
    },
    {
      icon: require('../../assets/per-customer-icon.png'),
      title: 'Per Customer',
      isCustom: true,
    },
  ];

  const onInstaProductClick = (index, isEdit) => {
    setOtcData(instaProductsList[index]);
    setIsEditor(isEdit || false);
    setIsOTCBotsOpen(true);
  };

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <MTItem
          icon={require('../../assets/current-fees-icon.png')}
          title={`Current ${selectedBrand.mtToolName}`}
          subTitle={`Examine Your Current ${selectedBrand.mtToolName} Structure`}
          onPress={() => setSelectedCategory('Current')}
          header="Analyze Monetization Tool"
          pos={1}
          desc={`You will be able to see the current rates which you have. These rates will are being deducted from all interest payments from your direct users`}
          onProductPress={
            selectedBrand?.title === 'InstaCrypto'
              ? (index) => onInstaProductClick(index, false)
              : null
          }
          expandMenu={
            selectedBrand?.title === 'CCSWealth'
              ? [
                  {
                    icon: require('../../assets/liquid-earnings-icon.png'),
                    title: 'Liquid Earnings',
                    onPress: () => {
                      setIsEditor(false);
                      setIsLiquid(true);
                      setIsAssetIoControllerOpen(true);
                    },
                  },
                  {
                    icon: require('../../assets/bonds-icon.png'),
                    title: 'Bond Earnings',
                    onPress: () => {
                      setIsEditor(false);
                      setIsLiquid(false);
                      setIsAssetIoControllerOpen(true);
                    },
                  },
                ]
              : instaProductsList
          }
        />
        <MTItem
          icon={require('../../assets/change-fee-icon.png')}
          title={`Change ${selectedBrand.mtToolName}`}
          subTitle={`Change ${selectedBrand.mtToolName}`}
          onPress={() => setSelectedCategory('Update')}
          header="Change Monetization Tool"
          pos={1}
          desc={`You will be able to see the current rates which you have. These rates will are being deducted from all interest payments from your direct users`}
          onProductPress={
            selectedBrand?.title === 'InstaCrypto'
              ? (index) => onInstaProductClick(index, true)
              : null
          }
          expandMenu={
            selectedBrand?.title === 'CCSWealth'
              ? [
                  {
                    icon: require('../../assets/liquid-earnings-icon.png'),
                    title: 'Liquid Earnings',
                    onPress: () => {
                      setIsEditor(true);
                      setIsLiquid(true);
                      setIsAssetIoControllerOpen(true);
                    },
                  },
                  {
                    icon: require('../../assets/bonds-icon.png'),
                    title: 'Bond Earnings',
                    onPress: () => {
                      setIsEditor(true);
                      setIsLiquid(false);
                      setIsAssetIoControllerOpen(true);
                    },
                  },
                ]
              : instaProductsList
          }
        />
      </ScrollView>
      <AssetsIoController
        selectedApp={selectedBrand}
        isOpen={isAssetIoControllerOpen}
        setIsOpen={setIsAssetIoControllerOpen}
        isLiquid={isLiquid}
        isEditor={isEditor}
      />
      <OTCBotsBottomSheet
        setIsSheetOpen={setIsOTCBotsOpen}
        isSheetOpen={isOTCBotsOpen}
        data={otcData}
        selectedApp={selectedBrand}
        isEditor={isEditor}
      />
    </View>
  );
};

export default SettingsSelector;

const styles = StyleSheet.create({
  container: {},
});
