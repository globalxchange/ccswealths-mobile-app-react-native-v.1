import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const MenuItem = ({onPress, icon, title, subTitle, style, disabled}) => {
  return (
    <TouchableOpacity disabled={disabled} onPress={onPress}>
      <View
        style={[styles.itemContainer, style || {}, disabled && {opacity: 0.5}]}>
        <Image style={styles.itemIcon} source={icon} resizeMode="contain" />
        <View style={styles.itemDetailsContainer}>
          <Text style={styles.itemName}>{title}</Text>
          <Text style={styles.itemSubText}>{subTitle}</Text>
        </View>
        <Image
          style={styles.arrowIcon}
          source={require('../../assets/arrow-carrot.png')}
          resizeMode="contain"
        />
      </View>
    </TouchableOpacity>
  );
};

export default MenuItem;

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 25,
    marginVertical: 7.5,
  },
  itemIcon: {
    width: 32,
    height: 32,
    marginRight: 15,
  },
  itemDetailsContainer: {
    flex: 1,
  },
  itemName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 16,
    color: ThemeData.APP_MAIN_COLOR,
  },
  itemSubText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 10,
    color: ThemeData.APP_MAIN_COLOR,
    opacity: 0.75,
  },
  arrowIcon: {
    width: 20,
    height: 12,
  },
});
