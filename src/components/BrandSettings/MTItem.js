import React, {useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const MTItem = ({
  onPress,
  header,
  icon,
  title,
  subTitle,
  style,
  disabled,
  pos,
  desc,
  expandMenu,
  onProductPress,
}) => {
  const [isExpanded, setIsExpanded] = useState(false);

  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={expandMenu ? () => setIsExpanded(!isExpanded) : onPress}>
      <View style={[styles.container, style || {}, disabled && {opacity: 0.5}]}>
        <View style={styles.headerContainer}>
          <Text style={styles.header}>{header}</Text>
          <View style={styles.divider} />
          <Text style={styles.toolNumber}>MT-{pos}</Text>
        </View>
        <View style={styles.itemDetails}>
          <View style={styles.itemContainer}>
            <Image style={styles.itemIcon} source={icon} resizeMode="contain" />
            <View style={styles.itemDetailsContainer}>
              <Text style={styles.itemName}>{title}</Text>
              <Text style={styles.itemSubText}>{subTitle}</Text>
            </View>
          </View>
          <Text style={styles.itemDescHeader}>Description</Text>
          <Text style={styles.itemDesc}>{desc}</Text>
        </View>
        {isExpanded && (
          <View style={styles.productList}>
            <Text style={styles.productListHeader}>Choose A Product</Text>
            {expandMenu?.map((item, index) => (
              <TouchableOpacity
                onPress={
                  onProductPress ? () => onProductPress(index) : item.onPress
                }
                key={item.title}
                style={styles.productItem}>
                <Image
                  style={styles.productImage}
                  source={item.icon}
                  resizeMode="contain"
                />
                <Text style={styles.productName}>{item.title}</Text>
                <Image
                  style={styles.arrowIcon}
                  source={require('../../assets/arrow-carrot.png')}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            ))}
          </View>
        )}
      </View>
    </TouchableOpacity>
  );
};

export default MTItem;

const styles = StyleSheet.create({
  container: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginVertical: 7.5,
  },
  headerContainer: {
    paddingHorizontal: 20,
    flexDirection: 'row',
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    alignItems: 'center',
    height: 35,
  },
  header: {
    flex: 1,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 12,
  },
  divider: {
    backgroundColor: ThemeData.BORDER_COLOR,
    width: 1,
    height: '100%',
    marginHorizontal: 20,
  },
  toolNumber: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 12,
  },
  itemDetails: {
    paddingHorizontal: 20,
    paddingVertical: 25,
  },
  itemContainer: {
    flexDirection: 'row',
    marginBottom: 20,
    alignItems: 'center',
  },
  itemIcon: {
    width: 32,
    height: 32,
    marginRight: 15,
  },
  itemDetailsContainer: {
    flex: 1,
  },
  itemName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 16,
    color: ThemeData.APP_MAIN_COLOR,
  },
  itemSubText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 10,
    color: ThemeData.APP_MAIN_COLOR,
    opacity: 0.75,
  },
  itemDescHeader: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 12,
    textDecorationLine: 'underline',
    marginBottom: 8,
  },
  itemDesc: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    opacity: 0.75,
    fontSize: 10,
  },
  productList: {
    paddingHorizontal: 20,
    marginBottom: 60,
  },
  productListHeader: {
    marginBottom: 10,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  productItem: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingVertical: 20,
    paddingHorizontal: 20,
    marginBottom: 10,
  },
  productImage: {
    width: 28,
    height: 28,
  },
  productName: {
    flex: 1,
    marginHorizontal: 15,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 13,
    color: ThemeData.APP_MAIN_COLOR,
  },
  arrowIcon: {
    width: 20,
    height: 12,
  },
});
