import React from 'react';
import {Dimensions, Image, StyleSheet, View} from 'react-native';

const {width} = Dimensions.get('window');

const CARD_WIDTH = width * 0.75;
const CARD_HEIGHT = CARD_WIDTH / 1.8;

const circleSize = 250;

const EmptyCreditCard = () => {
  const bgColor = '#0047cc';

  return (
    <View
      style={[styles.container, {backgroundColor: bgColor, width: CARD_WIDTH}]}>
      <View style={[styles.bgCircle, styles.rightBgCircle]} />
      <View style={[styles.bgCircle, styles.bottomBgCircle]} />
      <View style={styles.spendIconContainer}>
        <Image
          style={styles.cardIcon}
          source={require('../assets/spend-card-full-logo.png')}
          resizeMode="contain"
        />
      </View>
    </View>
  );
};

export default EmptyCreditCard;

const styles = StyleSheet.create({
  container: {
    padding: 20,
    borderRadius: 12,
    position: 'relative',
    height: CARD_HEIGHT,
    justifyContent: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
  },
  bgCircle: {
    position: 'absolute',
    backgroundColor: 'white',
    opacity: 0.05,
    height: circleSize,
    width: circleSize,
    borderRadius: circleSize,
  },
  rightBgCircle: {
    top: (-1 * circleSize) / 4,
    right: (-1 * circleSize) / 2,
  },
  bottomBgCircle: {
    bottom: (-1 * circleSize) / 2,
    left: (0 * (-1 * circleSize)) / 2,
  },
  textContainer: {
    overflow: 'hidden',
  },
  spendIconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardIcon: {
    width: CARD_WIDTH / 2,
    height: CARD_WIDTH / 2,
  },
});
