/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const ControllerMenu = ({activeMenu, setActiveMenu, color}) => {
  useEffect(() => {
    setActiveMenu(MENUS[1]);
  }, []);

  return (
    <View style={styles.container}>
      {MENUS.map((item) => (
        <TouchableOpacity
          key={item}
          onPress={() => setActiveMenu(item)}
          style={[
            styles.itemContainer,
            activeMenu === item && {
              borderBottomColor: color,
            },
          ]}>
          <Text
            style={[
              styles.itemLabel,
              activeMenu === item && {
                color,
                borderBottomColor: color,
                fontSize: 16,
              },
            ]}>
            {item}
          </Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};

export default ControllerMenu;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 10,
  },
  itemContainer: {
    zIndex: 3,
    flex: 1,
    borderBottomColor: '#e7e7e7',
    borderBottomWidth: 1,
    paddingBottom: 10,
  },
  itemLabel: {
    color: '#979797',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    marginTop: 5,
  },
});

const MENUS = ['Offers', 'About', 'Opportunity'];
