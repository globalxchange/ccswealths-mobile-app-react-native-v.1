import React from 'react';
import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const Opportunities = ({color, selectedItem}) => {
  return (
    <View style={styles.container}>
      <Text style={[styles.title, {color}]}>Earn With Us</Text>
      <Text style={[styles.subTitle]}>
        Select The One That Is Relevant To You
      </Text>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={Array(5).fill(1)}
        keyExtractor={(item, index) => `${index}`}
        renderItem={({item}) => (
          <View style={styles.itemContainer}>
            <Image
              resizeMode="cover"
              style={styles.itemImage}
              source={{
                uri:
                  'https://images.unsplash.com/photo-1603575449096-da705f622102?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=anthony-riera-it9kG_dWyVo-unsplash.jpg&w=640',
              }}
            />
            <View style={styles.detailsContainer}>
              <Text style={[styles.itemText, {color}]}>
                I Am Completely New
              </Text>
              <Text style={[styles.itemDesc]}>I Am Completely New</Text>
            </View>
          </View>
        )}
      />
    </View>
  );
};

export default Opportunities;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 30,
    marginTop: 30,
    marginBottom: -20,
  },
  title: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 20,
    marginBottom: 5,
  },
  subTitle: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginBottom: 20,
    color: '#292934',
    opacity: 0.5,
  },
  itemContainer: {
    flexDirection: 'row',
    marginBottom: 15,
    borderColor: '#EBEBEB',
    borderWidth: 1,
  },
  itemImage: {
    width: 95,
    height: 75,
  },
  detailsContainer: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  itemText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 14,
    marginBottom: 5,
  },
  itemDesc: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 10,
    color: '#292934',
    opacity: 0.5,
  },
});
