import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import BrandCarousel from '../BrandCarousel';
import BrandAbout from './BrandAbout';
import ControllerMenu from './ControllerMenu';
import Opportunities from './Opportunities';

const BrandDetails = ({selectedItem, color}) => {
  const [activeMenu, setActiveMenu] = useState();
  const [reloadFlag, setReloadFlag] = useState(false);

  let activeView = null;

  switch (activeMenu) {
    case 'About':
      activeView = (
        <BrandAbout
          key={Date.now()}
          color={color}
          selectedItem={selectedItem}
        />
      );
      break;
    case 'Offers':
      activeView = (
        <View style={styles.carouselContainer}>
          {selectedItem?.displayName === 'Global X Change' && <BrandCarousel />}
        </View>
      );
      break;
    case 'Opportunity':
      activeView = <Opportunities color={color} selectedItem={selectedItem} />;
      break;
  }

  return (
    <View style={styles.container}>
      <ControllerMenu
        color={color}
        activeMenu={activeMenu}
        setActiveMenu={(item) => {
          setActiveMenu(item);
          setReloadFlag(!reloadFlag);
        }}
      />
      <View style={styles.fragmentContainer}>{activeView}</View>
    </View>
  );
};

export default BrandDetails;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  fragmentContainer: {
    flex: 1,
  },
  carouselContainer: {
    flex: 1,
    justifyContent: 'center',
  },
});
