import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const BrandAddress = ({title, subtitle, color, selectedItem}) => {
  return (
    <View style={styles.container}>
      <Text style={[styles.title, {color}]}>{title}</Text>
      <Text style={[styles.subTitle, {color}]}>{subtitle}</Text>
      <View style={styles.itemContainer}>
        <Text style={styles.itemLabel}>HQ</Text>
        <Text numberOfLines={1} style={styles.itemValue}>
          {selectedItem.address}
        </Text>
      </View>
      <View style={styles.itemContainer}>
        <Text style={styles.itemLabel}>Country</Text>
        <Text style={styles.itemValue}>{selectedItem.country}</Text>
      </View>
      <View style={styles.itemContainer}>
        <Text style={styles.itemLabel}>Email</Text>
        <Text style={styles.itemValue}>{selectedItem.email}</Text>
      </View>
      <View style={styles.itemContainer}>
        <Text style={styles.itemLabel}>Phone</Text>
        <Text style={styles.itemValue}>{selectedItem.phone}</Text>
      </View>
    </View>
  );
};

export default BrandAddress;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 30,
    marginTop: 30,
  },
  title: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 20,
    marginBottom: 5,
  },
  subTitle: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginBottom: 20,
  },
  itemContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  itemLabel: {
    color: '#292934',
    opacity: 0.5,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 13,
  },
  itemValue: {
    flex: 1,
    textAlign: 'right',
    color: '#292934',
    opacity: 0.5,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
  },
});
