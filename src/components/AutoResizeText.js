import React, {useState} from 'react';
import {StyleSheet, Text} from 'react-native';

const AutoResizeText = ({text}) => {
  const [currentFont, setCurrentFont] = useState(14);

  const NUMBER_OF_LINES = 1;

  return (
    <Text
      maxFontSizeMultiplier={NUMBER_OF_LINES}
      adjustsFontSizeToFit
      numberOfLines={1}
      style={[styles.productTitle, {fontSize: currentFont}]}
      onTextLayout={(e) => {
        const {lines} = e.nativeEvent;
        if (lines.length > NUMBER_OF_LINES) {
          setCurrentFont(currentFont - 1);
        }
      }}>
      {text}
    </Text>
  );
};

export default AutoResizeText;

const styles = StyleSheet.create({
  productTitle: {
    color: 'white',
    fontFamily: 'Montserrat-Bold',
  },
});
