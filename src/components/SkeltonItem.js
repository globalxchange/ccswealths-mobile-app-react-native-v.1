/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, View} from 'react-native';
import Animated, {
  Easing,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import LinearGradient from 'react-native-linear-gradient';
import {useValue, loop} from 'react-native-redash';

const ANIMATED_LG = Animated.createAnimatedComponent(LinearGradient);

const SkeltonItem = ({style = {}, itemWidth, itemHeight}) => {
  const animatedValue = useValue(0);

  useCode(
    () => [
      set(
        animatedValue,
        loop({
          duration: 1000,
          easing: Easing.linear,
          autoStart: true,
        }),
      ),
    ],
    [],
  );

  return (
    <View
      style={[
        style,
        {
          backgroundColor: '#E1E9EE',
          borderColor: '#F2F8FC',
          height: itemHeight || itemWidth || 0,
          width: itemWidth || 0,
          overflow: 'hidden',
        },
      ]}>
      <ANIMATED_LG
        colors={['#E1E9EE', '#F2F8FC', '#E1E9EE', '#F2F8FC']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        style={{
          ...StyleSheet.absoluteFill,
          transform: [
            {
              translateX: interpolate(animatedValue, {
                inputRange: [0, 1],
                outputRange: [0, itemWidth],
              }),
            },
          ],
        }}
      />
    </View>
  );
};

export default SkeltonItem;

const styles = StyleSheet.create({});
