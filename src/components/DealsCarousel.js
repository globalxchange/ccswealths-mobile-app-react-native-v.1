import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

const DealsCarousel = () => {
  const {navigate} = useNavigation();

  return (
    <View style={styles.container}>
      {/* <ScrollView horizontal showsHorizontalScrollIndicator={false}> */}
      <TouchableOpacity
        onPress={() => navigate('CreateBrokerage')}
        style={styles.addButton}>
        <Image
          style={styles.addImage}
          source={require('../assets/add-colored.png')}
          resizeMode="contain"
        />
        <Text style={styles.addText}>New Deal</Text>
      </TouchableOpacity>
      {/* </ScrollView> */}
    </View>
  );
};

export default DealsCarousel;

const styles = StyleSheet.create({
  container: {
    marginTop: 30,
    marginBottom: 5,
    alignItems: 'center',
  },
  addButton: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 25,
    paddingVertical: 20,
  },
  addText: {
    color: '#001D41',
    fontFamily: 'Montserrat',
    marginTop: 10,
    fontSize: 10,
  },
  addImage: {
    width: 22,
    height: 22,
  },
});
