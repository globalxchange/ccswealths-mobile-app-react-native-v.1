import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {GX_API_ENDPOINT} from '../../configs';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import LoadingAnimation from '../LoadingAnimation';
import ProfileSettingsItem from '../ProfileSettingsItem';

const DestinationAppSelector = ({interestData, isLiquid, onClose}) => {
  const [appList, setAppList] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();

      axios
        .get(`${GX_API_ENDPOINT}/gxb/apps/registered/user`, {params: {email}})
        .then(({data}) => {
          console.log('Apps', data);
          setAppList(data?.userApps || []);
        })
        .catch((error) => {
          console.log('Error on getting apps', error);
        });
    })();
  }, []);

  const changeDestination = async (selectedApp) => {
    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const updateData = {
      ...(interestData || {
        payoutFrequency: 'daily',
        payoutDestination: 'vault',
      }),
      payoutDestination_data: {app_code: selectedApp.app_code},
    };

    let postData = {
      token,
      email,
    };

    if (isLiquid) {
      postData = {...postData, liquidData: updateData};
    } else {
      postData = {...postData, icedData: updateData};
    }

    // console.log('Post Data: ', postData);

    axios
      .post(
        `${GX_API_ENDPOINT}/coin/iced/set/user/interest/payout/destination`,
        postData,
      )
      .then(({data}) => {
        // console.log('update data', data);

        if (data.status) {
          onClose();
        } else {
          WToast.show({data: data.message, position: WToast.position.TOP});
        }
      })
      .catch((error) => {
        console.log('Error on updating destination app');
        WToast.show({data: 'Network Error', position: WToast.position.TOP});
      })
      .finally(() => setIsLoading(false));
  };

  if (!appList || isLoading) {
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <LoadingAnimation />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <FlatList
        data={appList}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item) => item.app_code}
        renderItem={({item}) => (
          <ProfileSettingsItem
            icon={{uri: item.app_icon}}
            title={item.app_name}
            onPress={() => changeDestination(item)}
          />
        )}
      />
    </View>
  );
};

export default DestinationAppSelector;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  appItem: {},
  appIcon: {},
});
