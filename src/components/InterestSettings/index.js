/* eslint-disable react-native/no-inline-styles */
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import LoadingAnimation from '../LoadingAnimation';
import ProfileSettingsItem from '../ProfileSettingsItem';
import EditDestination from './EditDestination';
import FrequencyEditor from './FrequencyEditor';

const InterestSettings = ({onBack, isLiquid}) => {
  const [interestData, setInterestData] = useState();
  const [isDestinationOpen, setIsDestinationOpen] = useState(false);
  const [isFrequencyEditorOpen, setIsFrequencyEditorOpen] = useState(false);

  useEffect(() => {
    if (isDestinationOpen) {
      setIsFrequencyEditorOpen(false);
    }
  }, [isDestinationOpen]);

  useEffect(() => {
    if (isFrequencyEditorOpen) {
      setIsDestinationOpen(false);
    }
  }, [isFrequencyEditorOpen]);

  useEffect(() => {
    getInterestData();
  }, [isLiquid]);

  const getInterestData = async () => {
    setInterestData();
    const email = await AsyncStorageHelper.getLoginEmail();

    axios
      .get(
        `${GX_API_ENDPOINT}/coin/iced/get/user/interest/payout/destination`,
        {params: {email}},
      )
      .then(({data}) => {
        // console.log('Interest Data', data);

        const destination = data?.destinations ? data?.destinations?.pop() : '';

        setInterestData(
          (isLiquid ? destination?.liquidData : destination?.icedData) || '',
        );
      })
      .catch((error) => {
        console.log('Error on getting Earning Data', error);
      });
  };

  if (interestData === undefined) {
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <LoadingAnimation />
      </View>
    );
  }

  if (isDestinationOpen) {
    return (
      <EditDestination
        onBack={() => setIsDestinationOpen(true)}
        interestData={interestData}
        isLiquid={isLiquid}
        onClose={() => {
          setIsDestinationOpen(false);
          getInterestData();
        }}
      />
    );
  }

  if (isFrequencyEditorOpen) {
    return (
      <FrequencyEditor
        onBack={() => setIsFrequencyEditorOpen(false)}
        interestData={interestData}
        isLiquid={isLiquid}
        onClose={() => {
          setIsFrequencyEditorOpen(false);
          getInterestData();
        }}
      />
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.activeDetailsContainer}>
        <View style={styles.detailsHeader}>
          <Image
            source={
              isLiquid
                ? require('../../assets/liquid-earnings-icon.png')
                : require('../../assets/bonds-icon.png')
            }
            resizeMode="contain"
            style={styles.headerIcon}
          />
          <Text style={styles.headerTitle}>
            {isLiquid ? 'Liquid' : 'Bond'} Earnings
          </Text>
        </View>
        <View style={styles.details}>
          <View style={styles.detailsItem}>
            <Text style={styles.detailLabel}>Frequency</Text>
            <Text style={styles.detailValue}>
              {interestData?.payoutFrequency}
            </Text>
          </View>
          <View style={styles.detailsItem}>
            <Text style={styles.detailLabel}>Destination</Text>
            <Text style={styles.detailValue}>
              {interestData?.payoutDestination_data?.app_code}
            </Text>
          </View>
        </View>
      </View>
      <ProfileSettingsItem
        title="Edit Frequency"
        subText="Update First, Last, & Your Nick Name"
        onPress={() => setIsFrequencyEditorOpen(true)}
      />
      <ProfileSettingsItem
        title="Edit Destination"
        subText="Update First, Last, & Your Nick Name"
        onPress={() => setIsDestinationOpen(true)}
      />
    </View>
  );
};

export default InterestSettings;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  activeDetailsContainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginBottom: 15,
  },
  detailsHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  headerIcon: {
    width: 25,
    height: 25,
    marginRight: 10,
  },
  headerTitle: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  details: {
    padding: 20,
  },
  detailsItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  detailLabel: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
  detailValue: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'right',
    textTransform: 'capitalize',
  },
});
