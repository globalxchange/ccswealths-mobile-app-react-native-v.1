import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const EmailType = ({onEmailClick, onBlockCheck}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onBlockCheck} style={styles.outlinedButton}>
        <Text style={styles.outlinedButtonText}>Use BlockCheck</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={onEmailClick} style={styles.filledButton}>
        <Text style={styles.filledButtonText}>Use Custom Email</Text>
      </TouchableOpacity>
    </View>
  );
};

export default EmailType;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 60,
  },
  outlinedButton: {
    borderColor: '#BDBDBD',
    borderWidth: 1,
    borderRadius: 6,
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
  },
  outlinedButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
  },
  filledButton: {
    borderRadius: 6,
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    marginTop: 20,
  },
  filledButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: 'white',
    textAlign: 'center',
  },
});
