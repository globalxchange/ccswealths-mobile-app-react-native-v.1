import Clipboard from '@react-native-community/clipboard';
import React, {useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import * as WebBrowser from 'expo-web-browser';
import ThemeData from '../../configs/ThemeData';

const Success = ({username, onClose, selectedApp}) => {
  const [isCopied, setIsCopied] = useState(false);

  const copyText = 'https://complete.setupmywallet.com';

  const onCompleteClick = () => {
    WebBrowser.openBrowserAsync(copyText);
  };

  const onCopyHandler = () => {
    Clipboard.setString(copyText);

    setIsCopied(true);

    setTimeout(() => setIsCopied(false), 10000);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>{username || 'Shorupan'} Has Been</Text>
      <Text style={styles.decs}>1. Added To Your Network in CCSWealth.</Text>
      <Text style={styles.decs}>
        2. Has Been Emailed Temporary Login Credentials For{' '}
        {selectedApp?.app_name}
      </Text>
      <TouchableOpacity onPress={onClose} style={styles.buttonOutlined}>
        <Text style={styles.buttonOutlinedText}>Send Them The App</Text>
      </TouchableOpacity>

      <TouchableOpacity disabled onPress={onCopyHandler}>
        <View style={[styles.button, {opacity: 0.4}]}>
          <Text style={styles.buttonText}>Issue Promotion</Text>
        </View>
      </TouchableOpacity>
      {/* <Image
        style={styles.nextIcon}
        source={require('../../assets/next-forward-icon.png')}
        resizeMode="contain"
      />
      <Text style={styles.subHeader}>
        Click This Like Your Life Depended On It
      </Text> */}
    </View>
  );
};

export default Success;

const styles = StyleSheet.create({
  container: {
    paddingTop: 30,
    paddingVertical: 20,
  },
  header: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
    fontSize: 22,
  },
  decs: {
    fontFamily: 'Montserrat-SemiBold',
    color: ThemeData.APP_MAIN_COLOR,
    marginTop: 15,
  },
  button: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
  },
  subHeader: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
  },
  nextIcon: {
    marginLeft: 'auto',
    marginRight: 'auto',
    height: 20,
    width: 20,
    paddingVertical: 30,
  },
  buttonOutlined: {
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderColor: ThemeData.APP_MAIN_COLOR,
    borderWidth: 1,
    marginBottom: 20,
    marginTop: 40,
  },
  buttonOutlinedText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-SemiBold',
  },
});
