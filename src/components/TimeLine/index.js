import React from 'react';
import {View, StyleSheet} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import StepItem from './StepItem';

const TimeLine = ({currentPath, currentStep, currentInstitution}) => {
  // console.log('currentPath', currentPath);
  // console.log('currentStep', currentStep);
  // console.log('currentInstitution', currentInstitution);

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {currentPath.stepsArray.map((item, index) => (
          <StepItem
            data={item}
            isCurrent={currentStep.status === item.status}
          />
        ))}
      </ScrollView>
    </View>
  );
};

export default TimeLine;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginVertical: 30,
  },
});
