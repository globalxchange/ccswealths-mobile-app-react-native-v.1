import React, {useState, useEffect, useContext, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  TouchableOpacity,
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native';
import Axios from 'axios';
import {AppContext} from '../../../contexts/AppContextProvider';
import {GX_API_ENDPOINT, APP_CODE} from '../../../configs';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import QuoteInput from '../QuoteInput';
import {WToast, WModalShowToastView} from 'react-native-smart-tip';
import {v4 as uuidv4} from 'uuid';
import CryHelper from '../../../utils/CryHelper';
import {formatterHelper} from '../../../utils';
import LoadingView from './LoadingView';
import CompleteView from './CompleteView';
import TransactionSteps from './TransactionSteps';
import FeeBreakdown from './FeeBreakdown';
import CheckoutEasyEntry from '../../CheckoutEasyEntry';
import ThemeData from '../../../configs/ThemeData';

let axiosToken = null;

const PaymentQuote = ({
  pathData,
  onClose,
  openTxnAudit,
  selectedCrypto,
  paymentType,
  paymentCurrency,
  selectedCountry,
  selectedBanker,
  setIsExpanded,
  setIsOpenFeeAudit,
}) => {
  const {updateWalletBalances, cryptoTableData} = useContext(AppContext);

  const [path, setPath] = useState();
  const [minimumValue, setMinimumValue] = useState(0);
  const [showView, setShowView] = useState(false);
  const [spendValue, setSpendValue] = useState('');
  const [gettingValue, setGettingValue] = useState('');
  const [errorText, setErrorText] = useState('');
  const [purchaseLoading, setPurchaseLoading] = useState(false);
  const [isSpendLoading, setIsSpendLoading] = useState(false);
  const [isGettingLoading, setIsGettingLoading] = useState(false);
  const [spendEdited, setSpendEdited] = useState();

  const [isLoading, setIsLoading] = useState(false);
  const [showFees, setShowFees] = useState(false);
  const [showSteps, setShowSteps] = useState(false);
  const [isCompeted, setIsCompeted] = useState(false);
  const [isInstantFund, setIsInstantFund] = useState(false);
  const [fundCurrency, setFundCurrency] = useState('');
  const [fundValue, setFundValue] = useState('');
  const [quoteResp, setQuoteResp] = useState();

  const spendLoaded = useRef(false);
  const getLoaded = useRef(false);
  const toastRef = useRef();

  useEffect(() => {
    // console.log('Path Data', pathData);
    // console.log('selectedCountry.value', selectedCountry.value);
    // console.log('selectedCrypto.coinSymbol', selectedCrypto.coinSymbol);
    // console.log('paymentCurrency.coinSymbol', paymentCurrency.coinSymbol);
    // console.log('paymentType.code', paymentType.code);
    // console.log('selectedBanker.bankerTag', selectedBanker.bankerTag);

    if (cryptoTableData && pathData) {
      pathData.forEach((pathItem) => {
        if (
          pathItem.country ===
            (selectedCountry.formData
              ? selectedCountry.formData.Name
              : selectedCountry.value) &&
          pathItem.to_currency === selectedCrypto.coinSymbol &&
          pathItem.from_currency === paymentCurrency.coinSymbol &&
          (pathItem.depositMethod === paymentType.code ||
            pathItem.paymentMethod === paymentType.code) &&
          pathItem.banker === selectedBanker.bankerTag
        ) {
          // console.log('Found Path', pathItem);
          if (pathItem.select_type === 'instantFund') {
            setIsInstantFund(true);
            const fund = cryptoTableData.find(
              (x) => x.coinSymbol === pathItem.deposit_currency,
            );
            setFundCurrency(fund);
            const minValue =
              pathItem.instant_fees.banker_fixed_fee +
              pathItem.instant_fees.gx_fixed_fee +
              pathItem.fund_fees.banker_fixed_fee +
              pathItem.fund_fees.gx_fixed_fee;

            getMinimumValue(minValue);
          } else {
            setIsInstantFund(false);
            getMinimumValue(pathItem.banker_fixed_fee + pathItem.gx_fixed_fee);
          }
          setPath(pathItem);
          return;
        }
      });
    }
  }, [
    pathData,
    selectedCountry,
    selectedCrypto,
    paymentCurrency,
    paymentType,
    selectedBanker,
    cryptoTableData,
  ]);

  useEffect(() => {
    if (!getLoaded.current) {
      getApproxQuote(gettingValue, false, true);
      setErrorText('');
    }
  }, [gettingValue]);

  useEffect(() => {
    if (!spendLoaded.current) {
      getApproxQuote(spendValue, true, true);
      setErrorText('');
    }
  }, [spendValue]);

  const getMinimumValue = (value) => {
    Axios.get(`${GX_API_ENDPOINT}/forex/convert`, {
      params: {buy: paymentCurrency.coinSymbol, from: 'USD'},
    })
      .then((resp) => {
        const {data} = resp;

        if (data.status) {
          const conversionRate =
            data[`usd_${paymentCurrency.coinSymbol.toLowerCase()}`];

          setMinimumValue(conversionRate * value);
        } else {
          setMinimumValue(value);
        }
      })
      .catch((error) => {
        setMinimumValue(value);
        console.log('Conversion Rate error', error);
      })
      .finally(() => setShowView(true));
  };

  const getApproxQuote = async (value, isSpending, showQuote) => {
    if (isNaN(parseFloat(value)) && !showQuote) {
      setGettingValue('');
      return toastRef.current({
        data: 'Please Input A Valid value',
        position: WToast.position.TOP,
      });
    }
    if (!path || isNaN(parseFloat(value)) || parseFloat(value) <= 0) {
      return;
    }
    if (!showQuote && isSpending && parseFloat(spendValue) < minimumValue) {
      return toastRef.current({
        data: `The Minimum Value is ${formatterHelper(
          minimumValue,
          paymentCurrency.coinSymbol,
        )}`,
        position: WToast.position.TOP,
      });
    }

    if (axiosToken) {
      axiosToken.cancel();
      // setIsLoading(false);
    }

    if (!showQuote) {
      setIsLoading(true);
    }

    showQuote
      ? isSpending
        ? setIsGettingLoading(true)
        : setIsSpendLoading(true)
      : setPurchaseLoading(true);

    axiosToken = Axios.CancelToken.source();
    const profileId = await AsyncStorageHelper.getProfileId();
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    let postData = {
      email,
      token,
      app_code: APP_CODE,
      profile_id: profileId,
      stats: showQuote,
      identifier: uuidv4(),
      path_id: path.path_id,
    };

    if (isSpending) {
      postData = {
        ...postData,
        purchased_from: paymentCurrency.coinSymbol,
        from_amount: parseFloat(value),
      };
    } else {
      postData = {
        ...postData,
        coin_purchased: selectedCrypto.coinSymbol,
        purchased_amount: parseFloat(value),
      };
    }

    // console.log('Post Data', postData);

    const encryptedData = CryHelper.encryptPostData(postData);

    setQuoteResp();

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/trade/execute`,
      {data: encryptedData},
      {
        cancelToken: axiosToken.token,
      },
    )
      .then((resp) => {
        const {data} = resp;

        // console.log('Quote Resp', data);

        if (isInstantFund) {
          const [fundQuote, instaQuote] = data;

          if (fundQuote.status && instaQuote.status) {
            if (!showQuote) {
              updateWalletBalances();
              setIsCompeted(true);
              setTimeout(() => {
                setIsLoading(false);
              }, 1000);
              toastRef.current({
                data: data.message,
                position: WToast.position.TOP,
              });
              setPurchaseLoading(false);
              return;
            }

            setQuoteResp(data);

            const fundAmount = isSpending
              ? fundQuote.finalToAmount
              : fundQuote.finalFromAmount;

            const instantAmount = isSpending
              ? instaQuote.finalToAmount
              : instaQuote.finalFromAmount;

            setFundValue(formatterHelper(fundAmount, fundCurrency.coinSymbol));

            isSpending
              ? (getLoaded.current = true)
              : (spendLoaded.current = true);

            isSpending
              ? setGettingValue(
                  formatterHelper(instantAmount, selectedCrypto.coinSymbol),
                )
              : setSpendValue(
                  formatterHelper(instantAmount, paymentCurrency.coinSymbol),
                );

            setErrorText('');

            isSpending ? setIsGettingLoading(false) : setIsSpendLoading(false);
          } else {
            if (!fundQuote.status) {
              toastRef.current({
                data: fundQuote.message,
                position: WToast.position.TOP,
              });
            }

            if (!instaQuote.status) {
              toastRef.current({
                data: instaQuote.message,
                position: WToast.position.TOP,
              });
            }

            if (showQuote) {
              setSpendValue(formatterHelper(0, paymentCurrency.coinSymbol));
              isSpending
                ? setIsGettingLoading(false)
                : setIsSpendLoading(false);
            } else {
              setIsLoading(false);
              setPurchaseLoading(false);
            }
          }
        } else {
          if (data.status) {
            if (!showQuote) {
              updateWalletBalances();
              setIsCompeted(true);
              setTimeout(() => {
                setIsLoading(false);
              }, 1000);
              toastRef.current({
                data: data.message,
                position: WToast.position.TOP,
              });
              setPurchaseLoading(false);
              return;
            }

            setQuoteResp(data);

            const cryptoAmount = isSpending
              ? data.finalToAmount
              : data.finalFromAmount;

            isSpending
              ? (getLoaded.current = true)
              : (spendLoaded.current = true);

            isSpending
              ? setGettingValue(
                  formatterHelper(cryptoAmount, selectedCrypto.coinSymbol),
                )
              : setSpendValue(
                  formatterHelper(cryptoAmount, paymentCurrency.coinSymbol),
                );
            setErrorText('');

            isSpending ? setIsGettingLoading(false) : setIsSpendLoading(false);
          } else {
            if (data.message !== 'System Error') {
              toastRef.current({
                data: data.message,
                position: WToast.position.TOP,
              });
            }
            if (showQuote) {
              setSpendValue(formatterHelper(0, paymentCurrency.coinSymbol));
              isSpending
                ? setIsGettingLoading(false)
                : setIsSpendLoading(false);
            } else {
              setIsLoading(false);
              setPurchaseLoading(false);
            }
          }
        }
      })
      .catch((error) => {
        console.log('Error getting quote', error);
        if (!showQuote) {
          setIsLoading(false);
        }
      })
      .finally(() => {
        resetFlags();
      });
  };

  const buyClickHandler = async () => {
    if (purchaseLoading) {
      return;
    }

    const value = spendEdited ? spendValue : gettingValue;

    if (isNaN(parseFloat(value)) || !parseFloat(value)) {
      return toastRef.current({
        data: 'Please Enter A Valid Value',
        position: WToast.position.TOP,
      });
    }

    if (parseFloat(spendValue) < minimumValue) {
      return toastRef.current({
        data: `The Minimum Value is ${formatterHelper(
          minimumValue,
          paymentCurrency.coinSymbol,
        )}`,
        position: WToast.position.TOP,
      });
    }

    // send the last input to trade api
    // getApproxQuote(spendEdited ? spendValue : gettingValue, spendEdited, false);
    setShowSteps(true);
  };

  const resetFlags = () => {
    setTimeout(() => {
      getLoaded.current = false;
      spendLoaded.current = false;
    }, 200);
  };

  const onSpendEdit = (value) => {
    setSpendValue(value);
    setSpendEdited(true);
  };

  const onGetEdit = (value) => {
    setGettingValue(value);
    setSpendEdited(false);
  };

  const openFeeBreakdown = () => {
    const value = spendEdited ? spendValue : gettingValue;

    if (isNaN(parseFloat(value)) || !parseFloat(value)) {
      return toastRef.current({
        data: 'Please Enter A Valid Value',
        position: WToast.position.TOP,
      });
    }

    setShowFees(true);
  };

  if (isLoading || !showView) {
    return <LoadingView />;
  }

  if (isCompeted) {
    return (
      <CompleteView
        sellCrypto={paymentCurrency}
        sellValue={spendValue}
        buyCrypto={selectedCrypto}
        buyValue={gettingValue}
        selectedBanker={selectedBanker.bankerTag}
        openTxnAudit={openTxnAudit}
        setIsLoading={setIsLoading}
      />
    );
  }

  if (showFees) {
    return (
      <FeeBreakdown
        onClose={() => setShowFees(false)}
        selectedCrypto={selectedCrypto}
        paymentCurrency={paymentCurrency}
        spendValue={spendValue}
        gettingValue={gettingValue}
        setIsExpanded={setIsExpanded}
        setIsOpenFeeAudit={setIsOpenFeeAudit}
        quoteResp={quoteResp}
        spendEdited={spendEdited}
        isInstantFund={isInstantFund}
      />
    );
  }

  if (showSteps) {
    return (
      <TransactionSteps
        stepsData={path.total_steps}
        onClose={onClose}
        selectedPath={path}
        selectedBanker={selectedBanker}
        selectedCrypto={selectedCrypto}
        paymentCurrency={paymentCurrency}
        executeData={{
          spendValue: spendValue,
          gettingValue: gettingValue,
          isSpendEdited: spendEdited,
        }}
        setIsCompeted={setIsCompeted}
        isInstantFund={isInstantFund}
      />
    );
  }

  return (
    <View style={styles.container}>
      <ScrollView
        bounces={false}
        contentContainerStyle={[
          {
            justifyContent: 'center',
          },
          !isInstantFund && {flex: 1},
        ]}
        showsVerticalScrollIndicator={false}>
        <TouchableWithoutFeedback>
          <View>
            <WModalShowToastView
              toastInstance={(toastInstance) =>
                (toastRef.current = toastInstance)
              }
            />
            <View style={styles.headerContinuer}>
              <Text style={styles.header}>Quote</Text>
              <TouchableOpacity
                style={styles.feeButton}
                onPress={openFeeBreakdown}>
                <Text style={[styles.feeText]}>Official Fee Audit</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.quoteContainer}>
              <QuoteInput
                image={{
                  uri: paymentCurrency.coinImage,
                }}
                unit={paymentCurrency.coinSymbol}
                enabled
                title={
                  isInstantFund
                    ? 'You Will Be Sending'
                    : `How Much ${paymentCurrency.coinSymbol} Do You Want To Sell?`
                }
                placeholder={formatterHelper('0', paymentCurrency.coinSymbol)}
                value={spendValue}
                setValue={onSpendEdit}
                isLoading={isSpendLoading}
              />
            </View>
            {isInstantFund && (
              <QuoteInput
                image={{
                  uri: fundCurrency.coinImage,
                }}
                unit={fundCurrency.coinSymbol}
                title="Your Account Will Be Receiving A Deposit Of"
                value={fundValue.toString()}
                enabled={false}
                setValue={onGetEdit}
                placeholder={formatterHelper('0', fundCurrency.coinSymbol)}
                isLoading={isGettingLoading || isSpendLoading}
              />
            )}
            <QuoteInput
              image={{
                uri: selectedCrypto.coinImage,
              }}
              unit={selectedCrypto.coinSymbol}
              title={
                isInstantFund
                  ? 'If Deposited Now, Would Convert Into'
                  : `This Is How Much ${selectedCrypto.coinName} You Will Be Getting`
              }
              enabled={!isInstantFund}
              value={gettingValue.toString()}
              setValue={onGetEdit}
              placeholder={formatterHelper('0', selectedCrypto.coinSymbol)}
              isLoading={isGettingLoading}
            />
            {errorText !== '' && (
              <View>
                <Text style={styles.errorText}>{errorText}</Text>
              </View>
            )}
            <View style={[styles.actionContainer]}>
              <CheckoutEasyEntry
                sellCurrency={paymentCurrency}
                buyCurrency={selectedCrypto}
                headerImage={require('../../../assets/insta-buy.png')}
                setSellCurrencyInput={onSpendEdit}
              />
              <TouchableOpacity
                style={[
                  styles.buyBtn,
                  (isSpendLoading || isGettingLoading) && styles.disabled,
                ]}
                onPress={buyClickHandler}
                disabled={isSpendLoading || isGettingLoading}>
                {purchaseLoading ? (
                  <>
                    <Text style={styles.buyBtnText}>Purchasing</Text>
                    <ActivityIndicator
                      style={{marginLeft: 20}}
                      size="small"
                      color="white"
                    />
                  </>
                ) : (
                  <Text style={styles.buyBtnText}>
                    {/* Purchase {gettingValue || 0} {selectedCrypto.coinName}s */}
                    Confirm Quote
                  </Text>
                )}
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>
    </View>
  );
};

export default PaymentQuote;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'space-between',
  },
  feeButton: {
    marginLeft: 'auto',
  },
  feeText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat',
    fontSize: 13,
    paddingBottom: 5,
    textDecorationLine: 'underline',
  },
  headerContinuer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'baseline',
    marginBottom: 10,
  },
  header: {
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'left',
    fontSize: 35,
    fontFamily: 'Montserrat-Bold',
  },
  disabled: {
    opacity: 0.5,
  },
  errorText: {
    color: '#FF2D55',
    textAlign: 'center',
    marginBottom: 20,
    marginTop: -10,
  },
  actionContainer: {
    flexDirection: 'row',
    marginTop: 30,
  },
  buyBtn: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    height: 45,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    borderRadius: 6,
  },
  buyBtnText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 13,
    fontFamily: 'Montserrat-SemiBold',
  },
  easyEntryBtn: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    flex: 1,
    marginRight: 15,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  easyEntryText: {
    fontSize: 13,
    fontFamily: 'Montserrat-SemiBold',
    color: '#9A9A9A',
  },
});
