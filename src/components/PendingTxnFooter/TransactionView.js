import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import TransactionsPopup from './TransactionsPopup';
import TxnsList from './TxnsList';
import {
  faChevronDown,
  faLongArrowAltUp,
} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';

const TransactionView = ({
  setIsTxnPopupOpen,
  setSelectedTnx,
  isDeposit,
  isTxnPopupOpen,
  isAllList,
  selectedTnx,
  setIsAllList,
  onBackPress,
  activeList,
  allList,
  listTypes,
}) => {
  const openTransaction = (item) => {
    // alert('dfssd');
    setSelectedTnx(item);
    setIsTxnPopupOpen(true);
  };

  return (
    <View>
      <View style={styles.headerContainer}>
        <TouchableOpacity style={styles.toggleButton} onPress={onBackPress}>
          <FontAwesomeIcon
            icon={faChevronDown}
            color={ThemeData.APP_MAIN_COLOR}
          />
        </TouchableOpacity>
        <Text style={styles.activeTypoe}>{listTypes}</Text>
        <TouchableOpacity
          style={styles.toggleButton}
          onPress={() => {
            setIsTxnPopupOpen(!isTxnPopupOpen);
            if (activeList?.length <= 0) {
              setIsAllList(true);
            }
          }}>
          <FontAwesomeIcon
            icon={faLongArrowAltUp}
            color={ThemeData.APP_MAIN_COLOR}
            transform={{rotate: 45}}
          />
        </TouchableOpacity>
      </View>
      <TxnsList
        isDeposit={isDeposit}
        list={activeList}
        openTransaction={openTransaction}
        listTypes={listTypes}
      />
      <TransactionsPopup
        isOpen={isTxnPopupOpen}
        onClose={() => setIsTxnPopupOpen(false)}
        list={isAllList ? allList : activeList}
        isDeposit={isDeposit}
        selectedTnx={selectedTnx}
        isAllList={isAllList}
        setIsAllList={setIsAllList}
        listTypes={listTypes}
      />
    </View>
  );
};

export default TransactionView;

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    flexDirection: 'row',
    paddingVertical: 8,
    paddingHorizontal: 15,
  },
  toggleButton: {
    backgroundColor: 'white',
    padding: 2,
    borderRadius: 2,
  },
  activeTypoe: {
    color: 'white',
    textAlign: 'center',
    flex: 1,
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
  },
});
