/* eslint-disable react-native/no-inline-styles */
import {useNavigation} from '@react-navigation/native';
import Axios from 'axios';
import React, {useContext, useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import {formatterHelper, getAssetData, isCrypto} from '../../utils';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import LoadingAnimation from '../LoadingAnimation';
import * as WebBrowser from 'expo-web-browser';

const TransactionDetails = ({selectedTransaction, onClose}) => {
  const navigation = useNavigation();

  const {cryptoTableData, pathData, getBlockCheckData} = useContext(AppContext);

  const [isLoading, setIsLoading] = useState(false);

  const cancellTransaction = async () => {
    setIsLoading(true);
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const selectedPath = pathData.find(
      (item) => item.path_id === selectedTransaction?.path_id,
    );

    const steps = selectedPath.total_steps;

    const stepsArray = [];

    if (steps) {
      const entries = Object.entries(steps);

      for (const [key, stepData] of entries) {
        stepsArray.push({key, ...stepData});
      }
    }

    const postData = {
      token,
      admin_email: email,
      additional_data: {},
      id: selectedTransaction._id.toString(),
      step: stepsArray[stepsArray.length - 1]
        ? stepsArray[stepsArray.length - 1].key
        : 'cancelled',
      reason_terminated: 'Cancelled by the user',
    };
    // console.log('postData', postData);

    const url = selectedTransaction.deposit
      ? `${GX_API_ENDPOINT}/coin/vault/service/admin/update/txn/status`
      : `${GX_API_ENDPOINT}/coin/vault/service/admin/cancel/withdraw/txn`;

    Axios.post(url, postData)
      .then(({data}) => {
        // console.log('Data', data);

        if (data.status) {
          getBlockCheckData();
          onClose();
          WToast.show({
            data: 'The Transaction has been cancelled',
            position: WToast.position.TOP,
          });
        } else {
          WToast.show({
            data: data.message || 'Error on Cancelling Transaction',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        console.log('Error on update step', error);
      })
      .finally(() => setIsLoading(false));
  };

  const openSupportChat = () => {
    onClose();

    navigation.navigate('Support', {
      openMessage: true,
      messageData: '',
    });
  };

  const openBlockCheck = () => {
    WebBrowser.openBrowserAsync(
      `https://blockcheck.io/#/${selectedTransaction?._id}`,
    );
  };

  const openCryptoBank = () => {
    WebBrowser.openBrowserAsync(
      `https://cryptobankwire.com/account/${selectedTransaction?._id}`,
    );
  };

  const openCryptoBankWire = () => {
    const selectedPath = pathData.find(
      (item) => item.path_id === selectedTransaction?.path_id,
    );
    // console.log('Path', selectedPath);
    WebBrowser.openBrowserAsync(selectedPath?.instant_link || '');
  };

  const ACTIONS = [
    {
      title: 'Blockcheck',
      onPress: openBlockCheck,
      icon: require('../../assets/block-check-full-logo.png'),
    },
    {
      title: 'ATMS',
      onPress: openCryptoBankWire,
      icon: require('../../assets/atms-icon.png'),
      disabled: isCrypto(selectedTransaction?.coin),
    },
    {
      title: 'Vault',
      onPress: null,
      icon: require('../../assets/vault-logo.png'),
      // disabled: true,
    },
    {
      title: 'Contact Support',
      onPress: openSupportChat,
      icon: require('../../assets/support-full-icon.png'),
    },
    {
      title: 'Cancel Transaction',
      onPress: cancellTransaction,
      icon: require('../../assets/cancel-full-icon.png'),
    },
  ];

  if (isLoading) {
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <LoadingAnimation />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.transactionItem}>
        <Image
          source={{
            uri: getAssetData(selectedTransaction?.coin, cryptoTableData)
              ?.coinImage,
          }}
          style={styles.txnIcon}
          resizeMode="contain"
        />
        <Text style={styles.txnAsset}>{selectedTransaction?.coin}</Text>
        <Text style={styles.txnValue}>
          {/* {selectedTransaction?.deposit ? '+' : '-'}{' '} */}
          {formatterHelper(
            (selectedTransaction.deposit
              ? selectedTransaction.amt_to_credit
              : selectedTransaction.final_buy_amount) ||
              selectedTransaction.amount ||
              0,
            selectedTransaction?.coin,
          )}
        </Text>
      </View>
      <View style={styles.actionContainer}>
        <ScrollView showsVerticalScrollIndicator={false}>
          {ACTIONS.map((item) => (
            <TouchableOpacity
              item={item.disabled}
              key={item.title}
              onPress={item.onPress}>
              <View
                style={[styles.actionItem, item.disabled && {opacity: 0.5}]}>
                <Image
                  source={item.icon}
                  resizeMode="contain"
                  style={styles.actionIcon}
                />
                {/* <Text style={styles.actionTitle}>{item.title}</Text> */}
              </View>
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
    </View>
  );
};

export default TransactionDetails;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginHorizontal: 25,
    marginTop: 25,
  },
  transactionItem: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    alignItems: 'center',
    marginHorizontal: 15,
    paddingVertical: 10,
    marginBottom: 10,
    backgroundColor: 'white',
    marginTop: 10,
  },
  txnIcon: {
    width: 25,
    height: 25,
  },
  txnAsset: {
    flex: 1,
    marginHorizontal: 10,
    fontFamily: ThemeData.FONT_BOLD,
    color: '#001D41',
    fontSize: 18,
  },
  txnValue: {
    color: '#001D41',
    fontFamily: 'Roboto',
    fontSize: 16,
    opacity: 0.5,
  },
  actionContainer: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  actionItem: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 15,
    paddingVertical: 20,
    marginBottom: 10,
  },
  actionTitle: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#001D41',
  },
  actionIcon: {
    width: 100,
    height: 26,
  },
});
