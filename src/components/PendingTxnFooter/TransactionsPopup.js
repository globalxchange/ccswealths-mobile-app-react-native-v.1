/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useState} from 'react';
import {
  Dimensions,
  Image,
  SectionList,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import PopupLayout from '../../layouts/PopupLayout';
import Moment from 'moment-timezone';
import {AppContext} from '../../contexts/AppContextProvider';
import {formatterHelper, getAssetData} from '../../utils';
import ThemeData from '../../configs/ThemeData';
import TransactionDetails from './TransactionDetails';
import {useNavigation} from '@react-navigation/native';

const {width, height} = Dimensions.get('window');

const TransactionsPopup = ({
  isOpen,
  onClose,
  list,
  isDeposit,
  selectedTnx,
  isAllList,
  setIsAllList,
  listTypes,
}) => {
  const navigation = useNavigation();

  const {top, bottom} = useSafeAreaInsets();

  const [isExpanded, setIsExpanded] = useState(false);
  const [searchInput, setSearchInput] = useState('');
  const [isSearchFocused, setIsSearchFocused] = useState(false);
  const [selectedTransaction, setSelectedTransaction] = useState();

  useEffect(() => {
    if (!isOpen) {
      setIsExpanded(false);
      setSearchInput('');
      setIsSearchFocused(false);
      setSelectedTransaction();
    }
  }, [isOpen]);

  useEffect(() => {
    setSelectedTransaction(selectedTnx);
  }, [selectedTnx]);

  const openTimeline = () => {
    onClose();
    navigation.navigate('Timeline', {
      screen: 'List',
      params: {data: selectedTransaction},
    });
  };

  const onBackPress = () => {
    if (selectedTransaction) {
      setSelectedTransaction();
    } else {
      onClose();
    }
  };

  let tempList = list || [];

  const addedList = [];

  const sortedList = [];

  tempList.forEach((tmpItem) => {
    if (!addedList.includes(tmpItem)) {
      const date = Moment(tmpItem.timestamp);
      const subList = [];
      addedList.push(tmpItem);
      subList.push(tmpItem);

      tempList.forEach((item) => {
        if (!addedList.includes(item)) {
          const itemDate = Moment(item.timestamp);
          if (date.isSame(itemDate, 'day')) {
            addedList.push(item);
            subList.push(item);
          }
        }
      });

      sortedList.push({title: date.format('dddd MMMM Do YYYY'), data: subList});
    }
  });

  return (
    <PopupLayout
      noScrollView
      isOpen={isOpen}
      onClose={onClose}
      noHeader
      containerStyles={
        isSearchFocused || isExpanded
          ? {
              height,
              width,
              maxHeight: height,
              paddingTop: top || 20,
              paddingBottom: bottom,
              backgroundColor: 'transparent',
              borderWidth: 0,
            }
          : {}
      }
      contentContainerStyle={{padding: 0}}>
      <View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity onPress={onBackPress} style={styles.actionButton}>
            <Image
              source={require('../../assets/back-icon-white.png')}
              style={styles.actionIcon}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <Text style={styles.headerText}>{listTypes}</Text>
          <TouchableOpacity
            onPress={() => setIsExpanded(!isExpanded)}
            style={[styles.actionButton, {right: 20, left: null}]}>
            <Image
              source={
                isExpanded
                  ? require('../../assets/collapse-icon-white.png')
                  : require('../../assets/expand-icon-white.png')
              }
              style={styles.actionIcon}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
        {selectedTransaction ? null : (
          <View style={styles.searchContainer}>
            <TextInput
              style={styles.searchInput}
              placeholder={'Search Transactions'}
              value={searchInput}
              onChangeText={(text) => setSearchInput(text)}
              placeholderTextColor="#788995"
              onBlur={() => setIsSearchFocused(false)}
              onFocus={() => setIsSearchFocused(true)}
            />
          </View>
        )}
        {selectedTransaction ? (
          <TransactionDetails
            selectedTransaction={selectedTransaction}
            onClose={onClose}
          />
        ) : (
          <View style={styles.listContainer}>
            {list && list.length === 0 ? (
              <View style={[styles.emptyContainer]}>
                <Text style={styles.emptyText}>Not Transactions Found</Text>
              </View>
            ) : (
              <TouchableWithoutFeedback>
                <SectionList
                  showsVerticalScrollIndicator={false}
                  style={styles.transactionList}
                  sections={sortedList}
                  keyExtractor={(item, index) => item + index}
                  renderItem={({item}) => (
                    <TransactionItem
                      onClose={onClose}
                      item={item}
                      onPress={() => setSelectedTransaction(item)}
                    />
                  )}
                  renderSectionHeader={({section: {title}}) => (
                    <TouchableWithoutFeedback>
                      <View style={styles.transactionGroup}>
                        <Text style={styles.date}>{title}</Text>
                      </View>
                    </TouchableWithoutFeedback>
                  )}
                />
              </TouchableWithoutFeedback>
            )}
          </View>
        )}
        <View style={styles.actionContainer}>
          {selectedTransaction ? (
            <TouchableOpacity
              onPress={() => openTimeline()}
              style={styles.filledButton}>
              <Text style={[styles.filledButtonText]}>
                Track My Transaction
              </Text>
            </TouchableOpacity>
          ) : (
            <>
              <TouchableOpacity
                onPress={() => setIsAllList(false)}
                style={[
                  [!isAllList ? styles.filledButton : styles.outlinedButton],
                ]}>
                <Text
                  style={[
                    !isAllList
                      ? styles.filledButtonText
                      : styles.outlinedButtonText,
                  ]}>
                  Processing
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setIsAllList(true)}
                style={[
                  isAllList ? styles.filledButton : styles.outlinedButton,
                ]}>
                <Text
                  style={[
                    isAllList
                      ? styles.filledButtonText
                      : styles.outlinedButtonText,
                  ]}>
                  All
                </Text>
              </TouchableOpacity>
            </>
          )}
        </View>
      </View>
    </PopupLayout>
  );
};

const TransactionItem = ({item, onClose, onPress}) => {
  const {cryptoTableData} = useContext(AppContext);

  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.transactionItem}>
        <Image
          source={{
            uri: getAssetData(item.coin, cryptoTableData)?.coinImage,
          }}
          style={styles.txnIcon}
          resizeMode="contain"
        />
        <Text style={styles.txnAsset}>{item.coin}</Text>
        <Text numberOfLines={1} style={styles.txnValue}>
          {formatterHelper(
            (item.deposit ? item.amt_to_credit : item.final_buy_amount) ||
              item.amount ||
              0,
            item.coin,
          )}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default TransactionsPopup;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  header: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 20,
    justifyContent: 'space-between',
  },
  headerText: {
    fontFamily: 'Montserrat-Bold',
    color: 'white',
    textAlign: 'center',
    flex: 1,
    fontSize: 20,
  },
  actionButton: {
    width: 25,
    padding: 4,
    position: 'absolute',
    zIndex: 6,
    left: 20,
    top: 0,
    bottom: 0,
  },
  actionIcon: {
    flex: 1,
    width: null,
    height: null,
  },
  searchContainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    marginHorizontal: 25,
    marginTop: 20,
  },
  searchInput: {
    height: 40,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
  },
  listContainer: {
    flex: 1,
    paddingHorizontal: 10,
  },
  transactionList: {
    flex: 1,
  },
  transactionGroup: {
    padding: 15,
    backgroundColor: 'white',
  },
  date: {
    fontFamily: 'Montserrat-SemiBold',
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
  transactionItem: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginHorizontal: 15,
    paddingVertical: 10,
    marginBottom: 10,
    backgroundColor: 'white',
  },
  txnIcon: {
    width: 22,
    height: 22,
  },
  txnAsset: {
    flex: 1,
    marginHorizontal: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: '#001D41',
  },
  txnValue: {
    color: '#001D41',
    fontFamily: 'Roboto',
    fontSize: 12,
    opacity: 0.7,
  },
  swipeButton: {
    justifyContent: 'center',
    // flex: 1,
  },
  slideText: {
    color: '#001D41',
    marginBottom: 10,
    paddingRight: 50,
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  emptyContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    marginHorizontal: 15,
    marginVertical: 15,
    flex: 1,
  },
  emptyText: {
    fontFamily: 'Montserrat-SemiBold',
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 18,
  },
  actionContainer: {
    flexDirection: 'row',
  },
  outlinedButton: {
    flex: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  outlinedButtonText: {
    color: '#788995',
    textAlign: 'center',
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 14,
  },
  filledButton: {
    flex: 1,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  filledButtonText: {
    color: 'white',
    textAlign: 'center',
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 14,
  },
});
