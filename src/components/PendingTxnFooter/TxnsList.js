/* eslint-disable react-native/no-inline-styles */
import React, {useContext} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {AppContext} from '../../contexts/AppContextProvider';
import {formatterHelper, getAssetData} from '../../utils';
import Moment from 'moment-timezone';
import ThemeData from '../../configs/ThemeData';
import SkeltonItem from '../SkeltonItem';

const TxnsList = ({isDeposit, list, openTransaction, listTypes}) => {
  return (
    <View style={styles.container}>
      {list ? (
        list.length > 0 ? (
          <FlatList
            contentContainerStyle={{paddingLeft: 5, paddingRight: 15}}
            horizontal
            showsHorizontalScrollIndicator={false}
            data={list}
            keyExtractor={(item) => item._id}
            renderItem={({item}) => (
              <TXNItem item={item} onPress={() => openTransaction(item)} />
            )}
          />
        ) : (
          <View style={styles.emptyContainer}>
            <Text style={styles.emptyText}>No Pending {listTypes}</Text>
          </View>
        )
      ) : (
        <LoadingSkeleton />
      )}
    </View>
  );
};

const LoadingSkeleton = () => (
  <View style={{flexDirection: 'row', paddingLeft: 10, overflow: 'hidden'}}>
    {Array(3)
      .fill(0)
      .map((_, index) => (
        <View key={index} style={styles.itemContainer}>
          <SkeltonItem
            itemHeight={25}
            itemWidth={25}
            style={styles.coinImage}
          />
          <View style={[styles.itemValueContainer, {alignItems: 'flex-end'}]}>
            <SkeltonItem
              itemHeight={20}
              itemWidth={80}
              style={[styles.txnValue, {marginBottom: 2}]}
            />
            <SkeltonItem
              itemHeight={8}
              itemWidth={100}
              style={styles.txnDate}
            />
          </View>
        </View>
      ))}
  </View>
);

const TXNItem = ({item, onPress}) => {
  const {cryptoTableData} = useContext(AppContext);

  const coinData = getAssetData(item.coin, cryptoTableData);

  return (
    <TouchableOpacity style={styles.itemContainer} onPress={onPress}>
      <Image
        source={{uri: coinData?.coinImage}}
        resizeMode="contain"
        style={styles.coinImage}
      />
      <View style={styles.itemValueContainer}>
        <Text numberOfLines={1} style={styles.txnValue}>
          {formatterHelper(
            (item.deposit ? item.amt_to_credit : item.final_buy_amount) ||
              item.amount ||
              0,
            item.coin,
          )}
        </Text>
        <Text numberOfLines={1} style={styles.txnDate}>
          {Moment(item.timestamp).format('MMM Do YYYY')}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default TxnsList;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 8,
  },
  itemContainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    width: 170,
    paddingHorizontal: 10,
    marginLeft: 10,
  },
  coinImage: {
    width: 25,
    height: 25,
    marginRight: 10,
    borderRadius: 12.5,
  },
  itemValueContainer: {
    flex: 1,
  },
  txnValue: {
    color: '#001D41',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    textAlign: 'right',
    fontSize: 16,
  },
  txnDate: {
    color: '#001D41',
    fontFamily: ThemeData.FONT_NORMAL,
    textAlign: 'right',
    opacity: 0.5,
    fontSize: 10,
  },
  emptyContainer: {
    height: 50,
    justifyContent: 'center',
  },
  emptyText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
});
