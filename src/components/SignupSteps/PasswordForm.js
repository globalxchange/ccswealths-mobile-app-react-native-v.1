/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import LoginInputField from '../LoginInputField';

const PasswordForm = ({
  passwordInput,
  isLoading,
  setPasswordInput,
  passwordValidity,
  setPasswordValidity,
  onNextClick,
}) => {
  const [showCheckList, setShowCheckList] = useState(false);
  const [lengthValidity, setLengthValidity] = useState(false);
  const [capitalValidity, setCapitalValidity] = useState(false);
  const [numberValidity, setNumberValidity] = useState(false);
  const [specialCharValidity, setSpecialCharValidity] = useState(false);

  useEffect(() => {
    const capRegex = new RegExp(/^.*[A-Z].*/);
    const numRegex = new RegExp(/^.*[0-9].*/);
    const speRegex = new RegExp(/^.*[!@#$%^&*()+=].*/);

    const password = passwordInput.trim();

    setLengthValidity(password.length >= 6);
    setCapitalValidity(capRegex.test(password));
    setNumberValidity(numRegex.test(password));
    setSpecialCharValidity(speRegex.test(password));
  }, [passwordInput]);

  useEffect(() => {
    if (
      lengthValidity &&
      capitalValidity &&
      numberValidity &&
      specialCharValidity
    ) {
      setPasswordValidity(true);
    } else {
      setPasswordValidity(false);
    }
  }, [lengthValidity, capitalValidity, numberValidity, specialCharValidity]);

  return (
    <View style={styles.loginFromContainer}>
      <Text style={styles.loginTitle}>Register</Text>
      <Text style={styles.loginSubTitle}>Step 5: Set Password</Text>
      <LoginInputField
        // icon={require('../../assets/password-icon.png')}
        autoFocus
        placeholder="PASSWORD"
        type="password"
        secureTextEntry
        value={passwordInput}
        editable={!isLoading}
        onChangeText={(text) => setPasswordInput(text)}
        validatorStatus={passwordValidity}
        showNext
        onNext={onNextClick}
      />
      <Text
        style={styles.checkText}
        onPress={() => setShowCheckList(!showCheckList)}>
        Password Checklist
      </Text>
      {showCheckList && (
        <View style={styles.checklistContainer}>
          <View style={styles.checklistItem}>
            <Text style={styles.checklistTitle}>Should Be 6 characters</Text>
            <View
              style={[
                styles.dot,
                {
                  backgroundColor: lengthValidity
                    ? ThemeData.APP_MAIN_COLOR
                    : '#D80027',
                },
              ]}
            />
          </View>
          <View style={styles.checklistItem}>
            <Text style={styles.checklistTitle}>
              Contains Atleast One Capital Letter
            </Text>
            <View
              style={[
                styles.dot,
                {
                  backgroundColor: capitalValidity
                    ? ThemeData.APP_MAIN_COLOR
                    : '#D80027',
                },
              ]}
            />
          </View>
          <View style={styles.checklistItem}>
            <Text style={styles.checklistTitle}>
              Contains Atleast One Number
            </Text>
            <View
              style={[
                styles.dot,
                {
                  backgroundColor: numberValidity
                    ? ThemeData.APP_MAIN_COLOR
                    : '#D80027',
                },
              ]}
            />
          </View>
          <View style={styles.checklistItem}>
            <Text style={styles.checklistTitle}>
              Contains Atleast One Special Character
            </Text>
            <View
              style={[
                styles.dot,
                {
                  backgroundColor: specialCharValidity
                    ? ThemeData.APP_MAIN_COLOR
                    : '#D80027',
                },
              ]}
            />
          </View>
        </View>
      )}
    </View>
  );
};

export default PasswordForm;

const styles = StyleSheet.create({
  loginFromContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  loginTitle: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
    fontSize: 50,
  },
  loginSubTitle: {
    color: '#999C9A',
    marginBottom: 40,
    fontFamily: 'Montserrat',
  },
  checkText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
  },
  checklistContainer: {
    marginTop: 15,
  },
  checklistItem: {
    flexDirection: 'row',
    paddingVertical: 5,
  },
  checklistTitle: {
    flex: 1,
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat',
  },
  dot: {
    width: 8,
    height: 8,
    borderRadius: 4,
  },
});
