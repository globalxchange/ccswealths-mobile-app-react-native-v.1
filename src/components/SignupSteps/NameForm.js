import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import LoginInputField from '../LoginInputField';

const NameForm = ({
  name,
  setName,
  placeholder = 'Ex. Shorupan',
  title = 'Step 2: Enter First Name',
  onNextClick,
}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.loginTitle}>Register</Text>
      <Text style={styles.loginSubTitle}>{title}</Text>
      <LoginInputField
        // icon={require('../../assets/user-icon.png')}
        placeholder={placeholder}
        type="name"
        value={name}
        onChangeText={(text) => setName(text)}
        showNext
        validatorStatus={name?.trim()?.length > 0}
        onNext={onNextClick}
      />
    </View>
  );
};

export default NameForm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  loginTitle: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
    fontSize: 50,
  },
  loginSubTitle: {
    color: '#999C9A',
    marginBottom: 40,
    fontFamily: 'Montserrat',
  },
});
