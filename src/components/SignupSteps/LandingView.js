import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const LandingView = ({gotoEmail, gotoPromo}) => {
  const onItemClick = (item) => {
    item !== 'Broker' ? gotoEmail() : gotoPromo();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.loginTitle}>Register</Text>
      <Text style={styles.loginSubTitle}>
        Before You Start. We Need To Know How You Found Out About Us
      </Text>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={listItems}
        keyExtractor={(item) => item.tittle}
        renderItem={({item}) => (
          <TouchableOpacity
            style={styles.listItem}
            onPress={() => onItemClick(item.tittle)}>
            <Image
              style={styles.itemIcon}
              source={item.image}
              resizeMode="contain"
            />
            <Text style={styles.itemName}>{item.tittle}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default LandingView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: 30,
  },
  loginTitle: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
    fontSize: 50,
    marginTop: 40,
  },
  loginSubTitle: {
    color: '#999C9A',
    marginBottom: 40,
    fontFamily: 'Montserrat',
  },
  listItem: {
    flexDirection: 'row',
    backgroundColor: 'white',
    borderColor: '#F1F4F6',
    borderWidth: 1,
    paddingVertical: 15,
    paddingHorizontal: 25,
    marginTop: 20,
  },
  itemIcon: {
    width: 22,
    height: 22,
  },
  itemName: {
    fontFamily: 'Montserrat',
    color: '#878788',
    marginLeft: 15,
  },
});

const listItems = [
  {tittle: 'Broker', image: require('../../assets/broker-app-icon-dark.png')},
  // {
  //   tittle: 'Banker',
  //   image: require('../../assets/default-breadcumb-icon/banker.png'),
  // },
  // {tittle: 'Google', image: require('../../assets/google-icon.png')},
  // {tittle: 'YouTube', image: require('../../assets/youtube-icon.png')},
  // {tittle: 'Facebook', image: require('../../assets/facebook-icon.png')},
  // {tittle: 'Instagram', image: require('../../assets/instagram-icon.png')},
  // {
  //   tittle: 'Social Media',
  //   image: require('../../assets/social-media-icon.png'),
  // },
  // {
  //   tittle: 'Email',
  //   image: require('../../assets/email-icon.png'),
  // },
  // {
  //   tittle: 'App Store',
  //   image: require('../../assets/apple-icon.png'),
  // },
];
