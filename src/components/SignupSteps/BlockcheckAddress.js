import React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import NextIconButton from './NextIconButton';

const BlockcheckAddress = ({
  blockcheckId,
  setBlockcheckId,
  onLegacySignup,
  onNextClick,
}) => {
  return (
    <View style={{flex: 1}}>
      <View style={styles.container}>
        <Text style={styles.loginTitle}>Register</Text>
        <Text style={styles.loginSubTitle}>
          Step 4: Create BlockCheck Address
        </Text>
        <View style={styles.inputContainer}>
          <TextInput
            placeholder="EX. shorupan"
            placeholderTextColor={'#878788'}
            style={styles.input}
            value={blockcheckId}
            onChangeText={(text) => setBlockcheckId(text)}
          />
          <Text style={styles.inputPostfix}>@blockcheck.io</Text>
          <NextIconButton
            disabled={!blockcheckId?.length > 0}
            style={{marginLeft: 15}}
            onNext={onNextClick}
          />
        </View>
      </View>
      <TouchableOpacity onPress={onLegacySignup}>
        <Text style={styles.legacySignupButton}>Use Custom Email</Text>
      </TouchableOpacity>
    </View>
  );
};

export default BlockcheckAddress;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  loginTitle: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
    fontSize: 50,
  },
  loginSubTitle: {
    color: '#999C9A',
    marginBottom: 40,
    fontFamily: 'Montserrat',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    flex: 1,
    borderBottomColor: ThemeData.APP_MAIN_COLOR,
    borderBottomWidth: 1,
    paddingHorizontal: 15,
    paddingVertical: 10,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  inputPostfix: {
    marginLeft: 5,
    fontFamily: ThemeData.FONT_MEDIUM,
    color: ThemeData.APP_MAIN_COLOR,
  },
  legacySignupButton: {
    color: '#999C9A',
    fontFamily: ThemeData.FONT_NORMAL,
    textDecorationLine: 'underline',
    fontSize: 15,
  },
});
