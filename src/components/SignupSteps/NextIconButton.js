import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const NextIconButton = ({onNext, style, disabled}) => {
  return (
    <TouchableOpacity disabled={disabled} onPress={onNext}>
      <View style={[styles.nextButton, style, disabled ? {opacity: 0.4} : {}]}>
        <Image
          style={styles.nextImage}
          resizeMode="contain"
          source={require('../../assets/right-arrow-carrot-white.png')}
        />
      </View>
    </TouchableOpacity>
  );
};

export default NextIconButton;

const styles = StyleSheet.create({
  nextButton: {
    height: 30,
    width: 30,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    padding: 8,
    borderRadius: 6,
  },
  nextImage: {
    flex: 1,
    height: null,
    width: null,
  },
});
