import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import LoginInputField from '../LoginInputField';

const ConfirmPassword = ({
  confirmPassword,
  isLoading,
  confirmPasswordValidity,
  setConfirmPassword,
  setPassword,
  goBack,
  password,
  setConfirmPasswordValidity,
  onNextClick,
}) => {
  const onChangePassword = () => {
    setPassword('');
    setConfirmPassword('');
    goBack();
  };

  useEffect(() => {
    setConfirmPasswordValidity(password === confirmPassword);
  }, [confirmPassword, password]);

  return (
    <View style={styles.loginFromContainer}>
      <Text style={styles.loginTitle}>Register</Text>
      <Text style={styles.loginSubTitle}>Step 6: Confirm Password</Text>
      <LoginInputField
        autoFocus
        // icon={require('../../assets/password-icon.png')}
        placeholder="RETYPE PASSWORD"
        type="password"
        secureTextEntry
        value={confirmPassword}
        editable={!isLoading}
        onChangeText={(text) => setConfirmPassword(text)}
        validatorStatus={confirmPasswordValidity}
        showNext
        onNext={onNextClick}
      />
      <Text style={styles.checkText} onPress={onChangePassword}>
        Change Password
      </Text>
    </View>
  );
};

export default ConfirmPassword;

const styles = StyleSheet.create({
  loginFromContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  loginTitle: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
    fontSize: 50,
  },
  loginSubTitle: {
    color: '#999C9A',
    marginBottom: 40,
    fontFamily: 'Montserrat',
  },
  checkText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
  },
});
