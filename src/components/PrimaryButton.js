import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import ThemeData from '../configs/ThemeData';

const PrimaryButton = ({title, style, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={[styles.filledButton, style]}>
      <Text style={styles.buttonText}>{title}</Text>
    </TouchableOpacity>
  );
};

export default PrimaryButton;

const styles = StyleSheet.create({
  filledButton: {
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    flex: 1,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
});
