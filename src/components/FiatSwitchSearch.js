import {useRoute} from '@react-navigation/native';
import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {AppContext} from '../contexts/AppContextProvider';
import SearchLayout from '../layouts/SearchLayout';
import {formatterHelper} from '../utils';

const FiatSwitchSearch = ({walletBalances, setActiveCrypto, onClose}) => {
  const {params, name} = useRoute();
  const {walletCoinData} = useContext(AppContext);

  const [searchInput, setSearchInput] = useState('');
  const [coinList, setCoinList] = useState();

  useEffect(() => {
    if (name === 'AddForexStepOne' || name === 'WithdrawForexStepOne') {
      const tempArr = [];
      walletBalances.map((item) => {
        tempArr.push({
          ...item,
          coinName: item.coin_metadata.coinName,
          coinImage: item.coin_metadata.coinImage,
        });
      });

      setCoinList(tempArr);
    } else if (name === 'AddForexStepTwo' || name === 'WithdrawForexStepTwo') {
      const tempArr = [];
      walletBalances.map((item) => {
        tempArr.push({
          ...item,
          coinName: item.metadata.name,
          coinImage: item.metadata.image,
        });
      });

      setCoinList(tempArr);
    } else if (
      name === 'AddForexStepThree' ||
      name === 'WithdrawForexStepThree'
    ) {
      const tempArr = [];
      walletBalances.map((item) => {
        tempArr.push({
          ...item,
          coinName: item.metadata.name,
          coinImage: item.metadata.icon,
        });
      });

      setCoinList(tempArr);
    } else if (
      name === 'AddForexStepFour' ||
      name === 'WithdrawForexStepFour'
    ) {
      const tempArr = [];
      walletBalances.map((item) => {
        tempArr.push({
          ...item,
          coinName: item.displayName,
          coinImage: item.icons.image1,
        });
      });

      setCoinList(tempArr);
    } else if (name === 'accountPaymentType') {
      setCoinList(walletBalances);
    } else if (name === 'bankType') {
      const tempArr = [];
      walletBalances.map((item) => {
        tempArr.push({
          ...item,
          coinName: item.institute_name,
          coinImage: item.profile_image,
        });
      });
      setCoinList(tempArr);
    } else if (name === 'selectCountry') {
      const tempArr = [];
      walletBalances.map((item) => {
        tempArr.push({
          ...item,
          coinName: item.name,
          coinImage: item.image,
        });
      });
      setCoinList(tempArr);
    }
  }, [walletCoinData, searchInput]);

  const onItemSelected = (item) => {
    setActiveCrypto(item);
    onClose();
  };

  return (
    <View style={styles.container}>
      <SearchLayout
        value={searchInput}
        setValue={setSearchInput}
        list={coinList || []}
        onBack={onClose}
        disableFilter
        onSubmit={(_, item) => onItemSelected(item)}
        showUserList
        placeholder=""
      />
    </View>
  );
};

export default FiatSwitchSearch;

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFill,
    zIndex: 10000,
  },
});
