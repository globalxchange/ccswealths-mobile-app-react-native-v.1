import React from 'react';
import {StyleSheet, View} from 'react-native';
import LoadingAnimation from '../../LoadingAnimation';

const LoadingView = () => {
  return (
    <View style={styles.container}>
      <LoadingAnimation />
    </View>
  );
};

export default LoadingView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -80,
    marginHorizontal: -30,
    backgroundColor: 'white',
  },
});
