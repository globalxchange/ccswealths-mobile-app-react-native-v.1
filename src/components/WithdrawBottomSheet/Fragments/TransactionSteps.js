import React, {useState, useEffect, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native';
import LoadingAnimation from '../../LoadingAnimation';
import {useNavigation} from '@react-navigation/native';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import {APP_CODE, GX_API_ENDPOINT} from '../../../configs';
import {v4 as uuidv4} from 'uuid';
import CryHelper from '../../../utils/CryHelper';
import Axios from 'axios';
import {WToast} from 'react-native-smart-tip';
import {AppContext} from '../../../contexts/AppContextProvider';
import ThemeData from '../../../configs/ThemeData';

const TransactionSteps = ({
  stepsData,
  onClose,
  selectedPath,
  selectedBanker,
  executeData,
  selectedCrypto,
  paymentCurrency,
  setIsCompeted,
}) => {
  const {updateWalletBalances} = useContext(AppContext);

  const navigation = useNavigation();

  const [steps, setSteps] = useState();
  const [isLoading, setIsLoading] = useState(false);

  const {spendValue, gettingValue, isSpendEdited} = executeData;

  useEffect(() => {
    if (stepsData) {
      const stepArr = [];
      const stepKeys = Object.keys(stepsData);
      for (let i = 0; i < stepKeys.length; i++) {
        const stepKey = stepKeys[i];
        const parsedStep = {...stepsData[stepKey], stepTitle: `Step ${i + 1}`};
        stepArr.push(parsedStep);
      }
      setSteps(stepArr);
    }

    return () => {};
  }, [stepsData]);

  const openSupportChat = (stepName) => {
    onClose();
    navigation.navigate('Support', {
      openMessage: true,
      messageData: `Hey Support,
      I am looking to get more information regarding a transaction which I am planning on doing with ${
        selectedBanker ? selectedBanker.bankerTag : ''
      }.
      I want to know more about the following step
      ${selectedPath ? selectedPath.path_id : ''} and ${stepName}`,
    });
  };

  const executeDeposit = async () => {
    if (isLoading) {
      return;
    }

    setIsLoading(true);

    const profileId = await AsyncStorageHelper.getProfileId();
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    let postData = {
      email,
      token,
      app_code: APP_CODE,
      profile_id: profileId,
      stats: false,
      identifier: uuidv4(), // unique txn identifier
      path_id: selectedPath.path_id, // optional
    };

    const inputAmount = isSpendEdited ? spendValue : gettingValue;

    if (isSpendEdited) {
      postData = {
        ...postData,
        purchased_from: selectedCrypto.coinSymbol,
        from_amount: parseFloat(inputAmount),
      };
    } else {
      postData = {
        ...postData,
        coin_purchased: paymentCurrency.coinSymbol,
        purchased_amount: parseFloat(inputAmount),
      };
    }

    const encryptedData = CryHelper.encryptPostData(postData);

    Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/trade/execute`, {
      data: encryptedData,
    })
      .then((resp) => {
        const {data} = resp;

        console.log('Quote Resp', data);

        if (data.status) {
          updateWalletBalances();
          WToast.show({
            data: data.message || 'Deposited Successfully',
            position: WToast.position.TOP,
          });
          setIsCompeted(true);
          setIsLoading(false);
          // onClose();
          // setTimeout(() => {
          //   setIsLoading(false);
          // }, 1000);
        } else {
          if (data.message !== 'System Error') {
            WToast.show({data: data.message, position: WToast.position.TOP});
          }
          setIsLoading(false);
        }
      })
      .catch((error) => {
        console.log('Error getting quote', error);
        WToast.show({data: 'API Error', position: WToast.position.TOP});
        setIsLoading(false);
      });
  };

  if (isLoading) {
    return (
      <View style={styles.loadingContainerFull}>
        <LoadingAnimation />
      </View>
    );
  }

  if (!steps) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={steps}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <View style={styles.stepContainer}>
            <View style={styles.thumbContainer}>
              <Image
                style={styles.thumbImage}
                source={{uri: item.icon}}
                resizeMode="contain"
              />
            </View>
            <View style={styles.stepsDetails}>
              <View style={styles.stepHeader}>
                <Text style={styles.stepHeaderText}>{item.status}</Text>
              </View>
              <View style={styles.stepBody}>
                <Text style={styles.stepName}>{item.name}</Text>
                {/* <Text style={styles.stepDesc}>{item.description || ''}</Text> */}
              </View>
              <View style={styles.stepActionContainer}>
                <TouchableOpacity
                  style={styles.stepAction}
                  onPress={() => openSupportChat(item.status)}>
                  <Text style={styles.stepActionText}>Inquire</Text>
                </TouchableOpacity>
                {index === 0 ? (
                  <TouchableOpacity
                    onPress={executeDeposit}
                    style={[styles.stepAction, styles.stepActionFilled]}>
                    <Text
                      style={[
                        styles.stepActionText,
                        styles.stepActionTextFilled,
                      ]}>
                      Proceed
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity style={styles.stepAction}>
                    <Text style={styles.stepActionText}>Details</Text>
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </View>
        )}
      />
    </View>
  );
};

export default TransactionSteps;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: -20,
    flex: 1,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingContainerFull: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -80,
    marginHorizontal: -30,
    backgroundColor: 'white',
  },
  stepContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#EBEBEB',
    borderRadius: 5,
    marginBottom: 10,
  },
  stepHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  stepHeaderText: {
    fontFamily: 'Montserrat-Bold',
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 20,
    width: '100%',
  },
  stepActionContainer: {
    marginTop: 8,
    flexDirection: 'row',
  },
  stepAction: {
    borderColor: ThemeData.APP_MAIN_COLOR,
    borderWidth: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    height: 26,
    marginRight: 8,
  },
  stepActionFilled: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
  },
  stepActionText: {
    fontFamily: 'Montserrat-SemiBold',
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 10,
  },
  stepActionTextFilled: {
    color: 'white',
  },
  disabled: {
    color: '#EBEBEB',
    borderColor: '#EBEBEB',
  },
  stepBody: {
    marginTop: 10,
  },
  stepName: {
    fontFamily: 'Montserrat',
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 13,
    marginBottom: 5,
  },
  stepDesc: {
    fontFamily: 'Montserrat',
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 11,
  },
  thumbContainer: {
    height: '100%',
    width: 120,
    borderRightWidth: 1,
    borderRightColor: '#EBEBEB',
    padding: 25,
  },
  thumbImage: {
    flex: 1,
    width: null,
    height: null,
  },
  stepsDetails: {
    padding: 15,
    flex: 1,
  },
});
