import React, {useState, useEffect, useContext} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionButton from '../ActionButton';
import CustomDropDown from '../../CustomDropDown';
import {AppContext} from '../../../contexts/AppContextProvider';
import LoadingAnimation from '../../LoadingAnimation';
import {WToast} from 'react-native-smart-tip';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import SkeletonCheckoutLoading from '../../BuyBottomSheet/SkeletonCheckoutLoading';
import ThemeData from '../../../configs/ThemeData';
import {useAppData} from '../../../utils/CustomHook';

const FundingCurrency = ({
  onNext,
  activeCrypto,
  setCheckOutPaymentCurrency,
  selectedCountry,
  paymentCurrency,
}) => {
  const {cryptoTableData} = useContext(AppContext);

  const [availableCurrencies, setAvailableCurrencies] = useState();
  const [isLoading, setIsLoading] = useState(true);

  const {data: appData} = useAppData();

  useEffect(() => {
    if (cryptoTableData && activeCrypto && selectedCountry) {
      setIsLoading(true);
      const filterCurrencyList = [];

      Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/stats/get`, {
        params: {
          to_currency: activeCrypto.coinSymbol,
          country: selectedCountry.formData
            ? selectedCountry.formData.Name
            : selectedCountry.value,
          select_type: 'withdraw',
        },
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('data', data);

          if (data.status) {
            const result = data.pathData;

            cryptoTableData.forEach((cryptoItem) => {
              result.from_currency.forEach((fromItem) => {
                if (fromItem._id === cryptoItem.coinSymbol) {
                  if (!filterCurrencyList.includes(cryptoItem)) {
                    filterCurrencyList.push(cryptoItem);
                    return;
                  }
                }
              });
            });
          } else {
            WToast.show({
              data: 'Error On Querying Path Data',
              position: WToast.position.TOP,
            });
          }

          setAvailableCurrencies(filterCurrencyList);
          setIsLoading(false);
        })
        .catch((error) => {
          WToast.show({
            data: 'Error On Querying Path Data',
            position: WToast.position.TOP,
          });
          console.log('Error On Querying Path Data', error);
        });
    }
  }, [cryptoTableData, activeCrypto, selectedCountry]);

  const onNextClick = () => {
    if (!paymentCurrency) {
      return WToast.show({
        data: 'Please select a funding currency',
        position: WToast.position.TOP,
      });
    }
    onNext();
  };

  if (!availableCurrencies || isLoading) {
    return <SkeletonCheckoutLoading />;
  }

  if (availableCurrencies.length <= 0) {
    return (
      <View style={styles.loadingContainer}>
        <Text style={styles.notAvailableText}>
          No Currencies Supported to Buy {activeCrypto.coinName} in{' '}
          {selectedCountry.value}
        </Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Currency</Text>
      <Text style={styles.subHeader}>
        Which Currency Do You Wish To Exchange For {activeCrypto.coinName}?
      </Text>
      <View style={styles.controlContainer}>
        <View style={styles.dropDownContainer}>
          <CustomDropDown
            placeholderIcon={require('../../../assets/default-breadcumb-icon/currency.png')}
            label="Select Asset"
            items={availableCurrencies}
            placeHolder="Available Currencies"
            onDropDownSelect={setCheckOutPaymentCurrency}
            selectedItem={paymentCurrency}
            numberLabel="Currencies"
            onNext={onNext}
            headerIcon={require('../../../assets/ccs-full-logo.png')}
            headerStyle={{
              backgroundColor: 'white',
              borderBottomColor: ThemeData.BORDER_COLOR,
              borderBottomWidth: 1,
            }}
          />
        </View>
      </View>
      <ActionButton text="Proceed To Methods" onPress={onNextClick} />
    </View>
  );
};

export default FundingCurrency;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  header: {
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 35,
    fontFamily: 'Montserrat-Bold',
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  controlContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  radioContainer: {
    flexDirection: 'row',
  },
  radioButton: {
    flex: 1,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 4,
    paddingVertical: 10,
  },
  radioButtonActive: {
    borderColor: ThemeData.APP_MAIN_COLOR,
  },
  radioText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#788995',
    textAlign: 'center',
  },
  radioTextActive: {
    color: ThemeData.APP_MAIN_COLOR,
  },
  dropDownContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  loadingContainer: {
    justifyContent: 'center',
    flex: 1,
  },
  notAvailableText: {
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
  },
});
