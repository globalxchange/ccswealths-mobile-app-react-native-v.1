import React, {useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import * as WebBrowser from 'expo-web-browser';
import {formatterHelper} from '../../../utils';
import {AppContext} from '../../../contexts/AppContextProvider';
import Axios from 'axios';
import {GX_API_ENDPOINT, APP_CODE} from '../../../configs';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import ThemeData from '../../../configs/ThemeData';
const CompleteView = ({
  sellCrypto,
  buyCrypto,
  sellValue,
  buyValue,
  bankerName,
  openTxnAudit,
  setIsLoading,
}) => {
  const {walletBalances} = useContext(AppContext);

  const updatedBuyBalance =
    walletBalances[`${buyCrypto.coinSymbol.toLowerCase()}_balance`] || 0;

  const sellUpdatedBalance =
    walletBalances[`${sellCrypto.coinSymbol.toLowerCase()}_balance`] || 0;

  const openTransactionAudit = async () => {
    setIsLoading(true);

    const profileId = await AsyncStorageHelper.getProfileId();

    const postData = {
      app_code: APP_CODE,
      profile_id: profileId,
      coin: buyValue.coinSymbol,
    };
    Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/txns/get`, postData)
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          openTxnAudit(data.txns[0]);
        }
        setIsLoading(false);
        // console.log('TXN List', data.txns);
      })
      .catch((error) => console.log('Error getting txn list', error));
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Completed</Text>
      <Text style={styles.subHeader}>
        You Have Successfully Withdrew{' '}
        {formatterHelper(buyValue, buyCrypto.coinSymbol)} {buyCrypto.coinSymbol}{' '}
        From Your Vault
      </Text>
      <View style={styles.valuesContainer}>
        {sellCrypto.coinSymbol === buyCrypto.coinSymbol ? (
          <View style={styles.valueItem}>
            <Text style={styles.title}>New {sellCrypto.coinName} Balance</Text>
            <View style={styles.inputContainer}>
              <Text style={styles.textInput}>
                {formatterHelper(sellUpdatedBalance, sellCrypto.coinSymbol)}
              </Text>
              <View style={styles.unitContainer}>
                <Text style={styles.unitValue}>{sellCrypto.coinSymbol}</Text>
                <Image
                  source={{uri: sellCrypto.coinImage}}
                  style={styles.unitImage}
                  resizeMode="cover"
                />
              </View>
            </View>
          </View>
        ) : (
          <>
            <View style={styles.valueItem}>
              <Text style={styles.title}>
                New {sellCrypto.coinName} Balance
              </Text>
              <View style={styles.inputContainer}>
                <Text style={styles.textInput}>
                  {formatterHelper(sellUpdatedBalance, sellCrypto.coinSymbol)}
                </Text>
                <View style={styles.unitContainer}>
                  <Text style={styles.unitValue}>{sellCrypto.coinSymbol}</Text>
                  <Image
                    source={{uri: sellCrypto.coinImage}}
                    style={styles.unitImage}
                    resizeMode="cover"
                  />
                </View>
              </View>
            </View>
            <View style={styles.valueItem}>
              <Text style={styles.title}>New {buyCrypto.coinName} Balance</Text>
              <View style={styles.inputContainer}>
                <Text style={styles.textInput}>
                  {formatterHelper(updatedBuyBalance, buyCrypto.coinSymbol)}
                </Text>
                <View style={styles.unitContainer}>
                  <Text style={styles.unitValue}>{buyCrypto.coinSymbol}</Text>
                  <Image
                    source={{uri: buyCrypto.coinImage}}
                    style={styles.unitImage}
                    resizeMode="cover"
                  />
                </View>
              </View>
            </View>
          </>
        )}
      </View>
      <View style={styles.actionContainer}>
        <ScrollView
          style={styles.scrollView}
          horizontal
          showsHorizontalScrollIndicator={false}>
          <TouchableOpacity
            style={[styles.action, {marginLeft: 10}]}
            onPress={openTransactionAudit}>
            <Image
              style={styles.actionIcon}
              source={require('../../../assets/app-logo.png')}
              resizeMode="contain"
            />
            <Text style={styles.actionText}>Transaction Audit</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.action, {marginLeft: 25}]}
            onPress={() =>
              WebBrowser.openBrowserAsync('https://iceprotocol.com/')
            }>
            <Image
              style={styles.actionIcon}
              source={require('../../../assets/iced-icon.png')}
              resizeMode="contain"
            />
            <Text style={[styles.actionText, styles.actionTextBlack]}>
              Invest Bitcoin
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.action, {marginLeft: 25, marginRight: 50}]}
            onPress={() =>
              WebBrowser.openBrowserAsync('https://cryptolottery.com/')
            }>
            <Image
              style={styles.actionIcon}
              source={require('../../../assets/lottery-icon.png')}
              resizeMode="contain"
            />
            <Text style={[styles.actionText, styles.actionTextBlack]}>
              Play Lottery
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    </View>
  );
};

export default CompleteView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    marginTop: -80,
    marginHorizontal: -30,
    backgroundColor: 'white',
    paddingHorizontal: 30,
    paddingVertical: 35,
  },
  header: {
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'left',
    marginBottom: 10,
    fontSize: 26,
    fontFamily: 'Montserrat-Bold',
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  valuesContainer: {
    marginVertical: 15,
  },
  actionContainer: {
    flexDirection: 'row',
    marginRight: -40,
  },
  scrollView: {
    marginHorizontal: -10,
  },
  action: {
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    backgroundColor: 'white',
    borderRadius: 8,
    paddingVertical: 20,
    paddingHorizontal: 30,
    flex: 1,
    marginVertical: 10,
  },
  actionIcon: {
    width: 35,
    height: 35,
  },
  actionText: {
    marginTop: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontSize: 12,
  },
  actionTextBlack: {
    color: '#464B4E',
  },
  valueItem: {marginBottom: 10},
  title: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  inputContainer: {
    flexDirection: 'row',
    borderWidth: 2,
    borderRadius: 8,
    borderColor: '#EBEBEB',
    height: 50,
    alignItems: 'center',
  },
  textInput: {
    flexGrow: 1,
    width: 0,
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingHorizontal: 20,
    color: 'black',
  },
  unitContainer: {
    width: '45%',
    flexDirection: 'row',
    alignItems: 'center',
    borderLeftColor: '#EBEBEB',
    borderLeftWidth: 2,
    justifyContent: 'center',
  },
  unitValue: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
  },
  unitImage: {
    height: 24,
    width: 24,
    borderRadius: 12,
    marginLeft: 15,
  },
});
