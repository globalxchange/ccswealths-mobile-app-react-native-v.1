import React, {useContext, useState, useRef, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Keyboard,
  Platform,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
  Image,
  Text,
} from 'react-native';
import {AppContext} from '../../contexts/AppContextProvider';
import Animated, {
  useCode,
  set,
  Clock,
  interpolate,
} from 'react-native-reanimated';
import Breadcrumb from './Breadcrumb';
import PaymentQuote from './Fragments/PaymentQuote';
import CountryChooser from './Fragments/CountryChooser';
import FundingMethodChooser from './Fragments/FundingMethodChooser';
import BankerChooser from './Fragments/BankerChooser';
import ReanimatedTimingHelper from '../../utils/ReanimatedTimingHelper';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../configs';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {useNavigation} from '@react-navigation/native';
import ThemeData from '../../configs/ThemeData';
import {useAppData} from '../../utils/CustomHook';

const {height} = Dimensions.get('window');

const WithdrawBottomSheet = ({
  isBottomSheetOpen,
  setIsBottomSheetOpen,
  activeCrypto,
  openTxnAudit,
  checkoutPayload,
  selectedItem,
}) => {
  const {navigate} = useNavigation();

  const {bottom} = useSafeAreaInsets();

  const {
    checkOutData,
    setWalletAddress,
    walletBalances,
    setSheetHide,
    isSheetHide,
  } = useContext(AppContext);

  const {selectedCrypto} = checkOutData;

  const [currentStep, setCurrentStep] = useState();
  const [isExpanded, setIsExpanded] = useState(false);
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const [pathData, setPathData] = useState();

  const [selectedCountry, setSelectedCountry] = useState('');
  const [paymentCurrency, setPaymentCurrency] = useState('');
  const [paymentType, setPaymentType] = useState('');
  const [selectedBanker, setSelectedBanker] = useState('');
  const [isOpenFeeAudit, setIsOpenFeeAudit] = useState(false);

  const {data: appData} = useAppData();

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );

    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
    };
  }, []);
  useEffect(() => {
    if (activeCrypto) {
      setPaymentCurrency(activeCrypto);
    }
  }, [activeCrypto]);

  useEffect(() => {
    setIsExpanded(false);
  }, [currentStep]);

  useEffect(() => {
    getWalletAddress();
    getPathData();
  }, [selectedCrypto]);

  useEffect(() => {
    if (!isBottomSheetOpen) {
      resetCheckoutData();
      setCurrentStep('');
      setSheetHide(false);
    }
  }, [isBottomSheetOpen]);

  useEffect(() => {
    if (checkoutPayload) {
      if (checkoutPayload.selectedCountry) {
        setSelectedCountry(checkoutPayload.selectedCountry);
        setCurrentStep('ChooseFunding');
        if (checkoutPayload.paymentType) {
          setPaymentType(checkoutPayload.paymentType);
          setCurrentStep('ChooseBanker');
        }
      }
    }
  }, [checkoutPayload]);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  const getWalletAddress = async () => {
    if (selectedCrypto && walletBalances) {
      switch (selectedCrypto.coinSymbol) {
        case 'BTC':
          setWalletAddress(walletBalances.coinAddress.BTC.address);
          break;
        case 'ETH':
          setWalletAddress(walletBalances.coinAddress.ETH.address);
          break;
      }
    }
  };

  const onKeyBoardShow = (e) => {
    setKeyboardHeight(e.endCoordinates.height);
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  const getPathData = () => {
    if (activeCrypto) {
      Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/paths/get`, {
        params: {coin: activeCrypto.coinSymbol},
      }).then((resp) => {
        const {data} = resp;

        const paths = data.paths || [];

        setPathData(paths);
      });
    }
  };

  const onBackKeyPress = () => {
    const index = stepKeys.findIndex((x) => x === currentStep);

    if (index < 0 || currentStep === 'ChooseCountry') {
      setIsBottomSheetOpen(false);
    } else {
      switch (currentStep) {
        case 'ChooseFunding':
          clearForCountry();
          break;
        case 'ChooseMethod':
          clearForCurrency();
          break;
        case 'ChooseBanker':
          setSelectedBanker('');
      }

      setCurrentStep(stepKeys[index - 1]);
    }
  };

  const showFundingMethod = () => {
    setCurrentStep('ChooseMethod');
  };

  const showBankerMethods = () => {
    setCurrentStep('ChooseBanker');
  };

  const showQuoteHandler = () => {
    setCurrentStep('ShowQuote');
  };
  const showFundingCurrency = () => {
    setCurrentStep('ChooseFunding');
  };

  const clearForCountry = () => {
    setPaymentType('');
    setSelectedBanker('');
  };

  const clearForCurrency = () => {
    setPaymentType('');
    setSelectedBanker('');
  };

  const resetCheckoutData = () => {
    setCurrentStep('');
    setIsExpanded(false);
    setIsKeyboardOpen(false);
    setKeyboardHeight(0);
    setSelectedCountry('');
    setPaymentCurrency('');
    setPaymentType('');
    setSelectedBanker('');
  };

  const renderFragment = () => {
    switch (currentStep) {
      case 'ChooseCountry':
        return (
          <CountryChooser
            onNext={showFundingMethod}
            activeCrypto={activeCrypto}
            selectedCountry={selectedCountry}
            setCheckOutCountry={setSelectedCountry}
          />
        );
      case 'ChooseMethod':
        return (
          <FundingMethodChooser
            onNext={showBankerMethods}
            activeCrypto={activeCrypto}
            paymentCurrency={paymentCurrency}
            paymentType={paymentType}
            selectedCountry={selectedCountry}
            setPaymentType={setPaymentType}
          />
        );
      case 'ChooseBanker':
        return (
          <BankerChooser
            onNext={() => {
              setSheetHide(true);
              navigate('Quote', {
                pathData: pathData,
                sellCurrency: paymentCurrency,
                buyCurrency: selectedCrypto,
                selectedBanker: selectedBanker,
                selectedCountry: selectedCountry,
                paymentType: paymentType,
                type: 'Withdraw',
                onClose: () => setIsBottomSheetOpen(false),
              });
            }}
            activeCrypto={activeCrypto}
            toggleExpansion={() => setIsExpanded(!isExpanded)}
            isExpanded={isExpanded}
            paymentCurrency={paymentCurrency}
            paymentType={paymentType}
            selectedBanker={selectedBanker}
            selectedCountry={selectedCountry}
            setSelectedBanker={setSelectedBanker}
          />
        );
      case 'ShowQuote':
        return (
          <PaymentQuote
            pathData={pathData}
            onClose={() => setIsBottomSheetOpen(false)}
            openTxnAudit={openTxnAudit}
            paymentCurrency={paymentCurrency}
            paymentType={paymentType}
            selectedBanker={selectedBanker}
            selectedCountry={selectedCountry}
            selectedCrypto={selectedCrypto}
            setIsExpanded={setIsExpanded}
            setIsOpenFeeAudit={setIsOpenFeeAudit}
          />
        );
      default:
        return (
          <CountryChooser
            onNext={showFundingMethod}
            activeCrypto={activeCrypto}
            selectedCountry={selectedCountry}
            setCheckOutCountry={setSelectedCountry}
          />
        );
    }
  };

  return (
    <Modal
      animationType="slide"
      visible={!isSheetHide && isBottomSheetOpen}
      transparent
      hardwareAccelerated
      statusBarTranslucent
      onDismiss={() => setIsBottomSheetOpen(false)}
      onRequestClose={onBackKeyPress}>
      <TouchableOpacity
        activeOpacity={1}
        style={styles.overlay}
        onPress={() => setIsBottomSheetOpen(false)}
        onPressOut={() => {}}>
        <TouchableWithoutFeedback style={{flex: 1}}>
          <Animated.View
            style={[
              styles.container,
              {maxHeight: isExpanded ? height - 60 : height * 0.6},
              {
                paddingBottom: bottom,
                transform: [
                  {
                    translateY: interpolate(chatKeyboardAnimation.current, {
                      inputRange: [0, 1],
                      outputRange: [0, -keyboardHeight],
                    }),
                  },
                ],
              },
            ]}>
            <View
              style={[
                styles.header,
                isOpenFeeAudit || {
                  backgroundColor: 'white',
                  borderBottomColor: ThemeData.BORDER_COLOR,
                  borderBottomWidth: 1,
                },
              ]}>
              <Image
                style={styles.headerLogo}
                source={{uri: selectedItem?.coinImage}}
                resizeMode="contain"
              />
              <Text style={styles.headerText}>{selectedItem?.coinName}</Text>
            </View>
            <View style={{display: isOpenFeeAudit ? 'none' : 'flex'}}>
              <Breadcrumb
                setCurrentStep={setCurrentStep}
                clearForCountry={clearForCountry}
                clearForCurrency={clearForCurrency}
                paymentCurrency={paymentCurrency}
                paymentType={paymentType}
                selectedBanker={selectedBanker}
                selectedCountry={selectedCountry}
                setSelectedBanker={setSelectedBanker}
              />
            </View>
            <View style={styles.fragmentContainer}>{renderFragment()}</View>
          </Animated.View>
        </TouchableWithoutFeedback>
      </TouchableOpacity>
    </Modal>
  );
};

export default WithdrawBottomSheet;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
  fragmentContainer: {
    flex: 1,
    paddingHorizontal: 40,
    paddingTop: 20,
    paddingBottom: 20,
  },
  // header: {
  //   backgroundColor: ThemeData.APP_MAIN_COLOR,
  //   shadowColor: '#000000',
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   paddingVertical: 15,
  // },
  // headerLogo: {
  //   height: 35,
  //   width: '100%',
  // },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 4,
    borderRadius: 4,
    backgroundColor: '#E4E9F2',
    marginBottom: 10,
  },
  header: {
    backgroundColor: 'white',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20,
    paddingBottom: 20,
    borderTopLeftRadius: 30,
    borderTopEndRadius: 30,
  },
  headerLogo: {
    height: 35,
    width: 35,
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#464B4E',
    fontSize: 30,
    paddingLeft: 10,
  },
});

const stepKeys = ['ChooseCountry', 'ChooseMethod', 'ChooseBanker', 'ShowQuote'];
