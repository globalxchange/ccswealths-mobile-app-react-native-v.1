import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';

const DetailsListItem = ({placeholder, icon}) => {
  return (
    <View style={styles.dropdownContainer}>
      <Image style={styles.image} source={icon} resizeMode="contain" />
      <Text style={styles.text}>{placeholder}</Text>
      <Image
        style={styles.dropIcon}
        source={require('../../assets/dropdown-icon.png')}
        resizeMode="contain"
      />
    </View>
  );
};

export default DetailsListItem;

const styles = StyleSheet.create({
  dropdownContainer: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 15,
    alignItems: 'center',
    height: 50,
    marginBottom: 15,
  },
  image: {
    height: 24,
    width: 24,
  },
  text: {
    flexGrow: 1,
    width: 0,
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
  },
  dropIcon: {
    width: 13,
  },
});
