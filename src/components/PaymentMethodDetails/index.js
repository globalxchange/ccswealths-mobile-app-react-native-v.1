import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import DetailsListItem from './DetailsListItem';
import LoadingAnimation from '../LoadingAnimation';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../configs';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';

const PaymentMethodDetails = ({
  isBottomSheetOpen,
  data,
  setIsBottomSheetOpen,
}) => {
  const [listDetails, setListDetails] = useState();

  useEffect(() => {
    if (!isBottomSheetOpen) {
      setListDetails();
    }
  }, [isBottomSheetOpen]);

  useEffect(() => {
    if (data) {
      Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/stats/get`, {
        params: {paymentMethod: data.code},
      }).then((resp) => {
        const pathData = resp.data;

        if (data.status) {
          setListDetails({
            banker: pathData.stats.banker,
            country: pathData.stats.country,
            paymentMethod: pathData.stats.paymentMethod,
            to_currency: pathData.stats.to_currency,
            from_currency: pathData.stats.from_currency,
          });
        }
      });
    }
  }, [data, isBottomSheetOpen]);

  // console.log('Data', data);

  return (
    <BottomSheetLayout
      isOpen={isBottomSheetOpen}
      onClose={() => setIsBottomSheetOpen(false)}
      reactToKeyboard>
      {data && (
        <>
          <View style={styles.header}>
            <Image
              style={styles.methodIcon}
              source={{uri: data.icon}}
              resizeMode="cover"
            />
            <Text style={styles.methodName}>{data.name}</Text>
          </View>
          <Text style={styles.description}>{data.description}</Text>
          <View style={styles.listContainer}>
            {listDetails ? (
              <>
                <DetailsListItem
                  placeholder={`Available In ${listDetails.country} Countries`}
                  icon={require('../../assets/default-breadcumb-icon/country.png')}
                />
                <DetailsListItem
                  placeholder={`Accepting In ${listDetails.to_currency} Currencies`}
                  icon={require('../../assets/default-breadcumb-icon/country.png')}
                />
                <DetailsListItem
                  placeholder={`Supported In ${listDetails.banker} Bankers`}
                  icon={require('../../assets/default-breadcumb-icon/country.png')}
                />
                <DetailsListItem
                  placeholder={`Enables Access To ${
                    listDetails.to_currency + listDetails.from_currency
                  } Assets`}
                  icon={require('../../assets/default-breadcumb-icon/country.png')}
                />
              </>
            ) : (
              <View style={styles.loadingContainer}>
                <LoadingAnimation />
              </View>
            )}
          </View>
          <TouchableOpacity style={styles.profileButton}>
            <Text style={styles.profileButtonText}>Full Insta Profile</Text>
          </TouchableOpacity>
        </>
      )}
    </BottomSheetLayout>
  );
};

export default PaymentMethodDetails;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
    padding: 30,
  },
  header: {
    paddingVertical: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  methodIcon: {
    width: 40,
    height: 40,
  },
  methodName: {
    marginLeft: 15,
    flex: 1,
    fontFamily: 'Montserrat-SemiBold',
    color: '#231F20',
    fontSize: 26,
  },
  description: {
    marginTop: 10,
    fontFamily: 'Montserrat',
    fontSize: 13,
  },
  profileButton: {
    backgroundColor: '#02C346',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: -30,
    marginBottom: -30,
    height: 55,
  },
  profileButtonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 15,
  },
  listContainer: {
    paddingTop: 35,
    paddingBottom: 20,
  },
  loadingContainer: {},
});
