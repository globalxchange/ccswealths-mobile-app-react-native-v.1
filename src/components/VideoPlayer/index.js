import React, {useEffect, useState} from 'react';
import {Platform} from 'react-native';
import VideoPlayerIOS from './VideoPlayerIOS';
import VideoPlayerAndroid from './VideoPlayerAndroid';
import Axios from 'axios';

const VideoPlayer = ({isBrainVideo, videoId, play, offsetHeight}) => {
  const [videoLink, setVideoLink] = useState('');

  useEffect(() => {
    if (isBrainVideo) {
      Axios.post(
        'https://vod-backend.globalxchange.io/get_user_profiled_video_stream_link',
        {video_id: videoId},
      )
        .then((resp) => {
          const {data} = resp;
          // console.log('Video Data', data);
          setVideoLink(data);
        })
        .catch((error) => {
          console.log('Error video player error', error);
        });
    }
  }, [videoId, isBrainVideo]);

  if (Platform.OS === 'ios') {
    return <VideoPlayerIOS videoLink={videoLink} play={play} />;
  }

  return (
    <VideoPlayerAndroid
      offsetHeight={offsetHeight}
      videoLink={videoLink}
      play={play}
    />
  );
};

export default VideoPlayer;
