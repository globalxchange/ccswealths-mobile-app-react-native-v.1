import React, {useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Dimensions,
  TouchableWithoutFeedback,
} from 'react-native';
import {AppContext} from '../contexts/AppContextProvider';
import FastImage from 'react-native-fast-image';
import {getUriImage} from '../utils';

const {width} = Dimensions.get('window');

const CountrySelector = () => {
  const {
    countryList,
    filterActiveCountry,
    setFilterActiveCountry,
    setCheckOutCountry,
  } = useContext(AppContext);

  const setSelectedCountry = (value) => {
    setFilterActiveCountry(value);
    // setCheckOutCountry(value);
  };

  return (
    <View style={styles.container}>
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={countryList || options}
        keyExtractor={(item) => item.value}
        renderItem={({item}) => (
          <TouchableWithoutFeedback
            disabled={
              filterActiveCountry
                ? filterActiveCountry.value === item.value
                : false
            }
            onPress={() => setSelectedCountry(item)}>
            <View
              style={[
                styles.item,
                filterActiveCountry
                  ? filterActiveCountry.value === item.value
                    ? styles.itemActive
                    : {}
                  : {},
              ]}>
              <FastImage
                style={styles.icon}
                source={{uri: getUriImage(item.formData.Flag)}}
                resizeMode="contain"
              />
              <Text style={styles.title}>{item.value}</Text>
            </View>
          </TouchableWithoutFeedback>
        )}
      />
    </View>
  );
};

export default CountrySelector;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    height: 110,
    alignItems: 'center',
  },
  item: {
    paddingHorizontal: 5,
    width: (width - 40) / 4,
    paddingVertical: 15,
    alignItems: 'center',
  },
  itemActive: {
    opacity: 0.4,
  },
  icon: {height: 50, width: 50},
  title: {
    textAlign: 'center',
    color: '#788995',
    marginTop: 5,
    fontSize: 10,
    fontFamily: 'Montserrat',
  },
});

const options = [
  {
    productName: 'InstaCryptoPurchase',
    formName: 'Countrydem',
    formData: {
      Name: 'Canada',
      CountryCode: 124,
      Flag:
        'https://d22n6gp5xyztod.cloudfront.net/5eb32453f10390217259d8a6847.png',
      Processor: ' Both',
    },
    value: 'Canada',
  },
  {
    productName: 'InstaCryptoPurchase',
    formName: 'Countrydem',
    formData: {
      Name: 'United States of America',
      CountryCode: 840,
      Flag:
        'https://d22n6gp5xyztod.cloudfront.net/5eb3246af10390217259d8a7298.png',
      Processor: ' Both',
    },
    value: 'United States',
  },
  {
    productName: 'InstaCryptoPurchase',
    formName: 'Countrydem',
    formData: {
      Name: 'India',
      CountryCode: 356,
      Flag:
        'https://d22n6gp5xyztod.cloudfront.net/5eb3248bf10390217259d8a85777.png',
      Processor: ' Both',
    },
    value: 'India',
  },
  {
    productName: 'InstaCryptoPurchase',
    formName: 'Countrydem',
    formData: {
      Name: 'Australia',
      CountryCode: 36,
      Flag:
        'https://d22n6gp5xyztod.cloudfront.net/5eb3249ff10390217259d8a98061.png',
      Processor: ' Both',
    },
    value: 'Australia',
  },
  {
    productName: 'InstaCryptoPurchase',
    formName: 'Countrydem',
    formData: {
      Name: 'Mexico',
      CountryCode: 484,
      Flag:
        'https://d22n6gp5xyztod.cloudfront.net/5eb3329df10390217259d8aa6824.png',
      Processor: ' Both',
    },
    value: 'Mexico',
  },
  {
    productName: 'InstaCryptoPurchase',
    formName: 'Countrydem',
    formData: {
      Name: 'Colombia',
      CountryCode: 170,
      Flag:
        'https://d22n6gp5xyztod.cloudfront.net/5eb332b3f10390217259d8ab434.png',
      Processor: ' Both',
    },
    value: 'Colombia',
  },
  {
    productName: 'InstaCryptoPurchase',
    formName: 'Countrydem',
    formData: {
      Name: 'Argentina',
      CountryCode: 32,
      Flag:
        'https://d22n6gp5xyztod.cloudfront.net/5eb332d0f10390217259d8ac1810.png',
      Processor: ' Both',
    },
    value: 'Argentina',
  },
  {
    productName: 'InstaCryptoPurchase',
    formName: 'Countrydem',
    formData: {
      Name: 'United Kingdom of Great Britain and Northern Ireland',
      CountryCode: 826,
      Flag:
        'https://d22n6gp5xyztod.cloudfront.net/5eb332f5f10390217259d8ad3267.png',
      Processor: ' Both',
    },
    value: 'United Kingdom',
  },
  {
    productName: 'InstaCryptoPurchase',
    formName: 'Countrydem',
    formData: {
      Name: 'Germany',
      CountryCode: 276,
      Flag:
        'https://d22n6gp5xyztod.cloudfront.net/5eb33313f10390217259d8ae1154.png',
      Processor: ' Both',
    },
    value: 'Germany',
  },
];
