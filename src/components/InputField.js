import React from 'react';
import {StyleSheet, Text, View, TextInput, Image} from 'react-native';

const InputField = ({placeHolder, value, onChange, icon, keyboardType}) => {
  return (
    <View style={styles.container}>
      <View style={styles.inputContainer}>
        <Text style={styles.placeHolder}>{placeHolder}</Text>
        <TextInput
          style={styles.input}
          value={value}
          onChange={onChange}
          keyboardType={keyboardType || 'default'}
          placeholderTextColor="#878788"
        />
      </View>
      <View style={styles.iconContainer}>
        <Image style={styles.icon} source={icon} resizeMode="contain" />
      </View>
    </View>
  );
};

export default InputField;

const styles = StyleSheet.create({
  container: {
    borderColor: '#E9E8E8',
    borderWidth: 1,
    borderRadius: 12,
    paddingHorizontal: 15,
    paddingTop: 6,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  inputContainer: {
    flex: 1,
  },
  placeHolder: {
    color: '#858585',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  input: {
    color: '#000000',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 14,
    paddingRight: 10,
  },
  iconContainer: {
    borderColor: '#F6F6F6',
    borderWidth: 1,
    width: 40,
    height: 40,
    padding: 10,
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  icon: {
    flex: 1,
    height: null,
    width: null,
  },
});
