import React from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import MenuItem from '../BrandSettings/MenuItem';
import MTItem from '../BrandSettings/MTItem';

const MenuListItems = ({selectedMenu}) => {
  return (
    <View style={styles.container}>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={selectedMenu?.menu}
        keyExtractor={(item) => item.title}
        renderItem={({item}) =>
          selectedMenu.title === 'Settings' ? (
            <MTItem
              icon={item.icon}
              title={item.title}
              subTitle={item.subTitle}
              onPress={item.onPress}
              disabled={item.disabled}
              header="Monetization Tool"
              pos={item.pos}
              desc={item.desc}
            />
          ) : (
            <MenuItem
              icon={item.icon}
              title={item.title}
              subTitle={item.subTitle}
              onPress={item.onPress}
              disabled={item.disabled}
            />
          )
        }
      />
    </View>
  );
};

export default MenuListItems;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
});
