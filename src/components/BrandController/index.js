import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import MenuController from './MenuController';
import MenuListItems from './MenuListItems';

const BrandController = ({selectedBrand}) => {
  const [selectedMenu, setSelectedMenu] = useState('');

  return (
    <View style={styles.container}>
      <MenuController
        selectedMenu={selectedMenu}
        setSelectedMenu={setSelectedMenu}
        selectedBrand={selectedBrand}
      />
      <MenuListItems selectedMenu={selectedMenu} />
    </View>
  );
};

export default BrandController;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
