import React, {useContext, useState} from 'react';

import {AppContext} from '../../../contexts/AppContextProvider';
import LoginReminder from '../LoginReminder';
import OnHoldView from './OnHoldView';
import Landing from './Landing';

const OnHold = () => {
  const {isLoggedIn} = useContext(AppContext);
  const [devLoggedIn, setDevLoggedIn] = useState(false);

  if (!isLoggedIn) {
    return <LoginReminder />;
  }

  if (devLoggedIn) {
    return <OnHoldView />;
  }

  return <Landing setDevLoggedIn={setDevLoggedIn} />;
};

export default OnHold;
