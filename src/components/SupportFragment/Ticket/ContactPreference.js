import React, {useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import ThemeData from '../../../configs/ThemeData';
import {SupportContext} from '../../../contexts/SupportContext';

const ContactPreference = ({goBack}) => {
  const {
    ticketPhoneNumber,
    ticketEmailId,
    setTicketPhoneNumber,
    setTicketEmailId,
  } = useContext(SupportContext);

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Contact Preference</Text>
      <View style={styles.inputContainer}>
        <Image
          style={styles.icon}
          source={require('../../../assets/phone-icon.png')}
          resizeMode="contain"
        />
        <TextInput
          style={styles.input}
          placeholder="Your Phone Number"
          value={ticketPhoneNumber}
          onChangeText={(text) => setTicketPhoneNumber(text)}
          keyboardType="number-pad"
          placeholderTextColor={'#878788'}
        />
      </View>
      <View style={styles.inputContainer}>
        <Image
          style={styles.icon}
          source={require('../../../assets/envelope-icon.png')}
          resizeMode="contain"
        />
        <TextInput
          style={styles.input}
          placeholder="Your Email"
          value={ticketEmailId}
          onChangeText={(text) => setTicketEmailId(text)}
          keyboardType="email-address"
          placeholderTextColor={'#878788'}
        />
      </View>
      <TouchableOpacity onPress={goBack} style={styles.filledButton}>
        <Text style={styles.buttonText}>Type More</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ContactPreference;

const styles = StyleSheet.create({
  container: {},
  header: {
    textAlign: 'center',
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    marginBottom: 30,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#E9E8E8',
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 20,
    marginBottom: 30,
  },
  icon: {
    width: 16,
    height: 16,
  },
  input: {
    flex: 1,
    fontFamily: 'Montserrat-Bold',
    paddingLeft: 20,
    height: 45,
    paddingVertical: 2,
    color: 'black',
  },
  filledButton: {
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    marginHorizontal: 50,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
});
