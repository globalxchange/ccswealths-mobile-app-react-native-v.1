/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useState} from 'react';
import {
  Image,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import ThemeData from '../../../../../configs/ThemeData';
import {SupportContext} from '../../../../../contexts/SupportContext';
import SearchLayout from '../../../../../layouts/SearchLayout';

const UserForm = ({
  selectedOption,
  setUserQuery,
  onSubmit,
  onBack,
  disableFilter,
  showUserList,
}) => {
  const {setIsFullScreen} = useContext(SupportContext);

  const [selectedInputType, setSelectedInputType] = useState(INPUT_OPTIONS[0]);
  const [inputValue, setInputValue] = useState('');
  const [isEditorOpen, setIsEditorOpen] = useState(false);

  useEffect(() => {
    setIsFullScreen(isEditorOpen);
  }, [isEditorOpen]);

  useEffect(() => {
    if (disableFilter) {
      setSelectedInputType(INPUT_OPTIONS[0]);
    }
  }, [disableFilter]);

  const onSubmitHandler = (submitValue) => {
    const input = submitValue || inputValue;

    if (!input) {
      return WToast.show({
        data: 'Please Input A Value',
        position: WToast.position.TOP,
      });
    }

    const value = {};

    value[`${selectedInputType.paramKey}`] = input?.toLowerCase().trim();

    setUserQuery(value);
    setIsEditorOpen(false);
    setIsFullScreen(false);
    onSubmit();
  };

  if (isEditorOpen) {
    return (
      <SearchLayout
        placeholder={`Enter ${selectedInputType.title}`}
        value={inputValue}
        setValue={setInputValue}
        onBack={() => setIsEditorOpen(false)}
        onSubmit={onSubmitHandler}
        keyboardOffset={Platform.OS === 'android' ? 75 : 100}
        filters={INPUT_OPTIONS}
        selectedFilter={selectedInputType}
        setSelectedFilter={setSelectedInputType}
        disableFilter={disableFilter}
        showUserList={showUserList}
      />
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>Lineage FAQ's</Text>
      </View>
      <View style={styles.selectedOption}>
        <Image
          style={styles.optionIcon}
          resizeMode="contain"
          source={selectedOption.icon}
        />
        <Text style={styles.optionName}>{selectedOption.title}</Text>
      </View>
      <View style={styles.inputForm}>
        <View style={styles.inputTypesContainer}>
          {INPUT_OPTIONS.map((item) => (
            <TouchableOpacity
              disabled={disableFilter}
              key={item.title}
              onPress={() => setSelectedInputType(item)}>
              <View
                style={[
                  styles.inputType,
                  selectedInputType.title === item.title && {opacity: 1},
                ]}>
                <Text
                  style={[
                    styles.inputTypeName,
                    selectedInputType.title === item.title && {
                      fontFamily: ThemeData.FONT_SEMI_BOLD,
                    },
                  ]}>
                  {item.title}
                </Text>
              </View>
            </TouchableOpacity>
          ))}
        </View>
        <TouchableOpacity onPress={() => setIsEditorOpen(true)}>
          <Text
            style={[styles.inputField, inputValue ? {} : {color: '#878788'}]}>
            {inputValue || `Enter ${selectedInputType.title}`}
          </Text>
        </TouchableOpacity>
        {/* <TextInput
          style={styles.inputField}
          placeholder={`Enter ${selectedInputType.title}`}
          placeholderTextColor={'#878788'}
          value={inputValue}
          onChangeText={(text) => setInputValue(text)}
        /> */}
        <View style={styles.actionContainer}>
          <TouchableOpacity onPress={onBack} style={styles.backButton}>
            <Text style={styles.backButtonText}>Back</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default UserForm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    backgroundColor: '#F1F4F6',
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  headerText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  selectedOption: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 20,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  optionIcon: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  optionName: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 15,
  },
  inputForm: {
    flex: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 20,
    borderTopWidth: 0,
  },
  inputTypesContainer: {
    flexDirection: 'row',
  },
  inputType: {
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginRight: 10,
    opacity: 0.4,
    minWidth: 85,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputTypeName: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 11,
    color: ThemeData.APP_MAIN_COLOR,
  },
  inputField: {
    marginTop: 30,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 15,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 14,
  },
  submitButton: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    paddingVertical: 12,
    width: 130,
    alignItems: 'center',
    marginLeft: 10,
  },
  submitButtonText: {
    color: 'white',
    fontSize: 14,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  actionContainer: {
    flexDirection: 'row',
    marginTop: 30,
  },
  backButton: {
    paddingVertical: 12,
    width: 130,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
  },
  backButtonText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 14,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
});

const INPUT_OPTIONS = [
  {title: 'Email', paramKey: 'email'},
  {title: 'Name', paramKey: 'name'},
  {title: 'Username', paramKey: 'username'},
];
