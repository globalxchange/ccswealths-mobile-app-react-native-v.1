import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../../../../configs/ThemeData';
import MyLicenses from '../../MyLicenses';
import SignUpStatus from './SignUpStatus';
import UserForm from './UserForm';
import UserLevels from './UserLevels';
import UserLicenses from './UserLicenses';

const FAQ = ({activeSubCategory, setHideSubCategory, setHideSubMenu}) => {
  const [selectedOption, setSelectedOption] = useState();
  const [userQuery, setUserQuery] = useState();
  const [showLocateUser, setShowLocateUser] = useState(false);
  const [showSignUpStatus, setShowSignUpStatus] = useState(false);
  const [showUserLicenses, setShowUserLicenses] = useState(false);

  useEffect(() => {
    if (showLocateUser) {
      setShowSignUpStatus(false);
    }
  }, [showLocateUser]);

  useEffect(() => {
    if (showSignUpStatus) {
      setShowLocateUser(false);
    }
  }, [showSignUpStatus]);

  useEffect(() => {
    if (!selectedOption) {
      setHideSubMenu(false);
    }
  }, [selectedOption]);

  const onItemPress = (item) => {
    setHideSubCategory(true);
    setHideSubMenu(true);
    setSelectedOption(item);
  };

  if (showUserLicenses) {
    return (
      <MyLicenses
        selectedOption={selectedOption}
        userEmail={userQuery?.email}
        onClose={() => setShowUserLicenses(false)}
        onBack={() => {
          setShowUserLicenses(false);
        }}
      />
    );
  }

  if (showSignUpStatus) {
    return (
      <SignUpStatus
        selectedOption={selectedOption}
        userEmail={userQuery?.email}
        onClose={() => setShowSignUpStatus(false)}
        onBack={() => {
          setShowSignUpStatus(false);
        }}
      />
    );
  }

  if (showLocateUser) {
    return (
      <UserLevels
        selectedOption={selectedOption}
        userQuery={userQuery}
        onClose={() => setShowLocateUser(false)}
        onBack={() => {
          setShowLocateUser(false);
        }}
      />
    );
  }

  if (selectedOption) {
    if (selectedOption?.title === 'Locate A Person') {
      return (
        <UserForm
          selectedOption={selectedOption}
          setUserQuery={setUserQuery}
          onSubmit={() => setShowLocateUser(true)}
          onBack={() => setSelectedOption()}
        />
      );
    }

    if (selectedOption?.title === 'Did This Person Sign Up Properly?') {
      return (
        <UserForm
          selectedOption={selectedOption}
          setUserQuery={setUserQuery}
          onSubmit={() => setShowSignUpStatus(true)}
          onBack={() => setSelectedOption()}
          disableFilter
        />
      );
    }

    if (selectedOption?.title === 'Is This Person Active?') {
      return (
        <UserForm
          selectedOption={selectedOption}
          setUserQuery={setUserQuery}
          onSubmit={() => setShowUserLicenses(true)}
          onBack={() => setSelectedOption()}
          disableFilter
          showUserList
        />
      );
    }

    if (selectedOption?.title === 'Who Is Above Me?') {
      return (
        <UserLevels
          selectedOption={selectedOption}
          onBack={() => setSelectedOption()}
        />
      );
    }
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Lineage FAQ's</Text>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={LIST}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => onItemPress(item)}
            style={styles.optionItem}>
            <Image
              source={item.icon}
              resizeMode="contain"
              style={styles.optionIcon}
            />
            <Text style={styles.optionName}>{item.title}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default FAQ;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30,
  },
  header: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    marginBottom: 15,
    fontSize: 16,
    color: ThemeData.APP_MAIN_COLOR,
  },
  optionItem: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 20,
    marginBottom: 10,
  },
  optionIcon: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  optionName: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
});

const LIST = [
  {
    icon: require('../../../../../assets/find-person.png'),
    title: 'Locate A Person',
  },
  {
    icon: require('../../../../../assets/sign-up-correct.png'),
    title: 'Did This Person Sign Up Properly?',
  },
  {
    icon: require('../../../../../assets/active-person.png'),
    title: 'Is This Person Active?',
  },
  {
    icon: require('../../../../../assets/upline-icon.png'),
    title: 'Who Is Above Me?',
  },
];
