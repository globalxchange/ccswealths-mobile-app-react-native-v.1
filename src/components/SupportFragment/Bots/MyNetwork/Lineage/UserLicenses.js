/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {WToast} from 'react-native-smart-tip';
import {GX_API_ENDPOINT} from '../../../../../configs';
import ThemeData from '../../../../../configs/ThemeData';
import {getUriImage} from '../../../../../utils';
import AsyncStorageHelper from '../../../../../utils/AsyncStorageHelper';
import LoadingAnimation from '../../../../LoadingAnimation';
import LicenseItem from '../../MyLicenses/LicenseItem';
import ViewHeader from './ViewHeader';

const UserLicenses = ({selectedOption, userEmail, onClose, onBack}) => {
  const [brandsList, setBrandsList] = useState();
  const [allProducts, setAllProducts] = useState();
  const [userLicenses, setUserLicenses] = useState();
  const [currentUserDetails, setCurrentUserDetails] = useState();

  useEffect(() => {
    (async () => {
      Axios.get(`${GX_API_ENDPOINT}/brokerage/stats/get/uplines`, {
        params: {email: userEmail},
      })
        .then(({data}) => {
          if (data.status) {
            setCurrentUserDetails(data.user || '');
          } else {
            WToast.show({data: data.message, position: WToast.position.TOP});
          }
        })
        .catch((error) => console.log('Error on locating user', error));
    })();
  }, [userEmail]);

  useEffect(() => {
    Axios.get('https://teller2.apimachine.com/admin/allBankers')
      .then(({data}) => {
        if (data.status) {
          setBrandsList(data.data || []);
          // console.log('data', data);
        } else {
          setBrandsList([]);
        }
      })
      .catch((error) => {
        setBrandsList([]);
      });
  }, []);

  useEffect(() => {
    (async () => {
      if (brandsList) {
        let all = [];

        for (let i = 0; i < brandsList.length; i++) {
          const brandItem = brandsList[i];

          const {data} = await Axios.get(`${GX_API_ENDPOINT}/gxb/product/get`, {
            params: {product_created_by: brandItem?.email},
          });

          all = [...all, ...(data.products || [])];
        }

        setAllProducts(all);
      }
    })();
  }, [brandsList, userEmail]);

  useEffect(() => {
    if (allProducts) {
      Axios.get(`${GX_API_ENDPOINT}/coin/vault/user/license/get`, {
        params: {email: userEmail},
      })
        .then((resp) => {
          const {data} = resp;

          if (data.status) {
            const licenceList = data.licenses || [];

            const parsedList = [];

            licenceList.forEach((item) => {
              const license = allProducts.find(
                (x) => x.product_id === item.product_id,
              );

              if (license) {
                const parsedItem = {
                  ...item,
                  name: license?.product_name,
                  icon: license?.product_icon,
                };

                parsedList.push(parsedItem);
              }
            });

            setUserLicenses(parsedList);
          } else {
            setUserLicenses([]);
          }
        })
        .catch((error) => {
          setUserLicenses([]);
          console.log('Error on getting broker data', error);
        });
    }
  }, [allProducts, userEmail]);

  return (
    <View style={styles.container}>
      <ViewHeader onBack={onBack} />
      <View style={styles.selectedOption}>
        <Image
          style={styles.optionIcon}
          resizeMode="contain"
          source={selectedOption.icon}
        />
        <Text style={styles.optionName}>{selectedOption.title}</Text>
      </View>
      <View style={styles.listContainer}>
        {currentUserDetails ? (
          <View style={styles.itemContainer}>
            <Text style={styles.itemHeader}>The Selected User</Text>
            <View style={styles.item}>
              <FastImage
                resizeMode="cover"
                style={styles.userImage}
                source={{uri: getUriImage(currentUserDetails?.profile_img)}}
              />
              <View style={styles.nameContainer}>
                <View style={{flexDirection: 'row'}}>
                  <Text numberOfLines={1} style={styles.userName}>
                    {currentUserDetails?.name}
                  </Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Text numberOfLines={1} style={styles.userEmail}>
                    {currentUserDetails?.email}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        ) : null}
        {userLicenses ? (
          <View style={{flex: 1}}>
            <FlatList
              showsVerticalScrollIndicator={false}
              data={userLicenses}
              keyExtractor={(item) => item._id}
              renderItem={({item}) => (
                <LicenseItem
                  item={item}
                  noActions
                  style={{marginBottom: 15, marginRight: 0, width: 'auto'}}
                />
              )}
              ListEmptyComponent={
                <View style={styles.emptyContainer}>
                  <Text style={styles.emptyText}>
                    No Licenses Found For This Person
                  </Text>
                </View>
              }
            />
          </View>
        ) : (
          <View style={{flex: 1, justifyContent: 'center'}}>
            <LoadingAnimation />
          </View>
        )}
      </View>
    </View>
  );
};

export default UserLicenses;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    backgroundColor: '#F1F4F6',
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  headerText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  selectedOption: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 20,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  optionIcon: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  optionName: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 15,
  },
  listContainer: {
    flex: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 20,
    borderTopWidth: 0,
  },
  backButton: {
    paddingVertical: 12,
    width: 130,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: ThemeData.APP_MAIN_COLOR,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  backButtonText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 14,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  itemContainer: {
    marginBottom: 20,
  },
  item: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemHeader: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    marginBottom: 10,
  },
  userImage: {
    width: 35,
    height: 35,
    borderRadius: 20,
    backgroundColor: '#C4C4C4',
  },
  nameContainer: {
    flex: 1,
    marginHorizontal: 10,
  },
  userName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 15,
    color: ThemeData.APP_MAIN_COLOR,
    textTransform: 'capitalize',
    paddingRight: 20,
  },
  userEmail: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 9,
    color: ThemeData.APP_MAIN_COLOR,
    paddingRight: 20,
  },
});
