/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, Text, View, FlatList} from 'react-native';
import FastImage from 'react-native-fast-image';
import {WToast} from 'react-native-smart-tip';
import {GX_API_ENDPOINT} from '../../../../../configs';
import ThemeData from '../../../../../configs/ThemeData';
import {getUriImage} from '../../../../../utils';
import AsyncStorageHelper from '../../../../../utils/AsyncStorageHelper';
import LoadingAnimation from '../../../../LoadingAnimation';
import ViewHeader from './ViewHeader';

const UserLevels = ({userQuery, selectedOption, onClose, onBack}) => {
  const [currentUserDetails, setCurrentUserDetails] = useState();
  const [uplineList, setUplineList] = useState();

  useEffect(() => {
    (async () => {
      let params;

      if (userQuery) {
        params = userQuery;
      } else {
        const email = await AsyncStorageHelper.getLoginEmail();
        params = {email};
      }

      Axios.get(`${GX_API_ENDPOINT}/brokerage/stats/get/uplines`, {
        params,
      })
        .then(({data}) => {
          // console.log('Daata', data);
          if (data.status) {
            setCurrentUserDetails(data.user || '');
            setUplineList(data.uplines || []);
          } else {
            WToast.show({data: data.message, position: WToast.position.TOP});
            onClose && onClose();
          }
        })
        .catch((error) => console.log('Error on locating user', error));
    })();
  }, [userQuery]);

  return (
    <View style={styles.container}>
      <ViewHeader onBack={onBack} />
      <View style={styles.selectedOption}>
        <Image
          style={styles.optionIcon}
          resizeMode="contain"
          source={selectedOption.icon}
        />
        <Text style={styles.optionName}>{selectedOption.title}</Text>
      </View>
      <View style={styles.listContainer}>
        {currentUserDetails ? (
          <View style={styles.list}>
            <View style={styles.itemContainer}>
              <Text style={styles.itemHeader}>The Selected User</Text>
              <View style={styles.item}>
                <FastImage
                  resizeMode="cover"
                  style={styles.userImage}
                  source={{uri: getUriImage(currentUserDetails?.profile_img)}}
                />
                <View style={styles.nameContainer}>
                  <View style={{flexDirection: 'row'}}>
                    <Text numberOfLines={1} style={styles.userName}>
                      {currentUserDetails?.name}
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text numberOfLines={1} style={styles.userEmail}>
                      {currentUserDetails?.email}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <FlatList
              showsVerticalScrollIndicator={false}
              data={uplineList}
              keyExtractor={(item) => item._id}
              renderItem={({item}) => (
                <View style={styles.itemContainer}>
                  {item?.dds <= 1 && (
                    <Text style={styles.itemHeader}>
                      {item?.dds === 0 ? 'Direct Upline' : 'Indirect Uplines'}
                    </Text>
                  )}
                  <View style={styles.item}>
                    <FastImage
                      resizeMode="cover"
                      style={styles.userImage}
                      source={{uri: getUriImage(item?.profile_img)}}
                    />
                    <View style={styles.nameContainer}>
                      <View style={{flexDirection: 'row'}}>
                        <Text
                          numberOfLines={1}
                          style={[styles.userName, {flex: 1}]}>
                          {item?.name}
                        </Text>
                        <Text
                          numberOfLines={1}
                          style={[
                            styles.userName,
                            {textAlign: 'right', paddingRight: 0},
                          ]}>
                          {item?.dds}
                        </Text>
                      </View>
                      <View style={{flexDirection: 'row'}}>
                        <Text
                          numberOfLines={1}
                          style={[styles.userEmail, {flex: 1}]}>
                          {item?.email}
                        </Text>
                        <Text
                          numberOfLines={1}
                          style={[
                            styles.userEmail,
                            {textAlign: 'right', paddingRight: 0},
                          ]}>
                          DD Level
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              )}
              ListEmptyComponent={
                <Text style={styles.emptyText}>
                  No Upline Found For This User
                </Text>
              }
            />
          </View>
        ) : (
          <View style={{flex: 1, justifyContent: 'center'}}>
            <LoadingAnimation />
          </View>
        )}
      </View>
    </View>
  );
};

export default UserLevels;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    backgroundColor: '#F1F4F6',
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  headerText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  selectedOption: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 20,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  optionIcon: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  optionName: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 15,
  },
  listContainer: {
    flex: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 20,
    borderTopWidth: 0,
  },
  list: {
    flex: 1,
    marginTop: 10,
  },
  itemContainer: {
    marginBottom: 20,
  },
  item: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemHeader: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    marginBottom: 10,
  },
  userImage: {
    width: 35,
    height: 35,
    borderRadius: 20,
    backgroundColor: '#C4C4C4',
  },
  nameContainer: {
    flex: 1,
    marginHorizontal: 10,
  },
  userName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 15,
    color: ThemeData.APP_MAIN_COLOR,
    textTransform: 'capitalize',
    paddingRight: 20,
  },
  userEmail: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 9,
    color: ThemeData.APP_MAIN_COLOR,
    paddingRight: 20,
  },
  emptyText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 14,
    color: ThemeData.APP_MAIN_COLOR,
    marginTop: 20,
  },
  backButton: {
    paddingVertical: 12,
    width: 130,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: ThemeData.APP_MAIN_COLOR,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  backButtonText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 14,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
});
