/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {GX_AUTH_URL} from '../../../../../configs';
import ThemeData from '../../../../../configs/ThemeData';
import LoadingAnimation from '../../../../LoadingAnimation';
import Moment from 'moment-timezone';
import ViewHeader from './ViewHeader';

const SignUpStatus = ({selectedOption, userEmail, onClose, onBack}) => {
  const [userData, setUserData] = useState('');
  const [statusMessage, setStatusMessage] = useState('');
  const [userCreationDate, setUserCreationDate] = useState();

  useEffect(() => {
    (async () => {
      Axios.get(`${GX_AUTH_URL}/gx/user/status/data/get`, {
        params: {email: userEmail},
      })
        .then(({data}) => {
          if (data.status) {
            setUserData(data?.gxData);

            if (data?.cognitoData?.status) {
              setUserCreationDate(
                Moment(data?.cognitoData?.user?.UserCreateDate).format(
                  'MMMM Do YYYY',
                ),
              );
            }

            let status = '';

            switch (data.userStatus) {
              case 'UNCONFIRMED':
                status = 'Email Remains Unverified';
                break;
              case 'FORCE_CHANGE_PASSWORD':
                status = 'Administrative Reset';
                break;
              case 'CONFIRMED':
                status = 'Valid User';
                break;
              case 'NotFound':
                status = 'Not Registered';
            }

            setStatusMessage(status);
          } else {
            WToast.show({data: data.message, position: WToast.position.TOP});
            onClose && onClose();
          }
        })
        .catch((error) => console.log('Error on locating user', error));
    })();
  }, [userEmail]);

  return (
    <View style={styles.container}>
      <ViewHeader onBack={onBack} />
      <View style={styles.selectedOption}>
        <Image
          style={styles.optionIcon}
          resizeMode="contain"
          source={selectedOption.icon}
        />
        <Text style={styles.optionName}>{selectedOption.title}</Text>
      </View>
      <View style={styles.listContainer}>
        {userData ? (
          <>
            {userData?.notFound ? null : (
              <View style={styles.itemContainer}>
                <Text style={styles.itemHeader}>The Selected User</Text>
                <View style={styles.item}>
                  <Image
                    resizeMode="cover"
                    style={styles.userImage}
                    source={{uri: userData?.profile_img}}
                  />
                  <View style={styles.nameContainer}>
                    <View style={{flexDirection: 'row'}}>
                      <Text numberOfLines={1} style={styles.userName}>
                        {userData?.name}
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Text numberOfLines={1} style={styles.userEmail}>
                        {userData?.email}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            )}
            <View style={styles.itemContainer}>
              <Text style={styles.itemHeader}>Status</Text>
              <View style={styles.item}>
                <Image resizeMode="cover" style={styles.userImage} />
                <View style={styles.nameContainer}>
                  <View style={{flexDirection: 'row'}}>
                    <Text numberOfLines={1} style={styles.userName}>
                      {statusMessage}
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    {userCreationDate ? (
                      <Text numberOfLines={1} style={styles.userEmail}>
                        Since {userCreationDate}
                      </Text>
                    ) : null}
                  </View>
                </View>
              </View>
            </View>
          </>
        ) : (
          <View style={{flex: 1, justifyContent: 'center'}}>
            <LoadingAnimation />
          </View>
        )}
      </View>
    </View>
  );
};

export default SignUpStatus;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    backgroundColor: '#F1F4F6',
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  headerText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  selectedOption: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 20,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  optionIcon: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  optionName: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 15,
  },
  listContainer: {
    flex: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 20,
    borderTopWidth: 0,
  },
  list: {
    flex: 1,
    marginTop: 10,
  },
  itemContainer: {
    marginBottom: 20,
  },
  item: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemHeader: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    marginBottom: 10,
  },
  userImage: {
    width: 35,
    height: 35,
    borderRadius: 20,
    backgroundColor: '#C4C4C4',
  },
  nameContainer: {
    flex: 1,
    marginHorizontal: 10,
  },
  userName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 15,
    color: ThemeData.APP_MAIN_COLOR,
    textTransform: 'capitalize',
  },
  userEmail: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 9,
    color: ThemeData.APP_MAIN_COLOR,
    paddingRight: 20,
  },
  emptyText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 14,
    color: ThemeData.APP_MAIN_COLOR,
    marginTop: 20,
  },
  backButton: {
    paddingVertical: 12,
    width: 130,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: ThemeData.APP_MAIN_COLOR,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  backButtonText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 14,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
});
