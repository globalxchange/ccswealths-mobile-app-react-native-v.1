import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {AGENCY_API_URL} from '../../../../configs';
import LoadingAnimation from '../../../LoadingAnimation';
import LeanCategoryView from './LeanCategoryView';

const LearnBot = ({publisher}) => {
  const [categoryList, setCategoryList] = useState();
  const [selectedCategory, setSelectedCategory] = useState();

  useEffect(() => {
    if (publisher) {
      (async () => {
        setCategoryList();
        setSelectedCategory();
        Axios.get(`${AGENCY_API_URL}/category/publication/${publisher.id}`)
          .then((resp) => {
            const {data} = resp;
            // console.log('Resp On ', data);
            setCategoryList(data.data || []);
          })
          .catch((error) => {
            console.log('Error On', error);
          });
      })();
    }
  }, [publisher]);

  const onItemSelected = (item) => {
    setSelectedCategory(item);
  };

  if (selectedCategory) {
    return <LeanCategoryView selectedCategory={selectedCategory} />;
  }

  return (
    <View style={styles.container}>
      {publisher ? (
        <>
          <View style={styles.headerContainer}>
            <Text style={styles.header}>
              Select One Of The Following Categories
            </Text>
          </View>
          {categoryList ? (
            <FlatList
              style={styles.listContainer}
              showsVerticalScrollIndicator={false}
              data={categoryList}
              keyExtractor={(item, index) =>
                `${item.coinSymbol || item._id}${index}`
              }
              renderItem={({item}) => (
                <ListItem item={item} onPress={() => onItemSelected(item)} />
              )}
              ListEmptyComponent={
                <Text style={styles.emptyText}>No Transactions Found</Text>
              }
            />
          ) : (
            <View style={styles.loadingContainer}>
              <LoadingAnimation />
            </View>
          )}
        </>
      ) : (
        <Text style={styles.emptyText}>Select A Publisher</Text>
      )}
    </View>
  );
};

const ListItem = ({item, onPress}) => {
  // console.log('item', item);

  return (
    <TouchableOpacity onPress={onPress} style={styles.listItem}>
      <Image
        source={{uri: item.thumbnail}}
        resizeMode="contain"
        style={styles.itemIcon}
      />
      <View style={styles.textContainer}>
        <Text numberOfLines={1} style={styles.itemTitle}>
          {item.title}
        </Text>
        <Text numberOfLines={1} style={styles.itemSubTitle}>
          {item.cv}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default LearnBot;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  header: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
  },
  headerButton: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 13,
    textDecorationLine: 'underline',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  listContainer: {
    marginTop: 20,
  },
  listItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
    borderColor: '#E7E7E7',
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  itemIcon: {
    width: 35,
    height: 35,
  },
  textContainer: {
    flex: 1,
    paddingLeft: 20,
  },
  itemTitle: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
  },
  itemSubTitle: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 11,
  },
  emptyText: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
    textAlign: 'center',
    color: '#464B4E',
  },
});
