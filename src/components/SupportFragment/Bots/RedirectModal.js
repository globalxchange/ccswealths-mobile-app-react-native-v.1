import React, {useContext} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../../configs/ThemeData';
import {SupportContext} from '../../../contexts/SupportContext';
import PopupLayout from '../../../layouts/PopupLayout';

const RedirectModal = () => {
  const {
    setIsRedirectModalOpen,
    isRedirectModalOpen,
    redirectData,
  } = useContext(SupportContext);

  return (
    <PopupLayout
      isOpen={isRedirectModalOpen}
      onClose={() => setIsRedirectModalOpen(false)}
      autoHeight
      headerTitle="Redirect Confirmation">
      <View style={styles.container}>
        <Text style={styles.message}>
          You Will Now Be Redirected To The {redirectData?.name} Page
        </Text>
        <View style={styles.actionContainer}>
          <TouchableOpacity
            onPress={redirectData?.onNavigate || null}
            style={styles.actionFilled}>
            <Text style={styles.actionFilledText}>Proceed</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setIsRedirectModalOpen(false)}
            style={styles.actionOutline}>
            <Text style={styles.actionOutlineText}>Back To Bots</Text>
          </TouchableOpacity>
        </View>
      </View>
    </PopupLayout>
  );
};

export default RedirectModal;

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  message: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    textAlign: 'center',
  },
  actionContainer: {
    flexDirection: 'row',
    marginTop: 30,
  },
  actionFilled: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
    height: 40,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    marginRight: 10,
  },
  actionFilledText: {
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 12,
  },
  actionOutline: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
    height: 40,
    borderColor: ThemeData.APP_MAIN_COLOR,
    borderWidth: 1,
  },
  actionOutlineText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 12,
  },
});
