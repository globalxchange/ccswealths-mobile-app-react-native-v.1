/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import Moment from 'moment-timezone';
import ThemeData from '../../../../configs/ThemeData';

const PurchasedLicenses = ({selectedLicense}) => {
  const getMethod = (cycle) => {
    let method = '';

    switch (cycle) {
      case 'lifetime':
        method = 'One Time';
        break;
      case 'monthly':
        method = 'Monthly Subscription';
        break;
      case 'annual':
        method = 'Annual Subscription';
        break;
      case 'staking':
        method = 'Staking';
        break;
    }

    return method;
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={selectedLicense?.licenses}
        keyExtractor={(item) => item._id}
        showsVerticalScrollIndicator={false}
        renderItem={({item}) => (
          <View style={styles.item}>
            <View style={styles.row}>
              <Text numberOfLines={1} style={styles.itemCode}>
                {item.license_id}
              </Text>
              <Text style={styles.itemDate}>
                {Moment(item.timestamp).format('MMM Do YYYY')}
              </Text>
            </View>
            <View style={styles.row}>
              <Text style={styles.method}>
                Billing Method: {getMethod(item.billing_method)}
              </Text>
              <Text style={styles.dateLabel}>Issuance Date</Text>
            </View>
            <View
              style={[
                styles.statusRibbon,
                {
                  backgroundColor:
                    item.license_status === 'active' ? '#30BC96' : '#FF2D55',
                },
              ]}
            />
          </View>
        )}
      />
    </View>
  );
};

export default PurchasedLicenses;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  item: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 25,
    marginBottom: 10,
  },
  itemCode: {
    flex: 1,
    marginRight: 10,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 17,
  },
  itemDate: {
    fontFamily: ThemeData.FONT_MEDIUM,
    color: ThemeData.APP_MAIN_COLOR,
  },
  method: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 10,
  },
  dateLabel: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 10,
  },
  statusRibbon: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    width: 10,
  },
});
