import React from 'react';
import {Image, StyleSheet, Text, TextInput, View} from 'react-native';
import ThemeData from '../../../../configs/ThemeData';

const SearchBar = ({searchText, setSearchText, placeHolder}) => {
  return (
    <View style={styles.searchContainer}>
      <Image
        style={styles.searchIcon}
        resizeMode="contain"
        source={require('../../../../assets/search-icon.png')}
      />
      <TextInput
        style={styles.inputText}
        placeholder={placeHolder}
        placeholderTextColor={'#878788'}
        value={searchText}
        onChangeText={(text) => setSearchText(text)}
      />
    </View>
  );
};

export default SearchBar;

const styles = StyleSheet.create({
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
  },
  searchIcon: {
    width: 20,
    height: 20,
  },
  inputText: {
    flex: 1,
    marginLeft: 10,
    height: 50,
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
  },
});
