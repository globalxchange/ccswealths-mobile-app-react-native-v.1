import React, {useContext} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {OneToOneChatContext} from '../../../../contexts/OneToOneChatContext';

const ChatHeader = () => {
  const {selectedUser, setSelectedUser} = useContext(OneToOneChatContext);

  return (
    <View style={styles.chatHeader}>
      <TouchableOpacity
        style={styles.backButton}
        onPress={() => setSelectedUser()}>
        <Image
          style={styles.backArrow}
          source={require('../../../../assets/back-arrow-icon-white.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <Image
        style={styles.agentAvatar}
        source={{uri: selectedUser?.avatar}}
        resizeMode="cover"
      />
      <View style={styles.header}>
        <Text style={styles.agentName}>{selectedUser?.first_name}</Text>
        <Text style={styles.brandingText}>{selectedUser?.bio}</Text>
      </View>
    </View>
  );
};

export default ChatHeader;

const styles = StyleSheet.create({
  chatHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
  },
  header: {
    flex: 1,
  },
  agentAvatar: {
    width: 30,
    height: 30,
    backgroundColor: 'white',
    borderRadius: 15,
    marginRight: 10,
  },
  agentName: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
  brandingText: {
    fontFamily: 'Montserrat',
    color: 'white',
    fontSize: 8,
    opacity: 0.7,
  },
  backButton: {
    width: 50,
    height: 30,
    marginRight: 10,
    padding: 5,
  },
  backArrow: {
    flex: 1,
    height: null,
    width: null,
  },
});
