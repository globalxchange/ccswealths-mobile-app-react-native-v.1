import React from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import FAQHeader from './FAQHeader';
import FAQCard from './FAQCard';
import FAQItem from './FAQItem';

const FAQ = () => {
  return (
    <View style={styles.container}>
      <View style={styles.background} />
      <FAQHeader />
      <ScrollView
        style={styles.scrollView}
        showsVerticalScrollIndicator={false}>
        <View style={styles.cardsContainer}>
          {list.map((item, index) => (
            <FAQCard key={item.header} data={item} pos={index + 1} />
          ))}
        </View>
        <Text style={styles.faqHeader}>FREQUENTLY ASKED QUESTIONS</Text>
        <FAQItem no={1} />
        <FAQItem no={2} />
        <FAQItem no={3} />
      </ScrollView>
    </View>
  );
};

export default FAQ;

const styles = StyleSheet.create({
  container: {flex: 1},
  background: {
    position: 'absolute',
    top: 100,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#F5F5F5',
    zIndex: -1,
  },
  scrollView: {
    paddingHorizontal: 20,
    marginTop: 20,
  },
  cardsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  faqHeader: {
    color: '#000000',
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
    marginVertical: 20,
  },
});

const list = [
  {
    header: 'GET STARTED',
    image: require('../../../assets/project-management-icon.png'),
    subHeading: 'Lorem Ipsum is simply',
  },
  {
    header: 'ACCOUNTS',
    image: require('../../../assets/accounts-icon.png'),
    subHeading: 'Lorem Ipsum is simply',
  },
  {
    header: 'SUBSCRIPTION',
    image: require('../../../assets/subscription-models-icon.png'),
    subHeading: 'Lorem Ipsum is simply',
  },
  {
    header: 'HELP',
    image: require('../../../assets/help-icon.png'),
    subHeading: 'Lorem Ipsum is simply',
  },
];
