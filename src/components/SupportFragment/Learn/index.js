import React, {useState} from 'react';
import Landing from './Landing';
import LearnView from './LearnView';

const Learn = () => {
  const [devLoggedIn, setDevLoggedIn] = useState(__DEV__ || false);

  // if (devLoggedIn) {
  return <LearnView />;
  // }

  // return <Landing setDevLoggedIn={setDevLoggedIn} />;
};

export default Learn;
