/* eslint-disable react-native/no-inline-styles */
import React, {useContext} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import {
  formatterHelper,
  getAssetData,
  percentageFormatter,
  usdValueFormatter,
} from '../../utils';
import CurrencyView from '../CheckoutComponents/CurrencyView';

const TotalFragment = ({quoteResp}) => {
  const {cryptoTableData} = useContext(AppContext);

  return (
    <View style={styles.container}>
      <Text style={styles.totalFeeText}>
        Total Fees Are{' '}
        <Text style={{fontFamily: ThemeData.FONT_SEMI_BOLD}}>
          {percentageFormatter.format(
            quoteResp?.percentage_fee_by_volume?.fees_with_broker,
          )}
          %
        </Text>{' '}
        Of The Trade Volume
      </Text>
      <View style={styles.coinViewContainer}>
        <View style={styles.coinDetails}>
          <Text style={styles.coinValue}>
            {formatterHelper(quoteResp?.gx_fee, quoteResp?.fees_in_coin)}
          </Text>
          <View style={styles.devider} />
          <Image
            source={{
              uri: getAssetData(quoteResp?.fees_in_coin, cryptoTableData)
                ?.coinImage,
            }}
            style={styles.coinIcon}
            resizeMode="contain"
          />
        </View>
        <View style={styles.coinDetails}>
          <Text style={styles.coinValue}>
            {usdValueFormatter.format(quoteResp?.gx_fee_usd)}
          </Text>
          <View style={styles.devider} />
          <Image
            source={{
              uri: getAssetData('USD', cryptoTableData)?.coinImage,
            }}
            style={styles.coinIcon}
            resizeMode="contain"
          />
        </View>
      </View>
      <Text style={styles.feeHeader}>
        This fee structure is calibrated with the quote price for this trade
      </Text>
      <CurrencyView
        coin={getAssetData(quoteResp?.purchased_from || 'USD', cryptoTableData)}
        value={quoteResp?.final_1_to_currency_in_from_currency}
        title={''}
        disabled
        onCoinClick={() => {}}
      />
    </View>
  );
};

export default TotalFragment;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  totalFeeText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#464B4E',
    textAlign: 'center',
    fontSize: 15,
  },
  feeHeader: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#464B4E',
    fontSize: 15,
    marginBottom: -30,
  },
  coinViewContainer: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    height: 65,
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 30,
  },
  coinDetails: {
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    borderLeftColor: ThemeData.BORDER_COLOR,
    borderLeftWidth: 1,
    height: '100%',
    flex: 1,
  },
  coinIcon: {
    width: 25,
    height: 25,
    marginLeft: 10,
  },
  coinValue: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#979797',
    fontSize: 17,
    flex: 1,
    textAlign: 'center',
  },
  devider: {
    width: 1,
    height: '100%',
    backgroundColor: ThemeData.BORDER_COLOR,
    marginHorizontal: 10,
  },
});
