/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import {formatterHelper, getUriImage, usdValueFormatter} from '../../utils';
import FastImage from 'react-native-fast-image';
import LoadingAnimation from '../LoadingAnimation';
import PopupLayout from '../../layouts/PopupLayout';
import ThemeData from '../../configs/ThemeData';

const FeeAudit = ({
  isShow,
  selectedCrypto,
  paymentCurrency,
  onClose,
  spendValue,
  gettingValue,
  quoteResp,
  spendEdited,
  isInstantFund,
  type,
  instantFundView = false,
}) => {
  const [bankerData, setBankerData] = useState('');
  const [brokerData, setBrokerData] = useState('');
  const [platformFee, setPlatformFee] = useState('');

  useEffect(() => {
    if (quoteResp) {
      // console.log('quoteResp', quoteResp);
      if (isInstantFund) {
        setBankerData([quoteResp[0].bankerData, quoteResp[1].bankerData]);
        setBrokerData([quoteResp[0].brokerData, quoteResp[1].brokerData]);
        setPlatformFee([quoteResp[0].gx_fee_usd, quoteResp[1].gx_fee_usd]);
      } else {
        setBankerData(quoteResp.bankerData);
        setBrokerData(quoteResp.brokerData);
        setPlatformFee(quoteResp.gx_fee_usd);
      }
    }
  }, [spendEdited, quoteResp, isInstantFund]);

  // console.log('brokerData', brokerData);
  // console.log('quoteResp', quoteResp);
  // console.log('instantFundView', instantFundView);
  // console.log('isInstantFund', isInstantFund);

  const totalFee =
    ((isInstantFund
      ? instantFundView
        ? bankerData[1]?.total_fee_usd
        : bankerData[0]?.total_fee_usd
      : bankerData.total_fee_usd) || 0) +
    ((isInstantFund
      ? instantFundView
        ? platformFee[1]
        : platformFee[0]
      : platformFee) || 0) +
    ((isInstantFund
      ? instantFundView
        ? brokerData[1]?.fee_usd
        : brokerData[0]?.fee_usd
      : brokerData.fee_usd) || 0);

  return (
    <PopupLayout
      autoHeight
      noScrollView
      isOpen={isShow}
      onClose={onClose}
      headerTitle={`You Are ${
        type === 'Deposit'
          ? 'Sending'
          : type === 'Withdraw'
          ? 'Withdrawing'
          : 'Selling'
      } ${formatterHelper(spendValue || 0, paymentCurrency.coinSymbol)} ${
        paymentCurrency.coinSymbol
      }`}>
      <View style={styles.container}>
        {quoteResp ? (
          <>
            <View style={styles.viewContainer}>
              <View style={styles.valueContainer}>
                <Text style={styles.label}>Banker Fees</Text>
                <View style={styles.feeContainer}>
                  <FastImage
                    source={{
                      uri: getUriImage(
                        isInstantFund
                          ? instantFundView
                            ? bankerData[1]?.profile_img
                            : bankerData[0]?.profile_img
                          : bankerData.profile_img,
                      ),
                    }}
                    resizeMode="contain"
                    style={styles.feeIcon}
                  />
                  <Text style={styles.bankerName}>
                    {isInstantFund
                      ? instantFundView
                        ? bankerData[1]?.name
                        : bankerData[0]?.name
                      : bankerData.name}
                  </Text>
                  <View style={styles.fee}>
                    <Text style={styles.feeAmount}>
                      {usdValueFormatter.format(
                        (isInstantFund
                          ? instantFundView
                            ? bankerData[1]?.total_fee_usd
                            : bankerData[0]?.total_fee_usd
                          : bankerData.total_fee_usd) || 0,
                      )}{' '}
                      USD
                    </Text>
                  </View>
                </View>
              </View>
              <View style={styles.valueContainer}>
                <Text style={styles.label}>Platform Fees</Text>
                <View style={styles.feeContainer}>
                  <Image
                    source={require('../../assets/insta-crypto-icon.png')}
                    resizeMode="contain"
                    style={styles.feeIconBig}
                  />
                  <View style={styles.fee}>
                    <Text style={styles.feeAmount}>
                      {usdValueFormatter.format(
                        (isInstantFund
                          ? instantFundView
                            ? platformFee[1]
                            : platformFee[0]
                          : platformFee) || 0,
                      )}{' '}
                      USD
                    </Text>
                  </View>
                </View>
              </View>
              <View style={styles.valueContainer}>
                <View
                  style={[styles.feeContainer, {opacity: 0.5, elevation: 0}]}>
                  <Image
                    source={require('../../assets/sef-icon-full-icon-inverted.png')}
                    resizeMode="contain"
                    style={[styles.feeIconBig, {height: 15}]}
                  />
                  <View style={styles.fee}>
                    <Text style={[styles.bankerName, {textAlign: 'right'}]}>
                      Coming Soon
                    </Text>
                  </View>
                </View>
              </View>
              <View style={styles.valueContainer}>
                <Text style={styles.label}>Broker Fees</Text>
                <View style={styles.feeContainer}>
                  <Image
                    source={{
                      uri: isInstantFund
                        ? instantFundView
                          ? brokerData[1]?.profile_img
                          : brokerData[0]?.profile_img
                        : brokerData.profile_img,
                    }}
                    resizeMode="contain"
                    style={styles.feeIcon}
                  />
                  <Text style={styles.bankerName}>
                    {isInstantFund
                      ? instantFundView
                        ? brokerData[1]?.name || brokerData[1]?.username
                        : brokerData[0]?.name || brokerData[0]?.username
                      : brokerData.name || brokerData.username}
                  </Text>
                  <View style={styles.fee}>
                    <Text style={styles.feeAmount}>
                      {usdValueFormatter.format(
                        (isInstantFund
                          ? instantFundView
                            ? brokerData[1]?.fee_usd
                            : brokerData[0]?.fee_usd
                          : brokerData.fee_usd) || 0,
                      )}{' '}
                      USD
                    </Text>
                  </View>
                </View>
              </View>
              <View style={styles.valueContainer}>
                <Text style={styles.label}></Text>
                <View style={styles.feeContainer}>
                  <Text style={styles.bankerName}>Total Fees</Text>
                  <View style={styles.fee}>
                    <Text style={styles.feeAmount}>
                      {usdValueFormatter.format(totalFee || 0, 'USD')} USD
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <Text numberOfLines={1} adjustsFontSizeToFit style={styles.header}>
              {`You Will Get ${formatterHelper(
                gettingValue || 0,
                selectedCrypto.coinSymbol,
              )} ${selectedCrypto.coinSymbol}`}
            </Text>
            {/* {isInstantFund && !instantFundView && (
              <Text
                numberOfLines={1}
                adjustsFontSizeToFit
                onPress={() => setInstantFundView(true)}
                style={styles.tradeFeesButton}>
                Analyze The Trade Fees After The Fund Is Complete
              </Text>
            )} */}
          </>
        ) : (
          <View style={styles.loadingContainer}>
            <LoadingAnimation />
          </View>
        )}
        {/* <View style={styles.actionContainer}>
          {isInstantFund && instantFundView && (
            <TouchableOpacity
              style={[styles.actionButton, {marginRight: 10}]}
              onPress={() => setInstantFundView(false)}>
              <Text style={styles.buttonText}>Part One</Text>
            </TouchableOpacity>
          )}
        </View> */}
      </View>
    </PopupLayout>
  );
};

export default FeeAudit;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    marginBottom: -20,
  },
  header: {
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    marginBottom: 10,
    fontSize: 16,
    fontFamily: 'Montserrat-Bold',
  },
  viewContainer: {
    // flex: 1,
    justifyContent: 'center',
    paddingVertical: 10,
  },
  valueContainer: {
    marginBottom: 20,
  },
  label: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat',
    fontSize: 11,
    marginBottom: 10,
  },
  feeContainer: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,

    elevation: 1,
  },
  feeIcon: {
    height: 22,
    width: 22,
  },
  feeIconBig: {
    width: 80,
  },
  bankerName: {
    paddingHorizontal: 10,
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  fee: {
    flex: 1,
  },
  feeAmount: {
    textAlign: 'right',
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
    fontSize: 17,
  },
  feePercentage: {
    textAlign: 'right',
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat',
    fontSize: 8,
  },
  actionButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    paddingVertical: 12,
    borderRadius: 6,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
  },
  actionContainer: {
    flexDirection: 'row',
    marginTop: 10,
  },
  loadingContainer: {
    justifyContent: 'center',
    paddingVertical: 50,
  },
  tradeFeesButton: {
    fontSize: 12,
    marginBottom: 10,
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat',
    textDecorationLine: 'underline',
    textAlign: 'center',
  },
});
