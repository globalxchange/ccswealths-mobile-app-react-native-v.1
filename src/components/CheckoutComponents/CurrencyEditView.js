/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Image,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import {formatterHelper} from '../../utils';

const CurrencyEditView = ({coin, title, onChange, value, onClose}) => {
  return (
    <View
      style={[
        styles.container,
        {marginBottom: Platform.OS === 'ios' ? 150 : 0},
      ]}>
      <View style={styles.headerContainer}>
        <Text style={styles.title}>{title}</Text>
      </View>
      <View style={styles.coinViewContainer}>
        <TextInput
          placeholder={formatterHelper(0, coin.coinSymbol)}
          autoFocus
          style={styles.coinValue}
          keyboardType="decimal-pad"
          returnKeyType="done"
          value={value}
          onChangeText={(text) => onChange(text)}
        />
        <View style={styles.coinDetails}>
          <Image
            style={styles.coinIcon}
            resizeMode="contain"
            source={{uri: coin.coinImage}}
          />
          <Text style={styles.coinName}>{coin.coinSymbol}</Text>
        </View>
      </View>
      <TouchableOpacity onPress={onClose} style={styles.nextButton}>
        <Text style={styles.nextText}>Next</Text>
      </TouchableOpacity>
    </View>
  );
};

export default CurrencyEditView;

const styles = StyleSheet.create({
  container: {
    marginTop: 25,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 15,
  },
  feeAuditButton: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    textDecorationLine: 'underline',
    fontSize: 12,
  },
  coinViewContainer: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 6,
    height: 65,
    alignItems: 'center',
    marginTop: 15,
  },
  coinValue: {
    flex: 1,
    textAlign: 'center',
    color: '#979797',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 23,
    paddingHorizontal: 30,
  },
  coinDetails: {
    paddingHorizontal: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderLeftColor: ThemeData.BORDER_COLOR,
    borderLeftWidth: 1,
    height: '100%',
    width: '35%',
  },
  coinIcon: {
    width: 25,
    height: 25,
    marginRight: 10,
  },
  coinName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#979797',
    fontSize: 18,
  },
  nextButton: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    height: 50,
    width: 180,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  nextText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: 'white',
    fontSize: 16,
  },
});
