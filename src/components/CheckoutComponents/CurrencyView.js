import React from 'react';
import {
  ActivityIndicator,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import {formatterHelper} from '../../utils';

const CurrencyView = ({
  coin,
  value,
  title,
  onPress,
  showFeeAudit,
  onAuditOpen,
  isLoading,
  disabled,
  onCoinClick,
}) => {
  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.title}>{title}</Text>
        {showFeeAudit && (
          <Text
            onPress={onAuditOpen}
            style={[styles.feeAuditButton, !value && {opacity: 0.25}]}>
            Official Fee Audit
          </Text>
        )}
      </View>
      <TouchableOpacity
        disabled={disabled}
        onPress={onPress}
        style={[styles.coinViewContainer]}>
        <Text style={styles.coinValue}>
          {formatterHelper(value || 0, coin.coinSymbol)}
        </Text>
        <TouchableOpacity
          disabled={!onCoinClick}
          style={[
            styles.coinDetails,
            onCoinClick && {backgroundColor: '#F1F4F6'},
          ]}
          onPress={onCoinClick}>
          <Image
            style={styles.coinIcon}
            resizeMode="contain"
            source={{uri: coin.coinImage}}
          />
          <Text style={styles.coinName}>{coin.coinSymbol}</Text>
        </TouchableOpacity>
        {isLoading && (
          <View style={styles.loadingContainer}>
            <ActivityIndicator color={ThemeData.APP_MAIN_COLOR} />
          </View>
        )}
      </TouchableOpacity>
    </View>
  );
};

export default CurrencyView;

const styles = StyleSheet.create({
  container: {
    marginTop: 25,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 14,
  },
  feeAuditButton: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    textDecorationLine: 'underline',
    fontSize: 12,
  },
  coinViewContainer: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 6,
    height: 65,
    alignItems: 'center',
    marginTop: 15,
  },
  coinValue: {
    flex: 1,
    textAlign: 'center',
    color: '#979797',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 23,
    paddingHorizontal: 30,
  },
  coinDetails: {
    paddingHorizontal: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderLeftColor: ThemeData.BORDER_COLOR,
    borderLeftWidth: 1,
    height: '100%',
    width: '35%',
  },
  coinIcon: {
    width: 25,
    height: 25,
    marginRight: 10,
  },
  coinName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#979797',
    fontSize: 18,
  },
  loadingContainer: {
    ...StyleSheet.absoluteFill,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(221, 221, 221, 0.7)',
    borderRadius: 6,
    zIndex: 1,
  },
});
