import React, {useContext, useEffect, useState} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';
import {showToastMessage} from '../../utils';
import AddressForm from './AddressForm';
import QuoteForm from './QuoteForm';

const ExternalWithdrawSheet = ({isOpen, onClose, activeWallet}) => {
  const {isShowQuote, setIsShowQuote} = useContext(WithdrawalContext);
  const [addressInput, setAddressInput] = useState('');

  const onProceedAmount = () => {
    if (!addressInput) {
      return showToastMessage('Please Enter External Wallet Address');
    }
    setIsShowQuote(true);
  };

  return (
    <BottomSheetLayout isOpen={isOpen} onClose={onClose} reactToKeyboard>
      <View style={styles.container}>
        <View style={styles.headerImage}>
          <Image
            source={{uri: activeWallet.coinImage}}
            style={styles.headerIcon}
          />
          <Text style={styles.headerTitle}>{activeWallet.coinName}</Text>
        </View>
        {isShowQuote ? (
          <View style={styles.crumbStyle}>
            <Text onPress={(e) => setIsShowQuote(false)}>
              Enter Address{` -> `}
            </Text>
            <View style={styles.activeCrumb}>
              <Text>Enter Amount</Text>
            </View>
          </View>
        ) : (
          <View style={styles.crumbStyle}>
            <Text>Actions{` -> `}</Text>
            <View style={styles.activeCrumb}>
              <Text>Enter Address</Text>
            </View>
          </View>
        )}

        <View style={styles.viewContainer}>
          {isShowQuote ? (
            <QuoteForm
              onClose={onClose}
              addressInput={addressInput}
              activeWallet={activeWallet}
            />
          ) : (
            <AddressForm
              activeWallet={activeWallet}
              addressInput={addressInput}
              setAddressInput={setAddressInput}
              onProceedAmount={onProceedAmount}
            />
          )}
        </View>
      </View>
    </BottomSheetLayout>
  );
};

export default ExternalWithdrawSheet;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 30,
    paddingVertical: 25,
  },
  headerIcon: {
    // resizeMode: 'contain',
    height: 40,
    // marginLeft: 'auto',
    // marginRight: 'auto',
    width: 40,
  },
  viewContainer: {
    marginTop: 20,
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    paddingTop: 10,
  },
  headerTitle: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: 35,
    fontWeight: '700',
    color: '#464B4E',
    paddingLeft: 5,
  },
  crumbStyle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 24,
    // paddingBottom: 10,
    color: '#5F6163',
  },
  activeCrumb: {
    borderStyle: 'solid',
    borderBottomColor: '#5F6163',
    borderBottomWidth: 1,
    marginBottom: 30,
    fontWeight: 'bold',
  },
});
