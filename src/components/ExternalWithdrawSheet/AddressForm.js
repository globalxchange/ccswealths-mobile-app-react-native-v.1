import React, {useContext, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import Clipboard from '@react-native-community/clipboard';
import WithdrawalContext from '../../contexts/WithdrawalContext';

const AddressForm = ({
  activeWallet,
  addressInput,
  setAddressInput,
  onProceedAmount,
}) => {
  const {setExternalAddress} = useContext(WithdrawalContext);

  const onPasteClick = async () => {
    const copiedText = await Clipboard.getString();
    setAddressInput(copiedText.trim());
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>Step 1: Enter Address</Text>
      </View>
      <Text style={styles.descText}>
        Enter The External {activeWallet.coinSymbol} Address That You Want The
        Coins Sent To
      </Text>
      <TextInput
        style={styles.addressInput}
        placeholder="Enter Address"
        value={addressInput}
        onChangeText={(text) => {
          setAddressInput(text);
          setExternalAddress(text);
        }}
      />
      <TouchableOpacity style={styles.pasteBtn} onPress={onPasteClick}>
        <Image
          source={require('../../assets/paste-icon-blue.png')}
          style={styles.pasteIcon}
        />
        <Text style={styles.pasteText}>Click To Paste Clipboard</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.actionBtn} onPress={onProceedAmount}>
        <Text style={styles.actionBtnText}>Proceed To Amount</Text>
      </TouchableOpacity>
    </View>
  );
};

export default AddressForm;

const styles = StyleSheet.create({
  container: {},
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 22,
    fontWeight: '800',
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  addressInput: {
    height: 63,
    marginVertical: 40,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 13,
    height: 16,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
    color: '#5F6163',
  },
  actionBtn: {
    backgroundColor: '#5F6163',
    borderRadius: 9,
    height: 54,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
});
