import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const ActionButton = ({text, onPress, style}) => {
  return (
    <TouchableOpacity style={[styles.container, style]} onPress={onPress}>
      <Text style={styles.text}>{text}</Text>
    </TouchableOpacity>
  );
};

export default ActionButton;

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
  },
  text: {
    color: 'white',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
  },
});
