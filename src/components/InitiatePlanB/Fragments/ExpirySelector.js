import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import ThemeData from '../../../configs/ThemeData';
import {formatterHelper} from '../../../utils';
import QuoteInput from '../../Connect/QuoteInput';
import ActionButton from '../ActionButton';

const ExpirySelector = ({onNext, expiryDays, setExpiryDays}) => {
  const onNextClick = () => {
    if (!expiryDays) {
      return WToast.show({
        data: 'Please Enter The Expiry',
        position: WToast.position.TOP,
      });
    }

    onNext();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Expiry</Text>
      <Text style={styles.subHeader}>
        How Long Does Your Recipient Have To Redeem The Coins
      </Text>
      <View style={{marginBottom: 40}}>
        <QuoteInput
          image={require('../../../assets/calender-iconjpg.png')}
          unit={'Days'}
          enabled
          value={expiryDays}
          setValue={setExpiryDays}
          placeholder={formatterHelper('0')}
          // isLoading={isGettingLoading}
        />
      </View>
      <ActionButton text="Proceed To Select Currency" onPress={onNextClick} />
    </View>
  );
};

export default ExpirySelector;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 40,
    paddingVertical: 20,
  },
  header: {
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
});
