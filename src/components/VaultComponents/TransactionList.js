import axios from 'axios';
import React, {useContext, useEffect, useMemo, useState} from 'react';
import {
  Image,
  SectionList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {APP_CODE, GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import {
  formatterHelper,
  getAssetData,
  usdValueFormatterWithoutSign,
} from '../../utils';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import SkeltonItem from '../SkeltonItem';
import Moment from 'moment-timezone';
import {useRoute} from '@react-navigation/native';
import CreditIcon from '../../assets/credit.svg';
import DebitIcon from '../../assets/debit.svg';

const TransactionList = ({
  activeWallet,
  typeFilter,
  statusFilter,
  openTxnAudit = () => {},
  isSearchOpen,
  setSearchList,
  selectedApp,
}) => {
  const {params} = useRoute();
  const {cryptoTableData} = useContext(AppContext);

  const [transactionsList, setTransactionsList] = useState();
  const [filteredTxnList, setFilteredTxnList] = useState();
  const [isLoading, setIsLoading] = useState(true);

  const [bondTxns, setBondTxns] = useState([]);
  const [moneyMarketTransactions, setMoneyMarketTransactions] = useState([]);

  useEffect(() => {
    console.log(params.type, 'kjwehfklerfkljberkjfbejrf');
    if (params.type === 'bonds') {
      getBondTransactions();
    } else if (params.type === 'moneymarket') {
      getMoneyMarketTransactions();
    } else {
      getTransactions();
    }
  }, [activeWallet, selectedApp, params]);

  useEffect(() => {
    setSearchList(transactionsList);
    return () => {
      setSearchList();
    };
  }, [isSearchOpen]);

  // useEffect(() => {
  //   if (transactionsList) {
  //     if (typeFilter === 'All') {
  //       setFilteredTxnList(transactionsList);
  //     } else {
  //       const newList = [];

  //       let type = statusFilter.toLowerCase();

  //       transactionsList.forEach((item) => {
  //         const isDeposit = typeFilter === 'Deposits';

  //         const currentStatus =
  //           type === 'all' ? item.status.toLowerCase() : type;

  //         if (
  //           item.deposit === isDeposit &&
  //           item.status.toLowerCase() === currentStatus
  //         ) {
  //           newList.push(item);
  //         }
  //       });

  //       setFilteredTxnList(newList);
  //     }
  //   } else if (bondTxns) {
  //     setFilteredTxnList(bondTxns);
  //   } else if (moneyMarketTransactions) {
  //     setFilteredTxnList(moneyMarketTransactions);
  //   }
  // }, [
  //   typeFilter,
  //   statusFilter,
  //   transactionsList,
  //   bondTxns,
  //   moneyMarketTransactions,
  // ]);

  useEffect(() => {
    if (params.type === 'moneymarket') {
      setFilteredTxnList(moneyMarketTransactions);
    } else if (params.type === 'bonds') {
      setFilteredTxnList(bondTxns);
    } else {
      if (typeFilter === 'All') {
        setFilteredTxnList(transactionsList);
      } else {
        const newList = [];

        let type = statusFilter.toLowerCase();

        transactionsList.forEach((item) => {
          const isDeposit = typeFilter === 'Deposits';

          const currentStatus =
            type === 'all' ? item.status.toLowerCase() : type;

          if (
            item.deposit === isDeposit &&
            item.status.toLowerCase() === currentStatus
          ) {
            newList.push(item);
          }
        });

        setFilteredTxnList(newList);
      }
    }
  }, [
    params.type,
    transactionsList,
    bondTxns,
    moneyMarketTransactions,
    typeFilter,
    statusFilter,
  ]);

  const getBondTransactions = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();
    setIsLoading(true);
    axios
      .get(
        `https://comms.globalxchange.com/coin/iced/interest/logs/get?email=${email}&coin=${activeWallet.coinSymbol}`,
      )
      .then(({data}) => {
        if (data.status) {
          const rawData = data.interestLogs || [];

          const txns = rawData.map((txn) => ({
            ...txn,
            name: txn._id,
            email: ` ${formatterHelper(txn.amount, txn.coin)} ${txn.coin}`,
            profile_img: txn.withdraw ? (
              <DebitIcon width={29} height={29} />
            ) : (
              <CreditIcon width={29} height={29} />
            ),
          }));
          setBondTxns(txns);
          setIsLoading(false);
        }
      });
  };

  const getMoneyMarketTransactions = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();

    setIsLoading(true);
    axios
      .get(
        `https://comms.globalxchange.com/coin/vault/service/user/app/interest/logs/get?email=${email}&app_code=${APP_CODE}&coin=${activeWallet?.coinSymbol}`,
        // `https://comms.globalxchange.com/coin/vault/service/user/app/interest/logs/get?email=${email}&app_code=${APP_CODE}&coin=${activeWallet.coinSymbol}}`,
      )
      .then(({data}) => {
        if (data.status) {
          const rawData = data.logs[0].logs;

          const txns = rawData.map((txn) => ({
            ...txn,
            name: txn._id,
            email: ` ${formatterHelper(txn.amount, txn.coin)} ${txn.coin}`,
            profile_img: txn.withdraw ? (
              <DebitIcon width={29} height={29} />
            ) : (
              <CreditIcon width={29} height={29} />
            ),
          }));

          setMoneyMarketTransactions(txns);
          setIsLoading(false);
        }
      });
  };

  const getTransactions = async () => {
    setIsLoading(true);

    const profileId = await AsyncStorageHelper.getProfileId();

    const postData = {
      app_code: selectedApp?.app_code || APP_CODE,
      profile_id: selectedApp?.profile_id || profileId,
      coin: activeWallet.coinSymbol,
    };

    axios
      .post(`${GX_API_ENDPOINT}/coin/vault/service/txns/get`, postData)
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          const rawData = data.txns || [];

          const txns = rawData.map((txn) => ({
            ...txn,
            name: txn._id,
            email: ` ${formatterHelper(txn.amount, txn.coin)} ${txn.coin}`,
            profile_img: getAssetData(txn.coin, cryptoTableData)?.coinImage,
          }));

          setTransactionsList(txns);
        }
        setIsLoading(false);
        // console.log('TXN List', data.txns);
      })
      .catch((error) => console.log('Error getting txn list', error));
  };

  const groupedTxns = useMemo(() => {
    let tempList = [...(filteredTxnList || [])];
    const addedList = [];
    const sortedList = [];

    tempList.forEach((tmpItem) => {
      if (!addedList.includes(tmpItem)) {
        const date = Moment(tmpItem.timestamp).startOf('day');
        const subList = [];
        addedList.push(tmpItem);
        subList.push(tmpItem);

        tempList.forEach((item) => {
          if (!addedList.includes(item)) {
            const itemDate = Moment(item.timestamp);
            if (date.isSame(itemDate, 'day')) {
              addedList.push(item);
              subList.push(item);
            }
          }
        });

        let title = date.format('dddd MMMM Do YYYY');
        if (date.isSame(new Date(), 'day')) {
          title = 'Today';
        } else if (date.diff(new Date(), 'day') === -1) {
          title = 'Yesterday';
        }

        sortedList.push({
          title,
          data: subList,
        });
      }
    });

    return sortedList;
  }, [filteredTxnList]);

  const conditionalAmount = (item) => {
    if (params.type === 'bonds' || params.type === 'moneymarket') {
      if (activeWallet.coinSymbol === 'USDT') {
        return usdValueFormatterWithoutSign.format(
          item.withdraw ? item.amount : item.credited_interest,
        );
      } else {
        return item.withdraw
          ? item.amount.toFixed(9)
          : item.credited_interest.toFixed(9);
        // return formatterHelper(
        //   item.withdraw ? item.amount : item.credited_interest,
        //   activeWallet.coinSymbol,
        // );
      }
    } else {
      if (activeWallet.coinSymbol === 'USDT') {
        return usdValueFormatterWithoutSign.format(item.amount);
      } else {
        return formatterHelper(item.amount, activeWallet.coinSymbol);
      }
    }
  };

  const conditionalUpdatedAmount = (item) => {
    if (params.type === 'bonds' || params.type === 'moneymarket') {
      if (activeWallet.coinSymbol === 'USDT') {
        return usdValueFormatterWithoutSign.format(item.updated_interest);
      } else {
        // return formatterHelper(item.updated_interest, activeWallet.coinSymbol);
        return item.updated_interest.toFixed(9);
      }
    } else {
      if (activeWallet.coinSymbol === 'USDT') {
        return usdValueFormatterWithoutSign.format(item.updated_balance);
      } else {
        return formatterHelper(item.updated_balance, activeWallet.coinSymbol);
      }
    }
  };

  return (
    <View style={styles.container}>
      {!isLoading ? (
        <SectionList
          style={styles.list}
          showsVerticalScrollIndicator={false}
          sections={groupedTxns}
          keyExtractor={(item, index) => item + index}
          renderSectionHeader={({section: {title}}) => (
            <View style={styles.transactionGroup}>
              <Text style={styles.dayHeader}>{title}</Text>
            </View>
          )}
          renderItem={({item}) => (
            <TouchableOpacity
              style={[styles.txnItem]}
              onPress={() => openTxnAudit(item)}>
              <View style={styles.txnIcons}>
                {item.withdraw ? (
                  <DebitIcon width={29} height={29} />
                ) : (
                  <CreditIcon width={29} height={29} />
                )}
              </View>
              {/* <Image
                style={styles.txnIcons}
                source={{uri: activeWallet.coinImage}}
                resizeMode="contain"
              /> */}
              <View style={styles.nameContainer}>
                <Text numberOfLines={1} style={styles.name}>
                  {`${activeWallet?.coinSymbol} ${
                    item.deposit ? 'Credit' : 'Debit'
                  }`}
                </Text>
                <Text style={styles.date}>{item.date}</Text>
              </View>
              <View style={styles.metaContainer}>
                <Text
                  style={[
                    styles.value,
                    item.deposit ? {color: '#59C36A'} : {color: '#D80027'},
                  ]}>
                  {item.deposit ? '+' : '-'}
                  {conditionalAmount(item)}
                </Text>
                <Text style={[styles.updatedBalance]}>
                  Balance : {conditionalUpdatedAmount(item)}
                </Text>
              </View>
            </TouchableOpacity>
          )}
          ListEmptyComponent={
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>
                {`No${statusFilter !== 'All' ? ` ${statusFilter}` : ''}${
                  typeFilter !== 'All' ? ` ${typeFilter}` : ''
                } ${activeWallet.coinSymbol} Transactions Found`}
              </Text>
            </View>
          }
        />
      ) : (
        <SkeltonAnimation />
      )}
    </View>
  );
};

const SkeltonAnimation = () => {
  return (
    <View style={styles.loadingContainer}>
      {Array(5)
        .fill(1)
        .map((_, index) => (
          <View key={index} style={styles.txnItem}>
            <SkeltonItem itemWidth={30} style={{borderRadius: 15}} />
            <View style={styles.nameContainer}>
              <SkeltonItem
                itemWidth={100}
                itemHeight={10}
                style={styles.name}
              />
              <SkeltonItem itemWidth={70} itemHeight={8} style={styles.value} />
            </View>
            <View style={styles.metaContainer}>
              <SkeltonItem itemWidth={80} itemHeight={8} style={styles.date} />
              <SkeltonItem
                itemWidth={40}
                itemHeight={8}
                style={{marginLeft: 'auto', marginTop: 5}}
              />
            </View>
          </View>
        ))}
    </View>
  );
};

export default TransactionList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {flex: 1},
  txnItem: {
    flexDirection: 'row',
    paddingHorizontal: 25,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginHorizontal: 20,
    borderRadius: 10,
  },
  txnIcons: {
    width: 30,
    height: 30,
  },
  nameContainer: {flexGrow: 1, width: 0, paddingHorizontal: 15},
  name: {fontFamily: 'Montserrat-Bold', color: '#001D41'},
  value: {
    fontFamily: 'Montserrat-Bold',
    color: '#001D41',
    fontSize: 14,
    marginTop: 5,
    textAlign: 'right',
  },
  metaContainer: {},
  date: {fontFamily: 'Montserrat', fontSize: 10},
  updatedBalance: {
    textAlign: 'right',
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 9,
    marginTop: 5,
  },
  loadingContainer: {
    flex: 1,
    // backgroundColor: '#fff',
    overflow: 'hidden',
  },
  loadingText: {marginTop: 10, fontFamily: 'Montserrat'},
  emptyContainer: {
    flex: 1,
    paddingHorizontal: 35,
    marginTop: 40,
  },
  emptyText: {
    fontFamily: 'Montserrat-Bold',
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontSize: 16,
  },
  transactionGroup: {
    padding: 15,
    backgroundColor: 'white',
  },
  dayHeader: {
    fontFamily: 'Montserrat-SemiBold',
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
});
