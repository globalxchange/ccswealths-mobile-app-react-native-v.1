import {useRoute} from '@react-navigation/native';
import axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Linking,
} from 'react-native';
import {APP_CODE, GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import {formatterHelper, usdValueFormatter} from '../../utils';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import SkeltonItem from '../SkeltonItem';
import VaultChart from '../VaultChart';
import {useAppData} from '../../utils/CustomHook';
import FilterTabs from '../MarketList/FilterTabs';

const Overview = ({BalanceComponent, activeWallet, selectedApp}) => {
  const {params} = useRoute();
  const {cryptoTableData} = useContext(AppContext);
  const {data: appData} = useAppData();

  const [moneyMarket, setMoneyMarket] = useState();
  const [loading, setLoading] = useState(false);
  const [bondData, setBondData] = useState([]);

  const getBondData = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();

    axios
      .get(
        `https://comms.globalxchange.com/coin/iced/contract/get?email=${email}&banker_email=${appData.created_by}`,
        // `https://comms.globalxchange.com/coin/iced/contract/get?email=shorupan@gmail.com&banker_email=rohith@nvestbank.com`,
      )
      .then(({data}) => {
        console.log(
          data.icedContracts[0].contracts,
          'jlwbedjfwbefjkwjeflwjebf',
        );
        setBondData(data.icedContracts[0]);
      });
    setMoneyMarket(null);
    setLoading(false);
  };

  useEffect(() => {
    (async () => {
      setLoading(true);
      setMoneyMarket();
      if (params?.type === 'moneymarket') {
        const email = await AsyncStorageHelper.getLoginEmail();

        axios
          .get(`${GX_API_ENDPOINT}/coin/iced/liquid/earnings/per/user/get`, {
            params: {
              app_code: selectedApp?.app_code || APP_CODE,
              coin: activeWallet?.coinSymbol || '',
              email,
            },
          })
          .then(({data}) => {
            if (data.status) {
              setMoneyMarket(data.coins[0]);
              setLoading(false);
            }
          })
          .catch((err) => {
            console.log('Error getting money market', err);
          });
      } else if (params?.type === 'bonds') {
        // setShowBalanceIn(cryptoTableData.find((o) => o.coinSymbol === 'USD'));
        getBondData();
      }
    })();
  }, [params, activeWallet]);

  const handleShowLink = async (id) => {
    const url = `https://tokenhash.com/bond/${id}`;
    console.log(url, 'kjgwcwgkehfwecwheflwheflef');
    const supported = await Linking.canOpenURL(url);

    if (supported) {
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  };

  const conditionalView = () => {
    switch (params.type) {
      case 'moneymarket':
        return (
          <View style={styles.moneymarketContainer}>
            {moneyMarket ? (
              <>
                <View style={styles.marketItem}>
                  <Text style={styles.marketValue}>
                    {moneyMarket.txns_count}
                  </Text>
                  <Text style={styles.marketTitle}>Interest Payments</Text>
                </View>
                <View style={styles.marketItem}>
                  <Text style={styles.marketValue}>
                    {formatterHelper(
                      moneyMarket.interest_earned,
                      activeWallet?.coinSymbol,
                    )}
                  </Text>
                  <Text style={styles.marketTitle}>
                    Total Earnings In {activeWallet?.coinName}
                  </Text>
                </View>
                <View style={styles.marketItem}>
                  <Text style={styles.marketValue}>
                    {usdValueFormatter.format(moneyMarket.interest_earned_usd)}
                  </Text>
                  <Text style={styles.marketTitle}>
                    Total {activeWallet?.coinSymbol} Earnings In USD
                  </Text>
                </View>
              </>
            ) : (
              <>
                {[...Array(3)].map((_, index) => (
                  <View key={index} style={styles.marketItem}>
                    <SkeltonItem itemWidth={100} itemHeight={40} />
                    <SkeltonItem itemWidth={150} itemHeight={15} />
                  </View>
                ))}
              </>
            )}
          </View>
        );

      case 'bonds':
        return (
          // <Text></Text>
          <View>
            {!loading ? (
              bondData?.contracts?.length > 0 ? (
                <ScrollView
                  showsVerticalScrollIndicator={false}
                  // bounces={false}
                  style={{marginBottom: 130}}>
                  {bondData?.contracts?.map((item) => {
                    return (
                      <View
                        style={{
                          borderBottomWidth: 1,
                          borderColor: '#EBEBEB',
                        }}>
                        <View style={styles.indSectionContainer}>
                          <View
                            style={[
                              styles.indHeaderSection,
                              {paddingBottom: 40},
                            ]}>
                            <View
                              style={{
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'center',
                              }}>
                              <Image
                                source={{uri: appData.app_icon}}
                                style={styles.headerIcon}
                              />
                              <View>
                                <Text style={styles.headerTitle}>
                                  {appData.app_name}
                                </Text>
                                <Text
                                  style={{
                                    fontFamily: ThemeData.FONT_NORMAL,
                                    fontSize: 10,
                                  }}>
                                  {item.countryData.code}
                                </Text>
                              </View>
                            </View>
                            <View
                              style={{
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'center',
                              }}>
                              <Image
                                source={{uri: bondData.coin_image}}
                                style={styles.headerIcon}
                              />
                              <View>
                                <Text style={styles.headerTitle}>
                                  {bondData._id}
                                </Text>
                                {/* <Text
                                  style={{
                                    fontFamily: ThemeData.FONT_NORMAL,
                                    fontSize: 10,
                                  }}>
                                  {bondData._id}
                                </Text> */}
                              </View>
                            </View>
                          </View>

                          <View style={styles.indHeaderSection}>
                            <View>
                              <Text style={styles.headerTitle}>
                                {item.investment.toFixed(4)}
                              </Text>
                              <Text style={styles.subtitle}>Investment</Text>
                            </View>
                            <View style={styles.pullRight}>
                              <Text style={styles.headerTitle}>
                                {item.voc.toFixed(4)}
                              </Text>
                              <Text style={styles.subtitle}>Current VOC</Text>
                            </View>
                          </View>
                          <View
                            style={[
                              styles.indHeaderSection,
                              {
                                paddingTop: 30,
                                paddingBottom: 40,
                                borderBottomWidth: 0.5,
                                borderColor: '#EBEBEB',
                              },
                            ]}>
                            <View>
                              <Text style={styles.headerTitle}>
                                {item.completed_stats.earnings.toFixed(4)}
                              </Text>
                              <Text style={styles.subtitle}>
                                Interest Earned
                              </Text>
                            </View>
                            <View style={styles.pullRight}>
                              <Text style={styles.headerTitle}>
                                {item.remaining_stats.earnings.toFixed(4)}
                              </Text>
                              <Text style={styles.subtitle}>
                                Interest Receivable
                              </Text>
                            </View>
                          </View>
                          <View style={styles.indSection}>
                            <View>
                              <Text style={styles.title}>Status</Text>
                            </View>
                            <View style={styles.pullRight}>
                              <Text style={styles.title}>{item.status}</Text>
                            </View>
                          </View>
                          <View style={styles.indSection}>
                            <View>
                              <Text style={styles.title}>Bonds</Text>
                            </View>
                            <View style={styles.pullRight}>
                              <Text style={styles.title}>
                                {item.num_of_bonds}
                              </Text>
                            </View>
                          </View>
                          <View style={styles.indSectionLast}>
                            <View>
                              <Text style={styles.title}>Days</Text>
                            </View>
                            <View style={styles.pullRight}>
                              <Text style={styles.title}>
                                {item.completed_stats.days}/{item.days}
                              </Text>
                            </View>
                          </View>
                          <View
                            style={{
                              display: 'flex',
                              flexDirection: 'row',
                              paddingBottom: 40,
                            }}>
                            <TouchableOpacity
                              style={[styles.whiteButton]}
                              onPress={(e) => handleShowLink(item._id)}>
                              <Text style={styles.whiteButtonText}>
                                Bond Hash
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.colouredButton, {opacity: 0.3}]}>
                              <Text style={styles.colouredButtonText}>
                                Renew
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.whiteButton, {opacity: 0.3}]}>
                              <Text style={styles.whiteButtonText}>Redeem</Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    );
                  })}
                </ScrollView>
              ) : // <>
              //   <View style={styles.marketItem}>
              //     <Text style={styles.marketValue}>
              //       {moneyMarket.txns_count}
              //     </Text>
              //     <Text style={styles.marketTitle}>Interest Payments</Text>
              //   </View>
              //   <View style={styles.marketItem}>
              //     <Text style={styles.marketValue}>
              //       {formatterHelper(
              //         moneyMarket.interest_earned,
              //         activeWallet?.coinSymbol,
              //       )}
              //     </Text>
              //     <Text style={styles.marketTitle}>
              //       Total Earnings In {activeWallet?.coinName}
              //     </Text>
              //   </View>
              //   <View style={styles.marketItem}>
              //     <Text style={styles.marketValue}>
              //       {usdValueFormatter.format(
              //         moneyMarket.interest_earned_usd,
              //       )}
              //     </Text>
              //     <Text style={styles.marketTitle}>
              //       Total {activeWallet?.coinSymbol} Earnings In USD
              //     </Text>
              //   </View>
              // </>
              null
            ) : (
              <>
                {[...Array(3)].map((_, index) => (
                  <View key={index} style={styles.marketItem}>
                    <SkeltonItem itemWidth={100} itemHeight={40} />
                    <SkeltonItem itemWidth={150} itemHeight={15} />
                  </View>
                ))}
              </>
            )}
          </View>
        );

      default:
        return (
          <VaultChart activeWallet={activeWallet} selectedApp={selectedApp} />
        );
    }
  };

  return (
    <View style={styles.container}>
      {BalanceComponent}
      {params.type === 'bonds' ? (
        <View
          style={[
            styles.filterContainer,
            {paddingTop: 40, marginTop: 20},
          ]}></View>
      ) : null}
      {conditionalView()}
    </View>
  );
};

export default Overview;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  moneymarketContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  marketItem: {
    alignItems: 'center',
  },
  marketValue: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.TEXT_COLOR,
    fontSize: 32,
  },
  marketTitle: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.TEXT_COLOR,
    fontSize: 13,
    textAlign: 'center',
  },
  indSectionContainer: {
    // backgroundColor: 'pink',
    paddingHorizontal: 30,
    paddingTop: 50,
  },
  indHeaderSection: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 10,
  },
  indSection: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 30,
    borderBottomWidth: 0.5,
    borderColor: '#EBEBEB',
  },
  indSectionLast: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 30,
  },
  headerTitle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 16,
    color: '#464B4E',
  },
  title: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 14,
    color: '#464B4E',
    textTransform: 'capitalize',
  },
  subtitle: {
    fontSize: 12,
    fontFamily: ThemeData.FONT_NORMAL,
    // paddingTop: 11,
  },
  pullRight: {
    display: 'flex',
    alignItems: 'flex-end',
  },
  action: {
    height: 38,
    // flex: 1,
    width: 105,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    // borderWidth: 1,
    // borderColor: '#E5E5E5',
    backgroundColor: '#04AF76',
    marginTop: 10,
    marginRight: 10,
  },
  actionFilled: {
    // backgroundColor: '#5F6163',
    color: 'white',
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 16,
  },
  whiteButton: {
    height: 38,
    width: 105,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    backgroundColor: 'white',
    marginRight: 10,
    borderWidth: 0.5,
    borderColor: '#EBEBEB',
    marginTop: 10,
  },
  colouredButton: {
    height: 38,
    width: 105,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    backgroundColor: '#04AF76',
    marginRight: 10,
    borderWidth: 0.5,
    borderColor: '#EBEBEB',
    marginTop: 10,
  },
  whiteButtonText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 11,
    color: '#464B4E',
  },
  colouredButtonText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 11,
    color: 'white',
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  filterContainer: {
    flexDirection: 'row',
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 30,
  },
});
