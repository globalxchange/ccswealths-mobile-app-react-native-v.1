import React, {useContext, useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import {DepositContext} from '../../contexts/DepositContext';
import WalletTransfer from '../WalletDeposit/WalletTransfer';
import BTCAddressPopup from '../BTCAddressPopup';
import ExternalWithdrawSheet from '../ExternalWithdrawSheet';
import {useNavigation, useRoute} from '@react-navigation/native';
import {AppContext} from '../../contexts/AppContextProvider';
import DepositBottomSheet from '../DepositBottomSheet';
import WithdrawBottomSheet from '../WithdrawBottomSheet';

const Actions = ({
  actionList,
  vaultType,
  isSearchOpen,
  setSearchList,
  setSearchCallback,
  closeSearch,
  openSearchList,
  activeWallet,
}) => {
  // const {activeWallet} = useContext(DepositContext);
  const {
    setIsWithdrawBottomSheetOpen,
    setIsDepositBottomSheetOpen,
    isDepositBottomSheetOpen,
    isWithdrawBottomSheetOpen,
    setSelectedCrypto,
  } = useContext(AppContext);
  const {params} = useRoute();
  const {navigate} = useNavigation();
  const [activeView, setActiveView] = useState('');
  const [isBTCModalOpen, setIsBTCModalOpen] = useState(false);
  const [isWithdrawOpen, setIsWithdrawOpen] = useState(false);
  const [actions, setActions] = useState([]);
  const [activeCrypto, setActiveCrypto] = useState();
  const [isAuditOpen, setIsAuditOpen] = useState(false);
  const [selectedAuditTxn, setSelectedAuditTxn] = useState();
  const [checkoutPayload, setCheckoutPayload] = useState('');

  useEffect(() => {
    setSearchList(actions?.list || LIST);
    setSearchCallback((item) => {
      setActiveView(item);
      closeSearch();
    });
    return () => {
      setSearchList();
      setSearchCallback(() => {});
    };
  }, [actions, isSearchOpen]);

  const openTxnAudit = (txn) => {
    setSelectedAuditTxn(txn);
    setIsAuditOpen(true);
  };

  useEffect(() => {
    if (crypto) {
      console.log(crypto, 'kjebfkflkejbrfje');
      setSelectedCrypto(crypto);
      setActiveCrypto(crypto);
    }
  }, [crypto]);

  useEffect(() => {
    if (params.type === 'bonds') {
      const temp = actionList?.find((x) => x.title === 'Bond Actions');
      setActions(temp);
    } else if (params.type === 'moneymarket') {
      const temp = actionList?.find((x) => x.title === 'MoneyMarket Actions');
      setActions(temp);
    } else if (params.type === 'fiat') {
      const temp = actionList?.find((x) => x.title === 'Forex Actions');
      setActions(temp);
    } else {
      const temp = actionList?.find(
        (x) => x.title === (vaultType?.title || 'Liquid'),
      );
      setActions(temp);
    }
  }, [params]);

  if (activeView?.title === 'View Address') {
    return <WalletTransfer />;
  }

  const onViewAddressClick = () => {
    navigate('ViewAddress');
    // if (activeWallet?.coinSymbol === 'BTC') {
    //   // setIsBTCModalOpen(true);

    // }
  };

  const onWithdrawSheetOpen = () => {
    // setIsWithdrawOpen(true);
    navigate('ExternalWithdraw');
  };

  const conditionalActionname = (item) => {
    if (item.title === 'External Transfer') {
      return `Withdraw ${activeWallet.coinName}`;
    } else if (item.title === 'Add' || item.title === 'Withdraw') {
      return `${item.title} ${activeWallet.coinName}`;
    } else {
      return item.title;
    }
  };

  const conditionalAction = (item) => {
    if (item?.title === 'View Address') {
      onViewAddressClick();
    } else if (item?.title === 'External Transfer') {
      onWithdrawSheetOpen();
    } else if (item.title === 'Add') {
      // setIsDepositBottomSheetOpen(true);
      navigate('AddForexStepOne');
    } else if (item.title === 'Withdraw') {
      navigate('WithdrawForexStepOne');
      // setIsWithdrawBottomSheetOpen(true);
      // TODO: add withdraw state here
    } else {
      setActiveView(item);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.vaultName}>
          Which Action Would You Like To Perform?
        </Text>
        <TouchableOpacity style={styles.searchButton} onPress={openSearchList}>
          <Image
            style={styles.searchIcon}
            source={require('../../assets/search-modern.png')}
          />
        </TouchableOpacity>
      </View>
      <FlatList
        data={actions?.list || LIST}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item) => item.title}
        renderItem={({item}) => (
          <TouchableOpacity
            disabled={item.disabled}
            onPress={(e) => conditionalAction(item)}>
            <View style={[styles.ationItem, item.disabled && {opacity: 0.3}]}>
              <Image
                style={styles.ationItemImage}
                source={item.icon}
                resizeMode="contain"
              />
              <Text style={styles.actionName}>
                {conditionalActionname(item)}
              </Text>
            </View>
          </TouchableOpacity>
        )}
      />
      <BTCAddressPopup
        isOpen={isBTCModalOpen}
        onClose={() => setIsBTCModalOpen(false)}
        activeWallet={activeWallet}
      />
      <ExternalWithdrawSheet
        isOpen={isWithdrawOpen}
        onClose={() => setIsWithdrawOpen(false)}
        activeWallet={activeWallet}
      />
      <DepositBottomSheet
        isBottomSheetOpen={isDepositBottomSheetOpen}
        setIsBottomSheetOpen={setIsDepositBottomSheetOpen}
        activeCrypto={activeCrypto}
        openTxnAudit={openTxnAudit}
        checkoutPayload={checkoutPayload}
        selectedItem={activeWallet}
      />
      <WithdrawBottomSheet
        isBottomSheetOpen={isWithdrawBottomSheetOpen}
        setIsBottomSheetOpen={setIsWithdrawBottomSheetOpen}
        activeCrypto={activeCrypto}
        openTxnAudit={openTxnAudit}
        checkoutPayload={checkoutPayload}
        selectedItem={activeWallet}
      />
    </View>
  );
};

export default Actions;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
  },
  ationItem: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 25,
    marginBottom: 10,
  },
  ationItemImage: {
    width: 30,
    height: 30,
  },
  actionName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    marginLeft: 10,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 22,
  },
  vaultName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
  searchButton: {
    width: 35,
    height: 35,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 10,
  },
  searchIcon: {
    flex: 1,
    width: undefined,
    height: undefined,
  },
});

const LIST = [
  {
    title: 'Withdraw Interest',
    icon: require('../../assets/withdraw-intrest-icon.png'),
  },
];
