import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import SkeltonItem from '../SkeltonItem';

const SkeletonItemLoading = () => {
  return (
    <View style={styles.container}>
      {Array(6)
        .fill(0)
        .map((_, index) => (
          <View key={index} style={styles.item}>
            <View style={styles.row}>
              <SkeltonItem
                itemHeight={25}
                itemWidth={25}
                style={styles.cryptoIcon}
              />
              <View style={styles.nameContainer}>
                <SkeltonItem
                  itemHeight={10}
                  itemWidth={80}
                  style={styles.cryptoName}
                />
              </View>
              <SkeltonItem
                itemHeight={10}
                itemWidth={60}
                style={styles.cryptoPrice}
              />
            </View>
            <View style={styles.row}>
              <SkeltonItem
                itemHeight={10}
                itemWidth={60}
                style={styles.cryptoPrice}
              />
              <SkeltonItem
                itemHeight={10}
                itemWidth={60}
                style={styles.cryptoPrice}
              />
            </View>
            <View style={[styles.row, {marginBottom: 0}]}>
              <SkeltonItem
                itemHeight={10}
                itemWidth={60}
                style={styles.cryptoPrice}
              />
              <SkeltonItem
                itemHeight={10}
                itemWidth={60}
                style={styles.cryptoPrice}
              />
            </View>
          </View>
        ))}
    </View>
  );
};

export default SkeletonItemLoading;

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: 'white', paddingHorizontal: 10},
  item: {
    paddingHorizontal: 15,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
  },
  cryptoIcon: {
    width: 25,
    height: 25,
    borderRadius: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  nameContainer: {
    flexGrow: 1,
    width: 0,
    marginLeft: 10,
  },
  cryptoName: {
    color: '#001D41',
    fontSize: 16,
    marginBottom: 2,
    fontFamily: 'Montserrat-Bold',
  },
  noOfVendors: {
    color: '#001D41',
    opacity: 0.5,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  cryptoPrice: {
    color: '#001D41',
    opacity: 0.5,
    marginBottom: 2,
    textAlign: 'right',
    fontFamily: 'Roboto-Bold',
    marginRight: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    marginBottom: 10,
  },
});
