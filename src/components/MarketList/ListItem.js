import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import {
  numberFormatter,
  percentageFormatter,
  usdValueFormatter,
} from '../../utils';

const ListItem = ({
  icon,
  name,
  price,
  change,
  changeInUSD,
  onPress,
  symbol,
  volume,
  marketCap,
}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.itemContainer}>
        <View style={styles.itemRow}>
          <Image style={styles.itemIcon} source={{uri: icon}} />
          <Text style={styles.itemName}>{name}</Text>
          <Text style={styles.itemPrice}>
            {usdValueFormatter.format(price || 0)}
          </Text>
        </View>
        <View style={styles.itemRow}>
          <Text style={styles.itemChangeTitle}>24 Hr :</Text>
          <View
            style={[
              styles.itemChangeSign,
              change < 0 ? styles.itemChangeSignDown : styles.itemChangeSignUp,
            ]}
          />
          <Text style={[styles.itemChange, change < 0 && styles.downValue]}>
            {percentageFormatter.format(change || 0)} %
          </Text>
          <Text
            style={[
              styles.itemChange,
              {marginLeft: 'auto'},
              changeInUSD < 0 && styles.downValue,
            ]}>
            {changeInUSD < 0 ? '-' : '+'}{' '}
            {usdValueFormatter.format(Math.abs(changeInUSD || 0))}
          </Text>
        </View>
        <View style={[styles.itemRow, {marginTop: 5}]}>
          <Text style={styles.itemVolume}>
            24 Hr Vol :{(Math.abs(Number(volume)) / 1.0e9).toFixed(2)} Billion
            {/* {numberFormatter.format(volume)} */}
          </Text>
          <Text style={[styles.itemVolume, {marginLeft: 'auto'}]}>
            Mkt Cap : $
            {marketCap
              ? (Math.abs(Number(marketCap)) / 1.0e9).toFixed(2)
              : 'NA'}{' '}
            {/* {usdValueFormatter.format(marketCap)} */}
            Billion
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default ListItem;

const styles = StyleSheet.create({
  itemContainer: {
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    backgroundColor: 'white',
  },
  itemRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
  },
  itemIcon: {
    width: 22,
    height: 22,
    resizeMode: 'contain',
    marginRight: 5,
  },
  itemName: {
    fontSize: 17,
    fontFamily: ThemeData.FONT_BOLD,
  },
  itemPrice: {
    marginLeft: 'auto',
    fontSize: 17,
    fontFamily: ThemeData.FONT_BOLD,
  },
  itemChange: {
    fontSize: 13,
    fontFamily: ThemeData.FONT_BOLD,
    color: '#0E9347',
  },
  itemBankerIcon: {
    width: 12,
    height: 12,
    resizeMode: 'contain',
    marginHorizontal: 5,
  },
  itemAssetIcon: {
    width: 14,
    height: 14,
    resizeMode: 'contain',
    marginLeft: 'auto',
    marginRight: 5,
  },
  itemChangeTitle: {fontSize: 12, fontFamily: ThemeData.FONT_NORMAL},
  itemVolume: {
    fontSize: 10,
    fontFamily: ThemeData.FONT_NORMAL,
    marginTop: 5,
  },
  downValue: {
    color: '#D32027',
  },
  itemChangeSign: {
    width: 0,
    height: 0,
    borderStyle: 'solid',
    borderLeftColor: 'transparent',
    borderLeftWidth: 5,
    borderRightColor: 'transparent',
    borderRightWidth: 5,
    marginHorizontal: 5,
  },
  itemChangeSignDown: {
    borderTopColor: '#D32027',
    borderTopWidth: 9,
  },
  itemChangeSignUp: {
    borderBottomColor: '#0E9347',
    borderBottomWidth: 9,
  },
});
