import {useNavigation, useRoute} from '@react-navigation/core';
import axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {FlatList, RefreshControl, StyleSheet, Text, View} from 'react-native';
import {useQuery} from 'react-query';
import {APP_CODE, GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import ListItem from './ListItem';
import SkeletonItemLoading from './SkeletonItemLoading';
import FilterTabs from './FilterTabs';
import AppMainLayout from '../../layouts/AppMainLayout';
import BuyBottomSheet from '../BuyBottomSheet';
import SellBottomSheet from '../SellBottomSheet';
import BuySellBottomSheet from '../../screens/MarketScreens/BuySellBottomSheet';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';

const getAssetList = async ({queryKey}) => {
  const [_key, assetType] = queryKey;

  let postData = {
    app_code: APP_CODE,
  };
  if (assetType === 'Crypto') {
    postData = {...postData, type: 'crypto'};
  } else if (assetType === 'Fiat') {
    postData = {...postData, type: 'fiat'};
  } else if (assetType === 'Stablecoins') {
    postData = {...postData, type: 'crypto', stable_coin: true};
  }

  const {data} = await axios.post(
    `${GX_API_ENDPOINT}/coin/vault/service/coins/get`,
    postData,
  );

  return data?.coins_data || [];
};

const MarketList = ({
  setSearchPlaceHolderText,
  transitionViewRef,
  setSearchList,
  setSearchCallback,
}) => {
  const {navigate} = useNavigation();
  const [isBottomSheetOpen, setIsBottomSheetOpen] = useState(false);
  const [isBuyBottomSheetOpen, setIsBuyBottomSheetOpen] = useState(false);
  const [isSellBottomSheetOpen, setIsSellBottomSheetOpen] = useState(false);
  const [activeCrypto, setActiveCrypto] = useState();
  const [isAuditOpen, setIsAuditOpen] = useState(false);
  const [selectedAuditTxn, setSelectedAuditTxn] = useState();
  const [checkoutPayload, setCheckoutPayload] = useState('');
  const [selectedItem, setSelectedItem] = useState();

  const {
    activeListCategory,
    setSelectedCrypto,
    selectedCrypto,
    isLoggedIn,
    setActiveRoute,
    pathData,
    walletCoinData,
    getWalletCoinData,
    selectedCoin,
    setSelectedCoin,
  } = useContext(AppContext);

  // const {
  //   params: {crypto},
  // } = useRoute();

  const {data: assetsList, status, error, refetch} = useQuery(
    [`assetList${activeListCategory.title}`, activeListCategory.title],
    getAssetList,
  );

  useEffect(() => {
    setSearchCallback((item) => navigate('Asset', {crypto: item}));
    setSearchList(assetsList);
    return () => {
      setSearchList();
      setSearchCallback(() => {});
    };
  }, [assetsList]);

  const openTxnAudit = (txn) => {
    setSelectedAuditTxn(txn);
    setIsAuditOpen(true);
  };

  useEffect(() => {
    if (crypto) {
      console.log(crypto, 'kjebfkflkejbrfje');
      setSelectedCrypto(crypto);
      setActiveCrypto(crypto);
    }
  }, [crypto]);

  return (
    <View style={styles.container}>
      <FilterTabs />
      {status === 'loading' && <SkeletonItemLoading />}
      {status === 'success' && (
        <FlatList
          style={styles.scrollView}
          data={assetsList}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item.coinSymbol}
          refreshControl={
            <RefreshControl
              refreshing={status === 'loading'}
              onRefresh={refetch}
            />
          }
          renderItem={({item}) => (
            <ListItem
              icon={item.coinImage}
              name={item.coinName}
              change={item._24hrchange}
              changeInUSD={item._24hrchange_priceUSD}
              price={item.price?.USD}
              // onPress={() => navigate('Asset', {crypto: item})}
              onPress={(e) => {
                setSelectedItem(item);
                setIsBottomSheetOpen(true);
              }}
              symbol={item.coinSymbol}
              marketCap={item.mkt_cap}
              volume={item.volume24hr}
            />
          )}
          ListEmptyComponent={
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>No Crypto Found In Market</Text>
            </View>
          }
        />
      )}
      <BuySellBottomSheet
        isBottomSheetOpen={isBottomSheetOpen}
        setIsBottomSheetOpen={setIsBottomSheetOpen}
        setIsBuyBottomSheetOpen={setIsBuyBottomSheetOpen}
        setIsSellBottomSheetOpen={setIsSellBottomSheetOpen}
        selectedItem={selectedItem}
      />
      <BuyBottomSheet
        isBottomSheetOpen={isBuyBottomSheetOpen}
        setIsBottomSheetOpen={setIsBuyBottomSheetOpen}
        activeCrypto={activeCrypto}
        openTxnAudit={openTxnAudit}
        checkoutPayload={checkoutPayload}
        selectedItem={selectedItem}
      />
      <SellBottomSheet
        isBottomSheetOpen={isSellBottomSheetOpen}
        setIsBottomSheetOpen={setIsSellBottomSheetOpen}
        activeCrypto={activeCrypto}
        openTxnAudit={openTxnAudit}
        checkoutPayload={checkoutPayload}
        selectedItem={selectedItem}
      />
    </View>
  );
};

export default MarketList;

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: 'white'},
  scrollView: {flex: 1, paddingHorizontal: 10, paddingTop: 10},
  emptyContainer: {
    marginTop: 20,
  },
  emptyText: {
    fontFamily: 'Montserrat',
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontSize: 16,
    paddingHorizontal: 30,
  },
});
