import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const FilterTabs = () => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={[styles.btnContiner, {backgroundColor: ThemeData.BORDER_COLOR}]}>
        <Image
          source={require('../../assets/add-colored.png')}
          style={styles.btnIcon}
        />
        <Text style={styles.btnText}>Add Filter</Text>
      </TouchableOpacity>
      <TouchableOpacity style={[styles.btnContiner]}>
        <Image
          source={require('../../assets/usa-today.png')}
          style={styles.btnIcon}
        />
        <Text style={styles.btnText}>USD</Text>
      </TouchableOpacity>
    </View>
  );
};

export default FilterTabs;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 30,
  },
  btnContiner: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 30,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 50,
    paddingHorizontal: 10,
  },
  btnIcon: {
    width: 12,
    height: 12,
    marginRight: 5,
  },
  btnText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 10,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
});
