import React from 'react';
import {StyleSheet, View} from 'react-native';
import LottieView from 'lottie-react-native';

const LoadingAnimation = ({height, width, white}) => {
  return (
    <View style={styles.container}>
      <LottieView
        style={{width: width || 80, height: height || 80}}
        source={
          white
            ? require('../assets/animation/loading-white.json')
            : require('../assets/animation/loading.json')
        }
        autoPlay
        loop
      />
    </View>
  );
};

export default LoadingAnimation;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});
