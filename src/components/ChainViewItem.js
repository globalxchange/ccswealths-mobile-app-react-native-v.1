/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState, useRef} from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import ThemeData from '../configs/ThemeData';
import {usdValueFormatter} from '../utils';
import ProfileAvatar from './ProfileAvatar';
import SkeltonItem from './SkeltonItem';

const {width} = Dimensions.get('window');

const SLIDER_WIDTH = width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.6);
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 6);

const ChainViewItem = ({
  level,
  userList,
  onUserSelected,
  isLoading,
  headerDesc,
  setLastSelectedUser,
  clearToIndex,
  emptyMessage,
  onTreeClick,
  isMoneyViewOpen,
  onPopupOpen,
  onSearchOpen,
  searchedUser,
}) => {
  const [selectedUser, setSelectedUser] = useState();

  const carouselRef = useRef();

  useEffect(() => {
    const timeout = setTimeout(() => {
      if (searchedUser && userList) {
        setSelectedUser(searchedUser);

        const index = userList.findIndex(
          (user) => user.email === searchedUser.email,
        );

        if (index >= 0) {
          carouselRef.current?.snapToItem(index);
        }
      }
    }, 500);

    return () => {
      clearTimeout(timeout);
    };
  }, [searchedUser, userList]);

  useEffect(() => {
    setSelectedUser();
  }, [userList]);

  return (
    <View style={styles.container}>
      <View style={styles.depthLabelContainer}>
        <Text style={styles.depthLabel}>DD {level}</Text>
        <View style={styles.separator} />
        {userList ? (
          <>
            <Text
              numberOfLines={1}
              adjustsFontSizeToFit
              style={styles.depthDesc}>
              {selectedUser
                ? `You Have Selected ${selectedUser.name}`
                : headerDesc}
            </Text>

            {selectedUser ? (
              <>
                <View style={[styles.separator, {marginRight: 5}]} />
                <TouchableOpacity
                  onPress={() => {
                    setSelectedUser();
                    clearToIndex(level);
                  }}
                  style={styles.editButton}>
                  <Image
                    style={styles.editIcon}
                    resizeMode="contain"
                    source={require('../assets/edit-button-icon.png')}
                  />
                </TouchableOpacity>
              </>
            ) : userList ? (
              <>
                <View style={[styles.separator, {marginRight: 5}]} />
                <TouchableOpacity
                  onPress={() => onSearchOpen(userList, level)}
                  style={styles.editButton}>
                  <Image
                    style={styles.editIcon}
                    resizeMode="contain"
                    source={require('../assets/search-icon-colorful.png')}
                  />
                </TouchableOpacity>
              </>
            ) : null}
          </>
        ) : (
          <View style={{flex: 1}} />
        )}
        <View style={[styles.separator, {marginHorizontal: 5}]} />
        <TouchableOpacity onPress={onTreeClick} style={styles.editButton}>
          <Image
            style={styles.editIcon}
            resizeMode="contain"
            source={require('../assets/chain-view-link-button.png')}
          />
        </TouchableOpacity>
      </View>
      <View style={styles.userListContainer}>
        {isLoading ? (
          <View style={styles.skeletonContainer}>
            <SkeltonView />
            <SkeltonView />
            <SkeltonView />
          </View>
        ) : userList !== undefined ? (
          userList?.length > 0 ? (
            <View>
              <Carousel
                ref={carouselRef}
                firstItem={15}
                data={userList}
                scrollEnabled={selectedUser ? false : true}
                sliderWidth={SLIDER_WIDTH}
                itemWidth={ITEM_WIDTH}
                containerCustomStyle={styles.carouselContainer}
                inactiveSlideShift={0}
                useScrollView={true}
                renderItem={({item}) => (
                  <TouchableOpacity
                    disabled={selectedUser ? true : false}
                    onPress={() => {
                      setSelectedUser(item);
                      setLastSelectedUser(item);
                      onUserSelected(item.email, level);
                    }}>
                    <View
                      style={[
                        styles.userItem,
                        selectedUser
                          ? selectedUser.email === item.email
                            ? {borderWidth: 1}
                            : {opacity: 0.5}
                          : {opacity: 1},
                      ]}>
                      <ProfileAvatar
                        avatar={item.profile_img}
                        name={item?.name}
                        size={35}
                      />
                      <View style={styles.nameContainer}>
                        <Text numberOfLines={1} style={styles.name}>
                          {item.name}
                        </Text>
                        <Text numberOfLines={1} style={styles.email}>
                          {isMoneyViewOpen
                            ? usdValueFormatter.format(
                                item?.comData?.total_earnings || 0,
                              )
                            : item.email}
                        </Text>
                      </View>
                      {isMoneyViewOpen ? (
                        <TouchableOpacity
                          style={styles.earingButton}
                          onPress={() => onPopupOpen(item.email)}>
                          <Image
                            source={require('../assets/support-category-icons/add.png')}
                            resizeMode="contain"
                            style={styles.earningIcon}
                          />
                        </TouchableOpacity>
                      ) : null}
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          ) : (
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>{emptyMessage}</Text>
            </View>
          )
        ) : null}
      </View>
    </View>
  );
};

const SkeltonView = () => (
  <View style={[styles.userItem, {width: ITEM_WIDTH}]}>
    <SkeltonItem itemWidth={35} style={[styles.profilePlaceholder]} />
    <View style={styles.nameContainer}>
      <SkeltonItem
        itemWidth={ITEM_WIDTH * 0.6}
        itemHeight={10}
        style={[styles.name]}
      />
      <SkeltonItem
        itemWidth={ITEM_WIDTH * 0.45}
        itemHeight={6}
        style={[styles.email, {marginTop: 5}]}
      />
    </View>
  </View>
);

export default ChainViewItem;

const styles = StyleSheet.create({
  container: {},
  depthLabelContainer: {
    borderTopColor: ThemeData.APP_MAIN_COLOR,
    borderTopWidth: 0.25,
    borderBottomColor: ThemeData.APP_MAIN_COLOR,
    borderBottomWidth: 0.25,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 20,
    height: 40,
  },
  depthLabel: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    paddingVertical: 8,
    fontSize: 13,
  },
  separator: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    width: 0.5,
    height: '100%',
    marginHorizontal: 25,
  },
  depthDesc: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 12,
    flex: 1,
    textTransform: 'capitalize',
  },
  userListContainer: {
    height: 150,
    justifyContent: 'center',
  },
  userItem: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 0.5,
    marginHorizontal: 8,
    paddingHorizontal: 10,
    height: 60,
  },
  userImage: {
    width: 35,
    height: 35,
    borderRadius: 17.5,
    backgroundColor: 'white',
  },
  nameContainer: {
    marginLeft: 10,
    flex: 1,
  },
  name: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 15,
    color: ThemeData.APP_MAIN_COLOR,
  },
  email: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 9,
    color: ThemeData.APP_MAIN_COLOR,
    marginTop: 2,
  },
  emptyContainer: {
    justifyContent: 'center',
    paddingHorizontal: 20,
    width,
  },
  emptyText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 14,
  },
  editButton: {
    width: 40,
    height: 40,
    padding: 12,
  },
  editIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  profilePlaceholder: {
    width: 35,
    height: 35,
    borderRadius: 17.5,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
  },
  placeholderText: {
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    textTransform: 'uppercase',
    fontSize: 13,
  },
  skeletonContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  earingButton: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
  },
  earningIcon: {
    width: 20,
    height: 20,
  },
});
