import React from 'react';
import {StyleSheet, View} from 'react-native';
import Connect from '.';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';

const ConnectModal = ({isOpen, setIsOpen}) => {
  return (
    <BottomSheetLayout
      isOpen={isOpen}
      onClose={() => setIsOpen(false)}
      reactToKeyboard>
      <View style={styles.fragmentContainer}>
        <Connect setIsOpen={setIsOpen} />
      </View>
    </BottomSheetLayout>
  );
};

export default ConnectModal;

const styles = StyleSheet.create({
  fragmentContainer: {
    backgroundColor: 'white',
  },
});
