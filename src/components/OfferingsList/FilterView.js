import axios from 'axios';
import React, {useState, Fragment, useContext, useEffect} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useQueries} from 'react-query';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import FilterPicker from './FilterPicker';

const getSubCategories = async ({queryKey}) => {
  const [_key, code] = queryKey;
  const {data} = await axios.get(
    `${GX_API_ENDPOINT}/coin/investment/sub/types/get`,
    {
      params: {parent_investment_type: code},
    },
  );

  const parsedList = data?.investmentSubTypes?.map((item) => ({
    ...item,
    icon: {uri: item.icon},
    title: item.name,
  }));

  return parsedList || [];
};

const FilterView = ({
  tokenCategories,
  subCategory,
  setSubCategory,
  contractType,
  setContractType,
}) => {
  const {activeListCategory, setActiveListCategory} = useContext(AppContext);

  const [isCategoryFilterOpen, setIsCategoryFilterOpen] = useState(false);
  const [openedModal, setOpenedModal] = useState('');

  const [{data: subCategoryList}] = useQueries([
    {
      queryKey: [
        `subCategories${activeListCategory?.title}`,
        activeListCategory?.code,
      ],
      queryFn: getSubCategories,
    },
  ]);

  useEffect(() => {
    setSubCategory('');
    setContractType('');
  }, [activeListCategory]);

  const filterMenu = [
    {
      title: 'Add Category Filter',
      modalTitle: 'Select Token Category',
      placeHolder: 'Search For Categories',
      filterList: tokenCategories || [],
      onItemSelected: (item) => setActiveListCategory(item),
      activeItem: activeListCategory,
    },
    {
      isDisabled:
        !activeListCategory?.title || activeListCategory?.title === 'All',
      title: 'Add Sub-Category Filter',
      modalTitle: 'Select Token Sub-Category',
      placeHolder: 'Search For Sub-Categories',
      filterList: subCategoryList || [],
      onItemSelected: (item) => setSubCategory(item),
      activeItem: subCategory,
    },
    {
      isDisabled:
        !activeListCategory?.title || activeListCategory?.title === 'All',
      title: 'Add Contract Type',
      modalTitle: 'Select Contract Type',
      placeHolder: 'Search For Contract Type',
      filterList: COTRACT_TYPES,
      isModalOpen: isCategoryFilterOpen,
      onItemSelected: (item) => setContractType(item),
      activeItem: contractType,
    },
  ];

  return (
    <View style={styles.headerContainer}>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        {filterMenu.map((item) => (
          <Fragment key={item.title}>
            <TouchableOpacity
              disabled={item.isDisabled}
              onPress={() => setOpenedModal(item.title)}>
              <View
                style={[
                  styles.headerBtn,
                  item.isDisabled && styles.itemDisabled,
                ]}>
                <Image
                  source={
                    item.activeItem?.title && item.activeItem?.title !== 'All'
                      ? item.activeItem?.icon
                      : require('../../assets/add-colored.png')
                  }
                  style={styles.headerBtnIcon}
                />
                <Text style={styles.headerBtnText}>
                  {item.activeItem?.title && item.activeItem?.title !== 'All'
                    ? item.activeItem?.title
                    : item.title || 'Add Filter'}
                </Text>
              </View>
            </TouchableOpacity>
            <FilterPicker
              {...item}
              isOpen={openedModal === item.title}
              onClose={() => setOpenedModal('')}
            />
          </Fragment>
        ))}
      </ScrollView>
    </View>
  );
};

export default FilterView;

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  headerBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 15,
    height: 28,
    borderRadius: 14,
    marginRight: 15,
  },
  headerBtnIcon: {
    width: 12,
    height: 12,
    resizeMode: 'contain',
    marginRight: 5,
  },
  headerBtnText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
  },
  itemDisabled: {
    opacity: 0.5,
  },
});

const COTRACT_TYPES = [
  {
    title: 'Exchange Offering',
    icon: require('../../assets/tokenContractFilters/exchange-offering.png'),
  },
  {
    title: 'ShareToken',
    icon: require('../../assets/tokenContractFilters/share-token.png'),
  },
  {
    title: 'Open-Ended Fund',
    icon: require('../../assets/tokenContractFilters/open-ended-fund.png'),
  },
  {
    title: 'Closed-Ended Fund',
    icon: require('../../assets/tokenContractFilters/close-ended-fund.png'),
  },
];
