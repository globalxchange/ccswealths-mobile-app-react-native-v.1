import axios from 'axios';
import React, {useContext, useEffect} from 'react';
import {Dimensions, FlatList, StyleSheet, Text, View} from 'react-native';
import {useQuery} from 'react-query';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import SkeltonItem from '../SkeltonItem';
import ListItem from './ListItem';

const {width} = Dimensions.get('window');

const getTokensList = async ({queryKey}) => {
  const [_key, category, subCategory] = queryKey;
  let params = {};
  if (category?.title !== 'All') {
    params = {...params, investmentType: category.code};
  }
  if (subCategory) {
    params = {...params, investmentSubType: subCategory};
  }

  console.log({params});

  const {data} = await axios.get(
    `${GX_API_ENDPOINT}/coin/investment/path/get`,
    {params},
  );

  return data.paths || [];
};

const OfferingsList = ({
  setSearchPlaceHolderText,
  setSearchList,
  setSearchCallback,
  subCategory,
  contractType,
}) => {
  const {activeListCategory} = useContext(AppContext);

  const {data: tokenList, status, error} = useQuery(
    [
      `tokenList${activeListCategory.title}${subCategory?.code}`,
      activeListCategory,
      subCategory?.code,
    ],
    getTokensList,
  );

  useEffect(() => {
    setSearchCallback((item) => {});
    setSearchList(tokenList);
    return () => {
      setSearchList();
      setSearchCallback(() => {});
    };
  }, [tokenList]);

  return (
    <View style={styles.container}>
      {status === 'loading' &&
        [...Array(5)].map((_, index) => (
          <View style={styles.itemContainer} key={index}>
            <SkeltonItem
              itemHeight={20}
              itemWidth={width - 60}
              style={{marginBottom: 10}}
            />
            <SkeltonItem
              itemHeight={10}
              itemWidth={width - 60}
              style={{marginBottom: 10}}
            />
            <SkeltonItem
              itemHeight={6}
              itemWidth={width - 60}
              style={{marginBottom: 10}}
            />
          </View>
        ))}
      {status === 'success' && (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={tokenList || []}
          keyExtractor={(item) => item._id}
          renderItem={({item}) => <ListItem item={item} />}
          ListEmptyComponent={
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>No Tokens Found In Market</Text>
            </View>
          }
        />
      )}
    </View>
  );
};

export default OfferingsList;

const styles = StyleSheet.create({
  container: {flex: 1},
  itemContainer: {
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
  },
  emptyContainer: {
    marginTop: 20,
  },
  emptyText: {
    fontFamily: 'Montserrat',
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontSize: 16,
    paddingHorizontal: 30,
  },
});
