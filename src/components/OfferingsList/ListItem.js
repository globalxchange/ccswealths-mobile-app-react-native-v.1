import React, {useContext} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import {formatterHelper, getAssetData} from '../../utils';

const ListItem = ({item, onPress}) => {
  const {walletCoinData} = useContext(AppContext);

  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.itemContainer}>
        <View style={styles.itemRow}>
          <Image
            style={styles.itemIcon}
            source={{uri: item?.token_profile_data?.coinImage}}
          />
          <Text style={styles.itemName}>
            {item?.token_profile_data?.coinName}
          </Text>
          <Text style={styles.itemAmount}>
            {formatterHelper(item.token_price, item.asset)}
          </Text>
        </View>
        <View style={styles.itemRow}>
          <Text style={styles.itemBanker}>By:</Text>
          <Image
            style={styles.itemBankerIcon}
            source={{uri: item?.banker_metaData?.profilePicURL}}
          />
          <Text style={styles.itemBanker}>
            {item?.banker_metaData?.displayName}
          </Text>
          <Image
            style={styles.itemAssetIcon}
            source={{
              uri: getAssetData(item.asset, walletCoinData)?.coinImage || '',
            }}
          />
          <Text style={styles.itemAssetName}>{item.asset}</Text>
        </View>
        <View style={styles.itemRow}>
          <Text style={styles.itemSupply}>
            Supply: {item.token_totalSupply}
          </Text>
          <Text style={[styles.itemSupply, {marginLeft: 'auto'}]}>
            Market Cap: {item.asset_balance}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default ListItem;

const styles = StyleSheet.create({
  itemContainer: {
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
  },
  itemRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
  },
  itemIcon: {
    width: 22,
    height: 22,
    resizeMode: 'contain',
    marginRight: 5,
  },
  itemName: {
    fontSize: 17,
    fontFamily: ThemeData.FONT_BOLD,
  },
  itemAmount: {
    marginLeft: 'auto',
    fontSize: 17,
    fontFamily: ThemeData.FONT_BOLD,
  },
  itemBanker: {
    fontSize: 11,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  itemBankerIcon: {
    width: 12,
    height: 12,
    resizeMode: 'contain',
    marginHorizontal: 5,
  },
  itemAssetIcon: {
    width: 14,
    height: 14,
    resizeMode: 'contain',
    marginLeft: 'auto',
    marginRight: 5,
  },
  itemAssetName: {fontSize: 11, fontFamily: ThemeData.FONT_BOLD},
  itemSupply: {
    fontSize: 11,
    fontFamily: ThemeData.FONT_NORMAL,
    marginTop: 5,
  },
});
