/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useRef, useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  Keyboard,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import ThemeData from '../../configs/ThemeData';
import PopupLayout from '../../layouts/PopupLayout';

const {width, height} = Dimensions.get('window');

const FilterPicker = ({
  isOpen,
  onClose,
  placeHolder,
  numberLabel,
  subTextKey,
  headerIcon,
  headerIconHeight,
  headerStyle,
  onItemSelected,
  filterList,
  modalTitle,
  activeItem,
}) => {
  const {top, bottom} = useSafeAreaInsets();

  const [isSearchFocused, setIsSearchFocused] = useState(false);
  const [searchInput, setSearchInput] = useState('');
  const [filteredList, setFilteredList] = useState('');

  const inputRef = useRef();

  useEffect(() => {
    if (filterList) {
      const searchQuery = searchInput.toLowerCase().trim();

      const list = filterList.filter(
        (x) =>
          x?.name?.toLowerCase().includes(searchQuery) ||
          x?.title?.toLowerCase().includes(searchQuery),
      );

      setFilteredList(list);
    }
  }, [filterList, searchInput]);

  useEffect(() => {
    if (isOpen) {
      setSearchInput('');
      setIsSearchFocused(false);
    }
  }, [isOpen]);

  return (
    <PopupLayout
      noScrollView
      headerImageHeight={headerIconHeight}
      headerImage={headerIcon}
      headerStyle={headerStyle}
      headerTitle={modalTitle}
      isOpen={isOpen}
      onClose={onClose}
      containerStyles={
        isSearchFocused
          ? {
              height,
              width,
              paddingTop: top,
              paddingBottom: bottom,
              backgroundColor: 'transparent',
              borderWidth: 0,
            }
          : {}
      }>
      <View style={styles.modalContainer}>
        <View style={styles.searchContainer}>
          {isSearchFocused && (
            <>
              <TouchableOpacity
                style={styles.backButton}
                onPress={() => {
                  Keyboard.dismiss();
                  inputRef.current.blur();
                  setIsSearchFocused(false);
                  onClose();
                }}>
                <Image
                  style={styles.backIcon}
                  source={require('../../assets/back-icon-colored.png')}
                  resizeMode="contain"
                />
              </TouchableOpacity>
              <View style={styles.divider} />
            </>
          )}
          <TextInput
            ref={inputRef}
            style={styles.searchInput}
            placeholderTextColor={'#9A9A9A'}
            placeholder={placeHolder || `Search ${numberLabel || ''}`}
            value={searchInput}
            onChangeText={(text) => setSearchInput(text)}
            onFocus={() => setIsSearchFocused(true)}
            onBlur={() => setIsSearchFocused(false)}
          />
        </View>
        <FlatList
          showsVerticalScrollIndicator={false}
          style={styles.dropDownList}
          data={filteredList}
          keyExtractor={(item) => item.title || item.name}
          renderItem={({item}) =>
            item.title !== 'All' && (
              <TouchableOpacity
                disabled={item.disabled}
                onPress={() => {
                  onItemSelected(item);
                  onClose();
                }}
                style={[styles.dropDownItem, item.disabled && {opacity: 0.5}]}>
                <Image
                  style={styles.countryImage}
                  source={item.icon}
                  resizeMode="contain"
                />
                <Text
                  style={[
                    styles.itemText,
                    activeItem?.title === item.title && styles.itemActive,
                  ]}>
                  {item.title}
                </Text>
                {subTextKey && (
                  <Text style={styles.itemSubText}>{item[subTextKey]}</Text>
                )}
              </TouchableOpacity>
            )
          }
          ListEmptyComponent={
            <Text style={styles.emptyText}>No Option Found...</Text>
          }
        />
        <View style={styles.actiosContainer}>
          <TouchableOpacity style={styles.actionBtn} onPress={onClose}>
            <Text style={styles.actionText}>Close</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.actionBtn,
              {backgroundColor: ThemeData.APP_MAIN_COLOR},
            ]}
            onPress={() => {
              onItemSelected({title: 'All'});
              onClose();
            }}>
            <Text style={[styles.actionText, {color: 'white'}]}>
              Clear Filter
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </PopupLayout>
  );
};

export default FilterPicker;

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: 'white',
    marginHorizontal: -5,
  },
  dropDownList: {
    // paddingHorizontal: 30,
  },
  dropDownItem: {
    flexDirection: 'row',
    paddingVertical: 20,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  countryImage: {height: 24, width: 24},
  itemText: {
    flex: 1,
    marginLeft: 15,
    fontFamily: 'Montserrat',
    color: ThemeData.APP_MAIN_COLOR,
  },
  itemActive: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  itemSubText: {
    fontFamily: 'Roboto',
    color: '#788995',
    fontSize: 12,
  },
  emptyText: {
    fontFamily: 'Montserrat-SemiBold',
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    paddingVertical: 20,
    fontSize: 18,
  },
  searchContainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15,
  },
  searchInput: {
    height: 40,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    color: '#788995',
    flex: 1,
  },
  backButton: {
    height: 40,
    justifyContent: 'center',
    marginLeft: -15,
    paddingLeft: 15,
    paddingRight: 15,
  },
  backIcon: {
    width: 20,
    height: 20,
  },
  divider: {
    marginRight: 15,
    height: '100%',
    width: 1,
    backgroundColor: ThemeData.BORDER_COLOR,
  },
  actiosContainer: {
    flexDirection: 'row',
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    marginBottom: -30,
    marginLeft: -30,
    marginRight: -30,
  },
  actionBtn: {
    flex: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionText: {
    textAlign: 'center',
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
});
