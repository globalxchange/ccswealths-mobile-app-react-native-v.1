import React from 'react';
import {StyleSheet, View, TextInput} from 'react-native';

const InputArea = ({placeholder, value, onChange}) => {
  const onSubmit = () => {
    alert('Submit');
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder={placeholder}
        // multiline
        value={value}
        onChange={onChange}
      />
    </View>
  );
};

export default InputArea;

const styles = StyleSheet.create({
  container: {
    borderColor: '#E9E8E8',
    borderWidth: 1,
    borderRadius: 12,
    paddingHorizontal: 15,
    marginBottom: 20,
    height: 150,
  },
  input: {
    color: '#000000',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 14,
    paddingRight: 10,
  },
});
