/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  Keyboard,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {GX_API_ENDPOINT} from '../configs';
import ThemeData from '../configs/ThemeData';
import {AppContext} from '../contexts/AppContextProvider';
import PopupLayout from '../layouts/PopupLayout';
import {formatterHelper, getUriImage, urlValidatorRegex} from '../utils';

const {width, height} = Dimensions.get('window');

const QuoteConvertCurrencyPicker = ({
  isOpen,
  onClose,
  onSelected,
  coinPrice,
  sellCurrency,
  buyCurrency,
}) => {
  const {top, bottom} = useSafeAreaInsets();

  const {cryptoTableData} = useContext(AppContext);

  const [isSearchFocused, setIsSearchFocused] = useState(false);
  const [searchInput, setSearchInput] = useState('');
  const [filteredList, setFilteredList] = useState();
  const [items, setItems] = useState();
  const [isFiat, setIsFiat] = useState(true);
  const [convertData, setConvertData] = useState();

  const inputRef = useRef();

  useEffect(() => {
    Axios.get(`${GX_API_ENDPOINT}/coin/getCmcprices`, {
      params: {convert: sellCurrency.coinSymbol || 'USD'},
    })
      .then(({data}) => {
        // console.log('Convert Data', data);
        setConvertData(data);
      })
      .catch((error) => {
        console.log('Error on getting convertData', error);
      });
  }, [sellCurrency]);

  useEffect(() => {
    if (cryptoTableData) {
      // console.log('cryptoTableData', cryptoTableData);

      let list = [];

      if (isFiat) {
        list = cryptoTableData.filter(
          (x) =>
            x.asset_type === 'Fiat' && buyCurrency?.coinSymbol !== x.coinSymbol,
        );
      } else {
        list = cryptoTableData.filter(
          (x) =>
            x.asset_type === 'Crypto' &&
            buyCurrency?.coinSymbol !== x.coinSymbol,
        );
      }
      setItems(list);
    }
  }, [cryptoTableData, isFiat, sellCurrency]);

  useEffect(() => {
    if (isOpen) {
      setSearchInput('');
      setIsSearchFocused(false);
    }
  }, [isOpen]);

  useEffect(() => {
    if (items) {
      // console.log('items', items);
      const searchQuery = searchInput.toLowerCase().trim();

      const list = items.filter(
        (x) =>
          x?.name?.toLowerCase().includes(searchQuery) ||
          x?.coinName?.toLowerCase().includes(searchQuery) ||
          x?.coinSymbol?.toLowerCase().includes(searchQuery),
      );

      setFilteredList(list);
    }
  }, [items, searchInput]);

  const onItemSelected = (item) => {
    onClose();
    onSelected(item);
  };

  return (
    <PopupLayout
      isOpen={isOpen}
      onClose={onClose}
      headerTitle="Choose Display Currency"
      noScrollView
      containerStyles={
        isSearchFocused && {
          height,
          width,
          maxHeight: height,
          paddingTop: top,
          paddingBottom: bottom,
          backgroundColor: 'transparent',
          borderWidth: 0,
        }
      }>
      <View style={styles.modalContainer}>
        <View style={styles.searchContainer}>
          {isSearchFocused && (
            <>
              <TouchableOpacity
                style={styles.backButton}
                onPress={() => {
                  Keyboard.dismiss();
                  inputRef.current.blur();
                  setIsSearchFocused(false);
                }}>
                <Image
                  style={styles.backIcon}
                  source={require('../assets/back-icon-colored.png')}
                  resizeMode="contain"
                />
              </TouchableOpacity>
              <View style={styles.divider} />
            </>
          )}
          <TextInput
            ref={inputRef}
            style={styles.searchInput}
            placeholderTextColor={'#9A9A9A'}
            placeholder={`Search ${isFiat ? 'Fiat' : 'Crypto'} Currencies`}
            value={searchInput}
            onChangeText={(text) => setSearchInput(text)}
            onFocus={() => setIsSearchFocused(true)}
            onBlur={() => setIsSearchFocused(false)}
          />
        </View>
        <FlatList
          showsVerticalScrollIndicator={false}
          style={styles.dropDownList}
          data={filteredList}
          keyExtractor={(item) => item.name}
          renderItem={({item}) => (
            <ListItem
              item={item}
              coinPrice={coinPrice}
              onItemSelected={onItemSelected}
              sellCurrency={sellCurrency}
              convertData={convertData}
            />
          )}
          ListEmptyComponent={
            <Text style={styles.emptyText}>No Option Found...</Text>
          }
        />
        <View style={styles.actionContainer}>
          <TouchableOpacity
            onPress={() => setIsFiat(true)}
            style={[styles.actionItem, isFiat && styles.actionItemAcive]}>
            <Text
              style={[
                styles.actionItemText,
                isFiat && styles.actionItemTextActive,
              ]}>
              Fiat
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setIsFiat(false)}
            style={[styles.actionItem, isFiat || styles.actionItemAcive]}>
            <Text
              style={[
                styles.actionItemText,
                isFiat || styles.actionItemTextActive,
              ]}>
              Crypto
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </PopupLayout>
  );
};

const ListItem = ({
  item,
  coinPrice,
  onItemSelected,
  convertData,
  sellCurrency,
}) => {
  return (
    <TouchableOpacity
      disabled={item.disabled}
      onPress={() =>
        onItemSelected({
          ...item,
          convertValue: convertData[item.coinSymbol] || 0,
        })
      }
      style={[styles.dropDownItem, item.disabled && {opacity: 0.5}]}>
      <FastImage
        style={styles.countryImage}
        source={
          urlValidatorRegex.test(item.image)
            ? {uri: getUriImage(item.image)}
            : item.image
        }
        resizeMode="contain"
      />
      <Text style={styles.itemText}>{item.name}</Text>
      <Text style={styles.itemSubText}>
        {convertData
          ? formatterHelper(convertData[item.coinSymbol] || 0, item.coinSymbol)
          : ''}
      </Text>
    </TouchableOpacity>
  );
};

export default QuoteConvertCurrencyPicker;

const styles = StyleSheet.create({
  container: {marginBottom: 20},
  header: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  dropdownContainer: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 15,
    alignItems: 'center',
    height: 50,
  },
  image: {
    height: 24,
    width: 24,
  },
  text: {
    flexGrow: 1,
    width: 0,
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    color: '#788995',
  },
  dropIcon: {
    width: 13,
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    flex: 1,
    justifyContent: 'center',
    paddingVertical: height * 0.25,
  },
  modalContainer: {
    flex: 1,
    backgroundColor: 'white',
    marginHorizontal: -5,
  },
  dropDownList: {
    // paddingHorizontal: 30,
  },
  dropDownItem: {
    flexDirection: 'row',
    paddingVertical: 20,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  countryImage: {height: 24, width: 24},
  itemText: {
    flex: 1,
    marginLeft: 15,
    fontFamily: 'Montserrat',
    color: '#9A9A9A',
  },
  itemSubText: {
    fontFamily: 'Roboto',
    color: '#788995',
    fontSize: 12,
  },
  emptyText: {
    fontFamily: 'Montserrat-SemiBold',
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    paddingVertical: 20,
    fontSize: 18,
  },
  searchContainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15,
  },
  searchInput: {
    height: 40,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    color: '#788995',
    flex: 1,
  },
  backButton: {
    height: 40,
    justifyContent: 'center',
    marginLeft: -15,
    paddingLeft: 15,
    paddingRight: 15,
  },
  backIcon: {
    width: 20,
    height: 20,
  },
  divider: {
    marginRight: 15,
    height: '100%',
    width: 1,
    backgroundColor: ThemeData.BORDER_COLOR,
  },
  actionContainer: {
    flexDirection: 'row',
    marginBottom: -30,
    marginHorizontal: -25,
  },
  actionItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    marginTop: 10,
  },
  actionItemAcive: {
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    backgroundColor: 'white',
  },
  actionItemText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 16,
    color: 'white',
  },
  actionItemTextActive: {
    color: '#788995',
  },
});
