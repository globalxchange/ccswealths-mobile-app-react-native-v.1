import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import ThemeData from '../configs/ThemeData';

const OutlinedButton = ({onPress, style, title, color, active, disabled}) => {
  return (
    <TouchableOpacity onPress={onPress} style={[styles.outlinedButton, style]}>
      <Text
        style={[
          styles.outlinedBtnText,
          active && styles.active,
          color && {color},
          disabled && {opacity: 0.5},
        ]}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default OutlinedButton;

const styles = StyleSheet.create({
  outlinedButton: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    flex: 1,
  },
  outlinedBtnText: {
    color: '#858585',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 12,
  },
  active: {
    fontFamily: 'Montserrat-Bold',
  },
});
