/* eslint-disable react-native/no-inline-styles */
import React, {useContext} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import BondsIcon from '../../assets/bottom-bar-icon/BondsIcon';
import DeFiIcon from '../../assets/bottom-bar-icon/DeFiIcon';
import LiquidIcon from '../../assets/bottom-bar-icon/LiquidIcon';
import MarketIcon from '../../assets/bottom-bar-icon/MarketIcon';
import MoneyMarketIcon from '../../assets/bottom-bar-icon/MoneyMarketIcon';
import BotsIcon from '../../assets/bottom-bar-icon/BotsIcon';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';

const MarketCustomBottomBar = ({state, descriptors, navigation}) => {
  const {isVideoFullScreen} = useContext(AppContext);

  const {bottom} = useSafeAreaInsets();

  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  const openVaultController = () => {};

  const bottomMenus = [
    {
      onPress: () => navigation.navigate('Liquid'),
      tabBarLabel: 'Liquid',
      tabBarIcon: ({color, size, opacity}) => (
        <LiquidIcon
          color={color}
          width={size}
          height={size}
          opacity={opacity}
        />
      ),
    },
    {
      onPress: () => navigation.navigate('Tokens'),
      tabBarLabel: 'Tokens',
      tabBarIcon: ({color, size, opacity}) => (
        <MarketIcon
          color={color}
          width={size}
          height={size}
          opacity={opacity}
        />
      ),
    },
    {
      onPress: () => navigation.navigate('Bonds', {screen: 'Bonds'}),
      tabBarLabel: 'Defi',
      tabBarIcon: ({color, size, opacity}) => (
        <DeFiIcon color={color} width={size} height={size} opacity={opacity} />
      ),
    },
    // {
    //   onPress: () => navigation.navigate('MoneyMarket', {screen: 'Home'}),
    //   tabBarLabel: 'Money Market',
    //   tabBarIcon: ({color, size, opacity}) => (
    //     <MoneyMarketIcon
    //       color={color}
    //       width={size}
    //       height={size}
    //       opacity={opacity}
    //     />
    //   ),
    // },
    {
      onPress: () => navigation.navigate('Bots'),
      tabBarLabel: 'Bots',
      tabBarIcon: ({color, size, opacity}) => (
        <BotsIcon color={color} width={size} height={size} opacity={opacity} />
      ),
    },
  ];

  return (
    <View
      style={[
        styles.container,
        {
          paddingBottom: bottom || 15,
          display: isVideoFullScreen ? 'none' : 'flex',
        },
      ]}>
      {bottomMenus.map((route, index) => {
        const label = route.tabBarLabel;

        if (label === 'CreateBrokerage') {
          return;
        }

        const Icon = route.tabBarIcon;

        const isFocused = state.index === index;

        const size = 25;

        const color = isFocused ? ThemeData.APP_MAIN_COLOR : '#424141';

        const opacity = isFocused ? 1 : 0.3;

        const fontSize = isFocused ? 10 : 9;

        return [
          <TouchableOpacity
            key={index}
            onPress={route.onPress}
            style={styles.button}>
            {Icon && <Icon color={color} size={size} opacity={opacity} />}
            <Text
              style={[
                styles.label,
                {
                  color,
                  opacity,
                  fontSize,
                  display: isFocused ? 'flex' : 'none',
                },
              ]}>
              {label}
            </Text>
          </TouchableOpacity>,
          index === 1 && (
            <View style={styles.brokerButtonContainer} key="blockButton">
              <TouchableOpacity
                onPress={() => openVaultController()}
                style={styles.brokerButton}>
                <Image
                  source={require('../../assets/app-logo.png')}
                  resizeMode="contain"
                  style={styles.brokerIcon}
                />
              </TouchableOpacity>
            </View>
          ),
        ];
      })}
    </View>
  );
};

export default MarketCustomBottomBar;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderTopColor: '#EBEBEB',
    borderTopWidth: 1,
    paddingVertical: 12,
    paddingHorizontal: 10,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  button: {
    flex: 1,
    alignItems: 'center',
  },
  icon: {
    height: 28,
    width: 28,
  },
  label: {
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 5,
    textAlign: 'center',
  },
  brokerButtonContainer: {
    marginBottom: -19,
    marginTop: -20,
    marginVertical: 10,
  },
  brokerButton: {
    backgroundColor: 'white',
    width: 75,
    height: 75,
    borderRadius: 37.5,
    padding: 19,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  brokerIcon: {
    flex: 1,
    height: null,
    width: null,
  },
});
