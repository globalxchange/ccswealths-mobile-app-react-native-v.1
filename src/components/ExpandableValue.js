import axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {GX_API_ENDPOINT} from '../configs';
import ThemeData from '../configs/ThemeData';
import {AppContext} from '../contexts/AppContextProvider';
import PopupLayout from '../layouts/PopupLayout';
import {cryptoList, formatterHelper, getAssetData} from '../utils';

const ExpandableValue = ({value, coin, style, usdValue}) => {
  const {cryptoTableData} = useContext(AppContext);

  const [isOpen, setIsOpen] = useState(false);
  const [currentUsdValue, setCurrentUsdValue] = useState();

  useEffect(() => {
    if (coin) {
      axios
        .get(`${GX_API_ENDPOINT}/forex/convert`, {
          params: {buy: 'USD', from: coin},
        })
        .then(({data}) => {
          // console.log('Convert Data', data);
          setCurrentUsdValue(
            ((data[`${coin.toLowerCase()}_usd`] || 1) * value).toFixed(8),
          );
        })
        .catch((error) => {
          console.log('Convert Error', error);
        });
    }
  }, [value, coin]);

  const isSmall = cryptoList.includes(coin) ? value <= 0.009 : value <= 0.00009;

  const coinData = getAssetData(coin, cryptoTableData);

  const usdData = getAssetData('USD', cryptoTableData);

  return (
    <React.Fragment>
      <Text
        onPress={() => (isSmall ? setIsOpen(true) : null)}
        style={[
          styles.txnAmount,
          isSmall && {textDecorationLine: 'underline'},
          style,
        ]}>
        {isSmall ? 'Expand' : formatterHelper(value, coin)}
      </Text>
      <PopupLayout
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
        headerImage={require('../assets/assets-io-white-icon.png')}
        headerImageHeight={22}
        noScrollView
        autoHeight>
        <View style={styles.popupContainer}>
          <Text style={styles.popupHeader}>Precise View</Text>
          <View style={styles.curencyView}>
            <Text style={styles.curencyViewTitle}>Currency Of Value</Text>
            <View style={styles.valueContainer}>
              <Text style={styles.valueTitle}>{coinData?.coinName}</Text>
              <View style={styles.currencyContainer}>
                <Image
                  source={{uri: coinData?.coinImage}}
                  resizeMode="contain"
                  style={styles.coinImage}
                />
                <Text style={styles.coinName}>{coinData?.coinSymbol}</Text>
              </View>
            </View>
          </View>
          <View style={styles.curencyView}>
            <Text style={styles.curencyViewTitle}>Value</Text>
            <View style={styles.valueContainer}>
              <Text style={styles.valueTitle}>{value}</Text>
              <View style={styles.currencyContainer}>
                <Image
                  source={{uri: coinData?.coinImage}}
                  resizeMode="contain"
                  style={styles.coinImage}
                />
                <Text style={styles.coinName}>{coinData?.coinSymbol}</Text>
              </View>
            </View>
          </View>
          <View style={styles.curencyView}>
            <Text style={styles.curencyViewTitle}>
              US Dollar Value At The TIme
            </Text>
            <View style={styles.valueContainer}>
              <Text style={styles.valueTitle}>
                {usdValue !== undefined
                  ? `${parseFloat(usdValue || 0).toFixed(8)}`
                  : '-'}
              </Text>
              <View style={styles.currencyContainer}>
                <Image
                  source={{uri: usdData?.coinImage}}
                  resizeMode="contain"
                  style={styles.coinImage}
                />
                <Text style={styles.coinName}>USD</Text>
              </View>
            </View>
          </View>
          <View style={styles.curencyView}>
            <Text style={styles.curencyViewTitle}>US Dollar Right Now</Text>
            <View style={styles.valueContainer}>
              <Text style={styles.valueTitle}>${currentUsdValue || 0}</Text>
              <View style={styles.currencyContainer}>
                <Image
                  source={{uri: usdData?.coinImage}}
                  resizeMode="contain"
                  style={styles.coinImage}
                />
                <Text style={styles.coinName}>USD</Text>
              </View>
            </View>
          </View>
        </View>
      </PopupLayout>
    </React.Fragment>
  );
};

export default ExpandableValue;

const styles = StyleSheet.create({
  txnAmount: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  popupContainer: {
    paddingBottom: 10,
  },
  popupHeader: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 26,
  },
  curencyView: {
    marginTop: 30,
  },
  curencyViewTitle: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  valueContainer: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    alignItems: 'center',
    paddingHorizontal: 20,
    marginTop: 15,
  },
  valueTitle: {
    flex: 1,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 16,
  },
  currencyContainer: {
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 20,
    borderLeftColor: ThemeData.BORDER_COLOR,
    borderLeftWidth: 1,
  },
  coinImage: {
    width: 20,
    height: 20,
    marginRight: 6,
  },
  coinName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
});
