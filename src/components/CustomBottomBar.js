/* eslint-disable react-native/no-inline-styles */
import React, {useContext} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import EngageIcon from '../assets/bottom-bar-icon/engage-icon';
import NetworkIcon from '../assets/bottom-bar-icon/network-icon';
import BrandsIcon from '../assets/bottom-bar-icon/brands-icon';
import ProfileIcon from '../assets/bottom-bar-icon/profile-icon';
import {AppContext} from '../contexts/AppContextProvider';
import ThemeData from '../configs/ThemeData';

const CustomBottomBar = ({state, descriptors, navigation}) => {
  const {isVideoFullScreen} = useContext(AppContext);

  const {bottom} = useSafeAreaInsets();

  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  const openCreateBrokerage = () => {
    navigation.navigate('CreateBrokerage', {screen: 'Controller'});
  };

  const bottomMenus = [
    {
      onPress: () => navigation.navigate('RootNetwork', {screen: 'Levels'}),
      tabBarLabel: 'Network',
      tabBarIcon: ({color, size, opacity}) => (
        <NetworkIcon
          color={color}
          width={size}
          height={size}
          opacity={opacity}
        />
      ),
    },
    {
      onPress: () => navigation.navigate('BrandNetwork', {screen: 'Feeds'}),
      tabBarLabel: 'Brands',
      tabBarIcon: ({color, size, opacity}) => (
        <BrandsIcon
          color={color}
          width={size}
          height={size}
          opacity={opacity}
        />
      ),
    },
    {
      onPress: () => navigation.navigate('EngageNetwork'),
      tabBarLabel: 'Engage',
      tabBarIcon: ({color, size, opacity}) => (
        <EngageIcon
          color={color}
          width={size}
          height={size}
          opacity={opacity}
        />
      ),
    },
    {
      onPress: () => navigation.navigate('ProfileNetwork'),
      tabBarLabel: 'Profile',
      tabBarIcon: ({color, size, opacity}) => (
        <ProfileIcon
          color={color}
          width={size}
          height={size}
          opacity={opacity}
        />
      ),
    },
  ];

  return (
    <View
      style={[
        styles.container,
        {
          paddingBottom: bottom || 15,
          display: isVideoFullScreen ? 'none' : 'flex',
        },
      ]}>
      {bottomMenus.map((route, index) => {
        const label = route.tabBarLabel;

        if (label === 'CreateBrokerage') {
          return;
        }

        const Icon = route.tabBarIcon;

        const isFocused = state.index === index;

        const size = 25;

        const color = isFocused ? ThemeData.APP_MAIN_COLOR : '#424141';

        const opacity = isFocused ? 1 : 0.3;

        const fontSize = isFocused ? 11 : 9;

        return [
          <TouchableOpacity
            key={index}
            onPress={route.onPress}
            style={styles.button}>
            {Icon && <Icon color={color} size={size} opacity={opacity} />}
            <Text
              style={[
                styles.label,
                {
                  color,
                  opacity,
                  fontSize,
                  display: isFocused ? 'flex' : 'none',
                },
              ]}>
              {label}
            </Text>
          </TouchableOpacity>,
          index === 1 && (
            <View style={styles.brokerButtonContainer} key="blockButton">
              <TouchableOpacity
                onPress={() => openCreateBrokerage()}
                style={styles.brokerButton}>
                <Image
                  source={require('../assets/app-logo-inverted.png')}
                  resizeMode="contain"
                  style={styles.brokerIcon}
                />
              </TouchableOpacity>
            </View>
          ),
        ];
      })}
    </View>
  );
};

export default CustomBottomBar;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderTopColor: '#EBEBEB',
    borderTopWidth: 1,
    paddingVertical: 12,
    paddingHorizontal: 10,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  button: {
    flex: 1,
    alignItems: 'center',
  },
  icon: {
    height: 28,
    width: 28,
  },
  label: {
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 5,
    textAlign: 'center',
  },
  brokerButtonContainer: {
    marginBottom: -19,
    marginTop: -20,
    marginVertical: 10,
  },
  brokerButton: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    width: 75,
    height: 75,
    borderRadius: 37.5,
    padding: 19,
  },
  brokerIcon: {
    flex: 1,
    height: null,
    width: null,
  },
});
