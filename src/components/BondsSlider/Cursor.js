import {useNavigation, useRoute} from '@react-navigation/core';
import React, {memo, useContext, useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Animated, {call, concat, useCode} from 'react-native-reanimated';
import {ReText} from 'react-native-redash';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import {formatterHelper} from '../../utils';

const {width} = Dimensions.get('window');

const TOOLTIP_HEIGHT = 180;
const KNOB_HEIGHT = 40;

const Cursor = ({translateX, days, earningsData, selectedVault, state}) => {
  const {navigate} = useNavigation();

  const {params} = useRoute();

  const {isLoggedIn} = useContext(AppContext);

  const [currentData, setCurrentData] = useState('');

  useCode(
    () => [
      call([days], ([v]) => {
        const cc = earningsData[v];

        // console.log({cc, v});
        setCurrentData(cc);
      }),
    ],
    [days, state, earningsData],
  );

  const onConfirmClick = () => {
    if (isLoggedIn) {
      navigate('BondIssue', {
        selectedBond: params?.selectedBond,
        selectedVault: selectedVault,
        earningsData: currentData,
      });
    } else {
      navigate('Landing');
    }
  };

  return (
    <View style={styles.container}>
      <Animated.View style={[styles.tooltipContainer, {left: translateX}]}>
        <View style={styles.tooltipHeader}>
          <ReText style={styles.numberOfDays} text={concat(days, ' Days')} />
          <TouchableOpacity
            disabled={!currentData || currentData?.days - 1 <= 0}
            style={styles.confrimButton}
            onPress={onConfirmClick}>
            <Text
              style={[
                styles.confrimButtonText,
                {opacity: !currentData || currentData?.days - 1 <= 0 ? 0.5 : 1},
              ]}>
              Confirm
            </Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.tooltipBody]}>
          <View style={[styles.inputWrapper, {borderRightWidth: 1}]}>
            <View style={styles.inputGroup}>
              <Text style={styles.inputValue}>
                {(currentData?.interest || 0).toPrecision(1)}%
              </Text>
              <Text style={styles.inputLabel}>Daily Rate</Text>
            </View>
            <View style={[styles.inputGroup, {marginTop: 20}]}>
              <Text style={styles.inputValue}>
                {formatterHelper(
                  currentData?.dailyAmount || 0,
                  selectedVault?.coinSymbol,
                )}
              </Text>

              <Text style={styles.inputLabel}>
                Daily Earning ({selectedVault?.coinSymbol})
              </Text>
            </View>
          </View>
          <View style={styles.inputWrapper}>
            <View style={styles.inputGroup}>
              <Text style={styles.inputValue}>
                {(currentData?.final_roiPercentage || 0).toPrecision(1)}%
              </Text>
              <Text style={styles.inputLabel}>Total ROI</Text>
            </View>
            <View style={[styles.inputGroup, {marginTop: 20}]}>
              <Text style={styles.inputValue}>
                {formatterHelper(
                  currentData?.final_roiAmount || 0,
                  selectedVault?.coinSymbol,
                )}
              </Text>
              <Text style={styles.inputLabel}>
                Total Earnings ({selectedVault?.coinSymbol})
              </Text>
            </View>
          </View>
        </View>
      </Animated.View>
      <View style={styles.knobContainer}>
        <Image
          style={styles.knobIcon}
          resizeMode="contain"
          source={require('../../assets/bonds-white-icon.png')}
        />
      </View>
    </View>
  );
};

export default memo(Cursor);

const styles = StyleSheet.create({
  container: {},
  knobContainer: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    height: KNOB_HEIGHT,
    width: KNOB_HEIGHT,
    borderRadius: KNOB_HEIGHT / 2,
    padding: 8,
  },
  knobIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  tooltipContainer: {
    width: width * 0.6,
    borderColor: '#D8D8D8',
    borderWidth: 1,
    borderRadius: 6,
    overflow: 'hidden',
    marginBottom: 10,
    height: TOOLTIP_HEIGHT,
  },
  tooltipHeader: {
    flexDirection: 'row',
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    height: 30,
    alignItems: 'center',
    borderTopEndRadius: 6,
    borderTopStartRadius: 6,
    overflow: 'hidden',
  },
  numberOfDays: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    flex: 1,
    color: 'white',
    textAlign: 'center',
  },
  confrimButton: {
    height: 30,
    backgroundColor: 'white',
    justifyContent: 'center',
    paddingHorizontal: 15,
    borderBottomColor: '#D8D8D8',
    borderBottomWidth: 1,
  },
  confrimButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 11,
  },
  tooltipBody: {
    flexDirection: 'row',
    backgroundColor: 'white',
    flex: 1,
  },
  inputWrapper: {
    flex: 1,
    borderColor: '#D8D8D8',
    paddingVertical: 20,
  },
  inputGroup: {
    alignItems: 'center',
    flex: 1,
  },
  inputValue: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 20,
    marginBottom: 5,
  },
  inputLabel: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 10,
  },
});
