/* eslint-disable react-native/no-inline-styles */
import axios from 'axios';
import React, {useEffect, useRef, useState} from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import {PanGestureHandler, State} from 'react-native-gesture-handler';
import Animated, {
  call,
  cond,
  divide,
  eq,
  interpolate,
  multiply,
  round,
  set,
  useCode,
} from 'react-native-reanimated';
import {
  diffClamp,
  onGestureEvent,
  useValue,
  withOffset,
} from 'react-native-redash';
import {APP_CODE, GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import Cursor from './Cursor';

const {width, height} = Dimensions.get('window');

const SLIDER_CONTAINER_WIDTH = width - 40;
const SLIDER_CONTAINER_HEIGHT = height * 0.5 - 40;

const RATIO = SLIDER_CONTAINER_WIDTH / (SLIDER_CONTAINER_HEIGHT - 60);

const MAX_SLIDER_OFFSET_Y = SLIDER_CONTAINER_WIDTH - 20;
const MAX_SLIDER_OFFSET_X = SLIDER_CONTAINER_HEIGHT - 20;

const BondsSlider = ({selectedVault}) => {
  const [activeTimeSpan, setActiveTimeSpan] = useState(TIMESPANS[1]);
  const [earningsData, setEarningsData] = useState([]);

  useEffect(() => {
    axios
      .get(`${GX_API_ENDPOINT}/coin/iced/contract/interest/rate/stats`, {
        params: {
          days: activeTimeSpan?.days + 1,
          coin: selectedVault?.coinSymbol,
          contractValue: selectedVault?.coinValue,
          app_code: APP_CODE,
        },
      })
      .then(({data}) => {
        // console.log('getBondData', data);
        setEarningsData(data?.dayStats || []);
      })
      .catch((error) => {
        console.log('Error getting bond data', error);
      });
  }, [activeTimeSpan, selectedVault]);

  const state = useRef(useValue(State.UNDETERMINED));
  const translationX = useRef(useValue(0));

  const dailyRate = useRef(useValue(0));
  const dailyEearning = useRef(useValue(0));
  const totalRate = useRef(useValue(0));
  const totalEarning = useRef(useValue(0));

  const gestureHandler = useRef(
    onGestureEvent({
      state: state.current,
      translationX: translationX.current,
    }),
  );

  const translateX = useRef(
    diffClamp(
      withOffset(translationX.current, state.current),
      0,
      MAX_SLIDER_OFFSET_Y,
    ),
  );

  const translateY = useRef(
    diffClamp(
      withOffset(multiply(translationX.current, RATIO), state.current),
      0,
      MAX_SLIDER_OFFSET_X,
    ),
  );

  const progress = round(
    multiply(
      divide(translateX.current, MAX_SLIDER_OFFSET_Y),
      activeTimeSpan.days,
    ),
  );

  useCode(
    () => [
      cond(
        eq(state.current, State.ACTIVE),
        call([progress], ([v]) => {
          // console.log({v});
        }),
      ),
      cond(
        eq(state.current, State.END),
        call([progress], ([v]) => {
          setCurrentData(v);
        }),
      ),
    ],
    [state.current, progress],
  );

  const setCurrentData = (position) => {
    // console.log('earningsData', earningsData);

    // console.log('position', position);
    const currentData = earningsData[position];

    // console.log('currentData', currentData);

    // console.log('currentData?.interest', currentData?.interest);

    set(dailyRate.current, currentData?.interest || 0);
    set(dailyEearning.current, currentData?.dailyAmount || 0);
    set(totalRate.current, currentData?.final_roiPercentage || 0);
    set(totalEarning.current, currentData?.final_interest_value || 0);
  };

  const tooltipTransalete = interpolate(translateY.current, {
    inputRange: [0, SLIDER_CONTAINER_HEIGHT + 20],
    outputRange: [0, -width * 0.6],
    extrapolate: 'clamp',
  });

  return (
    <View style={[styles.container]}>
      <View style={styles.sliderContainer}>
        <PanGestureHandler {...gestureHandler.current}>
          <Animated.View
            style={{
              top: interpolate(translateY.current, {
                inputRange: [0, SLIDER_CONTAINER_HEIGHT],
                outputRange: [0, -SLIDER_CONTAINER_HEIGHT],
                extrapolate: 'clamp',
              }),
              left: interpolate(translateX.current, {
                inputRange: [0, SLIDER_CONTAINER_WIDTH],
                outputRange: [0, SLIDER_CONTAINER_WIDTH],
                extrapolate: 'clamp',
              }),
            }}>
            <Cursor
              translateX={tooltipTransalete}
              days={progress}
              earningsData={earningsData}
              selectedVault={selectedVault}
              state={state.current}
            />
          </Animated.View>
        </PanGestureHandler>
      </View>
      <View style={[styles.triangle]} />
      <View style={styles.timesContainer}>
        {TIMESPANS.map((item) => (
          <TouchableOpacity
            key={item.title}
            onPress={() => setActiveTimeSpan(item)}
            style={styles.timeItem}>
            <Text
              style={[
                styles.timeItemText,
                item.title === activeTimeSpan?.title && {
                  fontFamily: ThemeData.FONT_SEMI_BOLD,
                  opacity: 1,
                },
              ]}>
              {item.title}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  );
};

export default BondsSlider;

const TIMESPANS = [
  {title: '1 Month', days: 30},
  {title: '1 Year', days: 365},
  {title: '5 Years', days: 1825},
];

const styles = StyleSheet.create({
  container: {
    marginHorizontal: -20,
    marginBottom: -35,
    marginTop: 'auto',
    // backgroundColor: 'rgba(0, 159, 255, 0.2)',
    justifyContent: 'center',
    alignItems: 'center',
    height: SLIDER_CONTAINER_HEIGHT + 40,
  },
  sliderContainer: {
    zIndex: 2,
    width: SLIDER_CONTAINER_WIDTH,
    height: SLIDER_CONTAINER_HEIGHT,
    justifyContent: 'flex-end',
    // backgroundColor: 'rgba(70,75,78,0.2)',
    position: 'absolute',
  },
  triangle: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: width,
    borderBottomWidth: SLIDER_CONTAINER_HEIGHT + 40,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#F2F2F2',
  },
  timesContainer: {
    flexDirection: 'row',
    marginTop: 'auto',
    marginBottom: 20,
    marginLeft: 'auto',
    marginRight: 20,
    zIndex: 10,
  },
  timeItem: {
    marginLeft: 20,
  },
  timeItemText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
    opacity: 0.7,
  },
});
