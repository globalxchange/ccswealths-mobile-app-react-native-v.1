/* eslint-disable react-native/no-inline-styles */
import React, {useRef} from 'react';
import {View, StyleSheet} from 'react-native';
import Animated, {
  useCode,
  Value,
  set,
  Clock,
  interpolate,
} from 'react-native-reanimated';
import ThemeData from '../../configs/ThemeData';
import ReanimatedTimingHelper from '../../utils/ReanimatedTimingHelper';
import AdsCarousel from './AdsCarousel';

const MarketCover = ({showHeader}) => {
  const headerAnimation = useRef(new Value(1));

  useCode(
    () =>
      showHeader
        ? [
            set(
              headerAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ]
        : [
            set(
              headerAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ],
    [showHeader],
  );

  return (
    <View style={styles.container}>
      <Animated.View
        style={{
          height: interpolate(headerAnimation.current, {
            inputRange: [0, 1],
            outputRange: [0, 160],
          }),
          overflow: 'hidden',
        }}>
        <AdsCarousel />
      </Animated.View>
    </View>
  );
};

export default MarketCover;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 20,
    backgroundColor: '#fbfbfb',
  },
  coverHeader: {
    textAlign: 'center',
    marginTop: 10,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 22,
    fontFamily: 'Montserrat-Bold',
  },
  coverSubHeader: {
    textAlign: 'center',
    marginTop: 10,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
    fontFamily: 'Montserrat-Bold',
  },
  searchContainer: {
    flexDirection: 'row',
    marginTop: 40,
    height: 45,
    paddingHorizontal: 15,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    backgroundColor: 'white',
    borderRadius: 2,
    elevation: 12,
  },
  countryIcon: {
    height: 22,
    width: 22,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginRight: 15,
  },
  separator: {
    backgroundColor: '#CACACA',
    width: 1,
    marginVertical: 8,
  },
  searchInput: {
    flexGrow: 1,
    width: 0,
    marginTop: 'auto',
    marginBottom: 'auto',
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    fontWeight: 'normal',
    color: 'black',
  },
  searchIcon: {
    height: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
});
