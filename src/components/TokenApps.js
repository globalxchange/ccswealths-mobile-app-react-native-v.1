import {CommonActions, useNavigation} from '@react-navigation/native';
import React, {useContext, useEffect, useState} from 'react';
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  TouchableWithoutFeedback,
} from 'react-native';
import {AppContext} from '../contexts/AppContextProvider';
import BottomSheetLayout from '../layouts/BottomSheetLayout';
import AdminMenu from './AdminMenu';
import Connect from './Connect';
import InitiatePlanB from './InitiatePlanB';
import * as WebBrowser from 'expo-web-browser';
import {WToast} from 'react-native-smart-tip';
import ThemeData from '../configs/ThemeData';

const {height, width} = Dimensions.get('window');

const TokenApps = ({isOpen, setIsOpen}) => {
  const navigation = useNavigation();

  const {forceRefreshApiData} = useContext(AppContext);

  const [selectedView, setSelectedView] = useState();
  const [isPlanBOpen, setIsPlanBOpen] = useState(false);
  const [isAdminMenuOpen, setIsAdminMenuOpen] = useState(false);

  useEffect(() => {
    if (!isOpen) {
      resetState();
    }
  }, [isOpen]);

  const resetState = () => {
    setSelectedView();
  };

  const onItemClick = (clickedItem) => {
    switch (clickedItem) {
      case 'BlockCheck':
        setIsOpen(false);
        navigation.navigate('BlockCheck');
        break;
      case 'UpdateHistory':
        setIsOpen(false);
        navigation.navigate('UpdateHistory');
        break;
      case 'Connect':
        setSelectedView(<Connect setIsOpen={setIsOpen} />);
        break;
      case 'Settings':
        setIsOpen(false);
        navigation.navigate('Settings');
        break;
      case 'TokenHash':
        setIsOpen(false);
        navigation.navigate('MoneyMarket');
        break;
      case 'RefreshBalances':
        WToast.show({
          data: 'Refreshing App Data',
          position: WToast.position.TOP,
        });
        setIsOpen(false);
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{name: 'Drawer'}],
          }),
        );
        forceRefreshApiData();
        break;
      case 'OnHold':
        setIsOpen(false);
        setIsAdminMenuOpen(true);
        break;
      case 'Support':
        setIsOpen(false);
        navigation.navigate('Support');
        break;
      case 'Chats':
        setIsOpen(false);
        navigation.navigate('Support', {openUserChat: true});
        break;
      case 'CryptoCoupen':
        WebBrowser.openBrowserAsync('https://cryptocoupon.com/');
        break;
      case 'ATM':
        WebBrowser.openBrowserAsync('https://atms.app/');
        break;
      case 'PlanB':
        setIsOpen(false);
        setIsPlanBOpen(true);
        break;
    }
  };

  return (
    <>
      <BottomSheetLayout
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
        reactToKeyboard>
        <View style={styles.fragmentContainer}>
          {selectedView ? (
            selectedView
          ) : (
            <View style={styles.categorySelector}>
              <Image
                source={require('../assets/tokenApps/token-apps-logo.png')}
                style={styles.headerImage}
                resizeMode="contain"
              />
              <TouchableWithoutFeedback>
                <ScrollView
                  showsVerticalScrollIndicator={false}
                  style={styles.listContainer}>
                  {items.map((item) => (
                    <TouchableOpacity key={item.title}>
                      <View
                        style={[
                          styles.listItem,
                          item.disabled && {opacity: 0.5},
                        ]}>
                        <Image
                          source={item.image}
                          resizeMode="contain"
                          style={styles.itemIcon}
                        />
                        <View style={styles.itemDetails}>
                          <Text style={styles.itemName}>{item.title}</Text>
                          <Text style={styles.itemSubText}>
                            {item.subtitle}
                          </Text>
                          <View style={styles.actionsContainer}>
                            <TouchableOpacity
                              style={[
                                styles.actionButton,
                                {backgroundColor: 'white'},
                              ]}>
                              <Text style={styles.actionButtonText}>Learn</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={styles.actionButton}
                              onPress={() => onItemClick(item.title)}>
                              <Text style={styles.actionButtonText}>
                                Launch
                              </Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  ))}
                </ScrollView>
              </TouchableWithoutFeedback>
              <TouchableOpacity
                onPress={() => onItemClick('RefreshBalances')}
                style={styles.action}>
                <Text style={styles.actionText}>Refresh App</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </BottomSheetLayout>
      <InitiatePlanB isOpen={isPlanBOpen} setIsOpen={setIsPlanBOpen} />
      <AdminMenu isOpen={isAdminMenuOpen} setIsOpen={setIsAdminMenuOpen} />
    </>
  );
};

export default TokenApps;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    backgroundColor: '#F1F4F6',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
  fragmentContainer: {
    backgroundColor: 'white',
  },
  categorySelector: {
    paddingHorizontal: 30,
    paddingTop: 40,
    maxHeight: height * 0.8,
  },
  headerImage: {
    height: 50,
    width: width * 0.65,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 25,
  },
  headerText: {
    fontFamily: 'Montserrat-Bold',
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
  },
  listContainer: {
    // marginBottom: 60,
  },
  listItem: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    paddingVertical: 18,
  },
  itemIcon: {
    height: 85,
    width: 85,
  },
  itemDetails: {
    flex: 1,
    marginLeft: 15,
  },
  itemName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 20,
    marginBottom: 5,
  },
  itemSubText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 13,
    marginBottom: 5,
    color: 'rgba(0, 0, 0, 0.47)',
  },
  actionsContainer: {
    flexDirection: 'row',
    marginTop: 5,
  },
  actionButton: {
    paddingHorizontal: 25,
    paddingVertical: 8,
    backgroundColor: '#F1F1F1',
    marginRight: 10,
    borderColor: '#F1F1F1',
    borderWidth: 1,
  },
  actionButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 12,
    color: 'rgba(0, 0, 0, 0.47)',
  },
  action: {
    backgroundColor: '#F1F4F6',
    marginHorizontal: -40,
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 16,
  },
});

const items = [
  {
    image: require('../assets/tokenApps/ic-block-check.png'),
    title: 'BlockCheck',
    subtitle: 'Off Chain Transaction Manager',
  },
  {
    image: require('../assets/tokenApps/ic-connect.png'),
    title: 'Connect',
    subtitle: 'Connect',
  },
  {
    image: require('../assets/tokenApps/ic-support.png'),
    title: 'Support',
    subtitle: 'Humans, Bots, And Content',
  },
  {
    image: require('../assets/tokenApps/ic-token-hash.png'),
    title: 'TokenHash',
    subtitle: 'Receipts For Tokenized Assets',
  },
  {
    image: require('../assets/tokenApps/ic-chats.png'),
    title: 'Chats',
    subtitle: 'Engage With Your Network',
  },
  {
    image: require('../assets/tokenApps/ic-plan-b.png'),
    title: 'PlanB',
    subtitle: 'Onboard New Bitcoin Users',
  },
];
