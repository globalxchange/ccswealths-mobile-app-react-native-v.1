import {useRoute} from '@react-navigation/native';
import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {AppContext} from '../contexts/AppContextProvider';
import SearchLayout from '../layouts/SearchLayout';
import {formatterHelper} from '../utils';

const CryptoSwitchSearch = ({
  walletBalances,
  setActiveCrypto,
  onClose,
  fromBuyCrypto,
}) => {
  const {params} = useRoute();
  const {walletCoinData} = useContext(AppContext);

  const [searchInput, setSearchInput] = useState('');
  const [coinList, setCoinList] = useState();

  useEffect(() => {
    console.log(params?.type, walletBalances.length, 'ljkhdkqhekdhqwlef3');
    if (params?.type === 'bonds') {
      setCoinList(walletBalances);
    } else if (fromBuyCrypto === true) {
      setCoinList(walletBalances);
    } else {
      console.log('outsideeeee');
      if (walletCoinData) {
        const list = walletCoinData.map((item) => ({
          ...item,
          email: `${formatterHelper(item.coinValue, item.coinSymbol)} ${
            item.coinSymbol
          }`,
        }));
        setCoinList(list);
      }
    }
  }, [walletCoinData, searchInput]);

  const onItemSelected = (item) => {
    setActiveCrypto(item);
    onClose();
  };

  return (
    <View style={styles.container}>
      <SearchLayout
        value={searchInput}
        setValue={setSearchInput}
        list={coinList || []}
        onBack={onClose}
        disableFilter
        onSubmit={(_, item) => onItemSelected(item)}
        showUserList
        placeholder=""
      />
    </View>
  );
};

export default CryptoSwitchSearch;

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFill,
    zIndex: 100,
  },
});
