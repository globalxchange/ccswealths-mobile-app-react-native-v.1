import React, {useEffect, useRef, useState} from 'react';
import {
  Dimensions,
  Image,
  Keyboard,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import QRCode from 'react-native-qrcode-svg';
import Animated, {
  Clock,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import ReanimatedTimingHelper from '../../utils/ReanimatedTimingHelper';

const {width} = Dimensions.get('window');

const QRCodeView = ({onBack, qrCode, onNext}) => {
  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image
          resizeMode="contain"
          style={styles.authenticatorIcon}
          source={require('../../assets/authenticator-icon.png')}
        />
        <Text style={styles.headerText}>Scan QR Code</Text>
      </View>
      <View style={styles.qrContainer}>
        <QRCode
          value={qrCode || 'Empty'}
          size={width * 0.6}
          color={ThemeData.APP_MAIN_COLOR}
        />
      </View>
      <View style={styles.actionContainer}>
        <TouchableOpacity style={styles.outlinedButton} onPress={onBack}>
          <Text style={styles.outlinedButtonText}>Go Back</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.filledButton} onPress={onNext}>
          <Text style={styles.filledButtonText}>Im Done</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default QRCodeView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 18,
    color: ThemeData.APP_MAIN_COLOR,
  },
  authenticatorIcon: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  actionContainer: {
    flexDirection: 'row',
    paddingHorizontal: 30,
  },
  outlinedButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  outlinedButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  filledButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    marginLeft: 15,
  },
  filledButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: 'white',
  },
  qrContainer: {
    alignItems: 'center',
    paddingVertical: 50,
  },
});
