import Clipboard from '@react-native-community/clipboard';
import React, {useEffect, useRef, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const KeyView = ({onBack, secretCode, onNext}) => {
  const onCopyClick = () => {
    Clipboard.setString(secretCode || '');
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image
          resizeMode="contain"
          style={styles.authenticatorIcon}
          source={require('../../assets/authenticator-icon.png')}
        />
        <Text style={styles.headerText}>Enter This Key</Text>
      </View>
      <View style={styles.keyForm}>
        <Text numberOfLines={1} style={styles.key}>
          {secretCode}
        </Text>
        <TouchableOpacity onPress={onCopyClick} style={styles.copyButton}>
          <Image
            source={require('../../assets/copy-icon.png')}
            resizeMode="contain"
            style={styles.copyIcon}
          />
        </TouchableOpacity>
      </View>
      <View style={styles.actionContainer}>
        <TouchableOpacity style={styles.outlinedButton} onPress={onBack}>
          <Text style={styles.outlinedButtonText}>Go Back</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.filledButton} onPress={onNext}>
          <Text style={styles.filledButtonText}>Im Done</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default KeyView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 18,
    color: ThemeData.APP_MAIN_COLOR,
  },
  authenticatorIcon: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  actionContainer: {
    flexDirection: 'row',
    paddingHorizontal: 30,
  },
  outlinedButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  outlinedButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  filledButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    marginLeft: 15,
  },
  filledButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: 'white',
  },
  keyForm: {
    borderColor: '#E7E7E7',
    borderWidth: 1,
    flexDirection: 'row',
    marginVertical: 50,
  },
  key: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    flex: 1,
    textAlign: 'center',
    marginTop: 'auto',
    marginBottom: 'auto',
    paddingHorizontal: 20,
  },
  copyButton: {
    backgroundColor: '#F3F3F3',
    width: 60,
    height: 60,
    padding: 20,
  },
  copyIcon: {
    flex: 1,
    height: null,
    width: null,
  },
});
