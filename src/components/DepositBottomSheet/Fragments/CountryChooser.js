import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionButton from '../ActionButton';
import {AppContext} from '../../../contexts/AppContextProvider';
import CustomDropDown from '../../CustomDropDown';
import LoadingAnimation from '../../LoadingAnimation';
import {WToast} from 'react-native-smart-tip';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import SkeletonCheckoutLoading from '../../BuyBottomSheet/SkeletonCheckoutLoading';
import ThemeData from '../../../configs/ThemeData';
import {useAppData} from '../../../utils/CustomHook';

const CountryChooser = ({
  onNext,
  activeCrypto,
  selectedCountry,
  setCheckOutCountry,
}) => {
  const {countryList} = useContext(AppContext);

  const [filteredCountries, setFilteredCountries] = useState();
  const [isLoading, setIsLoading] = useState(true);

  const {data: appData} = useAppData();

  const onNextClick = () => {
    if (!selectedCountry) {
      return WToast.show({
        data: 'Please Select A Country First',
        position: WToast.position.TOP,
      });
    }

    onNext();
  };

  useEffect(() => {
    if (countryList && activeCrypto) {
      setIsLoading(true);
      const filterCountryList = [];

      Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/stats/get`, {
        params: {to_currency: activeCrypto.coinSymbol, select_type: 'fund'},
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('data', data);

          if (data.status) {
            const result = data.pathData;

            countryList.forEach((countryItem) => {
              result.country.forEach((pathItem) => {
                if (
                  pathItem._id === countryItem.value ||
                  pathItem._id === countryItem.formData.Name
                ) {
                  if (!filterCountryList.includes(countryItem)) {
                    filterCountryList.push(countryItem);
                    return;
                  }
                }
              });
            });
          } else {
            WToast.show({
              data: 'Error On Querying Path Data',
              position: WToast.position.TOP,
            });
          }

          setFilteredCountries(filterCountryList);
          setIsLoading(false);
        })
        .catch((error) => {
          WToast.show({
            data: 'Error On Querying Path Data',
            position: WToast.position.TOP,
          });
          console.log('Error On Querying Path Data', error);
        });
    }
  }, [countryList, activeCrypto]);

  if (!filteredCountries || isLoading) {
    return <SkeletonCheckoutLoading />;
  }

  if (filteredCountries.length <= 0) {
    return (
      <View style={styles.loadingContainer}>
        <Text style={styles.notAvailableText}>
          No Countries Supported For {activeCrypto.coinName}
        </Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Country</Text>
      <Text style={styles.subHeader}>
        In Which Country Do You Want To Deposit {activeCrypto.coinName} Into
        Your Account?
      </Text>
      <View style={styles.controlContainer}>
        <CustomDropDown
          placeholderIcon={require('../../../assets/default-breadcumb-icon/country.png')}
          label="Select A Country"
          onDropDownSelect={setCheckOutCountry}
          placeHolder="Please Select A Country"
          items={filteredCountries}
          selectedItem={selectedCountry}
          numberLabel="Countries"
          onNext={onNext}
          headerIcon={require('../../../assets/ccs-full-logo.png')}
          headerStyle={{
            backgroundColor: 'white',
            borderBottomColor: ThemeData.BORDER_COLOR,
            borderBottomWidth: 1,
          }}
        />
      </View>
      <ActionButton text="Proceed To Payment Method" onPress={onNextClick} />
    </View>
  );
};

export default CountryChooser;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  header: {
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 35,
    fontFamily: 'Montserrat-Bold',
  },
  controlContainer: {
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  loadingContainer: {
    justifyContent: 'center',
    flex: 1,
  },
  notAvailableText: {
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
  },
});
