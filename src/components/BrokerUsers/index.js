import React, {useState, useEffect} from 'react';
import {StyleSheet, View, FlatList} from 'react-native';
import Axios from 'axios';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {GX_API_ENDPOINT} from '../../configs';
import HorizontalItem from './HorizontalItem';
import VerticalItem from './VerticalItem';

const BrokerUsers = ({
  selectedItem,
  onItemSelected,
  horizontal,
  searchText,
  type,
  isBrokers,
}) => {
  const [usersList, setUsersList] = useState([]);
  const [filterList, setFilterList] = useState([]);

  useEffect(() => {
    (async () => {
      if (type === 'Direct') {
        const email = await AsyncStorageHelper.getLoginEmail();
        Axios.get(`${GX_API_ENDPOINT}/brokerage/otc/user/txn/stats/get`, {
          params: {email},
        })
          .then((resp) => {
            const {data} = resp;
            // console.log('Data', data);

            if (data.status) {
              const userData = data.userStats;
              let totalTxns = 0;

              userData.forEach((item) => {
                totalTxns += item.txns;
              });

              const allData = {
                name: 'All Directs',
                image: require('../../assets/all-user-icon.png'),
                email: '',
                txns: totalTxns,
              };
              const finalArray = [allData, ...userData];

              setUsersList(finalArray);
              setFilterList(finalArray);
              onItemSelected(allData);
            }
          })
          .catch((error) => {
            console.log('Error on getting userlist', error);
          });
      } else {
        if (isBrokers) {
          const email = await AsyncStorageHelper.getLoginEmail();
          Axios.get(`${GX_API_ENDPOINT}/brokerage/otc/broker/team/stats`, {
            params: {email},
          })
            .then((resp) => {
              const {data} = resp;

              console.log('data', data);
              const allData = {
                name: 'All Indirect',
                image: require('../../assets/all-user-icon.png'),
                email: '',
              };
              if (data.status) {
                setUsersList([allData, ...data.data]);
                setFilterList([allData, ...data.data]);
                onItemSelected(allData);
              } else {
                setUsersList([allData]);
                setFilterList([allData]);
                onItemSelected(allData);
              }
            })
            .catch((error) => {
              console.log('Error on getting userlist', error);
            });
        } else {
          const affId = await AsyncStorageHelper.getAffId();
          Axios.get(`${GX_API_ENDPOINT}/brokerage/stats/getUsers`, {
            params: {affiliate_id: affId},
          })
            .then((resp) => {
              const {data} = resp;
              // console.log('Data', data);

              if (data.status) {
                const allData = {
                  name: 'All Indirect',
                  image: require('../../assets/all-user-icon.png'),
                  email: '',
                };

                setUsersList([allData, ...data.indirect.users]);
                setFilterList([allData, ...data.indirect.users]);
                onItemSelected(allData);
              }
            })
            .catch((error) => {
              console.log('Error on getting userlist', error);
            });
        }
      }
    })();
  }, [isBrokers, type]);

  useEffect(() => {
    if (searchText && usersList) {
      const searchQuery = searchText.trim().toLowerCase();

      const list = usersList.filter(
        (item) =>
          item.name.toLowerCase().includes(searchQuery) ||
          item.email.includes(searchQuery),
      );
      setFilterList(list);
    }
  }, [searchText, usersList]);

  return (
    <View style={[styles.container, horizontal ? {height: 110} : {flex: 1}]}>
      <FlatList
        horizontal={horizontal}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        data={horizontal ? usersList : filterList}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}) =>
          horizontal ? (
            <HorizontalItem
              item={item}
              onItemSelected={() =>
                onItemSelected ? onItemSelected(item) : null
              }
              selectedItem={selectedItem}
            />
          ) : (
            <VerticalItem
              item={item}
              onItemSelected={() =>
                onItemSelected ? onItemSelected(item) : null
              }
              selectedItem={selectedItem}
            />
          )
        }
      />
    </View>
  );
};

export default BrokerUsers;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    alignItems: 'center',
    marginTop: 10,
  },
});
