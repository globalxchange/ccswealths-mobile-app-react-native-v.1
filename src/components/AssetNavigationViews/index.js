import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import BankersSelector from './BankersSelector';
import CountrySelector from './CountrySelector';
import FromCurrencySelector from './FromCurrencySelector';
import MethodSelector from './MethodSelector';

const AssetNavigationViews = ({activeAsset, setCheckoutPayload}) => {
  const [activeView, setActiveView] = useState('');

  const [selectedCountry, setSelectedCountry] = useState('');
  const [paymentCurrency, setPaymentCurrency] = useState('');
  const [paymentType, setPaymentType] = useState('');
  const [selectedBanker, setSelectedBanker] = useState('');

  useEffect(() => {
    let newPayload = {};

    if (selectedCountry) {
      newPayload = {...newPayload, selectedCountry};
    }

    if (paymentCurrency) {
      newPayload = {...newPayload, paymentCurrency};
    }

    if (paymentType) {
      newPayload = {...newPayload, paymentType};
    }

    if (selectedBanker) {
      newPayload = {...newPayload, selectedBanker};
    }

    setCheckoutPayload(newPayload);
  }, [selectedCountry, paymentCurrency, paymentType, selectedBanker]);

  let currentView;

  switch (activeView) {
    case 'Countries':
      currentView = (
        <CountrySelector
          activeAsset={activeAsset}
          setSelectedCountry={setSelectedCountry}
          onNext={() => setActiveView('FromCurrency')}
        />
      );
      break;

    case 'FromCurrency':
      currentView = (
        <FromCurrencySelector
          activeAsset={activeAsset}
          selectedCountry={selectedCountry}
          setPaymentCurrency={setPaymentCurrency}
          onNext={() => setActiveView('Method')}
        />
      );
      break;

    case 'Method':
      currentView = (
        <MethodSelector
          activeAsset={activeAsset}
          selectedCountry={selectedCountry}
          paymentCurrency={paymentCurrency}
          setPaymentType={setPaymentType}
          onNext={() => setActiveView('Banker')}
        />
      );
      break;

    case 'Banker':
      currentView = (
        <BankersSelector
          activeAsset={activeAsset}
          selectedCountry={selectedCountry}
          paymentCurrency={paymentCurrency}
          paymentType={paymentType}
          setSelectedBanker={setSelectedBanker}
        />
      );
      break;

    default:
      currentView = (
        <CountrySelector
          activeAsset={activeAsset}
          setSelectedCountry={setSelectedCountry}
          onNext={() => setActiveView('FromCurrency')}
        />
      );
  }

  return <View style={styles.container}>{currentView}</View>;
};

export default AssetNavigationViews;

const styles = StyleSheet.create({
  container: {
    paddingBottom: 25,
    marginTop: 10,
    flex: 1,
  },
});

const views = ['Countries', 'FromCurrency', 'Method', 'Banker'];
