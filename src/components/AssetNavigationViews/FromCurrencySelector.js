import Axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {WToast} from 'react-native-smart-tip';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import {formatterHelper, getUriImage} from '../../utils';
import LoadingAnimation from '../LoadingAnimation';

const FromCurrencySelector = ({
  activeAsset,
  selectedCountry,
  setPaymentCurrency,
  onNext,
}) => {
  const {cryptoTableData} = useContext(AppContext);

  const [isLoading, setIsLoading] = useState(true);
  const [availableCurrencies, setAvailableCurrencies] = useState();

  useEffect(() => {
    if (cryptoTableData && activeAsset && selectedCountry) {
      setIsLoading(true);
      const filterCurrencyList = [];

      Axios.get(
        `${GX_API_ENDPOINT}/coin/vault/service/payment/path/averages/get`,
        {
          params: {
            to_currency: activeAsset.coinSymbol,
            country: selectedCountry.formData
              ? selectedCountry.formData.Name
              : selectedCountry.value,
          },
        },
      )
        .then((resp) => {
          const {data} = resp;

          // console.log('data', data);

          if (data.pathCountries) {
            const result = data.pathCountries;

            // console.log('result', result);

            const country = selectedCountry.formData
              ? selectedCountry.formData.Name
              : selectedCountry.value;

            cryptoTableData.forEach((cryptoItem) => {
              result[country].forEach((fromItem) => {
                if (fromItem.from_currency === cryptoItem.coinSymbol) {
                  const instantRate = fromItem.instantRate;

                  const pushItem = {...cryptoItem, instantRate};

                  if (!filterCurrencyList.includes(pushItem)) {
                    filterCurrencyList.push(pushItem);
                    return;
                  }
                }
              });
            });
          } else {
            WToast.show({
              data: 'Error On Querying Path Data',
              position: WToast.position.TOP,
            });
          }

          setAvailableCurrencies(filterCurrencyList);
          setIsLoading(false);
        })
        .catch((error) => {
          WToast.show({
            data: 'Error On Querying Path Data',
            position: WToast.position.TOP,
          });
          console.log('Error On Querying Path Data', error);
        });
    }
  }, [cryptoTableData, activeAsset, selectedCountry]);

  const onItemSelected = (item) => {
    setPaymentCurrency(item);
    onNext();
  };

  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      style={styles.flatList}
      data={availableCurrencies}
      keyExtractor={(item, index) => `${item.Key}${index}`}
      renderItem={({item}) => (
        <TouchableOpacity onPress={() => onItemSelected(item)}>
          <View style={styles.item}>
            <FastImage
              source={{uri: getUriImage(item.image)}}
              resizeMode="contain"
              style={styles.itemImage}
            />
            <Text style={styles.itemName}>{item.name}</Text>
            <View style={styles.pricingContainer}>
              <View style={[styles.priceItem]}>
                <View style={styles.price}>
                  <Text style={styles.priceValue}>
                    {formatterHelper(item.instantRate, item.coinSymbol)}
                  </Text>
                  <Text style={styles.priceUnit}>{item.coinSymbol}</Text>
                </View>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      )}
      ListEmptyComponent={
        <View style={styles.emptyContainer}>
          <Text style={styles.emptyText}>No From Currencies Available</Text>
        </View>
      }
    />
  );
};

export default FromCurrencySelector;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  flatList: {
    flex: 1,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
    marginHorizontal: 5,
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginBottom: 1,
  },
  itemImage: {
    width: 20,
    height: 20,
  },
  itemName: {
    flex: 1,
    paddingHorizontal: 10,
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
  },
  pricingContainer: {},
  priceItem: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  statusImage: {
    height: 12,
    width: 12,
  },
  price: {
    flexDirection: 'row',
    alignItems: 'baseline',
    marginLeft: 10,
  },
  priceValue: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    marginRight: 5,
  },
  priceUnit: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 9,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  emptyContainer: {
    flex: 1,
    paddingTop: 30,
  },
  emptyText: {
    textAlign: 'center',
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-SemiBold',
  },
});
