import Axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {WToast} from 'react-native-smart-tip';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import {formatterHelper, getUriImage} from '../../utils';
import LoadingAnimation from '../LoadingAnimation';

const CountrySelector = ({activeAsset, setSelectedCountry, onNext}) => {
  const {countryList} = useContext(AppContext);

  const [availableCountries, setAvailableCountries] = useState();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (countryList && activeAsset) {
      setIsLoading(true);
      const filterCountryList = [];

      Axios.get(
        `${GX_API_ENDPOINT}/coin/vault/service/payment/path/averages/get`,
        {
          params: {to_currency: activeAsset.coinSymbol},
        },
      )
        .then((resp) => {
          const {data} = resp;

          // console.log('data', data);

          if (data.countryData) {
            const result = data.countryData;

            // console.log('result', result);

            countryList.forEach((countryItem) => {
              Object.keys(result).forEach((key) => {
                if (
                  key === countryItem.value ||
                  key === countryItem.formData.Name
                ) {
                  const currency = Object.keys(result[key])[1];
                  const sellPrice = result[key][currency].sell.avg;
                  const buyPrice = result[key][currency].buy.avg;

                  const pushItem = {
                    ...countryItem,
                    sellPrice,
                    buyPrice,
                    currency,
                  };

                  if (!filterCountryList.includes(pushItem)) {
                    filterCountryList.push(pushItem);
                    return;
                  }
                }
              });
            });
          } else {
            WToast.show({
              data: 'Error On Querying Path Data',
              position: WToast.position.TOP,
            });
          }

          setAvailableCountries(filterCountryList);
          setIsLoading(false);
        })
        .catch((error) => {
          WToast.show({
            data: 'Error On Querying Path Data',
            position: WToast.position.TOP,
          });
          console.log('Error On Querying Path Data', error);
        });
    }
  }, [countryList, activeAsset]);

  const onItemSelected = (item) => {
    setSelectedCountry(item);
    onNext();
  };

  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      style={styles.flatList}
      data={availableCountries}
      keyExtractor={(item, index) => `${item.Key}${index}`}
      renderItem={({item}) => (
        <TouchableOpacity onPress={() => onItemSelected(item)}>
          <View style={styles.item}>
            <FastImage
              source={{uri: getUriImage(item.formData.Flag)}}
              resizeMode="contain"
              style={styles.itemImage}
            />
            <Text style={styles.itemName}>{item.Key}</Text>
            <View style={styles.pricingContainer}>
              <View style={[styles.priceItem, {marginBottom: 8}]}>
                <Image
                  source={require('../../assets/price-up-arrow.png')}
                  style={styles.statusImage}
                  resizeMode="contain"
                />
                <View style={styles.price}>
                  <Text style={styles.priceValue}>
                    {formatterHelper(item.sellPrice || 0, item.currency)}
                  </Text>
                  <Text style={styles.priceUnit}>{item.currency}</Text>
                </View>
              </View>
              <View style={styles.priceItem}>
                <Image
                  source={require('../../assets/price-down-arrow.png')}
                  style={styles.statusImage}
                  resizeMode="contain"
                />
                <View style={styles.price}>
                  <Text style={styles.priceValue}>
                    {formatterHelper(item.buyPrice || 0, item.currency)}
                  </Text>
                  <Text style={styles.priceUnit}>{item.currency}</Text>
                </View>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      )}
      ListEmptyComponent={
        <View style={styles.emptyContainer}>
          <Text style={styles.emptyText}>No Countries Available</Text>
        </View>
      }
    />
  );
};

export default CountrySelector;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  flatList: {
    flex: 1,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
    marginHorizontal: 5,
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginBottom: 1,
  },
  itemImage: {
    width: 20,
    height: 20,
  },
  itemName: {
    flex: 1,
    paddingHorizontal: 10,
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
  },
  pricingContainer: {},
  priceItem: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'space-between',
  },
  statusImage: {
    height: 12,
    width: 12,
  },
  price: {
    flexDirection: 'row',
    alignItems: 'baseline',
    marginLeft: 10,
    justifyContent: 'flex-end',
  },
  priceValue: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    marginRight: 5,
  },
  priceUnit: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 9,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  emptyContainer: {
    flex: 1,
    paddingTop: 30,
  },
  emptyText: {
    textAlign: 'center',
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-SemiBold',
  },
});
